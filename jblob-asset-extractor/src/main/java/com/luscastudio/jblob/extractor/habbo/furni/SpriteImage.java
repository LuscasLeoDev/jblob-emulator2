package com.luscastudio.jblob.extractor.habbo.furni;

import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.swf.tags.DefineBitsLossless2Tag;

/**
 * Created by Lucas on 04/11/2016.
 */

public class SpriteImage {

    DefineBitsLossless2Tag imageTag;

    private boolean isIcon;

    private SpriteNameData nameData;

    private boolean flipH;

    public SpriteImage(DefineBitsLossless2Tag imageTag){
        this(imageTag, imageTag.getHeader().getIo().getFrameName(), imageTag.getName());
    }

    public SpriteImage(DefineBitsLossless2Tag imageTag, String frameName, String tagName) {
        this.imageTag = imageTag;
        this.flipH = false;

        //String frameName = imageTag.getHeader().getIo().getFrameName();

        String namedataStr = tagName.replace(frameName + "_", "");

        try {
            this.nameData = new SpriteNameData(namedataStr);
        } catch (Exception e){
            JBlobAE.write("Error while parsing name data for " + tagName + " [" + namedataStr + "]");
            e.printStackTrace();
        }

    }

    public DefineBitsLossless2Tag getImageTag() {
        return imageTag;
    }



    public boolean isIcon() {
        return this.nameData.getType().equals("icon");
    }

    public boolean isFlipH() {
        return flipH;
    }

    public void setFlipH(boolean flipH) {
        this.flipH = flipH;
    }

    @Override
    public String toString() {
        return this.getImageTag().getName();
    }

    public void setReverseRotation() {
    }

    public SpriteNameData getNameData() {
        return nameData;
    }
}
