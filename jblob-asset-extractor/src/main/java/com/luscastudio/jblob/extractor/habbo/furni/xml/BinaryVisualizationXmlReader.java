package com.luscastudio.jblob.extractor.habbo.furni.xml;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.validator.Validator;
import com.luscastudio.jblob.extractor.xml.XmlFileReader;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.List;

/**
 * Created by Lucas on 06/11/2016.
 */

public class BinaryVisualizationXmlReader extends XmlFileReader {

    private List<SpriteVisualization> visualizations;

    public BinaryVisualizationXmlReader(String xml) {
        super(xml);

        this.visualizations = BCollect.newList();

        NodeList visualizationTypes = this.xml.getElementsByTagName("visualization");
        for(int i = 0; i < visualizationTypes.getLength(); i++){
            Element visualizationElement = (Element)visualizationTypes.item(i);

            String size = visualizationElement.getAttribute("size");
            int layerCount = Integer.valueOf(visualizationElement.getAttribute("layerCount"));
            String angle = visualizationElement.getAttribute("angle");

            this.visualizations.add(new SpriteVisualization(layerCount, size, angle, (Element)visualizationElement.getElementsByTagName("layers").item(0), (Element)visualizationElement.getElementsByTagName("colors").item(0)));
        }

    }

    public List<SpriteVisualization> getVisualizations() {
        return visualizations;
    }

    public SpriteVisualization getVisualization(Validator<SpriteVisualization> validator) {
        for (SpriteVisualization visualization : this.visualizations) {
            if(validator.validate(visualization))
                return visualization;
        }

        return null;
    }
    public SpriteVisualization getVisualization(String size, int rotation) {

        for(SpriteVisualization visualization : this.visualizations){
            if(visualization.getSize().equals(size) && visualization.getRotation() == rotation)
                return visualization;
        }

        return null;
    }

    public static String parseAngle(int rotation){
        switch(rotation){
            default:
            case 2:
                return "45";

            case 0:
                return "360";
        }
    }

    public static int parseRotation(String angle){
        switch (angle){
            case "45":
            default:
                return 0;

            case "360":
                return 2;
        }
    }
}
