package com.luscastudio.jblob.extractor.habbo.furni.xml;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.List;

/**
 * Created by Lucas on 06/11/2016.
 */

public class SpriteColor {

    private int id;
    private List<SpriteColorLayer> colors;

    public SpriteColor(int id, List<SpriteColorLayer> colors){
        this.id = id;
        this.colors = colors;
    }

    public SpriteColor(Element colorElement){
        this.id = Integer.valueOf(colorElement.getAttribute("id"));

        NodeList colorList = colorElement.getElementsByTagName("colorLayer");
        this.colors = BCollect.newList();

        for(int i = 0; i < colorList.getLength(); i++){
            Element colorLayer = (Element)colorList.item(i);

            colors.add(new SpriteColorLayer(Integer.valueOf(colorLayer.getAttribute("id")), colorLayer.getAttribute("color")));
        }

    }

    public int getId() {
        return id;
    }

    public List<SpriteColorLayer> getColors() {
        return colors;
    }
}
