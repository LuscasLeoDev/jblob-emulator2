package com.luscastudio.jblob.extractor.process;

import java.util.concurrent.*;

/**
 * Created by Lucas on 04/11/2016.
 */

public class ProcessExecutor {

    private static boolean initialized;

    private static ScheduledExecutorService executor;

    public static void init(){
        executor = new ScheduledThreadPoolExecutor(4, (ThreadFactory) Thread::new);
        initialized = true;
    }

    public static ScheduledFuture run(Runnable r){
        return executor.schedule(r, 0, TimeUnit.MILLISECONDS);
    }

}
