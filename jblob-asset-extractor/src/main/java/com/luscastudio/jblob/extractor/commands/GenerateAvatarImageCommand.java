//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.commands;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.gson.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.io.BFileReader;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.habbo.avatar.*;
import com.luscastudio.jblob.extractor.habbo.json.AvatarAssetJSONData;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.util.*;

/**
 * Created by Lucas on 02/03/2017 at 11:27.
 */
public class GenerateAvatarImageCommand implements Command {
    @Override
    public void handleCommand(CommandInput input) {
        //fa-3276-1320.hd-200-1.ch-235-1338.ea-3270-63-1408.wa-2005-63.ca-3085-80.lg-3202-86-79.hr-802-1346.sh-3089-81.he-1604-63.ha-1009-1321
        String figuremapPath = "C:\\wamp\\www\\toby.dev\\public\\game\\gordon\\PRODUCTION-201506161211-776084490\\figuremap.xml";
        String figuredataPath = "C:\\wamp\\www\\toby.dev\\public\\game\\gamedata\\figuredata.xml";

        String assetsDataDownload = "C:\\Users\\LUCAS\\IdeaProjects\\JBlob Emulator\\jblob-asset-extractor\\bin\\gordon\\";

        String avatarFigureString = "hd-200-1.ch-235-1338.ea-3270-63-1408.wa-2005-63.ca-3085-80.lg-3202-86-79.hr-802-1346.sh-3089-81.he-1604-63.ha-1009-1321.fa-3276-1320";

        int rotation = 2;
        String defaultAction = "std";
        int frameIndex = 0;
        String size = "h";
        String[] actions = null;

        if(input.setParam("-rotation")){
            String rotStr = input.getNext();
            if(NumberHelper.isInteger(rotStr))
                rotation = Integer.parseInt(rotStr);
        }

        if(input.setParam("-actions")) {
            actions = input.getNext().split(",");
        }

        if(input.setParam("-frame")){
            String frameStr = input.getNext();
            if(NumberHelper.isInteger(frameStr))
                frameIndex = Integer.parseInt(frameStr);
        }

        if(input.setParam("-size"))
            size = input.getNext();

        if(input.setParam("-figure"))
            avatarFigureString = input.getNext();


        AvatarFigure figure = new AvatarFigure(avatarFigureString);

        String figureMapContent = BFileReader.getFileContent(figuremapPath);
        String figureDataContent = BFileReader.getFileContent(figuredataPath);

        FigureMap figureMap = new FigureMap(figureMapContent);
        FigureData figureData = new FigureData(figureDataContent);

        List<SpriteAsset> assets = BCollect.newList();
        List<String> hiddenParts = BCollect.newList();

        LinkedListMultimap<String, AvatarAssetJSONData> data = LinkedListMultimap.create();
        for (AvatarFigureData avatarFigureData : figure.getFiguresData()) {
            //avatarFigureData is like the hd-200-1

            //avatarFigureData.getType() is the hd
            FigureDataSetType setType = figureData.getSetType(avatarFigureData.getType());
            if(setType == null)
                continue;

            //avatarFigureData.getPartId() is the 200
            FigureDataSet figureDataSet = setType.getFigureDataSet(avatarFigureData.getPartId());
            hiddenParts.addAll(figureDataSet.getHiddenLayers());

            for (FigureDataPart dataPart : figureDataSet.getDataParts()) {
                AvatarSpriteImage spriteImage;
                String selectedAction;
                FigureMapLib lib = figureMap.getLib(dataPart.getId(), dataPart.getType());
                if (lib == null) {
                    if ((lib = figureMap.getLib("1", dataPart.getType())) == null) {
                        JBlobAE.write("Could not find the lib with the partId " + dataPart.getId() + " and type " + dataPart.getType());
                        continue;
                    }
                }

                if(actions == null) {
                    spriteImage = getAssetImage(assetsDataDownload, lib.getId(), dataPart.getType(), rotation, defaultAction, frameIndex, size, dataPart.getId());
                    selectedAction = defaultAction;
                    if (spriteImage == null) {
                        JBlobAE.write("Could not find the image " + lib.getId() + "_" + size + "_" + defaultAction + "_" + dataPart.getType() + "_" + dataPart.getId() + "_" + rotation + "_" + frameIndex + " on the lib " + lib.getId());
                        continue;
                    }
                } else {
                    spriteImage = null;
                    selectedAction = defaultAction;
                    int i = 0;
                    while (i < actions.length && spriteImage == null) {
                        String action = actions[i];
                        spriteImage = getAssetImage(assetsDataDownload, lib.getId(), dataPart.getType(), rotation, action, frameIndex, size, dataPart.getId());

                        if (spriteImage == null) {
                            JBlobAE.write("Could not find the image " + lib.getId() + "_" + size + "_" + action + "_" + dataPart.getType() + "_" + dataPart.getId() + "_" + rotation + "_" + frameIndex + " on the lib " + lib.getId());
                        } else
                            selectedAction = action;

                        i++;
                    }

                    if(spriteImage == null) {
                        if ((spriteImage = getAssetImage(assetsDataDownload, lib.getId(), dataPart.getType(), rotation, defaultAction, frameIndex, size, dataPart.getId())) == null) {
                            JBlobAE.write("Could not find no one image for this asset: " + lib.getId() + "_" + size + "_[" + String.join(" or ", actions) + " or " + defaultAction + "]" + "_" + rotation + "_" + frameIndex);

                            continue;
                        }
                    }
                }

                ImageAssetManifest imageAssetManifest = getAssetManifest(assetsDataDownload, lib.getId());

                if (imageAssetManifest == null){
                    JBlobAE.write("Could not find the manifest file for the lib " + lib.getId());
                    continue;
                }
                String assetname = size + "_" + spriteImage.getAction() + "_" + dataPart.getType() + "_" + dataPart.getId() + "_" + spriteImage.getRotation() + "_" + spriteImage.getFrameIndex();
                ImageSpriteAssetData spriteAssetData = imageAssetManifest.getSpriteAssetData(assetname);

                if (spriteAssetData == null) {
                    JBlobAE.write("Could not find the Asset data " + assetname + " in the lib " + lib.getId());
                    continue;
                }

                boolean colorable;
                String color = "FFFFFF";
                if(colorable = dataPart.isColorable()){
                    FigurePalette palette = figureData.getPalette(setType.getPaletteId());
                    FigureColor figureColor;
                    if(dataPart.getColorIndex() == 1)
                        figureColor = palette.getFigureColor(avatarFigureData.getPrimaryColorId());
                    else if(dataPart.getColorIndex() == 2)
                        figureColor = palette.getFigureColor(avatarFigureData.getSecondaryColorId());
                    else
                        figureColor = null;

                    if(figureColor != null)
                        color = figureColor.getValue();
                }


                assets.add(new SpriteAsset(spriteImage.getImage(), spriteAssetData, dataPart.getType(), colorable, color));
                data.put(avatarFigureData.getType(), new AvatarAssetJSONData(lib.getId(), size, dataPart.getType(), dataPart.getId(), spriteImage.getRotation(), spriteImage.getFrameIndex(), spriteAssetData.getX(), spriteAssetData.getY()));
                try {
                    File file = new File("avatar_generator/part_" + dataPart.getType() + "_" + lib.getId() + "_" + spriteImage.getAction() + ".png");
                    if(!file.exists())
                        ImageIO.write(spriteImage.getImage(), "png", file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        assets.removeIf(c -> hiddenParts.contains(c.getType()));
        AvatarSpriteDrawSorter sorter = new AvatarSpriteDrawSorter();
        Comparator<SpriteAsset> sort = sorter.sort(rotation, defaultAction);
        assets.sort(sort);

        AvatarImageDrawing drawning = new AvatarImageDrawing(100, 100, assets);
        BufferedImage draw = drawning.draw();

        try {
            ImageIO.write(draw, "png", new File("avatar_generator/generated_" + draw.hashCode() + ".png"));

            /*Map<String, String> jsonData = BCollect.newLinkedMap();
            for (Map.Entry<String, Collection<AvatarAssetJSONData>> entry : data.asMap().entrySet()) {
                if(jsonData.containsKey(entry.getKey()))
                    continue;

                String json = JSONUtils.toJson(entry.getValue(), new TypeToken<Map<String, Collection<AvatarAssetJSONData>>>(){}.getType());
            }*/
            //dataStream.write( JSONUtils.toJson(data.asMap(), new TypeToken<Map<String, LinkedList<AvatarAssetJSONData>>>(){}.getType()).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ImageAssetManifest getAssetManifest(String downloadPath, String libname) {
        String filename = downloadPath + libname + "\\binaries\\" + libname + "_manifest.txt";
        String content = BFileReader.getFileContent(filename);
        if(content == null)
            return null;

        ImageAssetManifest manifest = new ImageAssetManifest(content);

        return manifest;
    }

    private AvatarSpriteImage getAssetImage(String downloadPath, String libname, String type, int rotation, String action, int frameIndex, String size, String partId){
        ByteBuffer buffer = null;
        while(buffer == null && frameIndex >= 0) {
            String filename = downloadPath + libname + "\\images\\" + libname + "_" + size + "_" + action + "_" + type + "_" + partId + "_" + rotation + "_" + frameIndex + ".png";
            if ((buffer = BFileReader.getFileBytes(filename)) == null)
                frameIndex--;
        }

        if(buffer == null)
            return null;

        try {
            return new AvatarSpriteImage(ImageIO.read(new ByteArrayInputStream(buffer.array())), libname, size, action, partId, type, rotation, frameIndex);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
