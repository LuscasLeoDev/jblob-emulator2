//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 11:32.
 */
public class AvatarFigure {
    private Map<String, AvatarFigureData> figureDataMap;

    public AvatarFigure(String figureString) {
        this.figureDataMap = BCollect.newMap();

        this.parseFigure(figureString);
    }

    private void parseFigure(String figureString){
        for (String figurePart : figureString.split("\\.")) {
            String[] parts = figurePart.split("-");
            String type = "null";
            int partId = 0;
            int primaryColor = 0;
            int secondaryColor = 0;
            for (int i = 0; i < parts.length; i++) {
                String val = parts[i];
                switch (i) {
                    case 0:
                        type = val;
                        break;

                    case 1:
                        if (NumberHelper.isInteger(val))
                            partId = Integer.parseInt(val);
                        break;
                    case 2:
                        if (NumberHelper.isInteger(val))
                            primaryColor = Integer.parseInt(val);
                        break;
                    case 3:
                        if (NumberHelper.isInteger(val))
                            secondaryColor = Integer.parseInt(val);
                        break;
                }
            }

            this.figureDataMap.put(type, new AvatarFigureData(type, partId, primaryColor, secondaryColor));
        }
    }

    public Collection<AvatarFigureData> getFiguresData(){
        return this.figureDataMap.values();
    }
}
