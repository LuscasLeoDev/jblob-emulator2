//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import java.util.Map;

/**
 * Created by Lucas on 03/03/2017 at 01:56.
 */
public class AvatarSpriteDrawOrder {
    private Map<String, Integer> orders;

    public AvatarSpriteDrawOrder(Map<String, Integer> orders) {
        this.orders = orders;
    }

    public Map<String, Integer> getOrders() {
        return orders;
    }
}
