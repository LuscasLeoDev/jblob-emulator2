package com.luscastudio.jblob.extractor.swf.tags;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.io.File;
import java.util.Map;

/**
 * Created by Lucas on 02/11/2016.
 */

public class SymbolClassTag extends SwfTag {

    private short id;
    private String name;

    private Map<Short, String> symbolMap;

    public SymbolClassTag(SwfTagHeader header) {
        super(header);

        this.symbolMap = BCollect.newMap();
        short smbs = buffer.getShort();
        int i = 0;
        while(i < smbs){
            short id = buffer.getShort();
            String name = buffer.getString();
            if(!symbolMap.containsKey(id))
                symbolMap.put(id, name);
            //else
                //JBlobAE.write("Symbol id already exists " + id + " " + name + " as " + symbolMap.get(id));
            i++;
        }
    }

    public Map<Short, String> getSymbolMap() {
        return symbolMap;
    }


    @Override
    public void save(File file) throws Exception {

    }

    @Override
    public String getSuffix() {
        return null;
    }

    @Override
    public String getFolderName() {
        return null;
    }

    public String getName(short id) {
        if(symbolMap.containsKey(id))
            return symbolMap.get(id);

        return "";
    }

    public boolean hasName(short id) {
        return (symbolMap.containsKey(id));
    }

    @Override
    public void dispose() {
        super.dispose();
        this.symbolMap.clear();
    }

    @Override
    public boolean isSavable() {
        return false;
    }
}
