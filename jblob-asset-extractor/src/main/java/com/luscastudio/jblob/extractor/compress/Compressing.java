package com.luscastudio.jblob.extractor.compress;

import lzma.sdk.lzma.Decoder;
import lzma.streams.LzmaInputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.Inflater;

/**
 * Created by Lucas on 31/10/2016.
 */

public class Compressing {

    public static ByteBuffer ZLibDecompress(ByteBuffer compressed) throws Exception{

            byte[] content;

            Inflater in = new Inflater();

            content = new byte[compressed.remaining()];
            compressed.get(content).order(ByteOrder.LITTLE_ENDIAN);
            in.setInput(content);

            content = new byte[1024];

            //compressed.position(0);
            //compressed.get(content, 0, 8);

            ByteArrayOutputStream newBytes = new ByteArrayOutputStream(content.length);

            //newBytes.write(content, 0, 8);

            while (!in.finished()) {
                int c = in.inflate(content);
                newBytes.write(content, 0, c);
            }

            newBytes.close();
            content = newBytes.toByteArray();
            in.end();

            return ByteBuffer.wrap(content);
    }

    public static ByteBuffer LZMADecompress(ByteBuffer compressed) {
        byte[] input = new byte[compressed.remaining()];
        compressed.get(input);
        try (InputStream lzmaCompressed = new LzmaInputStream(new BufferedInputStream(new ByteArrayInputStream(input)), new Decoder());
             ByteArrayOutputStream uncompressedBytes = new ByteArrayOutputStream(lzmaCompressed.available());
        ) {


            IOUtils.copy(lzmaCompressed, uncompressedBytes);

            return ByteBuffer.wrap(uncompressedBytes.toByteArray());
        } catch (Exception var3) {
            var3.printStackTrace();
            return null;
        }
    }

}
