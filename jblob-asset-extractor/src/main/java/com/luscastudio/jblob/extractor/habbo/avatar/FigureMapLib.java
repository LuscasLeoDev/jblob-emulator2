//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 11:56.
 */
public class FigureMapLib {
    private String id;
    private int revision;

    private List<FigureMapPart> partMap;

    public FigureMapLib(Element element){
        this.partMap = BCollect.newList();
        if(element.hasAttribute("id"))
            this.id = element.getAttribute("id");
        else
            this.id = "null";

        if(element.hasAttribute("revision") && NumberHelper.isInteger(element.getAttribute("revision")))
            this.revision = Integer.parseInt(element.getAttribute("revision"));
        else
            this.revision = 0;

        NodeList parts = element.getElementsByTagName("part");
        for (int i = 0; i < parts.getLength(); i++) {
            Element item = (Element) parts.item(i);
            FigureMapPart part = new FigureMapPart(item);
            this.partMap.add(part);
        }
    }

    public Collection<FigureMapPart> getParts(){
        return partMap;
    }

    public String getId() {
        return id;
    }

    public int getRevision() {
        return revision;
    }
}
