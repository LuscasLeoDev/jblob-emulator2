//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.furni;

import com.luscastudio.jblob.extractor.habbo.furni.xml.FurniSpriteAsset;

/**
 * Created by Lucas on 03/03/2017 at 05:15.
 */
public class FurniAssetData {

    private FurniSpriteAsset spriteAsset;
    private SpriteImage image;

    public FurniAssetData(FurniSpriteAsset spriteAsset, SpriteImage image) {
        this.spriteAsset = spriteAsset;
        this.image = image;
    }

    public FurniSpriteAsset getSpriteAsset() {
        return spriteAsset;
    }

    public SpriteImage getImage() {
        return image;
    }
}
