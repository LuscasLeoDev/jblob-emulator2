//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.commands;

import com.luscastudio.jblob.api.utils.io.BFileReader;
import com.sun.imageio.plugins.png.PNGImageReader;
import com.sun.imageio.plugins.png.PNGImageWriter;
import com.sun.imageio.plugins.png.PNGImageWriterSpi;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Lucas on 05/03/2017 at 14:09.
 */
public class TestCommand implements Command {

    @Override
    public void handleCommand(CommandInput input) {
        String imageFilename = "C:\\Users\\LUCAS\\IdeaProjects\\JBlob Emulator\\jblob-asset-extractor\\bin\\gordon\\gcrystal\\images\\gcrystal_h_std_fx156_1_1_0_0.png";

        try {
            BufferedImage image = ImageIO.read(new File(imageFilename));
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    Color color = new Color(image.getRGB(x, y), true);
                    int whiteAmout = (int)((((double)(color.getRed() + color.getGreen() + color.getBlue()) / 765) * 255));

                    int blackAmount = 255 - whiteAmout;

                    Color newColor = new Color(Math.min(color.getRed() + whiteAmout, 255), Math.min(color.getGreen() + whiteAmout, 255), Math.min(color.getBlue() + whiteAmout, 255), whiteAmout);
                    image.setRGB(x, y, newColor.getRGB());
                }
            }

            ImageIO.write(image, "png", new File("tests/fx.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
