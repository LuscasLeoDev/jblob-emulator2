package com.luscastudio.jblob.extractor.swf.tags;

import com.luscastudio.jblob.extractor.io.BufferIO;
import com.luscastudio.jblob.extractor.io.SwfIO;

import java.nio.ByteBuffer;

/**
 * Created by Lucas on 31/10/2016.
 */

public class SwfTagHeader {

    private int code;
    private int length;
    private int offset;

    private BufferIO buffer;
    private SwfIO io;

    public SwfTagHeader(SwfIO io){
        this.io = io;
        short tagAndCode = io.getBuffer().getShort();

        this.code = tagAndCode >> 6;
        this.length = tagAndCode & 0x3F;
        if(this.length == 0x3F) {
            this.length = io.getBuffer().getInt();
        }

        this.offset = io.getBuffer().position();

        this.buffer = new BufferIO(ByteBuffer.wrap(io.getBuffer().getBytes(offset, length)));
    }

    public int getCode() {
        return code;
    }

    public int getLength() {
        return length;
    }

    public int getOffset() {
        return offset;
    }

    public SwfIO getIo() {
        return io;
    }

    public BufferIO getBuffer() {
        return buffer;
    }

    public void dispose() {
        this.io = null;
        this.buffer.close();
        this.buffer = null;
    }
}
