package com.luscastudio.jblob.extractor.swf.tags;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Lucas on 05/11/2016.
 */

public class DoAbcTag extends SwfTag {

    private String content;
    private String name;
    private int flags;

    public DoAbcTag(SwfTagHeader header) {
        super(header);
        this.id = -1;
        this.flags = this.buffer.getInt();
        this.name = this.buffer.getString();

        StringBuilder builder = new StringBuilder();
        while(this.buffer.remaining() > 0)
            builder.append((char)this.buffer.get());
        this.content = builder.toString();


    }

    @Override
    public void save(File file) throws Exception {
        try(FileOutputStream outputStream = new FileOutputStream(file)){
            outputStream.write(content.getBytes());
        }
    }

    @Override
    public String getSuffix() {
        return ".abc";
    }

    @Override
    public String getFolderName() {
        return "doabc";
    }

    @Override
    public boolean isSavable() {
        return true;
    }
}
