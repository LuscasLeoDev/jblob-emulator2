package com.luscastudio.jblob.extractor.habbo.furni.xml;

import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.habbo.furni.SpriteNameData;
import org.w3c.dom.Element;

/**
 * Created by Lucas on 04/11/2016.
 */

public class FurniSpriteAsset {

    private String name;
    private int x;
    private int y;
    private String source;
    private boolean flipH;

    private Element element;
    private SpriteNameData nameData;

    public FurniSpriteAsset(SpriteNameData nameData, String name, int x, int y, String source, boolean flipH) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.flipH = flipH;
        this.source = source;
    }

    public FurniSpriteAsset(SpriteNameData nameData, String name, int x, int y){
        this.name = name;
        this.x = x;
        this.y = y;
        this.flipH = false;
        this.source = "";
    }

    public FurniSpriteAsset(Element element, String frameName){
        this.element = element;

        if(element.hasAttribute("name"))
            this.name = element.getAttribute("name");
        if(element.hasAttribute("x"))
            this.x = Integer.parseInt(element.getAttribute("x"));
        if(element.hasAttribute("y"))
            this.y = Integer.parseInt(element.getAttribute("y"));

        if(element.hasAttribute("flipH"))
            this.flipH = element.getAttribute("flipH") == "1";
        else
            this.flipH = false;

        if(element.hasAttribute("source"))
            this.source = element.getAttribute("source");
        else
            this.source = "";

        try {
            this.nameData = new SpriteNameData(this.name.replace(frameName + "_", ""));
        } catch (Exception e){
            JBlobAE.write("Error while parsing name data:" + this.name);
            e.printStackTrace();
        }
    }


    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isFlipH() {
        return flipH;
    }

    public boolean hasSource(){
        return !this.source.equals("");
    }

    public String getSource() {
        return source;
    }

    public SpriteNameData getNameData() {
        return nameData;
    }

    @Override
    public String toString() {
        return "'" + name + "' [" + x + "," + y + "]";
    }
}

