package com.luscastudio.jblob.extractor.commands;

/**
 * Created by Lucas on 02/03/2017 at 05:00.
 */
public interface Command {
    void handleCommand(CommandInput input);
}
