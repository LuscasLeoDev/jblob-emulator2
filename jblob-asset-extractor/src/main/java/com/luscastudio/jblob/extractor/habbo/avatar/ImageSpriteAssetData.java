//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by Lucas on 02/03/2017 at 20:49.
 */
public class ImageSpriteAssetData {
    private String assetName;
    private int x;
    private int y;

    public ImageSpriteAssetData(Element element) {

        if(element.hasAttribute("name"))
            this.assetName = element.getAttribute("name");
        else
            this.assetName = "null";

        NodeList params = element.getElementsByTagName("param");
        for (int i = 0; i < params.getLength(); i++) {
            Element item = (Element) params.item(i);

            if(!item.hasAttribute("key"))
                continue;

            switch(item.getAttribute("key").toLowerCase()){
                case "offset":

                    if(!item.hasAttribute("value"))
                        break;

                    String[] coords = item.getAttribute("value").split(",");
                    for (int i1 = 0; i1 < coords.length; i1++) {
                        if(!NumberHelper.isInteger(coords[i1]))
                            continue;
                        switch(i1){
                            case 0:
                                this.x = Integer.parseInt(coords[i1]);
                                break;

                            case 1:
                                this.y = Integer.parseInt(coords[i1]);
                                break;
                        }
                    }

                    break;
            }
        }
    }

    public String getAssetName() {
        return assetName;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
