//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 08:38.
 */
public class FigureDataSetType {
    private String type;
    private int paletteId;

    private Map<Integer, FigureDataSet> setMap;

    public FigureDataSetType(Element element) {
        if(element.hasAttribute("type"))
            this.type = element.getAttribute("type");
        else
            this.type = "null";

        if(element.hasAttribute("paletteid") && NumberHelper.isInteger(element.getAttribute("paletteid")))
            this.paletteId = Integer.parseInt(element.getAttribute("paletteid"));
        else
            this.paletteId = 0;


        this.setMap = BCollect.newMap();

        NodeList parts = element.getElementsByTagName("set");
        for (int i = 0; i < parts.getLength(); i++) {
            Element item = (Element) parts.item(i);
            FigureDataSet set = new FigureDataSet(item);
            this.setMap.put(set.getId(), set);
        }
    }

    public FigureDataSet getFigureDataSet(int id){
        return this.setMap.get(id);
    }

    public String getType() {
        return type;
    }

    public int getPaletteId() {
        return paletteId;
    }
}
