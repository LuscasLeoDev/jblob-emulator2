package com.luscastudio.jblob.extractor.commands;

import com.google.common.collect.Lists;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Lucas on 04/11/2016.
 */

public class CommandInput {

    private String[] cmd;
    private String commandName;

    private int pointer;

    public CommandInput(String cmd){
        this.cmd = cmd.split(" ");
        this.commandName = this.cmd[0];
        this.pointer = 0;
    }

    public boolean setParam(String p){
        for(int i = 0; i < cmd.length; i++)
            if(cmd[i].equals(p)){
                pointer = i;
                return true;
            }

        return false;

    }

    public String getNext(){
        return cmd[++pointer];
    }

    public String getNext(String param){
        if(!this.setParam(param))
            return "";
        return getNext();
    }

    public String getCommandName() {
        return commandName;
    }

    public boolean hasAllParams(String params) {
        List<String> strs = Lists.newArrayList(this.cmd);
        for (String param : params.split(" ")) {
            boolean found = false;
            Iterator<String> iterator = strs.iterator();
            while (iterator.hasNext()) {
                if(found = iterator.next().equals(param)) {
                    iterator.remove();
                    break;
                }
            }
            if(!found)
                return false;
        }

        return true;
    }
}
