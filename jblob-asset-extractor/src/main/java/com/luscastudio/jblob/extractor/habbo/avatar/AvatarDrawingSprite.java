//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.extractor.habbo.images.DrawingSprite;

/**
 * Created by Lucas on 03/03/2017 at 05:38.
 */
public interface AvatarDrawingSprite extends DrawingSprite {
    boolean isColorable();
    String getColor();
}
