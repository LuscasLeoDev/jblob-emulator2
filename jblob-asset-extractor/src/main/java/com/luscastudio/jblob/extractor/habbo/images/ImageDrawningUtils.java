package com.luscastudio.jblob.extractor.habbo.images;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Lucas on 02/03/2017 at 21:22.
 */
public class ImageDrawningUtils {

    public static void setAlpha(BufferedImage image, byte alpha) {
        alpha %= 0xff;
        for (int cx = 0; cx < image.getWidth(); cx++) {
            for (int cy = 0; cy < image.getHeight(); cy++) {
                int color = image.getRGB(cx, cy);

                int mc = (alpha << 24) | 0x00ffffff;
                int newcolor = color & mc;
                image.setRGB(cx, cy, newcolor);

            }
        }
    }

    public static void colorateImage(BufferedImage image, String color) {

        Color newColor = new Color(Integer.valueOf(color, 16));

        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++) {


                Color oColor = new Color(image.getRGB(x, y), true);

                int r = (int) (((double) newColor.getRed() / 255) * oColor.getRed());
                int g = (int) (((double) newColor.getGreen() / 255) * oColor.getGreen());
                int b = (int) (((double) newColor.getBlue() / 255) * oColor.getBlue());

                image.setRGB(x, y, new Color(r, g, b, oColor.getAlpha()).getRGB());


            }

    }

    public static void makeColorTransparent(BufferedImage image, int rgb) {

        int markerRGB = rgb | 0xFF000000;
        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++) {
                int crgb = image.getRGB(x, y);
                Color crgbColor = new Color(crgb);

                int r = crgbColor.getRed();
                int g = crgbColor.getGreen();
                int b = crgbColor.getBlue();

                int pcolor =
                        r >> 16 +
                                g >> 8 +
                                b;

                int newRgb = 0;
                // Mark the alpha bits as zero - transparent
                //newRgb = ((0xFF000000) & );

                image.setRGB(x, y, newRgb);
            }
    }

    public static BufferedImage copyImage(BufferedImage imageToCopy){
        BufferedImage img = new BufferedImage(imageToCopy.getWidth(), imageToCopy.getHeight(), BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics2D assetCopyGp = img.createGraphics();
        assetCopyGp.drawImage(imageToCopy, 0, 0, null);
        assetCopyGp.dispose();

        return img;
    }

    public static BufferedImage flipHImage(BufferedImage image) {
        BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), 3);


        for (int h = 0; h < image.getHeight(); h++)
            for (int w = image.getWidth() - 1, ondInt = 0; w >= 0; w--, ondInt++) {
                newImage.setRGB(ondInt, h, image.getRGB(w, h));
            }
        return newImage;
    }
}