//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import org.w3c.dom.Element;

/**
 * Created by Lucas on 02/03/2017 at 08:55.
 */
public class FigureDataPart {
    private String id;
    private String type;
    private boolean colorable;
    private int colorIndex;

    public FigureDataPart(Element element) {
        if(element.hasAttribute("id"))
            this.id = element.getAttribute("id");
        else
            this.id = "null";

        if (element.hasAttribute("type"))
            this.type = element.getAttribute("type");
        else
            this.type = "null";

        this.colorable = element.hasAttribute("colorable") && element.getAttribute("colorable").equals("1");

        if(element.hasAttribute("colorindex") && NumberHelper.isInteger(element.getAttribute("colorindex")))
            this.colorIndex = Integer.parseInt(element.getAttribute("colorindex"));
        else
            this.colorIndex = 0;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public boolean isColorable() {
        return colorable;
    }

    public int getColorIndex() {
        return colorIndex;
    }
}
