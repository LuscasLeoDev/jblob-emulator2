package com.luscastudio.jblob.extractor.habbo.avatar;

import java.awt.image.BufferedImage;

/**
 * Created by Lucas on 02/03/2017 at 21:15.
 */
public class SpriteAsset {
    private BufferedImage image;
    private ImageSpriteAssetData assetData;
    private String type;
    private boolean colorable;
    private String color;
    private String action;

    public SpriteAsset(BufferedImage image, ImageSpriteAssetData assetData, String type, boolean colorable, String color) {
        this.image = image;
        this.assetData = assetData;
        this.type = type;
        this.color = color;
        this.colorable = colorable;
    }

    public BufferedImage getImage() {
        return image;
    }

    public ImageSpriteAssetData getAssetData() {
        return assetData;
    }

    public String getType() {
        return type;
    }

    public boolean isColorable() {
        return colorable;
    }

    public String getColor() {
        return color;
    }

    public String getAction() {
        return action;
    }
}
