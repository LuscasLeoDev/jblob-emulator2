package com.luscastudio.jblob.extractor.boot;

/**
 * Created by Lucas on 27/02/2017 at 19:00.
 */
public interface IDebug {
    void onDebug(String e);
}
