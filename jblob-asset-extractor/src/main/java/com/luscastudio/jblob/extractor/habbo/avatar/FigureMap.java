//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.extractor.xml.XmlFileReader;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 11:53.
 */
public class FigureMap extends XmlFileReader {

    private Map<String, FigureMapLib> libMap;

    public FigureMap(String xml) {
        super(xml);
        this.libMap = BCollect.newMap();

        NodeList libs = this.xml.getElementsByTagName("lib");
        for (int i = 0; i < libs.getLength(); i++) {
            Element element = (Element) libs.item(i);
            FigureMapLib lib = new FigureMapLib(element);
            this.libMap.put(lib.getId(), lib);
        }
    }

    public FigureMapLib getLib(String libId){
        return this.libMap.get(libId);
    }

    public FigureMapLib getLib(String partId, String type){
        for (FigureMapLib lib : this.libMap.values()) {
            for (FigureMapPart part : lib.getParts()) {
                if(part.getId().equals(partId) && part.getType().equals(type))
                    return lib;
            }
        }

        return null;
    }
}
