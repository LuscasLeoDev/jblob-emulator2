package com.luscastudio.jblob.extractor.habbo.furni;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Lucas on 05/11/2016.
 */

public class SpriteImageCollection extends LinkedList<SpriteImage> {

    public SpriteImageCollection(){
        super();
    }

    public SpriteImageCollection getByRotation(int rotation){
        SpriteImageCollection newCollection = new SpriteImageCollection();
        for(SpriteImage image : this){
            if(image.getNameData().getRotation() == rotation)
                newCollection.add(image);
        }

        return newCollection;
    }

    public SpriteImageCollection getByStateIndex(int stateIndex){
        SpriteImageCollection newCollection = new SpriteImageCollection();
        for(SpriteImage image : this){
            if(image.getNameData().getStateIndex() == stateIndex)
                newCollection.add(image);
        }

        return newCollection;
    }

    public SpriteImageCollection getByOrder(String order){
        SpriteImageCollection newCollection = new SpriteImageCollection();
        for(SpriteImage image : this){
            if(image.getNameData().getOrder().equals(order))
                newCollection.add(image);
        }

        return newCollection;
    }

    public SpriteImageCollection getBySize(String size){
        SpriteImageCollection newCollection = new SpriteImageCollection();
        for(SpriteImage image : this){
            if(image.getNameData().getSize().equals(size))
                newCollection.add(image);
        }

        return newCollection;
    }

    public SpriteImageCollection getByType(String type){
        SpriteImageCollection newCollection = new SpriteImageCollection();
        for(SpriteImage image : this){
            if(image.getNameData().getType().equals(type))
                newCollection.add(image);
        }

        return newCollection;
    }

    public SpriteImage getByName(String source) {
        for(SpriteImage image : this){
            if(image.getImageTag().getName().equals(source))
                return image;
        }

        return null;
    }

    public Map<String, SpriteImage> asMap(){
        Map<String, SpriteImage> map = BCollect.newMap();

        for(SpriteImage image : this){
            if(!map.containsKey(image.getImageTag().getName()))
                map.put(image.getImageTag().getName(), image);
        }

        return map;
    }

    public boolean hasOrder(String order){
        for(SpriteImage image : this){
            if(image.getNameData().getOrder().equals(order))
                return true;
        }

        return false;
    }

    public boolean hasStateIndex(int stateIndex){
        for(SpriteImage image : this){
            if(image.getNameData().getStateIndex() == (stateIndex))
                return true;
        }

        return false;
    }

    public boolean hasRotation(int rotation){
        for(SpriteImage image : this){
            if(image.getNameData().getRotation() == (rotation))
                return true;
        }

        return false;
    }


    public SpriteImageCollection setFlipH(boolean flip){
        for(SpriteImage image : this)
            image.setFlipH(flip);

        return this;
    }

    public void setReverseRotation() {
        for(SpriteImage image : this){
            image.setReverseRotation();
        }
    }
}
