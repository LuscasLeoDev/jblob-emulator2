//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.commands;

import com.luscastudio.jblob.api.utils.io.BFileReader;
import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.io.SwfIO;

import java.io.File;
import java.nio.ByteBuffer;

/**
 * Created by Lucas on 02/03/2017 at 09:55.
 */
public class DecompileSwfCommand implements Command {

    private boolean debug;

    @Override
    public void handleCommand(CommandInput input) {
        String path;
        String output;

        this.debug = input.setParam("-debug");

        if(!input.hasAllParams("-path -output")){
            JBlobAE.write("Please ensure that you are using the main params -path and -output");
            return;
        }

        path = input.getNext("-path");
        output = input.getNext("-output");

        File swfPath = new File(path);
        if(!swfPath.exists()) {
            JBlobAE.write("The path " + swfPath.getAbsolutePath() + "doesn't exits!");
            return;
        }

        NameDelegate delegate = io ->
                new File(output.replace("%name", io.getFrameName()));


        if(swfPath.isFile())
            decompileSwf(swfPath, delegate);
        else {
            File[] files;
            if (input.setParam("-regex")) {
                String pattern = input.getNext();
                files = swfPath.listFiles((dir, name) -> name.matches(pattern));
            } else
                files = swfPath.listFiles((dir, name) -> name.endsWith(".swf"));
            if (files != null)
                for (File file : files) {
                    decompileSwf(file, delegate);
                }
        }
    }

    private void decompileSwf(File file, NameDelegate delegate){
        ByteBuffer swfBuffer = BFileReader.getFileBytes(file.getAbsolutePath());
        if(swfBuffer == null) {
            JBlobAE.write("The file " + file.getAbsolutePath() + "doesn't exits!");
            return;
        }

        try {
            SwfIO io = new SwfIO(swfBuffer);
            if (this.debug)
                io.setDebugLogger(JBlobAE::write);

            io.init();

            io.saveFiles(delegate.getFile(io));
            io.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private interface NameDelegate {
        File getFile(SwfIO io);
    }
}
