//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 02/03/2017 at 08:53.
 */
public class FigureDataSet {

    private int id;
    private boolean colorable;

    private List<FigureDataPart> dataParts;
    private List<String> hiddenLayers;

    public FigureDataSet(Element element) {
        this.dataParts = BCollect.newList();
        this.hiddenLayers = BCollect.newList();
        if (element.hasAttribute("id") && NumberHelper.isInteger(element.getAttribute("id")))
            this.id = Integer.parseInt(element.getAttribute("id"));
        else
            this.id = 0;

        this.colorable = element.hasAttribute("colorable") && element.getAttribute("colorable").equals("1");

        NodeList parts = element.getElementsByTagName("part");
        for (int i = 0; i < parts.getLength(); i++) {
            Element item = (Element) parts.item(i);
            FigureDataPart part = new FigureDataPart(item);
            this.dataParts.add(part);
        }

        NodeList hiddenlayersXml = element.getElementsByTagName("hiddenlayers");
        for (int i = 0; i < hiddenlayersXml.getLength(); i++) {
            NodeList layers = ((Element) hiddenlayersXml.item(i)).getElementsByTagName("layer");
            for (int i1 = 0; i1 < layers.getLength(); i1++) {
                Element item = (Element) layers.item(i1);
                if(item.hasAttribute("parttype"))
                    this.hiddenLayers.add(item.getAttribute("parttype"));
            }
        }
    }

    public Collection<FigureDataPart> getDataParts() {
        return dataParts;
    }

    public Collection<String> getHiddenLayers() {
        return hiddenLayers;
    }

    public int getId() {
        return id;
    }

    public boolean isColorable() {
        return colorable;
    }
}
