package com.luscastudio.jblob.extractor.habbo.furni.xml;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.validator.Validator;
import com.luscastudio.jblob.extractor.xml.XmlFileReader;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.List;

/**
 * Created by Lucas on 04/11/2016.
 */

public class BinaryAssetXmlReader extends XmlFileReader {

    private List<FurniSpriteAsset> assets;
    private List<FurniSpriteAsset> assetsWithSources;

    public BinaryAssetXmlReader(String xml, String frameName){
        super(xml);

        this.assets = BCollect.newList();
        this.assetsWithSources = BCollect.newList();

        NodeList nodes = this.xml.getElementsByTagName("asset");
        for (int i = 0; i < nodes.getLength(); i++) {

            Element e = (Element)nodes.item(i);
            FurniSpriteAsset asset = new FurniSpriteAsset(e, frameName);
            if(!asset.hasSource())
                this.assets.add(asset);
            else
                assetsWithSources.add(asset);
        }
    }

    public List<FurniSpriteAsset> getAssets(Validator<FurniSpriteAsset> validator) {
        List<FurniSpriteAsset> r  = BCollect.newList();
        for(FurniSpriteAsset item : this.assets)
                if(validator.validate(item))
                    r.add(item);

        return r;
    }

    public List<FurniSpriteAsset> getAssets() {
        return assets;
    }
}
