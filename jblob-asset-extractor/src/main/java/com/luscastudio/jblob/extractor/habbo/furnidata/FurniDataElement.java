package com.luscastudio.jblob.extractor.habbo.furnidata;

import org.w3c.dom.Element;

/**
 * Created by Lucas on 04/11/2016.
 */

public class FurniDataElement {

    private String name;
    private int revision;
    private int partColorIndex;

    public FurniDataElement(Element element){

        String nameAndIndex = element.getAttribute("classname");
        String[] spl = nameAndIndex.split("/*/");
        this.name = spl[0];
        if(spl.length > 1)
            this.partColorIndex = Integer.parseInt(spl[1]);
        else
            this.partColorIndex = 0;

        this.revision = Integer.valueOf(element.getElementsByTagName("revision").item(0).getTextContent());
    }

    public String getName() {
        return name;
    }

    public int getRevision() {
        return revision;
    }

    public int getPartColorIndex() {
        return partColorIndex;
    }
}
