//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.validator.ValueGetter;

import java.util.Comparator;
import java.util.Map;

/**
 * Created by Lucas on 03/03/2017 at 01:37.
 */
public class AvatarSpriteDrawSorter {
    private Map<Integer, Map<String, AvatarSpriteDrawOrder>> drawOrderByRotation;
    private Map<String, Integer> actionsDrawOrder;

    public AvatarSpriteDrawSorter() {
        this.drawOrderByRotation = BCollect.newMap();
        this.actionsDrawOrder = BCollect.newMap();

        this.register();
    }

    private void register() {


        //region Rotation Order
        {
            Map<String, AvatarSpriteDrawOrder> defaultOrder = BCollect.newMap();

            drawOrderByRotation.put(2, defaultOrder);
            drawOrderByRotation.put(-1, defaultOrder);
        /*drawOrderByRotation.put(1, BCollect.newMap());
        drawOrderByRotation.put(0, BCollect.newMap());
        drawOrderByRotation.put(3, BCollect.newMap());

        drawOrderByRotation.put(7, BCollect.newMap());*/

            Map<String, AvatarSpriteDrawOrder> orderMap;
            Map<String, Integer> orders;

            //region Rotation 2

            orderMap = drawOrderByRotation.get(2);

            //region Stand [std]]

            orders = BCollect.newMap();
            orders.put("hd", 9);
            orders.put("ey", 10);
            orders.put("fc", 11);
            orders.put("hr", 12);
            orders.put("ea", 13);
            orders.put("ha", 14);
            orders.put("he", 15);

            orders.put("bd", 0);
            orders.put("lh", 1);
            orders.put("ch", 2);
            orders.put("ls", 3);
            orders.put("lc", 4);
            orders.put("rh", 10);
            orders.put("rc", 11);
            orders.put("rs", 12);
            orders.put("ca", 8);

            orders.put("lg", 1);
            orders.put("sh", 2);
            orders.put("wa", 3);

            orderMap.put("std", new AvatarSpriteDrawOrder(orders));

            //endregion

            //region Wav [wav]

            orders = BCollect.newMap();


            //endregion

            //endregion

        }
        //endregion

        //region Action Order

        actionsDrawOrder.put("wav", 0);
        actionsDrawOrder.put("wlk", 0);
        actionsDrawOrder.put("std", 1);

        //endregion
    }

    private int getOrderOfAction(String act){
        if(!actionsDrawOrder.containsKey(act))
            return -1;
        return actionsDrawOrder.get(act);
    }


    public Comparator<SpriteAsset> sort(int rotation, String action) {
        Map<String, AvatarSpriteDrawOrder> byRotation = this.drawOrderByRotation.get(rotation);
        if(byRotation == null)
            byRotation = this.drawOrderByRotation.get(-1);

        AvatarSpriteDrawOrder byAction = byRotation.get(action);
        if(byAction == null)
            byAction = this.drawOrderByRotation.get(-1).get("std");

        AvatarSpriteDrawOrder finalByAction = byAction;
        return (o1, o2) -> {

            Integer o1Index = finalByAction.getOrders().get(o1.getType());
            Integer o2Index = finalByAction.getOrders().get(o2.getType());

            if(o1Index == null)
                o1Index = 0;
            if(o2Index == null)
                o2Index = 0;

            return o1Index - o2Index;

        };
    }

    public <T> Comparator<T> sortActionOrder(ValueGetter<T, String> valueGetter) {
        return Comparator.comparingInt(o -> this.getOrderOfAction(valueGetter.getValue(o)));
    }
}
