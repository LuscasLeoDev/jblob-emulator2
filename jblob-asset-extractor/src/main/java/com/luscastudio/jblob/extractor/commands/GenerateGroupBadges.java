//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.commands;

import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.sun.org.apache.xerces.internal.impl.PropertyManager;
import com.sun.xml.internal.bind.v2.runtime.output.XMLStreamWriterOutput;
import com.sun.xml.internal.stream.writers.XMLStreamWriterImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;

import static com.sun.org.apache.xerces.internal.impl.PropertyManager.CONTEXT_WRITER;

/**
 * Created by Lucas on 09/04/2017 at 14:20.
 */
public class GenerateGroupBadges implements Command {
    @Override
    public void handleCommand(CommandInput input) {


        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
            documentBuilder.reset();

            Document document = documentBuilder.newDocument();

            document.normalize();
            document.normalizeDocument();

            Element badgedata = document.createElement("badgedata");
            document.appendChild(badgedata);

            Element symbols = document.createElement("symbols");
            Element bases = document.createElement("bases");
            badgedata.appendChild(symbols);
            badgedata.appendChild(bases);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            DOMSource source = new DOMSource(document);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult streamResult = new StreamResult(outputStream);

            transformer.transform(source, streamResult);

            byte[] buffer = outputStream.toByteArray();
            outputStream.close();

            StringBuilder builder = new StringBuilder();
            for (byte b : buffer) {
                builder.append((char)b);
            }

            JBlobAE.write(builder.toString());

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*String path;
        String outputPath;
        if(!input.setParam("-path")){
            JBlobAE.write("I need a path to work on, bitch! Do you think that i get the files from you ass?");
            return;
        }

        path = input.getNext();

        if(!input.setParam("-output")){
            JBlobAE.write("I need a output to put that shit u want, u dumb");
            return;
        }

        outputPath = input.getNext();

        File baseFile = new File(path);
        if(!baseFile.exists()){
            JBlobAE.write("This path doesn't exists, idiot");
            return;
        }

        File[] files = baseFile.listFiles((dir, name) -> name.startsWith("badgepart_") && name.endsWith(".png"));
        */



    }
}
