//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import java.awt.image.BufferedImage;

/**
 * Created by Lucas on 03/03/2017 at 00:12.
 */
public class AvatarPartDrawingSprite implements AvatarDrawingSprite {

    private int x;
    private int y;

    private BufferedImage image;

    private boolean colorable;
    private String color;

    public AvatarPartDrawingSprite(SpriteAsset asset) {
        this.image = asset.getImage();
        this.x = asset.getAssetData().getX();
        this.y = asset.getAssetData().getY();
        this.colorable = asset.isColorable();
        this.color = asset.getColor();
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public BufferedImage getImage() {
        return image;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean isColorable() {
        return colorable;
    }

    @Override
    public String getColor() {
        return color;
    }
}
