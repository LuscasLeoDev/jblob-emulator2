package com.luscastudio.jblob.extractor.habbo.furni;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.extractor.habbo.furni.xml.*;
import com.luscastudio.jblob.extractor.habbo.furni.xml.SpriteLayer;
import com.luscastudio.jblob.extractor.habbo.images.DrawingSprite;
import com.luscastudio.jblob.extractor.habbo.images.ImageDrawningUtils;
import com.luscastudio.jblob.extractor.habbo.images.SpriteImageDrawing;
import com.luscastudio.jblob.extractor.io.SwfIO;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 04/11/2016.
 */

public class FurniSpriteImageDrawing extends SpriteImageDrawing {

    //private List<FurniSpriteAsset> assetItems;
    private SpriteVisualization visualization;
    //private Map<FurniSpriteAsset, Point> assetPoints;
    private List<DrawingSprite> drawingSprites;

    public FurniSpriteImageDrawing(SwfIO io, SpriteVisualization visualization, List<FurniAssetData> assetDatas){

        //this.assetPoints = BCollect.newMap();
        this.drawingSprites = BCollect.newList();
        this.visualization = visualization;
        for (FurniAssetData assetData : assetDatas) {
            this.drawingSprites.add(new FurniDrawingSprite(assetData.getImage(), assetData.getSpriteAsset(), false, ""));
        }

        prepareSize(io.getRect().getMaxX(), io.getRect().getMaxY());
    }

    public BufferedImage draw(SpriteColor color){

        int w = getBaseImageWidth();
        int h = getBaseImageHeight();


        /*SpriteColor color = null;
        if(visualization != null) {
            if (visualization.getColors().containsKey(partColorId))
                color = visualization.getColors().get(partColorId);
        }*/

        BufferedImage baseImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics gp = baseImage.getGraphics();


        int layerIndex = 0;
        boolean hasAssets = false;
        for(DrawingSprite item : drawingSprites) {
            hasAssets = true;
            BufferedImage image = ImageDrawningUtils.copyImage(item.getImage());

            int x = item.getX();
            int y = item.getY();

            //region Drawing


            //Colorating
            if(color != null){
                if(layerIndex < color.getColors().size())
                    ImageDrawningUtils.colorateImage(image, color.getColors().get(layerIndex).getColor());
            }

            for (SpriteLayer layer : visualization.getLayers()) {
                if (layer.getLayerId() == layerIndex) {
                    switch (layer.getInk()) {
                        case "ADD":

                            ImageDrawningUtils.makeColorTransparent(image, new Color(0, 1, 3).getRGB());

                            break;
                    }

                    if (layer.getAlpha() > -1) {
                        ImageDrawningUtils.setAlpha(image, (byte) layer.getAlpha());
                    }
                }
            }


            gp.drawImage(image, x, y, null);
            {
                //gp.drawImage(image, x, y, null);
                /*switch (item.getImage().getNameData().getOrder()) {
                    case "sd":

                        /*for(int sx = x; sx - x < image.getWidth() && sx < w; sx++)
                            for(int sy = y; sy - y < image.getHeight() && sy < h; sy++) {

                                int sprColorInt = image.getRGB(sx - x, sy - y);
                                int baseColorInt = baseImage.getRGB(sx, sy);

                                int a = (baseColorInt >> 24) & 0xff;
                                int r = (baseColorInt >> 16) & 0xff;
                                int g = (baseColorInt >> 8) & 0xff;
                                int b = (baseColorInt >> 0) & 0xff;

                                baseImage.setRGB(sx, sy, new Color(r, g, b, a).getRGB());


                            }*

                        break;

                    default:
                        gp.drawImage(image, x, y, null);
                        break;

                }*/
            }

            //endregion

                /*int cropX = x;
                int cropY = y;

                if(cropX < cropWidth)
                    cropWidth = cropX;

                if(cropY < cropHeight)
                    cropHeight = cropY;

                cropX = x + image.getWidth();
                cropY = y + image.getHeight();

                if(cropX > startCropX)
                    startCropX = cropX;

                if(cropY > startCropY)
                    startCropY = cropY;*/

            this.reviewSize(x, y, item.getImage().getWidth(), item.getImage().getHeight());


            layerIndex++;
        }

        if(hasAssets)
            baseImage = baseImage.getSubimage(getStartCropX(), getStartCropY(), getCropWidth() - getStartCropX(), getCropHeight() - getStartCropY());

        //gp.create(cropX, cropY, baseImage.getWidth() - cropX, baseImage.getHeight() - cropY);
        gp.dispose();

        return baseImage;
    }

    /*private void blendAdd(BufferedImage image, int x, int y){
        for (int h = 0; h < image.getHeight() && h + y < baseImage.getHeight(); h++)
            for(int w = 0; w < image.getWidth() && w + x < baseImage.getWidth(); w++){

                Color sprColor = new Color(image.getRGB(w, h));
                Color baseColor = new Color(baseImage.getRGB(w + x, h + y));

                int a = Math.max(sprColor.getAlpha(), baseColor.getAlpha());
                int r = Math.min(255, sprColor.getRed() + baseColor.getRed());
                int g = Math.min(255, sprColor.getGreen() + baseColor.getGreen());
                int b = Math.min(255, sprColor.getBlue() + baseColor.getBlue());

                baseImage.setRGB(w + x, h + y, new Color(r, g, b, a).getRGB());
            }

    }*/

    private int[] getMinSizeByCoords(){
        int w = 0;
        int h = 0;


        for(DrawingSprite item : this.drawingSprites){

            if(item.getX() > w)
                w = item.getX();

            if(item.getY() > h)
                h = item.getY();
        }

        return new int[]{w, h};
    }

    public Collection<? extends DrawingSprite> getDrawingSprites() {
        return this.drawingSprites;
    }


    /*public Point getAssetPoint(FurniSpriteAsset asset){
        Point p;
        if(!this.assetPoints.containsKey(asset))
            this.assetPoints.put(asset, p = new Point(asset.getX(), asset.getY()));
        else
            p = this.assetPoints.get(asset);

        return p;
    }

    public void setAssetPoint(FurniSpriteAsset asset, int x, int y){
        if(!this.assetPoints.containsKey(asset))
            this.assetPoints.put(asset, new Point(x, y));
        else
            this.assetPoints.replace(asset, new Point(x, y));
    }*/

}
