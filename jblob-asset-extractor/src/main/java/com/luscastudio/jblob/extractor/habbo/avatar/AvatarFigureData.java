//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

/**
 * Created by Lucas on 02/03/2017 at 11:33.
 */
public class AvatarFigureData {

    private String type;
    private int partId;
    private int primaryColorId;
    private int secondaryColorId;

    public AvatarFigureData(String type, int partId, int primaryColorId, int secondaryColorId) {
        this.type = type;
        this.partId = partId;
        this.primaryColorId = primaryColorId;
        this.secondaryColorId = secondaryColorId;
    }

    public String getType() {
        return type;
    }

    public int getPartId() {
        return partId;
    }

    public int getPrimaryColorId() {
        return primaryColorId;
    }

    public int getSecondaryColorId() {
        return secondaryColorId;
    }
}
