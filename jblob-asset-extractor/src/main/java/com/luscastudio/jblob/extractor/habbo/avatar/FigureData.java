//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.extractor.xml.XmlFileReader;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 07:58.
 */
public class FigureData extends XmlFileReader {
    private Map<Integer, FigurePalette> figureColorsMap;
    private Map<String, FigureDataSetType> setTypes;

    public FigureData(String xml) {
        super(xml);

        this.setTypes = BCollect.newMap();
        this.figureColorsMap = BCollect.newMap();

        NodeList palettes = this.xml.getElementsByTagName("palette");
        for (int i = 0; i < palettes.getLength(); i++) {
            Element element = (Element) palettes.item(i);
            FigurePalette palette = new FigurePalette(element);
            this.figureColorsMap.put(palette.getId(), palette);
        }

        NodeList settypes = this.xml.getElementsByTagName("settype");
        for (int i = 0; i < settypes.getLength(); i++) {
            Element item = (Element) settypes.item(i);
            FigureDataSetType setType = new FigureDataSetType(item);
            this.setTypes.put(setType.getType(), setType);
        }
    }

    public FigureDataSetType getSetType(String type){
        return setTypes.get(type);
    }

    public FigurePalette getPalette(int id){
        return this.figureColorsMap.get(id);
    }
}
