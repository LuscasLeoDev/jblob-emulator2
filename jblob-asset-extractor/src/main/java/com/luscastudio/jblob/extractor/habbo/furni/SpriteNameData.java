package com.luscastudio.jblob.extractor.habbo.furni;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.extractor.boot.JBlobAE;

/**
 * Created by Lucas on 28/02/2017 at 03:54.
 */
public class SpriteNameData {

    private String size;
    private String order;
    private int rotation;
    private int stateIndex;
    private String type;

    public SpriteNameData(String name) {

            String[] spl = name.split("_");
            this.type = spl[0];
            switch (type) {
                case "icon":
                    this.order = spl[1];
                    this.rotation = 2;
                    this.stateIndex = 0;
                    this.size = "0";
                    break;

                case "32":
                case "64":
                    this.size = spl[0];
                    this.order = spl[1];
                    if(NumberHelper.isInteger(spl[2]))
                        this.rotation = Integer.parseInt(spl[2]);
                    else
                        this.rotation = 0;
                    if(spl.length > 3 && NumberHelper.isInteger(spl[3]))
                        this.stateIndex = Integer.parseInt(spl[3]);
                    else
                        this.stateIndex = -1;
                    break;

                default:
                    this.size = "0";
                    this.order = "z";
                    this.rotation = 0;
                    this.stateIndex = -1;
                    break;
            }
    }

    public String getSize() {
        return size;
    }

    public String getOrder() {
        return order;
    }

    public int getRotation() {
        return rotation;
    }

    public int getStateIndex() {
        return stateIndex;
    }

    public String getType() {
        return type;
    }
}
