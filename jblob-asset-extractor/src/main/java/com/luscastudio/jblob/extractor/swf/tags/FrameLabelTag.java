package com.luscastudio.jblob.extractor.swf.tags;

import java.io.File;

/**
 * Created by Lucas on 04/11/2016.
 */

public class FrameLabelTag extends SwfTag {

    private String frameName;

    public FrameLabelTag(SwfTagHeader header) {
        super(header);

        this.frameName = header.getBuffer().getString();
    }

    public String getFrameName() {
        return frameName;
    }

    @Override
    public void save(File file) throws Exception {

    }

    @Override
    public String getSuffix() {
        return null;
    }

    @Override
    public String getFolderName() {
        return null;
    }

    @Override
    public boolean isSavable() {
        return false;
    }
}
