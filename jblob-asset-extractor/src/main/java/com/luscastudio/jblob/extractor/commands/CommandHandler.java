package com.luscastudio.jblob.extractor.commands;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.process.ProcessExecutor;

import java.util.Map;
import java.util.concurrent.ScheduledFuture;

/**
 * Created by Lucas on 04/11/2016.
 */

@SuppressWarnings("ControlFlowStatementWithoutBraces")
public class CommandHandler {

    private Map<String, Command> commands;

    public CommandHandler(){
        this.commands = BCollect.newMap();

        this.commands.put("icons", new ExtractIconCommand());
        this.commands.put("decompile", new DecompileSwfCommand());
        this.commands.put("avatar", new GenerateAvatarImageCommand());
        this.commands.put("test", new TestCommand());
        this.commands.put("gbgenerate", new GenerateGroupBadges());
    }

    public void handleCommand(CommandInput input) throws Exception {

        if(this.commands.containsKey(input.getCommandName().toLowerCase())) {
            Command command = this.commands.get(input.getCommandName().toLowerCase());

            command.handleCommand(input);

        } else
            JBlobAE.write("Command '" + input.getCommandName() + "' not found!");

        /*switch(input.getCommandName()){
            case "icon":
            {
                boolean userevision = JBlobAE.useRevisionFolder;
                String outputPath = JBlobAE.assetsOutPut;
                boolean customOutput = false;
                boolean usePath = false;
                int partColorId = 0;

                if(input.setParam("-useRevision")) {
                    userevision = input.getNext().toLowerCase().equals("true");
                }

                if(customOutput = input.setParam("-output")) {
                    outputPath = input.getNext();
                }

                String name = input.getNext("-name");
                String revision = input.getNext("-revision");

                if(input.setParam("-partcolor"))
                    partColorId = Integer.valueOf(input.getNext());

                if(usePath = input.setParam("-path"))
                    name = input.getNext();


                ByteBuffer fileBuf;
                if (!usePath) {
                    if (userevision)
                        fileBuf = BFileReader.getFileBytes(JBlobAE.swfInputPath + "/" + revision + "/" + name + ".swf");
                    else
                        fileBuf = BFileReader.getFileBytes(JBlobAE.swfInputPath + "/" + name + ".swf");
                } else {
                    fileBuf = BFileReader.getFileBytes(name);
                }

                if(fileBuf == null){
                    JBlobAE.write("SWF File not found!");
                    return;
                }

                SwfIO io = new SwfIO(fileBuf);
                io.init();
                FurniData furniData = new FurniData(io);

                File file;
                if(customOutput)
                    file = new File(outputPath);
                else
                    file = new File(outputPath + "/" + io.getFrameName() + "_icon.png");

                if(!file.exists())
                    file.mkdirs();

                try{
                    ImageIO.write(furniData.getIcon(partColorId), "png", file);
                }catch (Exception e){
                    e.printStackTrace();
                }

                JBlobAE.write(name + " icon extracted at '" + outputPath + "'");

                break;
            }

            case "icons":
            {
                boolean userevision = JBlobAE.useRevisionFolder;
                String outputPath = JBlobAE.assetsOutPut;
                boolean customOutput = false;
                boolean usePath = false;
                boolean debug = input.setParam("-debug");

                if(input.setParam("-useRevision")) {
                    userevision = input.getNext().toLowerCase().equals("true");
                }

                if(customOutput = input.setParam("-output")) {
                    outputPath = input.getNext();
                }

                String path = input.getNext("-path");
                String revision = input.getNext("-revision");

                String name = path;
                if(usePath = input.setParam("-name"))
                    name = input.getNext();

                File filePath = new File(path);
                if(filePath.isFile()) {
                    ByteBuffer fileBuf = BFileReader.getFileBytes(path);

                    if (fileBuf == null) {
                        JBlobAE.write("SWF File not found!");
                        return;
                    }

                    SwfIO swfIO = new SwfIO(fileBuf);
                    if(debug)
                        swfIO.setDebugLogger(JBlobAE::write);
                    swfIO.init();
                    FurniData furniData = new FurniData(swfIO);
                    try {
                        for (Map.Entry<Integer, BufferedImage> entry : furniData.getIcons().entrySet()) {
                            File file;
                            if (entry.getKey() >= 0)
                                file = new File(outputPath + "/" + swfIO.getFrameName() + "_" + entry.getKey() + "_icon.png");
                            else
                                file = new File(outputPath + "/" + swfIO.getFrameName() + "_icon.png");

                            if (!file.exists() && !file.mkdirs()) {
                                JBlobAE.write("Could not create the directory for '" + file.toString() + "'");
                                continue;
                            }


                            try {
                                ImageIO.write(entry.getValue(), "png", file);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            JBlobAE.write("Saving icon at " + file.getAbsolutePath());

                        }
                    } catch (Exception e){
                        e.printStackTrace();
                        throw new Exception("Error while getting icons for " + filePath.getFile());
                    }
                } else {
                    File[] files;
                    if(!input.setParam("-regex"))
                        files = filePath.listFiles((dir, name1) -> name1.endsWith(".swf"));
                    else {
                        String reg = input.getNext();
                        files = filePath.listFiles((dir, name1) -> name1.matches(reg));
                    }

                    if(files == null){
                        JBlobAE.write("Sorry, but the path is null!");
                        break;
                    }
                    for (File swfFile : files) {
                        try{
                            ByteBuffer buffer = BFileReader.getFileBytes(swfFile.getAbsolutePath());
                            if(buffer == null)
                                continue;

                            SwfIO io = new SwfIO(buffer);
                            if(debug)
                                io.setDebugLogger(JBlobAE::write);
                            io.init();

                            try {
                                FurniData furniAssetData = new FurniData(io);
                                for (Map.Entry<Integer, BufferedImage> entry : furniAssetData.getIcons().entrySet()) {
                                    File file;
                                    if (entry.getKey() >= 0)
                                        file = new File(outputPath + "/" + io.getFrameName() + "_" + entry.getKey() + "_icon.png");
                                    else
                                        file = new File(outputPath + "/" + io.getFrameName() + "_icon.png");

                                    if (!file.exists() && !file.mkdirs()) {
                                        JBlobAE.write("Could not create the directory for '" + file.toString() + "'");
                                        continue;
                                    }


                                    try {
                                        ImageIO.write(entry.getValue(), "png", file);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    JBlobAE.write("Saving icon at " + file.getAbsolutePath());
                                }
                            } catch (Exception e){
                                e.printStackTrace();
                                throw new Exception("Error getting icon to " + swfFile.getFile());
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                break;
            }

            case "decompile": {
                boolean userevision = JBlobAE.useRevisionFolder;
                String outputPath = JBlobAE.assetsOutPut;
                boolean customOutput = false;
                boolean usePath = false;

                if (input.setParam("-useRevision"))
                    userevision = input.getNext().toLowerCase().equals("true");

                if (customOutput = input.setParam("-output"))
                    outputPath = input.getNext();

                String name = input.getNext("-name");
                String revision = input.getNext("-revision");

                if (usePath = input.setParam("-path")) {
                    name = input.getNext();
                    userevision = false;
                }

                ByteBuffer fileBuf;

                if (!usePath) {
                    if (userevision)
                        fileBuf = BFileReader.getFileBytes(JBlobAE.swfInputPath + "/" + revision + "/" + name + ".swf");
                    else
                        fileBuf = BFileReader.getFileBytes(JBlobAE.swfInputPath + "/" + name + ".swf");
                } else {
                    fileBuf = BFileReader.getFileBytes(name);
                }


                if (fileBuf == null) {
                    JBlobAE.write("SWF File not found!");
                    return;
                }


                SwfIO io = new SwfIO(fileBuf);

                if(input.setParam("-debug"))
                    io.setDebugLogger(JBlobAE::write);
                io.init();

                if(!customOutput)
                    outputPath += "/" + io.getFrameName();

                Path path = JBlobAE.getOrCreatePath(outputPath).toAbsolutePath();

                io.saveFiles(path.toString());

                JBlobAE.write(name + " decompiled at '" + path.toString() + "'");

                break;
            }

            case "dump": {
                boolean userevision = JBlobAE.useRevisionFolder;
                String outputPath = JBlobAE.assetsOutPut;
                boolean customOutput = false;
                boolean usePath = false;

                int stateIndex = 0;
                int rotation = 2;
                String size = "64";
                String order = "a";

                int partColorId = 0;

                if(input.setParam("-partcolor"))
                    partColorId = Integer.valueOf(input.getNext());

                if(input.setParam("-state"))
                    stateIndex = Integer.valueOf(input.getNext());

                if(input.setParam("-rotation") || input.setParam("-rot") || input.setParam("-dir"))
                    rotation = Integer.valueOf(input.getNext());


                Map<String, SpriteImage> images = BCollect.newMap();
                List<FurniSpriteAsset> assetItemList = BCollect.newList();

                if (input.setParam("-useRevision"))
                    userevision = input.getNext().toLowerCase().equals("true");

                if (customOutput = input.setParam("-output"))
                    outputPath = input.getNext();

                String name = input.getNext("-name");
                String revision = input.getNext("-revision");

                if (usePath = input.setParam("-path")) {
                    name = input.getNext();
                    userevision = false;
                }


                ByteBuffer fileBuf;
                if (!usePath) {
                    if (userevision)
                        fileBuf = BFileReader.getFileBytes(JBlobAE.swfInputPath + "/" + revision + "/" + name + ".swf");
                    else
                        fileBuf = BFileReader.getFileBytes(JBlobAE.swfInputPath + "/" + name + ".swf");
                } else {
                    fileBuf = BFileReader.getFileBytes(name);
                }

                if (fileBuf == null) {
                    JBlobAE.write("SWF File not found!");
                    return;
                }

                SwfIO io = new SwfIO(fileBuf);

                FurniData assetData = new FurniData(io);

                BufferedImage image = assetData.getFurniView(stateIndex, rotation, size, order, partColorId);

                File file;
                if(customOutput)
                    file = new File(outputPath);
                else
                    file = new File(outputPath + "/" + name + ".png");

                if(!file.exists())
                    file.mkdirs();

                try{
                    ImageIO.write(image, "png", file);
                }catch (Exception e){
                    e.printStackTrace();
                }

                JBlobAE.write(name + " icon extracted at '" + outputPath + "'");

                break;
            }

            default: {
                JBlobAE.write("Command '" + input.getCommandName() + "' not found!");
                break;
            }
        }
        */
    }


}
