package com.luscastudio.jblob.extractor.io;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Lucas on 02/11/2016.
 */

public class BufferIO {

    private ByteBuffer buffer;

    public BufferIO(ByteBuffer buffer){
        this.buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    //region Getting Bytes Data

    public short getUShort(){
        return (short)(
                buffer.get() | (
                        buffer.get() << 8
                )
        );
    }

    private double collectFixed8() {

        int lo0, lo, hi0, hi;
        int full;
        double ret;

        lo0 = lo = this.buffer.get();
        hi0 = hi = this.buffer.get();
        if (hi < 128) {
            ret = (hi + lo) / 256.0;
        } else {
            full = 65536 - (256 * hi + lo);
            hi = full >> 8;
            lo = full & 0xff;
            ret = -(hi + lo / 256.0);
        }
        // echo sprintf("collectFixed8 hi=[0x%X], lo=[0x%X], return [%s]\n", hi0, lo0, ret);
        return ret;
    }

    public byte[] getBytes(int offset, int length) {
        int pos = this.position();

        byte[] ret = new byte[length];

        this.setPointer(offset);
        this.buffer.get(ret, 0, length);

        this.setPointer(pos);

        return ret;
    }

    public int getUInt() {
        byte[] data = new byte[]{
                buffer.get(),
                buffer.get(),
                buffer.get(),
                buffer.get()
        };

        return (data[3] << 24) | (data[2] << 16) | (data[1] << 8) | data[0];
    }

    public void setPointer(int pointer) {
        this.buffer.position(pointer);
    }

    //endregion

    //region ByteBuffer child
    public int position(){
        return buffer.position();
    }

    public Buffer position(int i){
        return buffer.position(i);
    }

    public byte get(){
        return buffer.get();
    }

    public ByteBuffer get(byte[] bytes) {
        return buffer.get(bytes);
    }

    public ByteBuffer getByteBuffer() {
        return buffer;
    }

    public short getShort(){
        return buffer.getShort();
    }

    public int getInt(){
        return buffer.getInt();
    }

    public int remaining() {
        return buffer.remaining();
    }


    //endregion

    public byte getByte(){
        return (byte)(get() & 255);
    }

    public String getString() {
        StringBuilder str = new StringBuilder();
        byte charr;
        while(buffer.remaining() > 0 && (charr = getByte()) != (byte)0) {
            str.append((char) charr);
        }

        return str.toString();
    }

    public void close() {
        if(this.buffer != null)
            this.buffer.clear();
        this.buffer = null;
    }
}
