//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 08:01.
 */
public class FigurePalette {
    private int id;
    private Map<Integer, FigureColor> colorMap;

    public FigurePalette(Element element){
        this.colorMap = BCollect.newMap();

        if(element.hasAttribute("id") && NumberHelper.isInteger(element.getAttribute("id")))
            this.id = Integer.parseInt(element.getAttribute("id"));
        else
            this.id = -1;

        NodeList colors = element.getElementsByTagName("color");
        for (int i = 0; i < colors.getLength(); i++) {
            Element item = (Element) colors.item(i);

            FigureColor color = new FigureColor(item);
            this.colorMap.put(color.getId(), color);
        }
    }

    public int getId() {
        return id;
    }

    public FigureColor getFigureColor(int id){
        return this.colorMap.get(id);
    }
}
