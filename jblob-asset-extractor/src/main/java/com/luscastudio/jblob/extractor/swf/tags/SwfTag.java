package com.luscastudio.jblob.extractor.swf.tags;

import com.luscastudio.jblob.extractor.io.BufferIO;

import java.io.File;

/**
 * Created by Lucas on 31/10/2016.
 */
public abstract class SwfTag {


    protected short id;
    protected String name;
    protected SwfTagHeader header;
    protected BufferIO buffer;

    public SwfTag(SwfTagHeader header){
        this.header = header;
        this.buffer = header.getBuffer();
        this.name = "";
    }


    public short getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BufferIO getBuffer() {
        return buffer;
    }

    public SwfTagHeader getHeader() {
        return header;
    }

    public abstract void save(File file) throws Exception;

    public abstract String getSuffix();

    public abstract String getFolderName();

    @Override
    public String toString() {
        return this.getClass().getName() + " #" + this.getId() + " Size:" + this.getHeader().getLength();
    }

    public void dispose(){
        this.buffer.close();
        this.header.dispose();
    }

    public abstract boolean isSavable();
}
