package com.luscastudio.jblob.extractor.habbo.furni.xml;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 06/11/2016.
 */

public class SpriteVisualization {

    private String size;
    private int layerCount;
    private String angle;

    private List<SpriteLayer> layers;
    private Map<Integer, SpriteColor> colors;

    public SpriteVisualization(int layerCount, String size, String angle, Element layersElement, Element colorsElement){
        this.layerCount = layerCount;
        this.size = size;
        this.angle = angle;

        this.layers = BCollect.newList();
        this.colors = BCollect.newMap();

        NodeList nodeList;
        if(layersElement != null) {
            nodeList = layersElement.getElementsByTagName("layer");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element layerElement = (Element) nodeList.item(i);
                int id = 0;
                String ink = "";
                int z = 0;

                int alpha = -1;

                if (layerElement.hasAttribute("id"))
                    id = Integer.valueOf(layerElement.getAttribute("id"));
                if (layerElement.hasAttribute("ink"))
                    ink = (layerElement.getAttribute("ink"));
                if (layerElement.hasAttribute("z"))
                    z = Integer.valueOf(layerElement.getAttribute("z"));
                if (layerElement.hasAttribute("alpha"))
                    alpha = Integer.valueOf(layerElement.getAttribute("alpha"));

                this.layers.add(new SpriteLayer(id, ink, z, alpha));
            }
        }

        if(colorsElement != null) {
            nodeList = colorsElement.getElementsByTagName("color");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element colorElement = (Element) nodeList.item(i);
                SpriteColor color = new SpriteColor(colorElement);
                this.colors.put(color.getId(), color);
            }
        }
    }

    public String getSize() {
        return size;
    }

    public int getLayerCount() {
        return layerCount;
    }

    public Map<Integer, SpriteColor> getColors() {
        return colors;
    }

    public List<SpriteLayer> getLayers() {
        return layers;
    }

    public String getAngle() {
        return angle;
    }

    public int getRotation() {
        return BinaryVisualizationXmlReader.parseRotation(this.angle);
    }

    @Override
    public String toString() {
        return "size " + size + ", " + layers.size() + " layers, angle: " + angle + " [" + getRotation() + "]";
    }
}
