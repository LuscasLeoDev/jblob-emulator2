package com.luscastudio.jblob.extractor.io;


import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.extractor.boot.IDebug;
import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.compress.Compressing;
import com.luscastudio.jblob.extractor.swf.SwfRectangle;
import com.luscastudio.jblob.extractor.swf.tags.*;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by Lucas on 30/10/2016.
 */

public class SwfIO {

    private IDebug debugLogger;
    private int frameCount;
    private double frameRate;
    private SwfRectangle rect;
    private BufferIO buffer;
    private byte version;
    private long size;

    private char compressType;

    private Map<Short, SwfTag> tags;
    private SymbolClassTag symbols;

    private Map<String, DefineBitsLossless2Tag> imagesByName;
    private Map<String, DefineBinaryDataTag> binariesByName;

    private boolean decompressed;

    private String frameName;

    public SwfIO(ByteBuffer buff) {

        this.imagesByName = BCollect.newLinkedMap();
        this.binariesByName = BCollect.newMap();

        this.buffer = new BufferIO(buff);
        this.tags = BCollect.newLinkedMap();
        this.decompressed = false;
    }

    public void init(){
        try {
            this.compressType = (char) this.buffer.get();

            buffer.get(); //W
            buffer.get(); //S

            this.version = buffer.get();
            this.size = buffer.getInt() & 32767;

            this.decompress();

            this.rect = new SwfRectangle(this);
            rect.readRectangle();

            this.frameRate = this.buffer.getShort() / 256;
            this.frameCount = this.buffer.getShort();


            this.debug("############################################\n" +
                    "Version: " + this.version + "\n" +
                    "Size: " + this.size + "\n" +
                    "Rectangle: " + this.rect.toString() + "\n" +
                    "Frame Rate: " + this.frameRate + "\n" +
                    "Frame Count: " + this.frameCount + "\n" +
                    "Frame Name: " + this.frameName + "\n" +
                    "############################################\n"
            );

            this.debug("Getting Tags");
            while (this.buffer.remaining() > 0) {
                SwfTag tag = this.readTag();
                if (tag == null)
                    continue;

                if (tag instanceof SymbolClassTag)
                    symbols = (SymbolClassTag) tag;
                else if (tag instanceof FrameLabelTag)
                    this.frameName = ((FrameLabelTag) tag).getFrameName();

                tags.put(tag.getId(), tag);
                this.debug("Got Tag: " + tag.toString());
            }


            this.debug("Flushing Tag Names");
            this.flushTagNames();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SwfTag readTag() {

        try {
            SwfTagHeader header = new SwfTagHeader(this);

            this.buffer.position(buffer.position() + header.getLength());

            SwfTag tag = SwfTagFactory.generateTag(header);

            return tag;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void decompress() {

        try {

            if (this.compressType == 'C') {
                this.buffer = new BufferIO(Compressing.ZLibDecompress(this.buffer.getByteBuffer()));
            } else if(this.compressType == 'Z'){
                this.buffer = new BufferIO(Compressing.LZMADecompress(this.buffer.getByteBuffer()));
            }

            this.decompressed = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void flushTagNames() {
        for (SwfTag tag : tags.values()) {
            if (symbols.hasName(tag.getId())) {
                tag.setName(symbols.getName(tag.getId()));
                if(tag instanceof DefineBinaryDataTag)
                    binariesByName.put(tag.getName(), (DefineBinaryDataTag)tag);
                else if(tag instanceof DefineBitsLossless2Tag)
                    imagesByName.put(tag.getName(), (DefineBitsLossless2Tag)tag);
            }
        }
    }

    public void saveFiles(File path) {
        try {

            for (SwfTag tag : tags.values()) {
                if (symbols.hasName(tag.getId())) {
                    if(!tag.isSavable())
                        continue;
                    tag.setName(symbols.getName(tag.getId()));

                    File subPath = new File(path.getAbsoluteFile() + "/" + tag.getFolderName());
                    if(!subPath.exists() && !subPath.mkdirs()){
                        JBlobAE.write("Could not make the dir " + subPath.getAbsolutePath());
                        continue;
                    }

                    String fileName = subPath.getAbsolutePath() + "/" + tag.getName() + tag.getSuffix();
                    this.debug("Saving File: " + fileName);
                    File file = new File(fileName);
                    tag.save(file);

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DefineBinaryDataTag getBinary(String name){
        if(this.binariesByName.containsKey(name))
            return this.binariesByName.get(name);
        return null;
    }

    public DefineBitsLossless2Tag getImage(String name){
        if(this.imagesByName.containsKey(name))
            return this.imagesByName.get(name);
        return null;
    }

    public BufferIO getBuffer() {
        return buffer;
    }

    public boolean isDecompressed() {
        return decompressed;
    }

    public Map<Short, SwfTag> getTags() {
        return tags;
    }

    public String getFrameName() {
        return frameName;
    }

    public SwfRectangle getRect() {
        return rect;
    }

    public byte getVersion() {
        return version;
    }

    public double getFrameRate() {
        return frameRate;
    }

    public int getFrameCount() {
        return frameCount;
    }

    public long getSize() {
        return size;
    }

    private void debug(String e){
        if(this.debugLogger == null)
            return;

        this.debugLogger.onDebug(e);
    }

    public void setDebugLogger(IDebug debugLogger) {
        this.debugLogger = debugLogger;
    }

    public void close() {
        for (SwfTag tag : this.tags.values()) {
            tag.dispose();
        }

        this.debugLogger = null;
        this.binariesByName.clear();
        this.imagesByName.clear();
        this.rect.dispose();
        this.rect = null;

        this.buffer.close();
        this.buffer = null;
    }
}