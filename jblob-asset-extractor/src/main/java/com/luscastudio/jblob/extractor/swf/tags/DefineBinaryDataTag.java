package com.luscastudio.jblob.extractor.swf.tags;

import com.luscastudio.jblob.extractor.boot.Configurations;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Lucas on 03/11/2016.
 */

public class DefineBinaryDataTag extends SwfTag {

    private String binaryContent;

    public DefineBinaryDataTag(SwfTagHeader header) {
        super(header);

        this.id = buffer.getShort();
        int sla = this.buffer.getUInt();
        StringBuilder builder = new StringBuilder();
        while(this.buffer.remaining() > 0)
            builder.append((char)this.buffer.get());
        this.binaryContent = builder.toString();
    }
    @Override
    public void save(File file) throws Exception {
        try(FileOutputStream outputStream = new FileOutputStream(file)){
            outputStream.write(binaryContent.getBytes());
        }
    }

    @Override
    public String getSuffix() {
        return Configurations.BINARY_DATA_FILE_FORMAT;
    }

    @Override
    public String getFolderName() {
        return Configurations.BINARY_DATA_FILE_PATH;
    }

    @Override
    public boolean isSavable() {
        return true;
    }

    public String getContent() {
        return this.binaryContent;
    }


}
