package com.luscastudio.jblob.extractor.swf.tags;

import com.sun.imageio.plugins.gif.GIFImageReader;
import com.sun.imageio.plugins.gif.GIFImageReaderSpi;
import com.sun.imageio.plugins.jpeg.JPEGImageReader;
import com.sun.imageio.plugins.jpeg.JPEGImageReaderSpi;
import com.sun.imageio.plugins.jpeg.JPEGImageWriter;
import com.sun.imageio.plugins.jpeg.JPEGImageWriterSpi;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import static com.luscastudio.jblob.extractor.boot.Configurations.JPEG3_DATA_FILE_PATH;

/**
 * Created by Lucas on 27/02/2017 at 20:03.
 */
public class DefineBitsJPEG3 extends SwfTag {
    private BufferedImage image;
    public DefineBitsJPEG3(SwfTagHeader header) {
        super(header);

        this.id = buffer.getUShort();

        int alphaDataOffset = buffer.getInt();
        //todo Continue JPEG3 asset
        byte[] imageData = new byte[alphaDataOffset];

        for (int i = 0; i < alphaDataOffset; i++) {
            imageData[i] = buffer.get();
        }

        ByteBuffer imageBuffer = ByteBuffer.wrap(imageData);
    }

    @Override
    public void save(File file) throws Exception {
        if(image != null)
            ImageIO.write(image, "png", file);
    }

    @Override
    public String getSuffix() {
        return ".png";
    }

    @Override
    public String getFolderName() {
        return JPEG3_DATA_FILE_PATH ;
    }

    @Override
    public void dispose() {
        super.dispose();
        this.image = null;
    }

    @Override
    public boolean isSavable() {
        return false;
    }
}
