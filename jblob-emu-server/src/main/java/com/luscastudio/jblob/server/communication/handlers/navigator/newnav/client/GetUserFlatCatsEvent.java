package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.client;

import com.luscastudio.jblob.api.utils.validator.Validator;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server.GetUserFlatCatsComposer;
import com.luscastudio.jblob.server.game.habbohotel.navigator.flatcats.NavigatorFlatCategory;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 10/12/2016.
 */

public class GetUserFlatCatsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Validator<NavigatorFlatCategory> flatCategoryValidator = navigatorFlatCategory -> JBlob.getGame().getPermissionManager().validateRight(session.getAvatar().getRank(), navigatorFlatCategory.getPermissionName());

        session.sendMessage(new GetUserFlatCatsComposer(JBlob.getGame().getNavigatorManager().getFlatCategoryMap(), flatCategoryValidator));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
