package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class OpenFlatConnectionComposer extends MessageComposer {
    public OpenFlatConnectionComposer() {

    }

    @Override
    public String id() {
        return "OpenConnectionMessageComposer";
    }
}
