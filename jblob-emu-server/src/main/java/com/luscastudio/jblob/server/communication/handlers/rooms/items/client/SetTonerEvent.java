package com.luscastudio.jblob.server.communication.handlers.rooms.items.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.TonerRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 20/02/2017 at 19:40.
 */
public class SetTonerEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int itemId = packet.getInt();
        int hue = packet.getInt();
        int saturation = packet.getInt();
        int lightness = packet.getInt();

        Room room  = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_use_furni"))
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);
        if(item == null || !(item instanceof TonerRoomItem))
            return;

        TonerRoomItem toner = (TonerRoomItem)item;
        toner.setTonerData(hue, saturation, lightness);
        if(toner.getExtradata().getEnabled() == 0)
            toner.toggle();
        else
            room.getRoomItemHandlerService().updateItem(toner, false);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
