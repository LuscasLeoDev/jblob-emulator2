//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 08/06/2017 at 23:16.
 */
public class RespectNotificationComposer extends MessageComposer {
    @Override
    public String id() {
        return "RespectNotificationMessageComposer";
    }

    public RespectNotificationComposer(int int1, int int2){
        message.putInt(int1);
        message.putInt(int2);
    }
}
