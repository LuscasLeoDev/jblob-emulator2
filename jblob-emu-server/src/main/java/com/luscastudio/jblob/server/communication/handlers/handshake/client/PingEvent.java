package com.luscastudio.jblob.server.communication.handlers.handshake.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 01/10/2016.
 */
public class PingEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
