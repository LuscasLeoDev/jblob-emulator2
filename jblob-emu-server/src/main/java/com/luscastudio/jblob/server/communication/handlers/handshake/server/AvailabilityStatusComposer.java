package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/10/2016.
 */
public class AvailabilityStatusComposer extends MessageComposer {
    public AvailabilityStatusComposer() {
        message.putBool(false);
        message.putBool(false);
        message.putBool(true);
    }

    @Override
    public String id() {
        return "AvailabilityStatusMessageComposer";
    }
}
