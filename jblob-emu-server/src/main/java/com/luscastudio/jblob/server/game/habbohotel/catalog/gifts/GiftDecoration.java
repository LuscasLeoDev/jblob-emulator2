//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.catalog.gifts;

/**
 * Created by Lucas on 25/06/2017 at 21:19.
 */
public class GiftDecoration {
    private int id;
    private int value;
    private String permissionName;

    public GiftDecoration(int id, int value, String permissionName) {
        this.id = id;
        this.value = value;
        this.permissionName = permissionName;
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public String getPermissionName() {
        return permissionName;
    }
}
