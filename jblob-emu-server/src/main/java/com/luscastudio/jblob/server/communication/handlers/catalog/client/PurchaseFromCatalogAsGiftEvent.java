//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.GiftWrappingErrorComposer;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.PurchaseOKComposer;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CreditBalanceComposer;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CurrencyNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.database.QueryHelper;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDeal;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogManager;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogNode;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.GiftData;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 27/06/2017 at 00:00.
 */
public class PurchaseFromCatalogAsGiftEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int pageId = packet.getInt();
        int dealId = packet.getInt();
        String extradata = packet.getString();

        String giftForwardName = packet.getString();
        String giftMessage = packet.getString();

        int giftSpriteId = packet.getInt(); //ColorSprite
        int wrapType = packet.getInt();
        int ribbon = packet.getInt();
        boolean showSender = packet.getBool();

        IAvatarDataCache userCacheByName;
        if(!giftForwardName.isEmpty())
            userCacheByName = JBlob.getGame().getUserCacheManager().getUserCacheByName(giftForwardName);
        else
            userCacheByName = session.getAvatar();

        if(userCacheByName == null) {
            session.sendMessage(new GiftWrappingErrorComposer());
            return;
        }

        int giftItemBaseId = JBlob.getGame().getCatalogManager().getGiftBaseId(giftSpriteId);

        FurniBase base = JBlob.getGame().getItemBaseManager().getItemBase(giftItemBaseId);

        if(base == null){
            session.sendMessage(new BubbleNotificationComposer("debug", "Oops! Gift base not found!"));
            return;
        }

        CatalogNode node = JBlob.getGame().getCatalogManager().getCatalogNode(pageId);

        if(node == null)
            return;

        CatalogDeal deal = node.getDeal(dealId);

        if(deal == null)
            return;

        if(!deal.isAllowsGift()) {
            session.sendMessage(new BubbleNotificationComposer("error", "Sorry, this deal can't be gifted!"));
            return;
        }

        if(!CatalogManager.isValidPurchase(deal, session.getAvatar(), 1, extradata))
            return;

        ArrayListMultimap<String, Integer> itemsTypesMap = ArrayListMultimap.create();
        if(!deal.tryGift(userCacheByName.getId(), extradata, itemsTypesMap)){
            session.sendMessage(new BubbleNotificationComposer("error", "Error while trying to gift the item!"));
            return;
        }

        session.getAvatar().getBalance().decrease(deal.getCreditsCost());
        if(deal.getCurrencyCost() != 0)
            session.getAvatar().getBalance().decrease(deal.getCurrencyId(), deal.getCurrencyCost());

        if (deal.getCreditsCost() > 0)
            session.sendQueueMessage(new CreditBalanceComposer(session.getAvatar().getBalance().getCreditValue()));

        if (deal.getCurrencyCost() != 0) {
            int userBalance = session.getAvatar().getBalance().get(deal.getCurrencyId());
            session.sendQueueMessage(new CurrencyNotificationComposer(deal.getCurrencyId(), userBalance, 0));
        }

        session.sendQueueMessage(new PurchaseOKComposer(deal));

        GiftData giftData = new GiftData(session.getAvatar().getId(), giftMessage, ribbon, wrapType, showSender);

        FurniProperties furniProperties = JBlob.getGame().getRoomItemFactory().createFurniProperties(base, userCacheByName.getId(), giftData.toJson(), false, 0);

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            String prepareStr = QueryHelper.makeInsert("gifts_contents", itemsTypesMap.values().size(), "gift_id", "type", "value");

            DBConnPrepare prepare = reactor.prepare(prepareStr, true);

            for (Map.Entry<String, Integer> entry : itemsTypesMap.entries()) {
                prepare.setInt(furniProperties.getId());
                prepare.setString(entry.getKey());
                prepare.setInt(entry.getValue());
            }

            prepare.runInsertKeys();

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

        if(userCacheByName instanceof HabboAvatar) {

            HabboAvatar targetAvatar = (HabboAvatar) userCacheByName;
            targetAvatar.getInventory().addFurni(furniProperties);
            targetAvatar.getSession().sendQueueMessage(new InventoryUpdateRequestComposer());
            targetAvatar.getSession().sendQueueMessage(new InventoryNotificationComposer().add(InventoryNotificationComposer.FURNI, furniProperties.getId()));
            targetAvatar.getSession().flush();
        }
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
