package com.luscastudio.jblob.server.communication.server;

import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 01/10/2016.
 */
public class MessageComposerBoxed {

    private PlayerSession session;
    private MessageComposer composer;

    public MessageComposerBoxed(PlayerSession session, MessageComposer composer) {
        this.session = session;
        this.composer = composer;
    }

    public MessageComposer getComposer() {
        return composer;
    }

    public PlayerSession getSession() {
        return session;
    }
}
