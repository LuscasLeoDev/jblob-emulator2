package com.luscastudio.jblob.server.game.habbohotel.inventory.badge;

/**
 * Created by Lucas on 10/11/2016.
 */

public class Badge {

    private int id;
    private String code;
    private int slotId;

    public Badge(int id, String code, int slotId){
        this.id = id;
        this.code = code;
        this.slotId = slotId;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }
}
