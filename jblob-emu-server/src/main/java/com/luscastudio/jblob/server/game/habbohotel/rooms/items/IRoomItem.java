package com.luscastudio.jblob.server.game.habbohotel.rooms.items;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.IRoomItemExtradata;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 09/10/2016.
 */
public interface IRoomItem {

    int getId();

    int getBaseId();

    int getOwnerId();

    IRoomItemExtradata getExtradata();

    IItemMessageParser getMessageParser();

    FurniBase getBase();

    FurniProperties getPorperties();

    RoomItemPosition getPosition();

    List<Point> getAffectedTiles();

    void setAffectedTiles(List<Point> tiles);

    void onUserWalkOn(IRoomAvatar avatar);

    void onUserWalkStand(IRoomAvatar avatar);

    void onUserWalkOff(IRoomAvatar avatar);

    boolean canWalk(IRoomAvatar avatar);

    boolean canWalk(IRoomAvatar avatar, Point from);

    boolean canContinuePath(IRoomAvatar avatar, Point from, boolean finalStep);

    boolean onMove(PlayerSession session, Point from);

    void onMoved(Point from);

    boolean onPlace(PlayerSession session);

    void onPlaced();

    boolean onRemove(PlayerSession session);

    boolean onRemove();

    void onRemoved();

    void onRoomLoad();

    void onPlayerTrigger(PlayerSession session, int requestId);

    void toggle();

    double getHeight();

    int getUseButtonCode();

    FurniProperties getProperties();

    WallItemCoordinate getWallCoordinate();

    void dispose();
}
