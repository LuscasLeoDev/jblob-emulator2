package com.luscastudio.jblob.server.game.players;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/10/2016.
 */
public interface IMessageComposerHandler {

    IMessageComposerHandler sendQueueMessage(MessageComposer message);

    void flush();

    void sendMessage(MessageComposer message);

}
