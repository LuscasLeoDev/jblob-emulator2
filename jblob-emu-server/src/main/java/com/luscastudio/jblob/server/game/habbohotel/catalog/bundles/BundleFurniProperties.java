package com.luscastudio.jblob.server.game.habbohotel.catalog.bundles;

import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;

/**
 * Created by Lucas on 16/02/2017 at 17:15.
 */
public class BundleFurniProperties {
    private int id;
    private FurniBase base;
    private String extradata;
    private boolean rare;

    private int x;
    private int y;
    private double z;
    private int rotation;

    private String wallCoordinate;

    public BundleFurniProperties(int id, FurniBase base, String extradata, boolean rare, int x, int y, double z, int rotation, String wallCoordinate) {
        this.id = id;
        this.base = base;
        this.extradata = extradata;
        this.rare = rare;

        this.x = x;
        this.y = y;
        this.z = z;
        this.rotation = rotation;
        this.wallCoordinate = wallCoordinate;
    }

    public int getId() {
        return id;
    }

    public String getExtradata() {
        return extradata;
    }

    public FurniBase getBase() {
        return base;
    }

    public boolean isRare() {
        return rare;
    }

    public double getZ() {
        return z;
    }

    public int getRotation() {
        return rotation;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getWallCoordinate() {
        return wallCoordinate;
    }
}
