package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 14/01/2017 at 21:16.
 */
public class VendingMachineRoomItem extends SimpleRoomItem {

    private RunProcess normalStateProcess;

    public VendingMachineRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
    }

    @Override
    public void onPlayerTrigger(PlayerSession session, int requestId) {

        RoomPlayerAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());

        if(avatar == null)
            return;


        if(this.position.getDistance(avatar.getSetPosition().getPoint()) > 1){
            avatar.moveTo(this.position.getSquareInFront(0));

            return;
        }
    }



}
