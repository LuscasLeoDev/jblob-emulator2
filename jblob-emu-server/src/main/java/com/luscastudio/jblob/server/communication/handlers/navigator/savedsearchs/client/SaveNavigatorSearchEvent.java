package com.luscastudio.jblob.server.communication.handlers.navigator.savedsearchs.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.navigator.savedsearchs.server.NavigatorSavedSearchesComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 12/02/2017 at 18:48.
 */
public class SaveNavigatorSearchEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        String searchCode = packet.getString();
        String filter = packet.getString();
        session.sendMessage(new NavigatorSavedSearchesComposer());
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
