package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.client;

import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 05/02/2017 at 19:10.
 */
public class WhisperEvent extends ChatEvent {

    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
