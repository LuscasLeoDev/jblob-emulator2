package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server.NavigatorFlatCatsComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 10/12/2016.
 */

public class GetNavigatorFlatsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new NavigatorFlatCatsComposer());
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
