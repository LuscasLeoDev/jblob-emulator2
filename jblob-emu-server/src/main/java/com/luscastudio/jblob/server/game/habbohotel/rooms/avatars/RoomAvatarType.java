package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars;

/**
 * Created by Lucas on 03/10/2016.
 */
public enum RoomAvatarType {

    PLAYER,
    BOT_PRIVATE,
    BOT_PUBLIC,
    PET

}
