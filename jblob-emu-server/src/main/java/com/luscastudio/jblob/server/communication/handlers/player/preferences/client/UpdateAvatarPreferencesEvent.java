package com.luscastudio.jblob.server.communication.handlers.player.preferences.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/12/2016.
 */

public class UpdateAvatarPreferencesEvent implements IMessageEventHandler {

    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        session.getAvatar().getPreferences().setInt(AvatarPreferences.FRIENDBAR_STATUS, packet.getInt());

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
