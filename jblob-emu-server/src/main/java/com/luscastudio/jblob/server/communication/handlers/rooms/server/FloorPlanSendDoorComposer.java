package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.api.utils.engine.IObjectPosition;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/02/2017 at 18:17.
 */
public class FloorPlanSendDoorComposer extends MessageComposer {
    @Override
    public String id() {
        return "FloorPlanSendDoorMessageComposer";
    }

    public FloorPlanSendDoorComposer(IObjectPosition doorPosition) {
        message.putInt(doorPosition.getX());
        message.putInt(doorPosition.getY());
        message.putInt(doorPosition.getRotation());
    }
}
