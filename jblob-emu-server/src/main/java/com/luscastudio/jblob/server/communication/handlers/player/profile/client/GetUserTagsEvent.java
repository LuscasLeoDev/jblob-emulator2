package com.luscastudio.jblob.server.communication.handlers.player.profile.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.profile.server.UserTagsComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 11/11/2016.
 */

public class GetUserTagsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int userId = packet.getInt();


        List<String> tags = BCollect.newList();

        tags.add("Chera");
        tags.add("Meo");
        tags.add("Cu");

        session.sendMessage(new UserTagsComposer(userId, tags));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
