package com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom;

/**
 * Created by Lucas on 04/02/2017 at 17:32.
 */
public class PetCustomPart {

    private int partType;
    private int partId;
    private int colorId;

    public PetCustomPart(int partType, int partId, int colorId){
        this.partType = partType;
        this.partId = partId;
        this.colorId = colorId;
    }

    public int getPartType() {
        return partType;
    }

    public int getColorId() {
        return colorId;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }
}
