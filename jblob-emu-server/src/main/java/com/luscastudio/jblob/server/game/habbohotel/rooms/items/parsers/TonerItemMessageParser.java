package com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.TonerRoomItemExtradata;

/**
 * Created by Lucas on 20/02/2017 at 19:50.
 */
public class TonerItemMessageParser implements IItemMessageParser {

    private TonerRoomItemExtradata extradata;

    public TonerItemMessageParser(TonerRoomItemExtradata tonerRoomItemExtradata) {
        this.extradata = tonerRoomItemExtradata;
    }

    @Override
    public void parse(PacketWriting message) {
        message.putInt(4); //int count
        message.putInt(this.extradata.getEnabled());
        message.putInt(this.extradata.getHue());
        message.putInt(this.extradata.getSaturation());
        message.putInt(this.extradata.getLightness());
    }

    @Override
    public int getCode() {
        return 5; //IntArrayParser
    }
}
