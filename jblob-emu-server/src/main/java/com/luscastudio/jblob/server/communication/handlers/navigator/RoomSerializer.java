package com.luscastudio.jblob.server.communication.handlers.navigator;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomType;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.statusses.RoomAvatarStatus;

/**
 * Created by Lucas on 03/10/2016.
 */

public class RoomSerializer {

    public static void serializeRoomData1(PacketWriting message, RoomProperties room) {

        message.putInt(room.getId()); //Id
        message.putString(room.getName()); //Name
        message.putInt(room.getOwnerId()); //Owner Id
        message.putString(room.getOwnerName()); //Owner name
        message.putInt(room.getLockType()); //Access level

        message.putInt(room.getUsersNow()); //Users now
        message.putInt(room.getMaxUsers()); //Max avatar

        message.putString(room.getDescription());

        message.putInt(room.getTradeType()); //Trade level
        message.putInt(room.getScore()); //Score

        message.putInt(room.getId()); //Rank

        message.putInt(room.getCategoryId()); //Category

        message.putInt(room.getTags().size()); //Tag count

        for (String tag : room.getTags()) {
            message.putString(tag);
        }

        int total = 0;

        //if(room.customImage)
        //total += 1;

        //if(room.hasGroup())
        //total += 2;

        //if(room.hasPromotion())
        //total += 4;

        if (room.getType() == RoomType.PRIVATE) {
            total += 56;
        }

        // if(room.allowPets)
        //total += 16;

        //if(something...)
        //total += 32;


        message.putInt(total); //Room Type -> X

        /*
            #(X & 1) > 0
                Has custom thumbnail image

            #(X & 2) > 0
                Has Group

            #(X & 4) > 0
                Has promotion

            #(X & 8) > 0
                Is private

            #(X & 16) > 0
                Allows pet

            #(X & 32) > 0
                ????
         */

    }

    public static void serializeRoomAvatar(IRoomAvatar avatar, PacketWriting message) {
        message.putInt(avatar.getId());
        message.putString(avatar.getName());
        message.putString(avatar.getMotto());
        message.putString(avatar.getFigureString());
        message.putInt(avatar.getVirtualId());

        message.putInt(avatar.getPosition().getX());
        message.putInt(avatar.getPosition().getY());
        message.putDouble(avatar.getPosition().getZ());

        message.putInt(avatar.getPosition().getRotation()); // Direction
        message.putInt(avatar.getAvatarType()); // Room User Type

        avatar.composeAvatar(message);

        /*switch (avatar.getAvatarType()) {

            //Player
            case 1:
                message.putString(avatar.getGender());
                message.putInt(0); // Group id
                message.putInt(0); // Group something
                message.putString(""); // Group name

                message.putString(""); // Some Strange thing, maybe group badge

                message.putInt(avatar.getScore()); //Avatar Score
                message.putBool(false); //Maybe Builders ?? o.o

                break;

            //Pet 1
            case 2:
                message.putInt(avatar.getPetData().getRaceId());
                message.putInt(avatar.getPetData().getOwnerId());
                message.putString(avatar.getPetData().getOwnerUsername());
                message.putInt(avatar.getPetData().getRarityLevel());
                message.putBool(false); // has saddle
                message.putBool(false); // is someone ridding
                message.putBool(false);
                message.putBool(false);
                message.putBool(false);
                message.putBool(false);
                message.putInt(0);
                //message.putInt(0);
                message.putString("");
                break;

            //Public Bot
            case 3:
                break;

            //Private Bot 2
            case 4:
                message.putString(avatar.getGender());
                message.putInt(avatar.getBotData().getOwnerId());
                message.putString(avatar.getName());
                message.putInt(avatar.getBotData().getActions().size());
                for (int actionId : avatar.getBotData().getActions())
                    message.putInt(actionId);

                break;
        }*/
    }

    public static void serializeRoomAvatarsStatus(IRoomAvatar avatar, PacketWriting message) {

        message.putInt(avatar.getVirtualId());
        message.putInt(avatar.getPosition().getX());
        message.putInt(avatar.getPosition().getY());
        message.putString(NumberHelper.doubleToString(avatar.getPosition().getZ() + avatar.getAditionalHeight()));

        message.putInt(avatar.getPosition().getRotation());
        message.putInt(avatar.getPosition().getHeadRotation());

        StringBuilder str = new StringBuilder("/");

        for (RoomAvatarStatus status : avatar.getStatusses().getStatuses().values()) {

            str.append(status.getName());

            if (!status.getValue().isEmpty()) {
                str.append(" ").append(status.getValue());
            }

            str.append("/");
        }

        str.append("/");

        message.putString(str.toString());


    }


    public static void serializeRoomChatSettings(PacketWriting message, RoomProperties roomData) {

        //region _SafeStr_2734

        message.putInt(roomData.getChatMode());
        message.putInt(roomData.getChatSize());
        message.putInt(roomData.getChatSpeed());
        message.putInt(roomData.getChatDistance());
        message.putInt(roomData.getChatExtraFlood());

        //endregion
    }

    public static void serializeRoomPermissions(PacketWriting message, RoomProperties roomData) {
        //region _SafeStr_3079

        message.putInt(roomData.getWhoCanMute()); //WhoCanMute
        message.putInt(roomData.getWhoCanKick()); //WhoCanKick
        message.putInt(roomData.getWhoCanBan()); //WhoCanBan

        //endregion
    }
}
