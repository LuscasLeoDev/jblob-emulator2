//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.rooms.forwarding;

import com.luscastudio.jblob.api.utils.engine.IObject2DPosition;
import com.luscastudio.jblob.api.utils.validator.Validator;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomEnterErrorFuture;

/**
 * Created by Lucas on 08/05/2017 at 11:30.
 */
public class ForwardProperties {
    private int roomId;
    private IObject2DPosition position;
    private HabboAvatar avatar;

    private Validator<Boolean> callback;
    private Validator<RoomEnterErrorFuture> callbackError;

    public ForwardProperties(int roomId, IObject2DPosition position, HabboAvatar avatar) {
        this.roomId = roomId;
        this.position = position;
        this.avatar = avatar;
    }

    public int getRoomId() {
        return roomId;
    }

    public IObject2DPosition getPosition() {
        return position;
    }

    public HabboAvatar getAvatar() {
        return avatar;
    }

    public ForwardProperties then(Validator<Boolean> callback){
        this.callback = callback;
        return this;
    }

    public ForwardProperties onError(Validator<RoomEnterErrorFuture> callback){
        this.callbackError = callback;
        return this;
    }

    public void call(Boolean b){
        if(this.callback != null)
            this.callback.validate(b);
    }

    public void callError(RoomEnterErrorFuture roomEnterErrorFuture) {
        if(this.callbackError != null)
            this.callbackError.validate(roomEnterErrorFuture);
    }
}
