package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 30/09/2016.
 */
public class SecretKeyComposer extends MessageComposer {

    public SecretKeyComposer(String key) {
        this.message.putString(key);
    }

    @Override
    public String id() {
        return "SecretKeyMessageComposer";
    }
}
