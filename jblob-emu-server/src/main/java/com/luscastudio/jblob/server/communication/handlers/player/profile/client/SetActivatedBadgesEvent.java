package com.luscastudio.jblob.server.communication.handlers.player.profile.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.BadgesComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.inventory.badge.Badge;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 11/11/2016.
 */

public class SetActivatedBadgesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        session.getAvatar().getInventory().clearSelectedBadges();


        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare;

            prepare = reactor.prepare("UPDATE avatars_badges SET slot_id = '0' WHERE avatar_id = ?");
            prepare.setInt(1, session.getAvatar().getId());
            prepare.run();

            int badgec = 5;

            Badge badge;

            while (badgec-- > 0) {

                int slotId = packet.getInt();
                String badgeCode = packet.getString();

                if (badgeCode.length() == 0) {
                    continue;
                }

                if (!session.getAvatar().getInventory().hasBadge(badgeCode) || slotId < 1 || slotId > 5) {
                    continue;
                }

                badge = session.getAvatar().getInventory().getBadge(badgeCode);

                badge.setSlotId(slotId);

                prepare = reactor.prepare("UPDATE avatars_badges SET slot_id = ? WHERE badge = ? AND avatar_id = ?");
                prepare.setInt(1, slotId);
                prepare.setString(2, badgeCode);
                prepare.setInt(3, session.getAvatar().getId());
                prepare.run();

            }
        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        //todo: add achievement progress

        session.sendMessage(new BadgesComposer(session.getAvatar().getInventory().getEquippedBadges()));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
