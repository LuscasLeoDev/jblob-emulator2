//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems.DealItemFurni;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;

/**
 * Created by Lucas on 08/03/2017 at 23:59.
 */
public class PurchasedCatalogDealFurniEventArgs extends EventArgs {
    private DealItemFurni deal;
    private FurniProperties properties;

    public PurchasedCatalogDealFurniEventArgs(DealItemFurni deal, FurniProperties properties) {
        this.deal = deal;
        this.properties = properties;
    }

    public DealItemFurni getDeal() {
        return deal;
    }

    public FurniProperties getProperties() {
        return properties;
    }
}
