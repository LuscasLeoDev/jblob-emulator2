package com.luscastudio.jblob.server.database;

import java.sql.ResultSet;

/**
 * Created by Lucas on 24/10/2016.
 */

public interface IASyncQueryCallback {

    void prepare(DBConnPrepare prepare);

    ASyncQueryType getType();

    void success(int updateCount, ResultSet set);

    void error(Exception e);

    void end(DBConnPrepare prepare);
}
