package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 17/12/2016 at 16:16.
 */
public class RoomInfoUpdatedComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomInfoUpdatedMessageComposer";
    }

    public RoomInfoUpdatedComposer(int roomId) {
        message.putInt(roomId);
    }
}
