package com.luscastudio.jblob.server.game.habbohotel.chat.commands.list;

import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.AvatarEffectComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.chat.commands.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.chat.commands.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 13/01/2017 at 23:57.
 */
public class TestCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {


        session.sendMessage(new AvatarEffectComposer(1, 79, 1));

        return true;
    }
}