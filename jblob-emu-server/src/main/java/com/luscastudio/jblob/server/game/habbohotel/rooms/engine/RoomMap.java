package com.luscastudio.jblob.server.game.habbohotel.rooms.engine;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.collect.MinHeap;
import com.luscastudio.jblob.api.utils.engine.IPoint;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.api.utils.engine.Position3D;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.HeightMapComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.UpdateSquaresHeightMapComposer;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.engine.validator.IFloorValidator;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 03/10/2016.
 */

public class RoomMap {

    private int mapWidth;
    private int mapHeight;

    public Room room;
    private int[][] heightCoordinates;

    private FloorStatus[][] statusCoordinates;

    private IRoomItem[][] highestItemsCoordinates;
    private Map<Integer, IRoomItem>[][] roomItemsByCoordinate;
    private List<Point> updateSquareHeights;

    private Map<Integer, IRoomAvatar>[][] roomAvatarCoordinates;

    public synchronized void dispose() {
        for (int x = 0; x < mapWidth; x++)
            for (int y = 0; y < mapHeight; y++) {
                ///this.heightCoordinates[w][h] = 0;
                this.statusCoordinates[x][y] = null;
                this.highestItemsCoordinates[x][y] = null;
                if(this.roomItemsByCoordinate[x][y] != null) {
                    this.roomItemsByCoordinate[x][y].clear();
                    this.roomItemsByCoordinate[x][y] = null;
                }

                if(this.roomAvatarCoordinates[x][y] != null) {
                    this.roomAvatarCoordinates[x][y].clear();
                    this.roomAvatarCoordinates[x][y] = null;
                }
            }

        this.updateSquareHeights.clear();
        this.updateSquareHeights = null;
    }

    public RoomMap(Room room) {

        this.room = room;
        this.generateCoords();

        this.roomItemsByCoordinate = new Map[mapWidth][mapHeight];
        this.highestItemsCoordinates = new IRoomItem[mapWidth][mapHeight];
        this.roomAvatarCoordinates = new Map[mapWidth][mapHeight];

        this.updateSquareHeights = BCollect.newList();

        this.room.getRoomProcess().enqueue(new RunProcess(this::onCycle, 1, 800));
    }

    //region Primary Functions

    private void generateCoords() {


        String[] coords = this.room.getProperties().getModel().getModelString().split("\r");

        int width = this.mapWidth = coords[0].length();
        int height = this.mapHeight = coords.length;

        this.heightCoordinates = new int[width][height];
        this.statusCoordinates = new FloorStatus[width][height];

        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {

                int floorHeight = 0;
                FloorStatus status = FloorStatus.BLOCKED;
                char floorCode = coords[y].charAt(x);

                switch (floorCode) {
                    default:
                    case 'x':
                        break;

                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        floorHeight = Integer.parseInt(String.valueOf(floorCode));
                        status = FloorStatus.FREE;
                        break;


                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                    case 'g':
                    case 'h':
                    case 'i':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'm':
                    case 'n':
                    case 'o':
                    case 'p':
                    case 'q':
                    case 'r':
                    case 's':
                    case 't':

                        floorHeight = NumberHelper.hexToInt((floorCode));
                        status = FloorStatus.FREE;
                        break;
                }
                this.heightCoordinates[x][y] = floorHeight;
                this.statusCoordinates[x][y] = status;
            }

    }

    public int getMapWidth() {
        return mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    //endregion

    //region Getting data

    public boolean isValidCoord(int x, int y) {
        return x >= 0 && y >= 0 && x < mapWidth && y < mapHeight;
    }

    public boolean isValidCoord(Point p) {
        return isValidCoord(p.getX(), p.getY());
    }

    public boolean isValidSquare(int x, int y) {
        return isValidCoord(x, y) && getSquareStatus(x, y) != FloorStatus.BLOCKED;
    }

    public boolean isValidSquare(Point p) {
        return isValidSquare(p.getX(), p.getY());
    }

    public double getSquareHeight(int x, int y) {
        if (!isValidCoord(x, y))
            return 0;

        return this.heightCoordinates[x][y];
    }

    public FloorStatus getSquareStatus(int x, int y) {
        if (!isValidCoord(x, y))
            return FloorStatus.BLOCKED;

        return this.statusCoordinates[x][y];
    }

    //endregion

    //region Methods with avatar

    private Map<Integer, IRoomAvatar> getOrMakeAvatarCoord(int x, int y){
        if(this.roomAvatarCoordinates[x][y] == null)
            return this.roomAvatarCoordinates[x][y] = BCollect.newConcurrentMap();

        return this.roomAvatarCoordinates[x][y];
    }

    public void addAvatarToMap(IRoomAvatar avatar, Point coordinate){
        this.getOrMakeAvatarCoord(coordinate.getX(), coordinate.getY()).put(avatar.getVirtualId(), avatar);
    }

    public void removeAvatarFromMap(IRoomAvatar avatar, Point coordinate){
        this.getOrMakeAvatarCoord(coordinate.getX(), coordinate.getY()).remove(avatar.getVirtualId());
    }

    public void updateAvatarCoordinate(IRoomAvatar avatar, Point oldValue, Point newValue){
        this.removeAvatarFromMap(avatar, oldValue);
        this.addAvatarToMap(avatar, newValue);
    }

    public Map<Integer, IRoomAvatar> getRoomAvatars(Point p){
        return this.getOrMakeAvatarCoord(p.getX(), p.getY());
    }

    //endregion

    //region Methods with User Path

    public List<Point> getPath(Point from, Point to, IFloorValidator validator, int limit) {

        try {
            MinHeap<PathFindItem> openList = new MinHeap<>(256);
            PathFindItem[][] pathMap = new PathFindItem[mapWidth][mapHeight];

            //Add the starting square to the open list
            openList.add(new PathFindItem(from));

            int i = 1;
            //While the open list has option...
            while (openList.size() > 0) {

                PathFindItem current = openList.remove();
                current.setClosed(true);

                for (Point p : current.getSides()) {
                    PathFindItem node;
                    if (!isValidCoord(p) || !validator.validatePathStep(current.getPoint(), p, p.equals(to)))
                        continue;

                    if (pathMap[p.getX()][p.getY()] == null) {
                        node = pathMap[p.getX()][p.getY()] = new PathFindItem(p, current, to);
                    } else {
                        node = pathMap[p.getX()][p.getY()];
                    }

                    if (!node.isClosed()) {
                        if (current.getF() < node.getF()) {
                            node.setF(current.getF());
                            node.setFather(current);
                        }

                        if (!node.isOpen() || node.count() > limit) {
                            if (node.getPoint().equals(to) || node.count() > limit) {
                                node.setFather(current);

                                openList.dispose();

                                openList = null;

                                pathMap = null;

                                return parseReversePath(node);
                            }

                            node.setOpen(true);
                            openList.add(node);
                        }
                    }
                }
            }
        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }
        return null;

    }

    private List<Point> parseReversePath(PathFindItem item) {
        List<Point> path = BCollect.newList();
        PathFindItem Iitem = item;
        while (Iitem != null) {
            path.add(Iitem.getPoint());
            Iitem = Iitem.getFather();
        }

        item.dispose();

        return path;
    }

    //endregion

    //region Methods with items

    public Map<Integer, IRoomItem> getItemListByCoordinate(int x, int y) {
        if (!isValidCoord(x, y))
            return null;

        if (roomItemsByCoordinate[x][y] == null)
            return roomItemsByCoordinate[x][y] = BCollect.newMap();

        return roomItemsByCoordinate[x][y];
    }

    public void removeItemFromMap(IRoomItem item) {

        for (Point p : item.getAffectedTiles()) {
            this.highestItemsCoordinates[p.getX()][p.getY()] = null;
            getItemListByCoordinate(p.getX(), p.getY()).remove(item.getId());
        }
    }

    public boolean addItemToMap(IRoomItem item) {
        for (Point p : item.getAffectedTiles()) {
            Map<Integer, IRoomItem> itemsByCoord = getItemListByCoordinate(p.getX(), p.getY());
            if (itemsByCoord != null) {
                this.highestItemsCoordinates[p.getX()][p.getY()] = null;
                itemsByCoord.put(item.getId(), item);
            } else
                return false;
        }

        return true;
    }

    public Collection<IRoomItem> getRoomItemsByCoordinate(int x, int y) {
        if(!isValidCoord(x, y))
            return BCollect.newList();

        return getItemListByCoordinate(x, y).values();
    }

    public double getSquareDynamicHeight(int x, int y, IRoomItem avoid) {
        double tZ = getSquareHeight(x, y);
        double lastItemZ = -1;
        for (IRoomItem item : getRoomItemsByCoordinate(x, y)) {
            if (item == avoid)
                continue;

            if (item.getPosition().getZ() + item.getHeight() > lastItemZ) {
                lastItemZ = item.getPosition().getZ() + item.getHeight();
                tZ = item.getPosition().getZ() + item.getHeight();
            }
        }

        return tZ;
    }

    public double getSquareDynamicHeightToFloorMap(int x, int y) {
        double Z = getSquareHeight(x, y);
        double lastItemZ = -1;
        IRoomItem item;
        if ((item = getHighestRoomItemBySq(x, y)) != null) {
            if (!item.getBase().isCanStackOn())
                return -1;

            if (item.getPosition().getZ() > lastItemZ) {
                lastItemZ = item.getPosition().getZ();
                Z = item.getHeight() + item.getPosition().getZ();
            }
        }

        return Z;
    }

    public double getSquareDynamicHeight(int x, int y) {
        double tZ = getSquareHeight(x, y);
        double lastItemZ = -1;
        for (IRoomItem item : getRoomItemsByCoordinate(x, y)) {

            if (item.getPosition().getZ() > lastItemZ) {
                lastItemZ = item.getPosition().getZ();
                tZ = item.getPosition().getZ() + item.getHeight();
            }
        }

        return tZ;
    }

    public double getSquareHeightForItem(IRoomItem item) {
        double Z = 0.0;
        for (Point p : item.getAffectedTiles()) {
            double hei = getSquareDynamicHeight(p.getX(), p.getY(), item);
            if (Z < hei)
                Z = hei;
        }

        return Z;
    }

    public IRoomItem getHighestRoomItemBySq(int x, int y) {
        if (!isValidCoord(x, y))
            return null;

        if (this.highestItemsCoordinates[x][y] != null)
            return this.highestItemsCoordinates[x][y];


        double z = -1;
        IRoomItem fItem = null;
        for (IRoomItem item : getRoomItemsByCoordinate(x, y)) {
            if (item.getPosition().getZ() + item.getHeight() > z) {
                fItem = item;
                z = item.getPosition().getZ() + item.getHeight();
            }
        }

        return this.highestItemsCoordinates[x][y] = fItem;
    }

    public List<Point> getAffectedTilesForItem(IRoomItem item, int x, int y, int rot) {
        return getAffectedTilesForItem(item.getBase().getWidth(), item.getBase().getLength(), x, y, rot);
    }

    public List<Point> getAffectedTilesForItem(int width, int length, int x, int y, int rotation) {

        List<Point> tiles = BCollect.newList();

        switch (rotation) {
            case 0:
            case 4:

                for (int a = 0; a < width; a++) {
                    for (int b = 0; b < length; b++) {
                        tiles.add(new Point(a + x, b + y));
                    }
                }

                break;


            case 2:
            case 6:
                for (int a = 0; a < length; a++) {
                    for (int b = 0; b < width; b++) {
                        tiles.add(new Point(a + x, b + y));
                    }
                }
                break;
        }

        return tiles;
    }

    //endregion

    //region Room Square heights
    public void updateSqHeight(List<Point> ps) {
        for (Point p : ps)
            updateSqHeight(p);
    }

    private void updateSqHeight(Point p) {
        if (!this.updateSquareHeights.contains(p))
            this.updateSquareHeights.add(p);
    }

    //endregion

    public void onCycle() {

        this.updateSquareHeights();
    }

    private void updateSquareHeights() {

        try {
            if (this.updateSquareHeights.size() == 0)
                return;
            List<Position3D> vectors = BCollect.newList();
            for (Point p : this.updateSquareHeights) {
                //vectors.add(new Position3D(p.getX(), p.getY(), getSquareDynamicHeight(p.getX(), p.getY())));
                vectors.add(new Position3D(p.getX(), p.getY(), getSquareDynamicHeightToFloorMap(p.getX(), p.getY())));
            }

            List<Position3D> vectorBuffer = BCollect.newList();

            Iterator<Position3D> iterator = vectors.iterator();

            while (iterator.hasNext()) {
                vectorBuffer.add(iterator.next());

                if (vectorBuffer.size() >= Byte.MAX_VALUE) {
                    this.room.sendMessage(new UpdateSquaresHeightMapComposer(vectorBuffer));
                    vectorBuffer.clear();
                }

            }
            if (vectorBuffer.size() > 0) {
                this.room.sendMessage(new UpdateSquaresHeightMapComposer(vectorBuffer));
                vectorBuffer.clear();
            }

            vectors.clear();
            this.room.flush();

        } catch (Exception ignored){
            BLogger.error(ignored, this.getClass());
        }

        this.updateSquareHeights.clear();
    }

    public void clearMaps() {
        this.updateSquareHeights.clear();

        for(int x = 0; x < mapWidth; x++)
            for(int y = 0; y < mapHeight; y++){
                if(this.roomItemsByCoordinate[x][y] != null)
                    this.roomItemsByCoordinate[x][y].clear();

                this.highestItemsCoordinates[x][y] = null;
        }

        this.room.sendMessage(new HeightMapComposer(this));
    }

    public List<Point> getWalkableSquares(Point startPoint, int radius) {
        List<Point> points = BCollect.newList();

        for(int x = -radius; x <= radius; x++)
            for(int y = -radius; y <= radius; y++){

            Point p = new Point(x + startPoint.getX(), y + startPoint.getY());

            if(!isValidSquare(p) || p.equals(startPoint))
                continue;

            points.add(p);
        }

        return points;
    }

    public double getSquareDynamicHeight(IPoint point) {
        return getSquareDynamicHeight(point.getX(), point.getY());
    }
}
