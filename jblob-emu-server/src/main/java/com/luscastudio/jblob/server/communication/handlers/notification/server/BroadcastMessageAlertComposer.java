package com.luscastudio.jblob.server.communication.handlers.notification.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 01/10/2016.
 */
public class BroadcastMessageAlertComposer extends MessageComposer {
    public BroadcastMessageAlertComposer(String text) {
        message.putString(text);
    }

    @Override
    public String id() {
        return "BroadcastMessageAlertMessageComposer";
    }
}
