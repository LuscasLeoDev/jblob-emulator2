package com.luscastudio.jblob.server.game.habbohotel.navigator.categories;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.RoomUnloadEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.navigator.INavigatorItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Lucas on 03/10/2016.
 */

public abstract class NavigatorCategory implements INavigatorItem {

    private int id;
    private String identifier;
    private String name;
    private int parentId;
    private int orderNum;

    private boolean collapse;
    private int expandStatus;
    private int showType;

    private EventDelegate unloadDelegate;

    private String extradata;

    public NavigatorCategory(int id, String identifier, String name, int parentId, int orderNum, boolean collapse, int expandStatus, int showType, String extradata) {

        this.id = id;
        this.identifier = identifier;
        this.name = name;
        this.parentId = parentId;
        this.orderNum = orderNum;

        this.collapse = collapse;
        this.expandStatus = expandStatus;
        this.showType = showType;

        this.extradata = extradata;

        this.unloadDelegate = JBlob.getGame().getRoomManager().getEventHandler().on("unload", this::roomUnloaded);
    }

    private void roomUnloaded(EventDelegate delegate, EventArgs args) {

        this.onRoomUnload(((RoomUnloadEventArgs) args).getProperties());
        //delegate.cancel();
    }

    public abstract void init(DBConnReactor reactor) throws SQLException;

    protected abstract void onRoomUnload(RoomProperties sender);

    public abstract List<RoomProperties> getRoomsData(PlayerSession session, String search);

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getOrderId() {
        return orderNum;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public int getParentId() {
        return parentId;
    }

    public int getExpandStatus() {
        return expandStatus;
    }

    public int getShowType() {
        return showType;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public String getExtradata() {
        return extradata;
    }

    public boolean isCollapsed() {
        return collapse;
    }

    public void dispose(){
        this.unloadDelegate.cancel();
    }
}
