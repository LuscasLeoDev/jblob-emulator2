package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 20/01/2017 at 02:09.
 */
public class TestComposer extends MessageComposer {
    @Override
    public String id() {
        return "TestMessageComposer";
    }

    public TestComposer(){
        message.putInt(1);
        message.putInt(1);
        message.putBool(true);
    }
}
