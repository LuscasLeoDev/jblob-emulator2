//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.catalog.gifts.handlers;

import com.google.common.collect.Multimap;
import com.luscastudio.jblob.api.utils.engine.IObjectPosition;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.TriggerClientEventComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.AddFloorItemComposer;
import com.luscastudio.jblob.server.database.dao.rooms.items.RoomItemHandlerDao;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 29/06/2017 at 21:39.
 */
public class SingleFurniGiftHandler implements IGiftHandler {
    @Override
    public boolean validate(Multimap<String, Integer> itemsTypesMap) {
        return itemsTypesMap.size() == 1 && itemsTypesMap.get("furni") != null;
    }

    @Override
    public boolean handle(PlayerSession session, Multimap<String, Integer> itemsTypesMap, Room room, IObjectPosition giftPosition) {
        FurniProperties properties = JBlob.getGame().getRoomItemFactory().getFurniProperty(itemsTypesMap.values().iterator().next());

        if (properties == null)
            return false;

        IRoomItem item = JBlob.getGame().getRoomItemFactory().createRoomItemFromProperties(properties, room, giftPosition.getX(), giftPosition.getY(), giftPosition.getZ(), giftPosition.getRotation(), "");


        if(room.getRoomItemHandlerService().addItemToRoom(item)) {

            JBlob.getGame().getRoomItemFactory().showFurniProperties(item.getId());

            RoomItemHandlerDao.addItemToRoom(room.getProperties().getId(), item.getId(), item.getPosition().getX(), item.getPosition().getY(), item.getPosition().getZ(), item.getPosition().getRotation());

            IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCache(properties.getOwnerId());

            room.sendMessage(new AddFloorItemComposer(item, cache != null ? cache.getUsername() : String.format("Unknown #%d", properties.getOwnerId())));
        } else {
            item.dispose();
            session.getAvatar().getInventory().addFurni(properties);
            session.sendQueueMessage(new InventoryUpdateRequestComposer());
            session.sendQueueMessage(new InventoryNotificationComposer().add(InventoryNotificationComposer.FURNI, properties.getId()));
            session.sendQueueMessage(new TriggerClientEventComposer(String.format("inventory/open/furni/%d", properties.getId())));
            session.flush();
        }


        return true;
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
