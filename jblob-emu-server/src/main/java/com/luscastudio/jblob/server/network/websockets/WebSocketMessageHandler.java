//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.network.websockets;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;

/**
 * Created by Lucas on 12/03/2017 at 19:06.
 */
public class WebSocketMessageHandler extends SimpleChannelInboundHandler<WebSocketFrame> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame msg) throws Exception {
        this.handleWebSocketMessage(ctx, msg);
    }

    private void handleHttpMessage(ChannelHandlerContext ctx, FullHttpMessage msg) {
        WebMessage message = new WebMessage(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        String responseString = "Your ass is mine :)";
        message.headers().set("Content-Type", "text/txt");
        HttpHeaders.setContentLength(message, responseString.length());
        ctx.channel().write(message);
        ctx.channel().write(Unpooled.wrappedBuffer(responseString.getBytes()));
        ctx.channel().writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT).addListener(ChannelFutureListener.CLOSE);

    }

    private void handleWebSocketMessage(ChannelHandlerContext ctx, WebSocketFrame msg) {
        System.out.print(msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}
