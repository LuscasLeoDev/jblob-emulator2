package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 12/12/2016.
 */

public class ActionComposer extends MessageComposer {
    @Override
    public String id() {
        return "ActionMessageComposer";
    }

    public ActionComposer(int virtualId, int actionId){
        message.putInt(virtualId).putInt(actionId);
    }
}
