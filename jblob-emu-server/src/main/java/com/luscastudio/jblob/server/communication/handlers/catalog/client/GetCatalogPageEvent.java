package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CatalogPageComposer;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogNode;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 28/10/2016.
 */

public class GetCatalogPageEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int cataId = packet.getInt();
        int someshit = packet.getInt();
        String mode = packet.getString();

        CatalogNode node = JBlob.getGame().getCatalogManager().getCatalogNode(cataId);
        if(node == null)
            return;
        session.sendMessage(new CatalogPageComposer(node, mode));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
