package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 17/12/2016 at 16:14.
 */
public class RoomSettingsSavedComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomSettingsSavedMessageComposer";
    }

    public RoomSettingsSavedComposer(int roomId) {
        message.putInt(roomId);
    }
}
