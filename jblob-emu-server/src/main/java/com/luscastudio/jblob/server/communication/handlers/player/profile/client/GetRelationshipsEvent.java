package com.luscastudio.jblob.server.communication.handlers.player.profile.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.profile.server.RelationshipsComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 11/11/2016.
 */

public class GetRelationshipsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int userId = packet.getInt();

        //HabboAvatarDataCache cache = JBlob.getGame().getUserCacheManager().generateHabboAvatarDataCache(userId);

        session.sendMessage(new BubbleNotificationComposer("debug", "Users relationship is on development"));
        session.sendMessage(new RelationshipsComposer(userId));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
