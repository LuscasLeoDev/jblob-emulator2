package com.luscastudio.jblob.server.game.habbohotel.inventory;

/**
 * Created by Lucas on 16/10/2016.
 */
public enum InventoryItemType {
    FURNI,
    PET,
    BOT,
    RENTAL,
    BADGE
}
