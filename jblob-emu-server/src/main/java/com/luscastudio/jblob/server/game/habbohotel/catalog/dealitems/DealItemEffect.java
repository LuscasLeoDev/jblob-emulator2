package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.google.common.collect.Multimap;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDealItem;

import java.util.List;

/**
 * Created by Lucas on 08/12/2016.
 */

public class DealItemEffect extends CatalogDealItem {

    private int effectId;
    public DealItemEffect (int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);

        if(NumberHelper.isInteger(extradata))
            this.effectId = Integer.parseInt(extradata);
        else
            this.effectId = 0;
    }

    @Override
    public boolean generateValue(int ownerId, int userId, String pageExtradata, List<Object> outValues, int amount, DBConnReactor reactor, Multimap<String, Integer> generatedItems) {
        return true;
    }

    @Override
    public void handleValues(HabboAvatar avatar, List<Object> values) {

    }

    @Override
    public String getGiftTypeIdentification() {
        return null;
    }

    @Override
    public boolean isGiftable() {
        return false;
    }

    @Override
    public void parsePageComposer(PacketWriting message){
        message.putInt(this.effectId);
        message.putString(this.getExtradata());
        message.putInt(this.getAmount());

        message.putBool(this.isLimited());
        if (this.isLimited()) {
            message.putInt(this.getLimitedAmount());
            message.putInt(this.getLimitedReaming());
        }
    }
}
