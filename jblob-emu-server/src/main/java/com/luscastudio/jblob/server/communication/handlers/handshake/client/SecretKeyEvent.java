package com.luscastudio.jblob.server.communication.handlers.handshake.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.SecretKeyComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import com.luscastudio.jblob.server.protocols.ClientResponseInEncryptionDecoder;

/**
 * Created by Lucas on 30/09/2016.
 */
public class SecretKeyEvent implements IMessageEventHandler {

    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        String key = packet.getString();

        String plaintextKey = session.getBuild().getRsa().decrypt(key).replace(Character.toString((char) 0), "");

        String p = session.getBuild().getRsa().sign(session.getDiffieHellman().getPublicKey().toString());

        session.sendMessage(new SecretKeyComposer(p));

        session.getDiffieHellman().generateSharedKey(plaintextKey);
        byte[] sharedKey = session.getDiffieHellman().getSharedKey().toByteArray();

        session.getChannel().pipeline().addBefore("messageDecoder", "encryptionDecoder", new ClientResponseInEncryptionDecoder(sharedKey));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
