//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events;

/**
 * Created by Lucas on 06/03/2017 at 20:45.
 */
public interface IEventDelegateFuture {
    void fire(EventDelegate delegate, EventArgs eventArgs);
}
