package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class FurnitureAliasesComposer extends MessageComposer {
    public FurnitureAliasesComposer() {
        message.putInt(0);//Aliases count
    }

    @Override
    public String id() {
        return "FurnitureAliasesMessageComposer";
    }
}
