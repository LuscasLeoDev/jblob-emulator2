package com.luscastudio.jblob.server.game.permissions;

/**
 * Created by Lucas on 12/12/2016.
 */

public class PermissionManagerPermissions {

    public static final String P_MOD_OWN_ANY_ROOM = "mod_own_any_room";

    public static final String ROOM_APPLY_DECORATION = "room_apply_decoration";
    public static final String ROOM_COMMAND_UNLOAD = "room_command_unload";
    public static final String ROOM_OWNER_RIGHT = "room_owner_right";
    public static final String ROOM_COMMAND_PICKALL = "room_command_pickall";
}
