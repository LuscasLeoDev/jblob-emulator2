//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.catalog.gifts;

/**
 * Created by Lucas on 25/06/2017 at 20:59.
 */
public class GiftWrapping {
    private int id;
    private int spriteId;
    private int itemId;
    private String permissionName;

    public GiftWrapping(int id, int spriteId, int itemId, String permissionName) {
        this.id = id;
        this.spriteId = spriteId;
        this.itemId = itemId;
        this.permissionName = permissionName;
    }

    public int getId() {
        return id;
    }

    public int getSpriteId() {
        return spriteId;
    }

    public int getItemId() {
        return itemId;
    }

    public String getPermissionName() {
        return permissionName;
    }
}
