package com.luscastudio.jblob.server.game.habbohotel.navigator;

/**
 * Created by Lucas on 16/10/2016.
 */
public interface INavigatorItem {

    int getId();

    int getOrderId();

    String getName();

    String getIdentifier();

    int getParentId();
}
