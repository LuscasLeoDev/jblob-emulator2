//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list.boot;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 18/06/2017 at 15:27.
 */
public class SessionDisconnectedEventArgs extends EventArgs {

    private PlayerSession session;

    public SessionDisconnectedEventArgs(PlayerSession session) {
        this.session = session;
    }

    public PlayerSession getSession() {
        return session;
    }
}
