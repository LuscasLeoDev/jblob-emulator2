package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.api.utils.engine.Position3D;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.List;

/**
 * Created by Lucas on 11/10/2016.
 */

public class UpdateSquaresHeightMapComposer extends MessageComposer {
    public UpdateSquaresHeightMapComposer(Position3D p) {
        message.putByte(1);
        putTile(p.getX(), p.getY(), p.getZ());
    }

    public UpdateSquaresHeightMapComposer(List<Position3D> tiles) {
        message.putByte(tiles.size());
        for (Position3D p : tiles) {
            putTile(p.getX(), p.getY(), p.getZ());
        }
    }

    @Override
    public String id() {
        return "UpdateFurniStackHeightMapMessageComposer";
    }

    private void putTile(int x, int y, double z) {
        message.putByte(x);
        message.putByte(y);
        message.putShort((short) (Math.min(z, 127) * 256));
    }
}
