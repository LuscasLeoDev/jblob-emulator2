//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomRightsListComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 11/06/2017 at 22:06.
 */
public class GetRoomRightsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int roomId = packet.getInt();

        RoomProperties properties = JBlob.getGame().getRoomManager().getRoomData(roomId);
        if(properties == null)
            return;

        List<IAvatarDataCache> dataCaches = BCollect.newList();

        properties.getAvatarRights().forEach((id, rank) -> {
            IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCache(id);
            if(cache != null)
                dataCaches.add(cache);
        });

        session.sendMessage(new RoomRightsListComposer(roomId, dataCaches));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
