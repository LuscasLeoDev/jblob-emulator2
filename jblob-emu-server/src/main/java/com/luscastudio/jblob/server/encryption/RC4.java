package com.luscastudio.jblob.server.encryption;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class RC4 {
    private static final int POOL_SIZE = 256;
    private int i = 0;
    private int j = 0;
    private int[] table = new int[256];

    public RC4() {
    }

    public RC4(byte[] key) {
        this.init(key);
    }

    public void init(byte[] key) {
        this.i = 0;
        this.j = 0;

        for (this.i = 0; this.i < 256; ++this.i) {
            this.table[this.i] = (byte) this.i;
        }

        for (this.i = 0; this.i < 256; ++this.i) {
            this.j = this.j + this.table[this.i] + key[this.i % key.length] & 255;
            this.swap(this.i, this.j);
        }

        this.i = 0;
        this.j = 0;
    }

    public void swap(int a, int b) {
        int k = this.table[a];
        this.table[a] = this.table[b];
        this.table[b] = k;
    }

    public byte next() {
        this.i = ++this.i & 255;
        this.j = this.j + this.table[this.i] & 255;
        this.swap(this.i, this.j);
        return (byte) this.table[this.table[this.i] + this.table[this.j] & 255];
    }

    public ByteBuf decipher(ByteBuf bytes) {
        ByteBuf result = Unpooled.buffer();

        while (bytes.isReadable()) {
            result.writeByte((byte) (bytes.readByte() ^ this.next()));
        }

        return result;
    }
}

