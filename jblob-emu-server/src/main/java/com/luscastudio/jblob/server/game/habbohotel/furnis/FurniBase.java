package com.luscastudio.jblob.server.game.habbohotel.furnis;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Lucas on 09/10/2016.
 */

public class FurniBase {

    private int id;
    private int spriteId;
    private String className;
    private String type;

    private int width;
    private double height;
    private int length;

    private boolean canStandOn;
    private boolean canSitOn;
    private boolean canLayOn;
    private boolean canStackOn;

    private boolean canRecycle;
    private boolean canTrade;
    private boolean canInventoryStack;

    private String interactionType;

    private String custom;

    private List<Double> heights;

    private int interactionCount;
    private int inventoryCode;

    public FurniBase(ResultSet set) throws SQLException {
        this.id = set.getInt("id");
        this.spriteId = set.getInt("sprite_id");
        this.className = set.getString("class_name");
        this.type = set.getString("type").toLowerCase();

        this.width = set.getInt("width");
        this.length = set.getInt("length");
        this.height = set.getDouble("height");

        this.canStandOn = set.getInt("can_stand_on") == 1;
        this.canSitOn = set.getInt("can_sit_on") == 1;
        this.canLayOn = set.getInt("can_lay_on") == 1;
        this.canStackOn = set.getInt("can_stack_on") == 1;

        this.canRecycle = set.getInt("can_recycle") == 1;
        this.canTrade = set.getInt("can_trade") == 1;
        this.canInventoryStack = set.getInt("can_inventory_stack") == 1;

        this.interactionCount = set.getInt("interaction_count");
        this.interactionType = set.getString("interaction_type").toLowerCase();
        this.custom = set.getString("custom");

        this.inventoryCode = set.getInt("inventory_code");

        this.heights = BCollect.newList();
        String hData = set.getString("height_list");
        if(hData.contains(",")) {
            for (String e : hData.split(",")) {
                if (!NumberHelper.isDouble(e)) continue;

                heights.add(NumberHelper.parseDouble(e));
            }
        }
    }


    public int getId() {
        return id;
    }

    public double getHeight() {
        return height;
    }

    public int getSpriteId() {
        return spriteId;
    }

    public int getInteractionCount() {
        return interactionCount;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public List<Double> getHeights() {
        return heights;
    }

    public String getClassName() {
        return className;
    }

    public String getCustom() {
        return custom;
    }

    public String getType() {
        return type;
    }

    public String getInteractionType() {
        return interactionType;
    }

    public boolean isCanLayOn() {
        return canLayOn;
    }

    public boolean isCanSitOn() {
        return canSitOn;
    }

    public boolean isCanStackOn() {
        return canStackOn;
    }

    public boolean isCanStandOn() {
        return canStandOn;
    }

    public int getInventoryCode() {
        return inventoryCode;
    }

    public boolean isCanInventoryStack() {
        return canInventoryStack;
    }

    public boolean isCanRecycle() {
        return canRecycle;
    }

    public boolean isCanTrade() {
        return canTrade;
    }
}
