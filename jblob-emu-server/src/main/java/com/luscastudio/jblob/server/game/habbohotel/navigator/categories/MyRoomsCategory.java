package com.luscastudio.jblob.server.game.habbohotel.navigator.categories;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Lucas on 18/12/2016 at 14:45.
 */
public class MyRoomsCategory extends NavigatorCategory {


    public MyRoomsCategory(int id, String identifier, String name, int parentId, int orderNum, boolean collapse, int expandStatus, int showType, String extradata) {
        super(id, identifier, name, parentId, orderNum, collapse, expandStatus, showType, extradata);
    }

    @Override
    public void init(DBConnReactor reactor) throws SQLException {

    }

    @Override
    protected void onRoomUnload(RoomProperties sender) {

    }

    @Override
    public List<RoomProperties> getRoomsData(PlayerSession session, String search) {
        List<RoomProperties> properties = BCollect.newList();
        if(search.isEmpty()) {
            try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
                DBConnPrepare prepare = reactor.prepare("SELECT id FROM players_rooms WHERE owner_id = ?");
                prepare.setInt(1, session.getAvatar().getId());

                ResultSet set = prepare.runQuery();

                while (set.next()) {
                    properties.add(JBlob.getGame().getRoomManager().getRoomData(set.getInt("id")));
                }

            } catch (Exception e) {
                BLogger.error(e, this.getClass());
            }
        }

        return properties;
    }

}
