package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

/**
 * Created by Lucas on 09/01/2017 at 15:49.
 */
public class WallItemUpdateCompser extends MessageComposer {
    @Override
    public String id() {
        return "ItemUpdateMessageComposer";
    }

    public WallItemUpdateCompser(IRoomItem item) {
        ItemSerializer.serializeWallItem(message, item);
    }
}
