package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/10/2016.
 */

public class CanCreateRoomComposer extends MessageComposer {
    public CanCreateRoomComposer(boolean can, int limit) {
        this.message.putInt(!can ? 1 : 0);
        this.message.putInt(limit);
    }

    @Override
    public String id() {
        return "CanCreateRoomMessageComposer";
    }
}
