package com.luscastudio.jblob.server.game.habbohotel.friendship;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 02/10/2016.
 */
public class AvatarFriendshipItem {

    private int userId;
    private short relationType;

    public AvatarFriendshipItem(int friendId) {
        this.userId = friendId;
    }

    public PlayerSession getSession() {
        return JBlob.getGame().getSessionManager().getSession(userId);
    }

    public IAvatarDataCache getUserData() {
        return JBlob.getGame().getUserCacheManager().getUserCache(userId);
    }

    public int getUserId() {
        return userId;
    }

    public short getRelationType() {
        return relationType;
    }

    public void setRelationType(short relationType) {
        this.relationType = relationType;
    }
}
