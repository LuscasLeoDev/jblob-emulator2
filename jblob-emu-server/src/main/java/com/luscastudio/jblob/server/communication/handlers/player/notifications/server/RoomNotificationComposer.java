package com.luscastudio.jblob.server.communication.handlers.player.notifications.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 24/12/2016 at 13:35.
 */
public class RoomNotificationComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomNotificationMessageComposer";
    }

    public RoomNotificationComposer(String code){
        message.putString(code);

        message.putInt(0);
    }

    public RoomNotificationComposer(String image, String title, String text){
        message.putString(image);

        message.putInt(2);

        message.putString("title");
        message.putString(title);

        message.putString("message");
        message.putString(text);


    }

    public RoomNotificationComposer(String image, String... obj){
        message.putString(image);

        message.putInt(obj.length / 2);

        for(int i = 0; i < obj.length / 2; i++) {
            message.putString(obj[i * 2]);
            message.putString(obj[(i * 2) + 1]);
        }
    }

}
