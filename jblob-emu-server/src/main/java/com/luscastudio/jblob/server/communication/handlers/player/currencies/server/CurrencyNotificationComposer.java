package com.luscastudio.jblob.server.communication.handlers.player.currencies.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 07/12/2016.
 */

public class CurrencyNotificationComposer extends MessageComposer {
    @Override
    public String id() {
        return "HabboActivityPointNotificationMessageComposer";
    }

    public CurrencyNotificationComposer(int type, int value, int newValue){
        message.putInt(value);
        message.putInt(newValue);
        message.putInt(type);
    }
}
