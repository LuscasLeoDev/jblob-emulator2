package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.RemoveInventoryFurniComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.AddWallItemComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/01/2017 at 19:40.
 */

public class AddStickyNoteEvent implements IMessageEventHandler{
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        Room room = session.getAvatar().getCurrentRoom();

        if(room == null)
            return;
        //todo: Set the permissio name static
        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_add_sticky_note"))
            return;

        int itemId = packet.getInt();
        String wallData = packet.getString();

        FurniProperties furniProp = session.getAvatar().getInventory().getFurni(itemId);
        if(furniProp == null)
            return;

        IRoomItem roomItem = JBlob.getGame().getRoomItemFactory().createRoomItemFromProperties(furniProp, room, 0, 0, 0, 0, wallData);

        if (!room.getRoomItemHandlerService().addItemToRoom(roomItem, wallData)) {
            session.sendMessage(new BubbleNotificationComposer("furni_placing_error"));
            return;
        }

        roomItem.getWallCoordinate().set(wallData);

        session.sendQueueMessage(new RemoveInventoryFurniComposer(itemId));
        room.sendMessage(new AddWallItemComposer(roomItem, session.getAvatar().getUsername()));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
