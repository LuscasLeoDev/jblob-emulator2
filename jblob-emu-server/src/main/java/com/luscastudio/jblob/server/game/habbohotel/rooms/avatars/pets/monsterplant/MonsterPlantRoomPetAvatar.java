package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.monsterplant;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.ChatComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server.ChangePetLevelComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server.UpdatePetStatusComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomUsersComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.SlideObjectBundleComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.UserUpdateComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.IPetAppliableItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.RoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.PetItemRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 09/02/2017 at 16:31.
 */
public class MonsterPlantRoomPetAvatar extends RoomPetAvatar implements IPetAppliableItem{

    private String ownerName;

    private MonsterPlantData monsterPlantData;

    private RunProcess cycleProcess;

    private int lastGrownLevel;

    private boolean disposing;

    public MonsterPlantRoomPetAvatar(Room room, int virtualId, PetData petData) {
        super(room, virtualId, petData);
        this.ownerName = null;
        this.disposing = false;
    }

    @Override
    public String getMotto() {
        return "JBlob Monster Plant Avatar";
    }

    @Override
    public void composeAvatar(PacketWriting message) {
        message.putInt(this.getPetData().getPetBase().getPetType());
        message.putInt(this.getPetData().getOwnerId());
        message.putString(this.getOwnerName());
        message.putInt(this.getPetData().getRarityLevel()); //RarityLevel
        message.putBool(false); // has saddle
        message.putBool(false); // is someone ridding
        message.putBool(false); //can breed?
        message.putBool(false); //?
        message.putBool(this.monsterPlantData.isDead()); // Is Monster Plant dead
        message.putBool(false); //?
        message.putInt(this.lastGrownLevel);// Level
        message.putString("");
    }

    @Override
    public void composePetAvatarInformation(PacketWriting message) {
        message.putInt(this.getId()); //Id
        message.putString(this.getName()); //Name
        message.putInt(this.lastGrownLevel); //Level or Monster Plant grown Level
        message.putInt(this.lastGrownLevel); //Max Level

        message.putInt(this.lastGrownLevel); //Experience
        message.putInt(this.lastGrownLevel); //Max Experience

        //message.putInt(this.getPetData().getEnergy()); //Energy
        //message.putInt(this.getPetData().getMaxEnergy()); //Max Energy
        message.putInt(this.monsterPlantData.getTimeToGrownTimeStamp() - DateTimeUtils.getUnixTimestampInt()); //Monster Plant Life Time
        message.putInt(this.monsterPlantData.getWellBeingTotalTime()); //Monster Plant Total Life Time

        message.putInt(this.getPetData().getHappiness()); //Happiness
        message.putInt(this.getPetData().getMaxHappiness()); //Max Happiness

        message.putInt(this.getPetData().getRespects()); //Pet Respects
        message.putInt(this.getPetData().getOwnerId()); //Owner Id

        message.putInt((int) TimeUnit.SECONDS.toDays((long)(DateTimeUtils.getUnixTimestampInt() - this.getPetData().getCreateTimestamp()))); //Age in days

        message.putString(this.getOwnerName()); //Owner name

        message.putInt(this.getPetData().getRaceId()); // Pet Race

        message.putBool(true); //Has Saddle
        message.putBool(true); //Is Ridding

        /*message.putInt(this.getPetData().getAvaliableCommands().size());//Int Count
        for(int commandId : this.getPetData().getAvaliableCommands())
        {
            message.putInt(commandId);
        }*/

        message.putInt(0);//Horse jump data

        message.putInt(this.lastGrownLevel); //Monster Plant Grown level / Horse Anyone car ride on
        message.putBool(true); //Breedable
        message.putBool(false); //
        message.putBool(this.monsterPlantData.isDead()); //Monster Plant Is Dead?
        message.putInt(this.petData.getRarityLevel()); //Monster Plant RarityLevel
        message.putInt(this.monsterPlantData.getWellBeingTotalTime()); //Monster Plant Total Life Time
        message.putInt(Math.max(0, this.monsterPlantData.getWellBeingExpireTimestamp() - DateTimeUtils.getUnixTimestampInt())); //Monster Plant Life Time
        message.putInt(this.monsterPlantData.getTimeToGrownTimeStamp() - DateTimeUtils.getUnixTimestampInt()); //Monster Plant Time until grow up

        message.putBool(false); // Allow Breed

    }

    @Override
    public void onPlace(HabboAvatar placerAvatar) {

    }

    @Override
    public void onRemove(HabboAvatar removerAvatar) {

    }

    @Override
    public void onRespect(HabboAvatar avatar) {
        this.monsterPlantData.setWellBeingExpireTimestamp(this.monsterPlantData.getWellBeingTotalTime() + DateTimeUtils.getUnixTimestampInt());
        this.serializeStatus();
        this.getRoom().getRoomAvatarService().saveAvatar(this);
    }

    @Override
    public void onAvatarSay(HabboAvatar avatar, String message) {

    }

    @Override
    public void onEnter() {

        {
            try{
                this.monsterPlantData = new Gson().fromJson(this.petData.getExtradata(), new TypeToken<MonsterPlantData>(){}.getType());
            }catch (Exception e){
                this.monsterPlantData = generateMonsterPetData(this.getPetData().getRarityLevel());
                this.getRoom().getRoomAvatarService().saveAvatar(this);
            }

            if(this.monsterPlantData == null) {
                this.monsterPlantData = generateMonsterPetData(this.getPetData().getRarityLevel());
                this.getRoom().getRoomAvatarService().saveAvatar(this);
            }
        }

        this.lastGrownLevel = getGrownLevel();

        this.serializeStatus();

        this.getRoom().getRoomProcess().enqueue(this.cycleProcess = new RunProcess(this::onCycle, 1, 1000));
    }

    @Override
    public void onLeave() {
        this.cycleProcess.cancel();
    }

    private void onCycle() {
        if(this.disposing) {
            return;
        }
        this.cycleProcess.pause(500);
        int now = DateTimeUtils.getUnixTimestampInt();
        if(!this.monsterPlantData.isDead() && this.monsterPlantData.getWellBeingExpireTimestamp() <= now){
            this.monsterPlantData.setDead(true);
            this.serializeStatus();
            this.updateNeeded(true);
        } else if(!this.monsterPlantData.isDead()){

            //Grown Level Checking
            if(this.lastGrownLevel <= 7) {
                int newGrownLevel = getGrownLevel();
                if(newGrownLevel != this.lastGrownLevel)
                    this.getRoom().sendMessage(new ChangePetLevelComposer(this.getVirtualId(), this.getId(), newGrownLevel));
                if (newGrownLevel > this.lastGrownLevel) {
                    this.lastGrownLevel = newGrownLevel;
                    this.petData.setLevel(this.lastGrownLevel);
                    this.serializeStatus();
                    this.updateNeeded(true);
                    this.getRoom().getRoomAvatarService().saveAvatar(this);
                }
            }
        }

        this.updateNeeded(true);
    }

    private void serializeStatus(){

        this.getStatusses().remove("grw1");
        this.getStatusses().remove("grw2");
        this.getStatusses().remove("grw3");
        this.getStatusses().remove("grw4");
        this.getStatusses().remove("grw5");
        this.getStatusses().remove("grw6");
        this.getStatusses().remove("grw7");

        if(this.monsterPlantData.isDead()) {
            this.getStatusses().set("rip", "");
        } else if(this.lastGrownLevel < 7){
            this.getStatusses().set("grw" + this.lastGrownLevel, "");
        }
    }

    public String getOwnerName() {
        if(this.ownerName != null)
            return this.ownerName;

        IAvatarDataCache cache;
        return this.ownerName = (cache = JBlob.getGame().getUserCacheManager().getUserCache(this.petData.getOwnerId())) != null ? cache.getUsername() : "unknown_" + this.petData.getOwnerId();
    }

    @Override
    public void save(DBConnReactor reactor) {

        this.petData.setExtradata(new Gson().toJson(this.monsterPlantData));

        super.save(reactor);
    }

    @Override
    public synchronized void dispose() {
        this.disposing = true;
        synchronized (this.cycleProcess){
            if(this.cycleProcess != null)
                this.cycleProcess.cancel();
            this.cycleProcess = null;
        }

        synchronized (this.monsterPlantData) {
            this.monsterPlantData = null;
        }
        super.dispose();
    }

    private int getGrownLevel(){

        int now = DateTimeUtils.getUnixTimestampInt();
        int startedGrown = this.monsterPlantData.getStartedGrownTimeStamp();
        int endGrown = this.monsterPlantData.getTimeToGrownTimeStamp();

        return 1 + Math.max(0,(int)((double)(now - startedGrown) / this.monsterPlantData.getTotalTimeToGrown() * 6));
    }

    public static MonsterPlantData generateMonsterPetData(int rarityLevel){

            return new MonsterPlantData(24 * 3600, 11 * 3600);
    }

    @Override
    public void applyItem(PetItemRoomItem item) {
        String[] spl = item.getApplyData().split(":");
        if(spl.length < 3)
            return;

        String petType = spl[0];
        String applyType = spl[1];
        String value = spl[2];

        if(!petType.equals(String.valueOf(this.getPetData().getPetBase().getPetType())))
            return;

        switch(applyType) {
            case "fert": {
                if (!NumberHelper.isInteger(value))
                    break;

                int secsToGrown = Integer.parseInt(value);

                int grownLevel = this.lastGrownLevel;
                this.monsterPlantData.setStartedGrownTimeStamp(DateTimeUtils.getUnixTimestampInt());
                this.monsterPlantData.setTimeToGrownTimeStamp(DateTimeUtils.getUnixTimestampInt() + 15);
                this.monsterPlantData.setTotalTimeToGrown(15);
                this.getStatusses().set("gst", "spd", 4);
                this.onCycle();

                this.updateNeeded(true);
                this.getRoom().getRoomAvatarService().saveAvatar(this);

                this.getRoom().getRoomItemHandlerService().removeRoomItem(item);
                this.getRoom().sendMessage(new RemoveFloorItemComposer(item.getId(), -1, true, 500));
                JBlob.getGame().getRoomItemFactory().hideFurniProperties(item.getProperties().getId());
            }
                break;

            case "rev": {
                if (!NumberHelper.isInteger(value))
                    break;

                int percent = Math.min(Integer.parseInt(value), 100);

                int decValue = (this.monsterPlantData.getWellBeingTotalTime() * percent) / 100;

                this.monsterPlantData.setWellBeingExpireTimestamp(DateTimeUtils.getUnixTimestampInt() + decValue);
                this.monsterPlantData.setDead(false);
                this.getStatusses().remove("rip");
                this.getStatusses().set("gst", "rev", 2);
                this.serializeStatus();
                this.updateNeeded(true);
                this.getRoom().sendMessage(new UpdatePetStatusComposer(this.getVirtualId(), this.getId(), false, false, this.monsterPlantData.isDead(), false));
                this.getRoom().getRoomAvatarService().saveAvatar(this);

                this.getRoom().getRoomItemHandlerService().removeRoomItem(item);
                this.getRoom().sendMessage(new RemoveFloorItemComposer(item.getId(), -1, true, 500));
                JBlob.getGame().getRoomItemFactory().hideFurniProperties(item.getProperties().getId());

            }
                break;
        }
    }
}
