//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 08/04/2017 at 23:16.
 */
public class ChangePetLevelComposer extends MessageComposer {
    @Override
    public String id() {
        return "ChangePetLevelMessageComposer";
    }

    public ChangePetLevelComposer(int virtualId, int id, int toLevel){
        message.putInt(virtualId);
        message.putInt(id);
        message.putInt(toLevel);
    }
}
