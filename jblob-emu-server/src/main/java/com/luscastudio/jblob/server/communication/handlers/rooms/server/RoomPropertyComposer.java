package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class RoomPropertyComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomPropertyMessageComposer";
    }

    public RoomPropertyComposer(String name, String val) {
        message.putString(name);
        message.putString(val);
    }
}
