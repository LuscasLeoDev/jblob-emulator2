package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.SellablePetBreedsComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.game.habbohotel.pet.PetBase;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;

/**
 * Created by Lucas on 29/01/2017 at 15:53.
 */
public class GetSellablePetBreedsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        String productName = packet.getString();

        SellablePetBreedsComposer composer = new SellablePetBreedsComposer(productName);

        Collection<PetBase> bases = JBlob.getGame().getPetManager().getSellableBreeds(productName);

        if(bases == null) {
            session.sendMessage(new BubbleNotificationComposer("debug", "Sellable pet breeds not found for '"+productName+"'\nShowing 50 races unknowns"));

            int petId = Integer.parseInt(productName.replace("a0 pet", ""));

            int i = -1;
            while (i++ < 50)
                composer.addBreed(petId, i, i, true, true);

        } else {

            for(PetBase base : bases){
                composer.addBreed(base.getPetType(), base.getRaceId(), base.getRaceId(), true, true);
            }
        }

        session.sendQueueMessage(composer).flush();

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
