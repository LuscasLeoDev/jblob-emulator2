package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets;

import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.PetItemRoomItem;

/**
 * Created by Lucas on 19/02/2017 at 14:26.
 */
public interface IPetAppliableItem {
    void applyItem(PetItemRoomItem item);
}
