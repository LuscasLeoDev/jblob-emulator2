package com.luscastudio.jblob.server.game.habbohotel.furnis;

import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.IRoomItemExtradata;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;

/**
 * Created by Lucas on 16/10/2016.
 */

public class FurniProperties {

    private int id;
    private int baseId;
    private FurniBase base;
    private int groupId;
    private int ownerId;
    private IRoomItemExtradata extradata;
    private boolean rare;

    public FurniProperties(int id, FurniBase base, int ownerId, IRoomItemExtradata extradata, boolean rare, int groupId) {
        this.id = id;
        this.base = base;
        this.baseId = base.getId();
        this.ownerId = ownerId;
        this.extradata = extradata;
        this.groupId = groupId;
        this.rare = rare;
    }



    public int getId() {
        return id;
    }

    public int getBaseId() {
        return baseId;
    }

    public FurniBase getBase() {
        return base;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public IRoomItemExtradata getExtradata() {
        return extradata;
    }

    public IItemMessageParser getMessageParser() {
        return getExtradata().getMessageParser();
    }

    public boolean isRare() {
        return rare;
    }
}
