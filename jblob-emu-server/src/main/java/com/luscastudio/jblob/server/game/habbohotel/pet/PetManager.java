package com.luscastudio.jblob.server.game.habbohotel.pet;

import com.google.gson.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomData;
import com.luscastudio.jblob.server.game.habbohotel.pet.custom.PetCustomDataValues;
import com.luscastudio.jblob.server.game.habbohotel.rooms.AvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.IAvatarObjectPosition;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 28/01/2017 at 13:04.
 */
public class PetManager implements IFlushable {


    private Map<Integer, PetBase> petBaseMap;
    private Map<Integer, Map<Integer, PetBase>> petBasesByType;

    //Map<PetType, Map<
    private Map<Integer, PetCustomDataValues> mapCustomsData;

    private Map<String, List<PetBase>> sellablePetBreeds;


    public PetManager(){
        this.petBaseMap = BCollect.newConcurrentMap();
        this.petBasesByType = BCollect.newConcurrentMap();
        this.sellablePetBreeds = BCollect.newConcurrentMap();
        this.mapCustomsData = BCollect.newConcurrentMap();
    }

    @Override
    public void flush() throws Exception {
        this.petBaseMap.clear();
        this.petBasesByType.clear();
        this.sellablePetBreeds.clear();
        this.mapCustomsData.forEach((integer, petCustomDataValues) -> {
            petCustomDataValues.dispose();
        });

        this.mapCustomsData.clear();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM pets_bases");

            ResultSet set = prepare.runQuery();

            while(set.next())
                this.tryAddPet(set);


            prepare = reactor.prepare("SELECT * FROM catalog_pets_breeds");

            set = prepare.runQuery();

            while(set.next())
                this.tryAddSellablePetBreed(set);

            prepare = reactor.prepare("SELECT * FROM pets_custom_bases");

            set = prepare.runQuery();

            while(set.next()){

                int petType = set.getInt("pet_type");
                PetCustomDataValues dataValues;
                if(this.mapCustomsData.containsKey(petType))
                    dataValues = this.mapCustomsData.get(petType);
                else
                    this.mapCustomsData.put(petType, dataValues = new PetCustomDataValues());

                dataValues.parseSet(set);


            }

        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }

    }

    @Override
    public void performFlush() {

    }

    private void tryAddSellablePetBreed(ResultSet set) throws SQLException {
        List<PetBase> bases = BCollect.newList();

        for(String id : set.getString("bases_id").split(",")){
            if(!NumberHelper.isInteger(id))
                continue;

            PetBase base = petBaseMap.get(Integer.parseInt(id));
            if(base == null)
                continue;

            bases.add(base);
        }

        this.sellablePetBreeds.put(set.getString("product_name"), bases);
    }

    public Collection<PetBase> getSellableBreeds(String productName){
        return this.sellablePetBreeds.get(productName);
    }

    //Pet Base Handling

    private void tryAddPet(ResultSet set) throws SQLException {

        Map<Integer, Integer> maxEnergies = JSONUtils.toMap(set.getString("max_energy"), new TypeToken<Map<Integer, Integer>>(){}.getType());
        Map<Integer, Integer> maxHappiness = JSONUtils.toMap(set.getString("max_happiness"), new TypeToken<Map<Integer, Integer>>(){}.getType());
        Map<Integer, Integer> maxExperiences = JSONUtils.toMap(set.getString("max_experience"), new TypeToken<Map<Integer, Integer>>(){}.getType());

        //<CommandId, Level
        Map<Integer, Integer> commandData = JSONUtils.toMap(set.getString("commands_data"), new TypeToken<Map<Integer, Integer>>(){}.getType());

        this.addPet(new PetBase(set.getInt("id"), set.getInt("pet_type"), set.getInt("race_id"), set.getInt("rarity_level"), set.getInt("max_level"), maxExperiences ,maxEnergies, maxHappiness, commandData, set.getString("pet_behavior")), false);
    }

    public Map<Integer, PetBase> getBasesByType(int type){
        if(!this.petBasesByType.containsKey(type)) {
            Map<Integer, PetBase> bases = BCollect.newMap();
            this.petBasesByType.put(type, bases);
            return bases;
        }

        return this.petBasesByType.get(type);
    }

    public void addPet(PetBase base, boolean replace){
        if(this.petBaseMap.containsKey(base.getId()) && replace){
            this.petBaseMap.replace(base.getId(), base);
            this.getBasesByType(base.getPetType()).replace(base.getId(), base);
        } else if(!petBaseMap.containsKey(base.getId())){
            this.petBaseMap.put(base.getId(), base);
            this.getBasesByType(base.getPetType()).put(base.getId(), base);
        }
    }

    public PetBase getPetBase(int id){
        return this.petBaseMap.get(id);
    }

    //endregion

    //region Pet Data Handling

    public Map<PetData, IAvatarObjectPosition> getPetsDataForRoom(int roomId){
        Map<PetData, IAvatarObjectPosition> pets = BCollect.newMap();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM avatars_pets_data WHERE room_id = ?");
            prepare.setInt(1, roomId);

            ResultSet set = prepare.runQuery();

            while(set.next())
                pets.put(parsePetData(set), new AvatarObjectPosition(set.getInt("x"), set.getInt("y"), 0, set.getInt("rotation"), set.getInt("rotation")));

        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return pets;
    }

    public List<PetData> getPetsDataForOwner(int ownerId){
        List<PetData> pets = BCollect.newList();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM avatars_pets_data WHERE owner_id = ?");
            prepare.setInt(1, ownerId);

            ResultSet set = prepare.runQuery();

            while(set.next())
                pets.add(parsePetData(set));

        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return pets;
    }

    public PetData parsePetData(ResultSet set) throws SQLException {
        int baseId = set.getInt("base_id");
        PetBase base = getPetBase(baseId);
        if(base == null)
            base = getPetBase(0);

        return new PetData(base, set.getInt("id"), set.getString("name"), set.getInt("owner_id"), set.getInt("room_id"), set.getString("color"), new PetCustomData(set.getString("custom_data")), set.getInt("level"), set.getInt("rarity_level"), set.getInt("energy"), set.getInt("happiness"), set.getInt("experience"), set.getInt("respects"), set.getInt("created_timestamp"), set.getString("extradata"), set.getInt("race_id"));
    }

    public PetCustomDataValues getPetCustomDataValues(int petType) {
        return this.mapCustomsData.get(petType);
    }

    //endregion

    public PetData createPetData(int roomId, String name, PetBase base, int ownerId, String color, PetCustomData customData, int startLevel, int rarityLevel, int startEnergy, int startExperience, int startHappiness) {
        int petId;

        int created = DateTimeUtils.getUnixTimestampInt();

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reactor.prepare("INSERT INTO avatars_pets_data (name, base_id, owner_id, user_id, room_id, color, custom_data, level, rarity_level, energy, experience, happiness, created_timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", true);

            prepare.setString(1, name);
            prepare.setInt(2, base.getId());
            prepare.setInt(3, ownerId);
            prepare.setInt(4, ownerId);
            prepare.setInt(5, roomId);
            prepare.setString(6, color);
            prepare.setString(7, customData.toDbString());
            prepare.setInt(8, startLevel);
            prepare.setInt(9, rarityLevel);
            prepare.setInt(10, startEnergy);
            prepare.setInt(11, startExperience);
            prepare.setInt(12, startHappiness);
            prepare.setInt(13, created);

            petId = prepare.runInsert();

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
            return null;
        }

        return new PetData(base, petId, name, ownerId, roomId, color, customData, startLevel, rarityLevel, startEnergy, startHappiness, startExperience, 0, created, "", -1);
    }
}
