package com.luscastudio.jblob.server.game.habbohotel.furnis;

import com.luscastudio.jblob.server.game.habbohotel.inventory.IInventoryHandler;

import java.util.Collection;

/**
 * Created by Lucas on 29/01/2017 at 19:08.
 */
public class FurniInventoryHandler implements IInventoryHandler<FurniProperties> {

    @Override
    public void load() {

    }

    @Override
    public void clear() {

    }

    @Override
    public FurniProperties getItem(int itemId) {
        return null;
    }

    @Override
    public FurniProperties removeitem(int itemId) {
        return null;
    }

    @Override
    public Collection<FurniProperties> getItems() {
        return null;
    }
}
