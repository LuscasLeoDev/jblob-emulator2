package com.luscastudio.jblob.server.database;

import com.luscastudio.jblob.server.boot.JBlob;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 24/10/2016.
 */

public class DBConnectionAsyncQueriesHandler implements Runnable {

    private ScheduledFuture scheduled;
    private Deque<IQueryRunnable> queue;

    public DBConnectionAsyncQueriesHandler() {
        this.queue = new ConcurrentLinkedDeque<>();
        JBlob.getGame().getProcessManager();
    }

    public void enqueue(IQueryRunnable query) {
        this.queue.add(query);
    }

    public void enqueue(IMultiAsyncQuery iMultiAsyncQuery) {
        this.queue.add(iMultiAsyncQuery);
    }

    public void init() {
        this.scheduled = JBlob.getGame().getProcessManager().runLoop(this, 1, 400, TimeUnit.MILLISECONDS);
    }

    public void run() {

        while (queue.size() > 0) {

            IQueryRunnable query = queue.poll();

            if (query == null) //Whut?
                continue;

            query.run();

        }

    }
}
