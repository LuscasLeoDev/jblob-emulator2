package com.luscastudio.jblob.server.communication.server;

/**
 * Created by Lucas on 01/10/2016.
 */
public class PacketWritingValue {

    public boolean boolValue;
    public short shortValue;
    public int intValue;
    public String strValue;
    public byte byteValue;
    public double doubleValue;

    public PacketWriting.PacketWritingType type;

    public PacketWritingValue(String val) {
        this.strValue = val;
        this.type = PacketWriting.PacketWritingType.STRING;
    }

    public PacketWritingValue(int val) {
        this.intValue = val;
        this.type = PacketWriting.PacketWritingType.INT;
    }

    public PacketWritingValue(short val) {
        this.shortValue = val;
        this.type = PacketWriting.PacketWritingType.SHORT;
    }

    public PacketWritingValue(byte val) {
        this.byteValue = val;
        this.type = PacketWriting.PacketWritingType.BYTE;
    }

    public PacketWritingValue(boolean val) {
        this.boolValue = val;
        this.type = PacketWriting.PacketWritingType.BOOL;
    }

    public PacketWritingValue(double val) {
        this.doubleValue = val;
        this.type = PacketWriting.PacketWritingType.DOUBLE;
    }
}
