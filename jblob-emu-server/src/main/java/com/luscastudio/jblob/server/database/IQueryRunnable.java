package com.luscastudio.jblob.server.database;

/**
 * Created by Lucas on 11/11/2016.
 */
public interface IQueryRunnable {

    void run();
}
