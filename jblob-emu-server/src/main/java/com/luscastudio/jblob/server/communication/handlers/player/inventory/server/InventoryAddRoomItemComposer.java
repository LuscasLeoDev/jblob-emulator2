package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

/**
 * Created by Lucas on 17/10/2016.
 */

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;

public class InventoryAddRoomItemComposer extends MessageComposer {
    public InventoryAddRoomItemComposer(FurniProperties furniProperties) {
        ItemSerializer.serializeInventoryFurni(message, furniProperties);
    }

    @Override
    public String id() {
        return "FurniListAddMessageComposer";
    }
}
