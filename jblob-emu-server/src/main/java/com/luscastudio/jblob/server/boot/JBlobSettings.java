package com.luscastudio.jblob.server.boot;

/**
 * Created by Lucas on 09/12/2016.
 */

public class JBlobSettings {

    public static final int AVATAR_INVENTORY_FURNIS_SHOW_MAX_PER_TYPE = 1000;

    public static final int AP_DEFAULT_VOLUME_SYSTEM = 100;
    public static final int AP_DEFAULT_VOLUME_FURNI = 100;
    public static final int AP_DEFAULT_VOLUME_MUSIC = 100;
    public static final boolean AP_DEFAULT_OLD_CHAT_ENABLED = false;
    public static final boolean AP_DEFAULT_MESSENGER_INVINTE_ENABLED = false;
    public static final boolean AP_DEFAULT_DISABLE_CAMERA_FOLLOW = false;
    public static final int AP_DEFAULT_FRIENDBAR_STATUS = 3;
    public static final int AP_DEFAULT_LAST_CHAT_BUBBLE = 3;

    public static final int AVATAR_EXECUTOR_CYCLE_INTERVAL = 30000;
    public static final boolean COMMAND_MANAGER_HANDLER_RETURN_TRUE_IF_COMMAND_FOUND = true;

    public static final String UTIL_EMPTY_STRING = "";
    public static final String UTIL_CHAR_SPACE = " ";

    public static final int SERVER_PROCESS_THREAD_COUNT = 10;
    public static final java.lang.String ROOM_PROPERTIES_TAG_DELIMITER = "\0";
}
