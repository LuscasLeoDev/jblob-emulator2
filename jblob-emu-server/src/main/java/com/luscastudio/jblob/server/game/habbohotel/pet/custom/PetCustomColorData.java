package com.luscastudio.jblob.server.game.habbohotel.pet.custom;

/**
 * Created by Lucas on 08/02/2017 at 20:33.
 */
public class PetCustomColorData {

    private int colorId;
    private int rarityLevel;
    private String name;

    public PetCustomColorData(int colorId, int rarityLevel, String name){
        this.colorId = colorId;
        this.rarityLevel = rarityLevel;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getColorId() {
        return colorId;
    }

    public int getRarityLevel() {
        return rarityLevel;
    }
}
