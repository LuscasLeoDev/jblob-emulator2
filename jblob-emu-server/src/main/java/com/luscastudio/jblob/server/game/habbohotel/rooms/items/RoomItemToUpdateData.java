package com.luscastudio.jblob.server.game.habbohotel.rooms.items;

/**
 * Created by Lucas on 10/10/2016.
 */

public class RoomItemToUpdateData {

    private IRoomItem item;
    private boolean both;

    public RoomItemToUpdateData(IRoomItem item, boolean both) {
        this.item = item;
        this.both = both;
    }

    public IRoomItem getItem() {
        return item;
    }

    public boolean isBoth() {
        return both;
    }
}
