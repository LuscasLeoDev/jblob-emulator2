package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.triggers;

import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.WhisperComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.AvatarSaysSomethingEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.TriggerWiredBox;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by Lucas on 16/02/2017 at 01:11.
 */
public class TriggerAvatarSaysSomething extends TriggerWiredBox {

    private String keyword;
    private EventDelegate delegate;

    public TriggerAvatarSaysSomething(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);

        this.keyword = "";
        this.delegate = this.room.getEventHandler().on("avatar.says.something", this::onEvent);
    }

    private void onEvent(EventDelegate eventDelegate, EventArgs args) {
        AvatarSaysSomethingEventArgs eventArgs = (AvatarSaysSomethingEventArgs) args;

        String message = eventArgs.getMessage();

        if(!message.toLowerCase().equals(keyword))
            return;

        this.getExtradata().toggle();
        this.room.getRoomItemHandlerService().updateItem(this, false);
        if(!(eventArgs.getAvatar() instanceof RoomPlayerAvatar))
            return;
        RoomPlayerAvatar avatar = ((RoomPlayerAvatar) eventArgs.getAvatar());

        HabboAvatar habbo = avatar.getAvatar();
        int chatBubbleId = habbo.getPreferences().getInt("last.chat.bubble");


        if(!this.triggerConditionBoxes(avatar, null)) {
            return;
        }

        habbo.getSession().sendMessage(new WhisperComposer(avatar.getVirtualId(), chatBubbleId, 0, message));
        args.cancel();

        this.triggerEffectBoxes(avatar, null);
    }

    @Override
    public void saveWiredData(PlayerSession session, Collection<Integer> integerList, String stringData, Collection<Integer> selectedFurniList, int someInt) {
        this.keyword = stringData;
        this.getExtradata().save(new WiredSaveData(null, null, 0, keyword));
        this.room.getRoomItemHandlerService().saveItem(this);
        session.sendMessage(new HideWiredConfigComposer());
    }

    @Override
    public int getWiredCode() {
        return 0;
    }

    @Override
    public boolean requireAvatar() {
        return true;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return Collections.emptyList();
    }

    @Override
    public String getStringData() {
        return keyword;
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        try{

            WiredSaveData saveData = this.getExtradata().getSavedData();
            if(saveData != null){
                this.keyword = saveData.getStringData();
            }

        }catch (Exception ignored){

        }
    }

    @Override
    public void dispose() {
        this.delegate.cancel();
        this.delegate = null;
    }
}
