//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.chat.filter.parsers;

/**
 * Created by Lucas on 25/06/2017 at 16:34.
 */
public class RegexChatFilterParser implements IParser {
    @Override
    public String phase(String input, String value, String replacement) {
        return input.replaceAll(String.format("(?i)%s", value), replacement);
    }
}
