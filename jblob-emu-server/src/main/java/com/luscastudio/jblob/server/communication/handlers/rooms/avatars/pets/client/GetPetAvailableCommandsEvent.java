package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server.PetAvailableCommandsComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 03/02/2017 at 15:35.
 */
public class GetPetAvailableCommandsEvent implements IMessageEventHandler{
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int petId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().getPet(petId);

        if(petAvatar == null)
            return;

        session.sendMessage(new PetAvailableCommandsComposer(petAvatar.getId(), petAvatar.getPetData().getPetBase().getCommands(), petAvatar.getPetData().getAvaliableCommands()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
