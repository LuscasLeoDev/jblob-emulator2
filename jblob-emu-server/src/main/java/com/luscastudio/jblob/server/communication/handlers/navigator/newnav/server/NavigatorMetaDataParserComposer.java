package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.navigator.tabs.NavigatorTopLevel;

import java.util.Collection;

/**
 * Created by Lucas on 02/10/2016.
 */

public class NavigatorMetaDataParserComposer extends MessageComposer {

    @Override
    public String id() {
        return "NavigatorMetaDataParserMessageComposer";
    }

    public NavigatorMetaDataParserComposer(Collection<NavigatorTopLevel> topLevels) {

        message.putInt(topLevels.size());// TopLevelContext count

        //region TopLevelContext class

        for (NavigatorTopLevel tab : topLevels) {
            message.putString(tab.getName());//name

            message.putInt(1); //SavedSearch

            //region SavedSearch

            message.putInt(1); //id
            message.putString("search_code"); //search code
            message.putString("filter"); //filter
            message.putString("localization"); //localization [Whut??
            //endregion
        }


        //endregion
    }
}
