package com.luscastudio.jblob.server.game.players.cache;


import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;

/**
 * Created by Lucas on 02/10/2016.
 */
public interface IAvatarDataCache {

    int getId();

    String getUsername();

    String getFigureString();

    String getMotto();

    int getLastOnlineTimestamp();

    String getGender();

    boolean isInRoom();

    boolean isOnline();

    AvatarInventory getInventory();

    int getRank();
}
