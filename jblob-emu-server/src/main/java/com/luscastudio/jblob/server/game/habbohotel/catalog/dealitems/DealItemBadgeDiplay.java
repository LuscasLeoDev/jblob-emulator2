package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.google.common.collect.Multimap;
import com.luscastudio.jblob.server.database.DBConnReactor;

import java.util.List;

/**
 * Created by Lucas on 21/02/2017 at 18:25.
 */
public class DealItemBadgeDiplay extends DealItemFurni {
    public DealItemBadgeDiplay(int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);
    }

    @Override
    public boolean generateValue(int ownerId, int userId, String pageExtradata, List<Object> outValues, int amount, DBConnReactor reactor, Multimap<String, Integer> generatedItems) {
        return true;
    }

    @Override
    public String getGiftTypeIdentification() {
        return null;
    }

    @Override
    public boolean isGiftable() {
        return false;
    }
}