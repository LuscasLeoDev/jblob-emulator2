package com.luscastudio.jblob.server.communication.handlers.rooms.items.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.FloorItemUpdateComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.WallItemUpdateCompser;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/01/2017 at 15:44.
 */
public class MoveWallItemEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        int itemId = packet.getInt();
        String wallData = packet.getString();

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);

        //todo: Set this permission name as static
        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_move_furni")) {
            session.sendMessage(new FloorItemUpdateComposer(item));
            return;
        }

        if(item == null)
            return;

        item.getWallCoordinate().set(wallData);

        room.sendMessage(new WallItemUpdateCompser(item));

        room.getRoomItemHandlerService().saveItem(item);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
