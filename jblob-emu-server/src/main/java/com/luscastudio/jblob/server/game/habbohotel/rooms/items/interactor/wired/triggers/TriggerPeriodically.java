package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.triggers;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.TriggerWiredBox;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Lucas on 16/02/2017 at 12:47.
 */
public class TriggerPeriodically extends TriggerWiredBox {

    private RunProcess process;
    private List<Integer> integers;

    public TriggerPeriodically(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.startCycle();
        this.integers = BCollect.newList();
    }

    private int getDelay(){
        if(this.integers.size() == 0)
            return 0;
        return this.integers.get(0);
    }

    private void startCycle() {
        this.room.getRoomProcess().enqueue(this.process = new RunProcess(this::doCycle, 1, 500));
    }

    private void doCycle() {
        this.getExtradata().toggle();
        this.room.getRoomItemHandlerService().updateItem(this, false);
        if(this.triggerConditionBoxes(null, null))
            this.triggerEffectBoxes(null, null);

        this.process.pause(this.getDelay() * 500);
    }

    @Override
    public void saveWiredData(PlayerSession session, Collection<Integer> integerList, String stringData, Collection<Integer> selectedFurniList, int someInt) {
        this.integers.clear();
        this.integers.addAll(integerList);
        WiredSaveData saveData = new WiredSaveData(this.integers, null, 0, null);
        this.getExtradata().save(saveData);
        this.room.getRoomItemHandlerService().saveItem(this);
        session.sendMessage(new HideWiredConfigComposer());
    }

    @Override
    public int getWiredCode() {
        return 6;
    }

    @Override
    public boolean requireAvatar() {
        return false;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return Collections.emptyList();
    }

    @Override
    public String getStringData() {
        return "";
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return this.integers;
    }

    @Override
    protected void loadWiredData() {
        try{

            WiredSaveData data = this.getExtradata().getSavedData();
            if(data != null){
                this.integers = data.getIntegerList();
            }

        } catch(Exception ignored){

        }
    }

    @Override
    public void dispose() {
        if(this.process != null){
            this.process.cancel();
            this.process = null;
        }
    }
}
