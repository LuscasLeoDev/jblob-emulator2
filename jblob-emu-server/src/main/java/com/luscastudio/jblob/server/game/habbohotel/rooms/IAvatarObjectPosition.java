package com.luscastudio.jblob.server.game.habbohotel.rooms;

import com.luscastudio.jblob.api.utils.engine.IPosition3D;
import com.luscastudio.jblob.api.utils.engine.Point;

/**
 * Created by Lucas on 04/10/2016.
 */
public interface IAvatarObjectPosition extends IPosition3D {

    int getHeadRotation();

    void setHeadRotation(int value);

    void setDirection(int value, boolean bodyOnly);

    void setRotation(int value);

    void setPosition(int x, int y, double z, int direction);

    Point getSquareInFront();
    Point getSquareBehind();
    Point getSquareLeft();
    Point getSquareRight();

    int getRotation();

    Point getPoint();
}
