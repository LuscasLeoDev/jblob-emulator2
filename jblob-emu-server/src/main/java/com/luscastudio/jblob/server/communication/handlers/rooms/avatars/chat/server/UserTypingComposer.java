package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 29/12/2016 at 18:48.
 */
public class UserTypingComposer extends MessageComposer {
    @Override
    public String id() {
        return "UserTypingMessageComposer";
    }

    public UserTypingComposer(int virtualId, boolean typping){
        message.putInt(virtualId);
        message.putInt(typping ? 1 : 0);
    }
}
