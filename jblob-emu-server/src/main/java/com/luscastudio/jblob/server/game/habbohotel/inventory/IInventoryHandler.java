package com.luscastudio.jblob.server.game.habbohotel.inventory;

import java.util.Collection;

/**
 * Created by Lucas on 29/01/2017 at 18:07.
 */
public interface IInventoryHandler<T> {

    void load();
    void clear();

    T getItem(int itemId);
    T removeitem(int itemId);

    Collection<T> getItems();

}
