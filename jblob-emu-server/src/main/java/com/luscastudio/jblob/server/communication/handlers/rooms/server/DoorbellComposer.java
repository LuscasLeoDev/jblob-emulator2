package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 12/02/2017 at 20:25.
 */
public class DoorbellComposer extends MessageComposer {
    @Override
    public String id() {
        return "DoorbellMessageComposer";
    }

    public DoorbellComposer(String username){
        message.putString(username);
    }
}
