package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.AvatarEffectComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.events.list.AvatarTeleportToSquareEventArgs;
import com.luscastudio.jblob.server.events.list.RoomMapSquareStateChangeEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.EffectWiredBox;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.*;

/**
 * Created by Lucas on 15/02/2017 at 19:40.
 */
public class EffectTeleportAvatarTo extends EffectWiredBox {

    private int delay;
    private Map<Integer, IRoomItem> selectedRoomItems;
    private Map<Integer, IRoomAvatar> avatars;
    private RunProcess process;

    public EffectTeleportAvatarTo(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.selectedRoomItems = BCollect.newMap();
        this.avatars = BCollect.newConcurrentMap();
    }

    @Override
    public int getDelay() {
        return this.delay;
    }

    private int getPlusDelay(){
        return 1000;
    }

    private int getTotalTime(){
        return (this.delay * 500) + this.getPlusDelay();
    }

    @Override
    public void onWiredTriggered(IRoomAvatar avatar, IRoomItem triggeredItem) {
        this.run();
        if(this.avatars.containsKey(avatar.getVirtualId()))
            return;
        this.room.getRoomItemHandlerService().updateItem(this, false);
        this.getExtradata().toggle();

        this.avatars.put(avatar.getVirtualId(), avatar);
        this.room.sendMessage(new AvatarEffectComposer(avatar.getVirtualId(), 4, 0));

        avatar.getEventHandler().on("avatar.leave.room", (delegate, eventArgs) -> {
            this.avatars.remove(avatar.getVirtualId());
        });
    }

    private void run(){
        if(this.process == null){
            this.room.getRoomProcess().enqueue(this.process = new RunProcess(this::teleportThem, this.getTotalTime()));
        }
    }

    private void teleportThem(){

        if(!this.selectedRoomItems.isEmpty()) {
            for (IRoomAvatar avatar : this.avatars.values()) {
                IRoomItem randomItem = BCollect.getRandomThing(this.selectedRoomItems, new Random(Math.abs((int)(Math.random() * 100000))));

                avatar.getPath().clearPath();

                avatar.getStatusses().remove("mv");
                avatar.getStatusses().remove("sit");
                avatar.getStatusses().remove("lay");

                avatar.getPath().setWalking(false);

                this.room.getRoomMap().updateAvatarCoordinate(avatar, avatar.getSetPosition().getPoint(), randomItem.getPosition());
                avatar.setPosition(randomItem.getPosition().getX(), randomItem.getPosition().getY(), this.room.getRoomMap().getSquareDynamicHeight(randomItem.getPosition().getX(), randomItem.getPosition().getY()));
                avatar.setSetPosition(randomItem.getPosition().getX(), randomItem.getPosition().getY(), this.room.getRoomMap().getSquareDynamicHeight(randomItem.getPosition().getX(), randomItem.getPosition().getY()));

                avatar.getPosition().setHeadRotation(randomItem.getPosition().getRotation());
                avatar.getPosition().setRotation(randomItem.getPosition().getRotation());
                avatar.getPath().setGoal(avatar.getSetPosition().getPoint());
                avatar.updateNeeded(true);
                this.room.sendMessage(new AvatarEffectComposer(avatar.getVirtualId(), 0, 1000));

                this.room.getEventHandler().fireEvent("room.map.square.state.change", new RoomMapSquareStateChangeEventArgs(new Point(avatar.getSetPosition().getPoint())));
                avatar.getEventHandler().fireEvent("avatar.teleport.to", new AvatarTeleportToSquareEventArgs(new Point(avatar.getSetPosition().getPoint())));
            }
        }

        this.avatars.clear();
        this.process.cancel();
        this.process = null;
    }

    @Override
    public int getWiredCode() {
        return 8;
    }

    @Override
    public boolean requireAvatar() {
        return true;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return this.selectedRoomItems.values();
    }

    @Override
    public String getStringData() {
        return "";
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return Collections.emptyList();
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        try{
            WiredSaveData saveData = this.getExtradata().getSavedData();
            if(saveData != null){
                this.delay = saveData.getDelay();
                this.selectedRoomItems = generateSelectedItems(saveData.getItemsId());
            }
        } catch (Exception e){

        }
    }

    @Override
    public void saveWiredData(PlayerSession session, List<Integer> integerList, String stringData, List<Integer> selectedFurniList, int delay, int lastInt1) {
        if(!this.room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_save_wired_config"))
            return;

        this.delay = delay;
        this.selectedRoomItems = generateSelectedItems(selectedFurniList);

        WiredSaveData saveData = new WiredSaveData(null, this.selectedRoomItems.keySet(), delay, null);

        this.getExtradata().save(saveData);
        this.room.getRoomItemHandlerService().saveItem(this);

        session.sendMessage(new HideWiredConfigComposer());
    }
}
