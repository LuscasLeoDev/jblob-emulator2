package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 09/01/2017 at 20:35.
 */
public class StickyNoteComposer extends MessageComposer {
    @Override
    public String id() {
        return "StickyNoteMessageComposer";
    }

    public StickyNoteComposer(int itemId, String extradata) {
        message.putString(String.valueOf(itemId));
        message.putString(extradata);
    }
}
