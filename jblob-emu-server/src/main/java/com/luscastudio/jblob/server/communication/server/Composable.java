package com.luscastudio.jblob.server.communication.server;

/**
 * Created by Lucas on 24/01/2017 at 17:06.
 */
public abstract class Composable {

    protected boolean composed;

    public void compose(){
        this.composed = true;
    }

    public boolean isComposed() {
        return composed;
    }
}
