package com.luscastudio.jblob.server.database;

import com.luscastudio.jblob.server.debug.BLogger;

import java.sql.*;

/**
 * Created by Lucas on 01/10/2016.
 */
public class DBConnPrepare {

    private ResultSet resultSet;
    protected PreparedStatement prepare;
    private boolean returnKeys;

    private int index;

    public DBConnPrepare(String sql, PreparedStatement prepare, boolean returnKeys) {
        try {
            this.index = 1;
            this.returnKeys = returnKeys;
            //this.prepare = returnKeys ? this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS) : this.connection.prepareStatement(sql);

            this.prepare = prepare;
        } catch (Exception e) {
            BLogger.error("Could not prepare sql [" + sql + "]", e, this.getClass());
        }
    }

    public DBConnPrepareQueue setInt(int index, int val) {
        return new DBConnPrepareQueue(this, index, val);
    }

    public DBConnPrepareQueue setInt(int val) {
        return new DBConnPrepareQueue(this, index++, val);
    }

    public DBConnPrepareQueue setString(int index, String val) {
        return new DBConnPrepareQueue(this, index, val);
    }

    public DBConnPrepareQueue setString(String val) {
        return new DBConnPrepareQueue(this, index++, val);
    }

    public ResultSet runInsertKeys(){
        try{
            if(resultSet != null)
                resultSet.close();

            this.run();
            return this.resultSet = this.prepare.getGeneratedKeys();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public int runInsert(){
        try{
            if(resultSet != null)
                resultSet.close();

            this.prepare.execute();
            this.resultSet = this.prepare.getGeneratedKeys();
            if(this.resultSet.next())
                return resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public ResultSet runQuery() {
        try {

            if (resultSet != null)
                resultSet.close();

            if (!returnKeys) {
                return this.resultSet = prepare.executeQuery();
            } else {
                prepare.execute();
                return this.resultSet = prepare.getGeneratedKeys();
            }
        } catch (Exception e) {
            BLogger.error("Error while running query [" + prepare.toString() + "]", e, this.getClass());
            return null;
        }
    }

    public void run() {
        try {
            prepare.execute();
        } catch (Exception e) {
            BLogger.error("Error while executing query [" + prepare.toString() + "]", e, this.getClass());
        }
    }

    public int runUpdate() {
        try {
            return prepare.executeUpdate();
        } catch (Exception e) {
            BLogger.error("Error while executing update [" + prepare.toString() + "]", e, this.getClass());

            return -1;
        }
    }

    public void close() {

        try {
            this.prepare.close();
        } catch (SQLException e) {
            BLogger.error(e, this.getClass());
        }

        try {
            if (this.resultSet != null && !this.resultSet.isClosed()) {
                this.resultSet.close();
            }
        } catch (SQLException e) {
            BLogger.error(e, this.getClass());
        }
    }

}
