package com.luscastudio.jblob.server.communication.handlers.player.currencies.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.currency.AvatarCurrency;

import java.util.List;

/**
 * Created by Lucas on 07/12/2016.
 */

public class CurrencyComposer extends MessageComposer {
    @Override
    public String id() {
        return "ActivityPointsMessageComposer";
    }

    public CurrencyComposer(List<AvatarCurrency> currencyList) {
        message.putInt(currencyList.size());

        for(AvatarCurrency currency : currencyList){
            message.putInt(currency.getCurrencyId());
            message.putInt(currency.getValue());
        }
    }

    public CurrencyComposer(int type, int value){
        message.putInt(1);

        message.putInt(type);
        message.putInt(value);
    }
}
