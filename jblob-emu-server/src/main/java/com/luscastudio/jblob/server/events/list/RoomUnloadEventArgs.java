//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;

/**
 * Created by Lucas on 08/03/2017 at 23:49.
 */
public class RoomUnloadEventArgs extends EventArgs {
    private RoomProperties properties;

    public RoomUnloadEventArgs(RoomProperties properties) {
        this.properties = properties;
    }

    public RoomProperties getProperties() {
        return properties;
    }
}
