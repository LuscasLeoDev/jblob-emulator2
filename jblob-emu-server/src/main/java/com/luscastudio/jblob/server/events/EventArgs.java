//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events;

/**
 * Created by Lucas on 06/03/2017 at 20:42.
 */
public class EventArgs {
    private boolean cancelled;
    private String eventKey;

    public boolean isCancelled() {
        return cancelled;
    }

    public void cancel(){
        this.cancelled = true;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }
}
