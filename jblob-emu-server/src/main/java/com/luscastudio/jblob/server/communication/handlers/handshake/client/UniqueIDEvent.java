package com.luscastudio.jblob.server.communication.handlers.handshake.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 30/09/2016.
 */
public class UniqueIDEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.setMachineId(packet.getString(), packet.getString(), packet.getString());
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
