package com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.EffectWiredBox;

/**
 * Created by Lucas on 14/02/2017 at 23:47.
 */
public class WiredEffectConfigComposer extends MessageComposer {
    @Override
    public String id() {
        return "WiredEffectConfigMessageComposer";
    }

    public WiredEffectConfigComposer(EffectWiredBox wiredItem){

        wiredItem.parseHeader(message);

        message.putInt(wiredItem.getWiredCode()); //Wired Trigger Code
        message.putInt(wiredItem.getDelay()); //??

        message.putInt(wiredItem.getIncompatibleItems().size()); //Incompatible Effects Wired Item Sprite Ids
        for (IRoomItem item : wiredItem.getIncompatibleItems()) {
            message.putInt(item.getProperties().getBase().getSpriteId());
        }
    }
}
