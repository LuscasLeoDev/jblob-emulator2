package com.luscastudio.jblob.server.game.habbohotel.pet.custom;

/**
 * Created by Lucas on 09/02/2017 at 01:08.
 */
public class RequiredPetPart {
    private int partType;
    private int minRarityLevel;
    private int maxRarityLevel;

    public RequiredPetPart(int partType, int minRarityLevel, int maxRarityLevel){
        this.minRarityLevel = minRarityLevel;
        this.maxRarityLevel = maxRarityLevel;
        this.partType = partType;
    }

    public int getPartType() {
        return partType;
    }

    public int getMinRarityLevel() {
        return minRarityLevel;
    }

    public int getMaxRarityLevel() {
        return maxRarityLevel;
    }
}
