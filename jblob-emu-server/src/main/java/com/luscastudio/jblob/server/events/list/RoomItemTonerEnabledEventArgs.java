//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.TonerRoomItem;

/**
 * Created by Lucas on 09/03/2017 at 00:13.
 */
public class RoomItemTonerEnabledEventArgs extends EventArgs {
    private TonerRoomItem toner;

    public RoomItemTonerEnabledEventArgs(TonerRoomItem toner) {
        this.toner = toner;
    }

    public TonerRoomItem getToner() {
        return toner;
    }
}
