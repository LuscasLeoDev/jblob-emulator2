//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.database;

import com.luscastudio.jblob.api.utils.validator.ValueGetter;

/**
 * Created by Lucas on 29/06/2017 at 16:33.
 */
public class QueryHelper {

    public static String makeInsert(String tableName, int insertAmount, String... columns){
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("INSERT INTO %s ", tableName));
        builder.append("(").append(String.join(", ", columns)).append(")");
        builder.append(" VALUES ");

        StringBuilder paramsBuilder = new StringBuilder("(");
        for (int i = columns.length - 1; i >= 0; i--) {
            paramsBuilder.append("?");
            if(i > 0)
                paramsBuilder.append(",");
        }
        paramsBuilder.append(")");

        for (int i = 0; i < insertAmount; i++) {
            builder.append(paramsBuilder);
            if(i < insertAmount - 1)
                builder.append(",");
        }
        return builder.toString();
    }

    public static String makeArrayValues(String charValue, int amount){
        return makeArrayValues(integer -> charValue, amount);
    }

    public static String makeArrayValues(ValueGetter<Integer, String> strGetter, int amount){
        StringBuilder builder = new StringBuilder("(");

        for (int i = 0; i < amount; i++) {
            builder.append(strGetter.getValue(i));
            if(i < amount - 1)
                builder.append(",");
        }

        return builder.append(")").toString();
    }

}
