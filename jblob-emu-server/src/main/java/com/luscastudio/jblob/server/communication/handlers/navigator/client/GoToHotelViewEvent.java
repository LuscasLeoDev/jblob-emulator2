package com.luscastudio.jblob.server.communication.handlers.navigator.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.CloseFlatConnectionComposer;
import com.luscastudio.jblob.server.events.list.AvatarEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 04/10/2016.
 */

public class GoToHotelViewEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();

        if (room == null)
            return;

        room.getRoomAvatarService().removePlayerAvatar(session.getAvatar());
        session.sendMessage(new CloseFlatConnectionComposer());

        session.getAvatar().getFriendship().onUpdate();
        session.getAvatar().getEventHandler().fireEvent("avatar.room.leave", new AvatarEventArgs(session.getAvatar().getId()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
