package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server;

import com.luscastudio.jblob.api.utils.validator.Validator;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.navigator.flatcats.NavigatorFlatCategory;

import java.util.Collection;

/**
 * Created by Lucas on 10/12/2016.
 */

public class GetUserFlatCatsComposer extends MessageComposer {
    @Override
    public String id() {
        return "UserFlatCatsMessageComposer";
    }

    public GetUserFlatCatsComposer(Collection<NavigatorFlatCategory> categories, Validator<NavigatorFlatCategory> visibleValidator){
        message.putInt(categories.size());

        for(NavigatorFlatCategory category : categories) {
            message.putInt(category.getId()); //Id
            message.putString(category.getName()); //Name
            message.putBool(visibleValidator.validate(category)); //visible
            message.putBool(false); //?
            message.putString(""); //Not in use
            message.putString(category.getPublicName()); //external_flash_texts name
            message.putBool(false); //?
        }
    }
}
