package com.luscastudio.jblob.server.communication.handlers.catalog.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDeal;

/**
 * Created by Lucas on 08/12/2016.
 */

public class PurchaseOKComposer extends MessageComposer {
    @Override
    public String id() {
        return "PurchaseOKMessageComposer";
    }

    public PurchaseOKComposer(CatalogDeal deal){
        deal.parsePacket(message);
    }
}
