//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.horse.HorseRoomPetAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:49.
 */
public class AvatarRideHorseEventArgs extends EventArgs {
    private HorseRoomPetAvatar horseAvatar;

    public AvatarRideHorseEventArgs(HorseRoomPetAvatar horseAvatar) {
        this.horseAvatar = horseAvatar;
    }

    public HorseRoomPetAvatar getHorseAvatar() {
        return horseAvatar;
    }
}
