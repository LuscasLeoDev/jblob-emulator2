package com.luscastudio.jblob.server.game.habbohotel.rooms;

import com.luscastudio.jblob.api.utils.engine.ObjectPosition;
import com.luscastudio.jblob.api.utils.engine.Point;

/**
 * Created by Lucas on 04/10/2016.
 */

public final class AvatarObjectPosition extends ObjectPosition implements IAvatarObjectPosition {

    protected int headRotation;
    protected Point point;

    public AvatarObjectPosition(int x, int y, double z, int rotation, int headRotation) {
        super(x, y, z, rotation);
        this.headRotation = headRotation;
    }

    @Override
    public int getHeadRotation() {
        return headRotation;
    }

    @Override
    public void setHeadRotation(int value) {
        this.headRotation = value;
    }

    @Override
    public void setDirection(int value, boolean bodyOnly) {
        if(bodyOnly)
            this.rotation = value;
        else
            this.rotation = this.headRotation = value;
    }

    @Override
    public void setRotation(int value) {
        this.rotation = value;
    }

    @Override
    public void setPosition(int x, int y, double z, int direction) {
        super.setPosition(x, y, z, direction);
    }

    @Override
    public Point getSquareInFront() {
        return new Point(x + 1, y);
    }

    @Override
    public Point getSquareBehind() {
        return null;
    }

    @Override
    public Point getSquareLeft() {
        return null;
    }

    @Override
    public Point getSquareRight() {
        return null;
    }

    @Override
    public Point getPoint() {
        return this;
    }
}
