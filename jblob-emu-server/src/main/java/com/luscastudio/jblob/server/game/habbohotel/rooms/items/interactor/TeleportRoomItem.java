package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.IPoint;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomForwardComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.AvatarWalkOnSquareEventArgs;
import com.luscastudio.jblob.server.events.list.EventArgsWithAvatar;
import com.luscastudio.jblob.server.events.list.PlayerAvatarEnterRoomEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Lucas on 22/12/2016 at 23:12.
 */
public class TeleportRoomItem extends SimpleRoomItem {

    private IRoomAvatar interactingAvatar;

    private List<RunProcess> processLists;

    private EventDelegate blockUserWalkDelegate;

    public TeleportRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.processLists = BCollect.newList();
        this.extradata.setExtradata("0");
    }

    @Override
    public void onPlayerTrigger(PlayerSession session, int requestId) {
        //super.onPlayerTrigger(session, requestId);

        RoomPlayerAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());

        if(avatar == null || this.interactingAvatar != null)
            return;


        Point squareInFront = this.position.getSquareInFront(0);

        if(avatar.getPath().isWalking() && avatar.getPath().getGoal().equals(squareInFront))
            return;

        if(!avatar.getSetPosition().equals(squareInFront) && !avatar.getSetPosition().equals(this.position)) {
            avatar.moveTo(squareInFront);

            this.room.getEventHandler().on("room.avatar.walk.on.square", (delegate, eventArgs) -> {
                AvatarWalkOnSquareEventArgs args = ((AvatarWalkOnSquareEventArgs) eventArgs);

                IRoomAvatar avatarSender = args.getAvatar();

                if (avatar != avatarSender) {
                    return;
                }

                if(!avatar.getPath().getGoal().equals(squareInFront)){
                    delegate.cancel();
                    return;
                }

                IPoint p = args.getPoint();

                if (!p.equals(squareInFront)) {
                    return;
                }

                this.startTeleportStage1(avatar);
                delegate.cancel();
            });
        } else {
            this.startTeleportStage1(avatar);
        }
    }

    private void startTeleportStage1(IRoomAvatar avatar) {

        this.getExtradata().setExtradata("1");
        this.room.getRoomItemHandlerService().updateItem(this, false);

        this.interactingAvatar = avatar;

        if (!avatar.getSetPosition().equals(this.position))
            avatar.moveTo(this.position);
        else
            this.startTeleportStage2();

        if (blockUserWalkDelegate != null)
            blockUserWalkDelegate.cancel();


        //Block the user of walking
        blockUserWalkDelegate = this.room.getEventHandler().on("room.avatar.start.move room.avatar.auto.start.move", (delegate, eventArgs) -> {
            if(eventArgs instanceof EventArgsWithAvatar && ((EventArgsWithAvatar)eventArgs).getAvatar() == avatar)
                eventArgs.cancel();


        });

    }

    @Override
    public void onUserWalkStand(IRoomAvatar avatar) {
        if(avatar == interactingAvatar){
            this.startTeleportStage2();
        }
    }

    private void startTeleportStage2() {


        this.room.getRoomProcess().enqueue(this.makeProcess(new RunProcess(() -> {
            

            //this.extradata.setExtradata("0");
            this.room.getRoomItemHandlerService().updateItem(this, false);

            this.startTeleportStage3();
        }, 500)));


    }

    private void startTeleportStage3(){
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("SELECT room.id as \"roomId\", links.tele_two_id as \"teleId\" FROM players_rooms room INNER JOIN teleports_links links ON links.tele_one_id = ? INNER JOIN avatars_items_data item ON links.tele_two_id = item.id WHERE item.room_id = room.id");
            prepare.setInt(1, this.getId());
            ResultSet set = prepare.runQuery();
            if(!set.next()) {
                this.cancelStages();
                this.extradata.setExtradata("1");
                this.room.getRoomItemHandlerService().updateItem(this, false);
                //this.restoreExtradata(1000);
            }
            else
                this.startTeleportStage4(set.getInt("roomId"), set.getInt("teleId"));

        } catch (Exception e){
            BLogger.error(e, this.getClass());
            this.cancelStages();
        }
    }

    private void startTeleportStage4(int roomId, int teleId) {
        if (roomId == this.room.getProperties().getId()) {
            TeleportRoomItem teleport = (TeleportRoomItem) this.room.getRoomItemHandlerService().getRoomItemById(teleId);
            if (teleport == null) {
                cancelStages();
                return;
            }

            this.room.getRoomProcess().enqueue(this.makeProcess(new RunProcess(() -> {


                this.extradata.setExtradata("2");
                this.room.getRoomItemHandlerService().updateItem(this, false, true);
                this.restoreExtradata(1000);


                teleport.startTeleportStage5(this.interactingAvatar, this.blockUserWalkDelegate);
                this.interactingAvatar = null;
            }, 500)));


        } else {

            Room room = JBlob.getGame().getRoomManager().loadRoom(roomId);

            if (room == null) {
                this.cancelStages();
                return;
            }


            TeleportRoomItem teleport = (TeleportRoomItem) room.getRoomItemHandlerService().getRoomItemById(teleId);

            if (teleport == null) {
                this.cancelStages();
                return;
            }

            HabboAvatar habbo = ((RoomPlayerAvatar) this.interactingAvatar).getAvatar();

            this.room.getRoomProcess().enqueue(this.makeProcess(new RunProcess(() -> {


                this.extradata.setExtradata("2");
                this.room.getRoomItemHandlerService().updateItem(this, false, true);
                this.restoreExtradata(1000);


                room.getRoomProcess().enqueue(teleport.makeProcess(new RunProcess(() -> {
                    habbo.getSession().sendMessage(new RoomForwardComposer(room.getProperties().getId()));

                    room.getEventHandler().on("room.player.avatar.enter", (delegate, eventArgs) -> {
                        PlayerAvatarEnterRoomEventArgs args = ((PlayerAvatarEnterRoomEventArgs) eventArgs);
                        if (args.getAvatar() != habbo)
                            return;
                        RoomPlayerAvatar avatar = args.getPlayerAvatar();

                        avatar.setPosition(teleport.getPosition().getX(), teleport.getPosition().getY(), teleport.getPosition().getZ());
                        avatar.setSetPosition(teleport.getPosition().getX(), teleport.getPosition().getY(), teleport.getPosition().getZ());

                        avatar.getPosition().setHeadRotation(teleport.getPosition().getRotation());
                        avatar.getPosition().setRotation(teleport.getPosition().getRotation());

                        teleport.startTeleportStage5(avatar, null);
                        this.interactingAvatar = null;
                        this.cancelStages();
                    });
                }, 1000)));

            }, 500)));

            //RoomEnterErrorFuture errorFuture = room.getRoomAvatarService().tryAddPlayerAvatar(habbo, teleport.position, true);
        }
    }

    //Receiving Player
    private void startTeleportStage5(IRoomAvatar avatar, EventDelegate blockUserWalkDelegate){
        if(this.interactingAvatar != null){

            this.room.getRoomMap().updateAvatarCoordinate(this.interactingAvatar, (Point)avatar.getSetPosition(), new Point(this.position));
            this.interactingAvatar.setPosition(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ());
            this.interactingAvatar.setSetPosition(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ());
            this.interactingAvatar.getPosition().setRotation(this.position.getRotation());
            this.interactingAvatar.getPosition().setHeadRotation(this.position.getRotation());
            this.interactingAvatar.updateNeeded(true);


            return;
        }
        this.interactingAvatar = avatar;
        this.blockUserWalkDelegate = blockUserWalkDelegate;


        this.room.getRoomProcess().enqueue(this.makeProcess(new RunProcess(() -> {
            

            this.room.getRoomMap().updateAvatarCoordinate(this.interactingAvatar, (Point)avatar.getSetPosition(), new Point(this.position));
            this.interactingAvatar.setPosition(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ());
            this.interactingAvatar.setSetPosition(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ());
            this.interactingAvatar.getPosition().setRotation(this.position.getRotation());
            this.interactingAvatar.getPosition().setHeadRotation(this.position.getRotation());
            this.interactingAvatar.updateNeeded(true);

            this.extradata.setExtradata("2");
            this.room.getRoomItemHandlerService().updateItem(this, false, true);

        }, 1000)));


        this.room.getRoomProcess().enqueue(this.makeProcess(new RunProcess(() -> {
            
            if(this.blockUserWalkDelegate != null)
                this.blockUserWalkDelegate.cancel();
            this.interactingAvatar.moveTo(this.position.getSquareInFront(0));

            this.extradata.setExtradata("1");
            this.room.getRoomItemHandlerService().updateItem(this, false, true);
        }, 2500)));

        this.room.getRoomProcess().enqueue(this.makeProcess(new RunProcess(() -> {
            
            this.restoreExtradata(1000);
            this.interactingAvatar = null;
            this.cancelStages();
        }, 4000)));
    }

    private void startTeleportStage6(IRoomAvatar avatar, EventDelegate blockUserWalkDelegate){

    }

    private void restoreExtradata(int time) {
        this.room.getRoomProcess().enqueue((new RunProcess(() -> {
            

            this.extradata.setExtradata("0");
            this.room.getRoomItemHandlerService().updateItem(this, false);
        }, time)));
    }

    private void cancelStages(){
        if(this.blockUserWalkDelegate != null)
            this.blockUserWalkDelegate.cancel();

        this.blockUserWalkDelegate = null;

        if(this.interactingAvatar != null)
            this.interactingAvatar.moveTo(this.getPosition().getSquareInFront(0));

        this.interactingAvatar = null;

        this.extradata.setExtradata("0");
        this.room.getRoomItemHandlerService().updateItem(this, false);

        Iterator<RunProcess> processIterator = processLists.iterator();
        while(processIterator.hasNext()) {
            processIterator.next().cancel();
            processIterator.remove();
        }
    }

    private RunProcess makeProcess(RunProcess process){
        this.processLists.add(process);
        return process;
    }

    @Override
    public boolean onMove(PlayerSession session, Point from) {
        this.interactingAvatar = null;
        this.cancelStages();
        return super.onMove(session, from);
    }

    @Override
    public boolean canContinuePath(IRoomAvatar avatar, Point from, boolean finalStep) {
        return avatar == interactingAvatar || super.canContinuePath(avatar, from, finalStep);
    }

    @Override
    public boolean canWalk(IRoomAvatar avatar) {
        return avatar == interactingAvatar || super.canWalk(avatar);
    }

    @Override
    public boolean canWalk(IRoomAvatar avatar, Point from) {
        return avatar == interactingAvatar || super.canWalk(avatar, from);
    }

    @Override
    public void onUserWalkOn(IRoomAvatar avatar) {
        if(avatar == interactingAvatar){

        }
    }

    @Override
    public double getHeight() {
        return this.getBase().getHeight();
    }

    @Override
    public int getUseButtonCode() {
        return 2;
    }
}
