package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.WiredConditionConfigComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

/**
 * Created by Lucas on 15/02/2017 at 23:14.
 */
public abstract class ConditionWiredBox extends WiredBoxRoomItem implements ISavableWiredCondition{
    public ConditionWiredBox(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
    }

    @Override
    protected MessageComposer getWiredConfigComposer() {
        return new WiredConditionConfigComposer(this);
    }

    public abstract boolean validateCase(IRoomAvatar avatar, IRoomItem item);
}
