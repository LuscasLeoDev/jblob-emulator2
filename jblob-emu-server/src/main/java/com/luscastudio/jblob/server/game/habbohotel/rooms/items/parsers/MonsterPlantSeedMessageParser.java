package com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.MonsterPlantSeedRoomItemExtradata;

/**
 * Created by Lucas on 06/02/2017 at 19:10.
 */
public class MonsterPlantSeedMessageParser implements IItemMessageParser{

    private MonsterPlantSeedRoomItemExtradata extradata;
    public MonsterPlantSeedMessageParser(MonsterPlantSeedRoomItemExtradata extradata) {
        this.extradata = extradata;
    }


    @Override
    public void parse(PacketWriting message) {
        message.putInt(2);//Count
        message.putString("state");
        message.putString(this.extradata.getState());

        message.putString("rarity");
        message.putString(String.valueOf(this.extradata.getRarityLevel()));
    }

    @Override
    public int getCode() {
        return 1;//MapStuffData
    }
}
