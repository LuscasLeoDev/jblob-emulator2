package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets;

import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.ChatComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.pets.server.RespectPetNotificationComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomAvatarType;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.ai.PetAI;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 22/01/2017 at 15:50.
 */
public class SimpleRoomPetAvatar extends RoomPetAvatar {

    private RunProcess runProcess;
    private Random random;

    private int minRadius = 0;
    private int maxRadius = 10;

    private String ownerName;

    private PetAI ai;

    public SimpleRoomPetAvatar(Room room, int virtualId, PetData petData) {
        super(room, virtualId, petData);
        this.random = new Random(this.hashCode());

        this.ownerName = null;
        this.ai = new PetAI(this);
    }

    private void onCycle() {

        this.ai.onCycle();
    }

    private void onEvent(EventDelegate delegate, EventArgs args, Object sender) {
        switch (args.getEventKey()){
            case "path.create.fail":
                this.onPathFail(delegate);
                break;

            case "path.create.success":
                this.onPathSuccess(delegate);
        }
    }

    private void onPathFail(EventDelegate delegate) {

        if(this.maxRadius > 0)
            this.maxRadius--;


        delegate.cancel();
    }

    private void onPathSuccess(EventDelegate delegate) {
        if(this.maxRadius < 10)
            this.maxRadius++;

        delegate.cancel();
    }

    private void startCycle(){
        this.getRoom().getRoomProcess().enqueue(this.runProcess = new RunProcess(this::onCycle, 1000, 1000));
    }

    private void stopCycle(){
        if(this.runProcess != null)
            this.runProcess.cancel();
        this.runProcess = null;
    }

    @Override
    public synchronized void dispose(){
        this.stopCycle();
        this.ai.dispose();
        super.dispose();
    }

    @Override
    public int getId() {
        return petData.getId();
    }

    @Override
    public String getName() {
        return petData.getName();
    }

    @Override
    public String getMotto() {
        return "JBlob Simple Pet Avatar Class";
    }

    @Override
    public RoomAvatarType getType() {
        return RoomAvatarType.PET;
    }

    @Override
    public int getAvatarType() {
        return 2;
    }

    @Override
    public void composeAvatar(PacketWriting message) {
        message.putInt(this.getPetData().getPetBase().getPetType());
        message.putInt(this.getPetData().getOwnerId());
        message.putString(this.getOwnerName());
        message.putInt(this.getPetData().getRarityLevel()); //RarityLevel
        message.putBool(false); // has saddle
        message.putBool(false); // is someone ridding
        message.putBool(false); //can breed?
        message.putBool(false); //?
        message.putBool(false); //
        message.putBool(false);
        message.putInt(this.getPetData().getLevel()); // Level
        message.putString("");
    }

    @Override
    public void composePetAvatarInformation(PacketWriting message) {

        message.putInt(this.getId()); //Id
        message.putString(this.getName()); //Name
        message.putInt(this.getPetData().getLevel()); //Level
        message.putInt(this.getPetData().getPetBase().getMaxLevels()); //Max Level

        message.putInt(this.getPetData().getExperience()); //Experience
        message.putInt(this.getPetData().getMaxExperience()); //Max Experience

        message.putInt(this.getPetData().getEnergy()); //Energy
        message.putInt(this.getPetData().getMaxEnergy()); //Max Energy

        //its happiness, dumb
        message.putInt(this.getPetData().getHappiness()); //Happiness
        message.putInt(this.getPetData().getMaxHappiness()); //Max Happiness

        message.putInt(this.getPetData().getRespects()); //Pet Respects
        message.putInt(this.getPetData().getOwnerId()); //Owner Id

        message.putInt((int)TimeUnit.SECONDS.toDays((long)(DateTimeUtils.getUnixTimestampInt() - this.getPetData().getCreateTimestamp()))); //Age in days

        message.putString(this.getOwnerName()); //Owner name

        message.putInt(this.getPetData().getRaceId()); // Pet Race

        message.putBool(false); //Has Saddle
        message.putBool(false); //Is Ridding

        message.putInt(0);//Horse jump data

        message.putInt(0); //Monster Plant Grown level / Horse Anyone car ride on
        message.putBool(false);
        message.putBool(false); //?
        message.putBool(false); //Monster Plant Is Dead?
        message.putInt(0); //Monster Plant RarityLevel
        message.putInt(0); //Monster Plant Total Life Time
        message.putInt(0); //Monster Plant Life Time
        message.putInt(0); //Monster Plant Time until grow up

        message.putBool(true);
    }

    @Override
    public void onPlace(HabboAvatar placerAvatar) {
        this.getStatusses().set("sit", "0");
    }

    @Override
    public void onRemove(HabboAvatar removerAvatar) {
        this.getRoom().sendMessage(new ChatComposer(this.getVirtualId(), 5, 0, "Saindoo!"));
    }

    @Override
    public void onRespect(HabboAvatar avatar) {
        super.onRespect(avatar);
        this.getRoom().sendMessage(new RespectPetNotificationComposer(this));
    }

    @Override
    public void onAvatarSay(HabboAvatar avatar, String message) {

    }

    @Override
    public void onEnter() {
        this.startCycle();
    }

    @Override
    public void onLeave() {
        this.stopCycle();
    }

    /*@Override
    public void saveWiredData() {
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("UPDATE avatars_pets_data SET x = ?, y = ?, rotation = ?, level = ?, energy = ?, happiness = ?, experience = ?, respects = ?, extradata = ?, custom_data = ? WHERE id = ?");
            prepare.setInt(1, this.getSetPosition().getX());
            prepare.setInt(2, this.getSetPosition().getY());
            prepare.setInt(3, this.getPosition().getRotation());

            prepare.setInt(4, this.getPetData().getLevel());
            prepare.setInt(5, this.getPetData().getEnergy());
            prepare.setInt(6, this.getPetData().getHappiness());
            prepare.setInt(7, this.getPetData().getExperience());
            prepare.setInt(8, this.getPetData().getRespects());
            prepare.setString(9, this.getPetData().getExtradata());
            prepare.setString(10, this.getPetData().getCustom().toDbString());
            prepare.setInt(11, this.getPetData().getId());

            prepare.run();
        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }*/

    public String getOwnerName() {
        if(this.ownerName != null)
            return this.ownerName;

        IAvatarDataCache cache;
        return this.ownerName = (cache = JBlob.getGame().getUserCacheManager().getUserCache(this.petData.getOwnerId())) != null ? cache.getUsername() : "unknown_" + this.petData.getOwnerId();
    }

    @Override
    public PetData getPetData() {
        return this.petData;
    }
}
