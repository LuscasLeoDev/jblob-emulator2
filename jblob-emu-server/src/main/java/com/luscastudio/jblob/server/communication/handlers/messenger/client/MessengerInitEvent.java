package com.luscastudio.jblob.server.communication.handlers.messenger.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.FriendListComposer;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.MessengerInitComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 02/10/2016.
 */
public class MessengerInitEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendQueueMessage(new MessengerInitComposer());
        session.getAvatar().getFriendship().friendListPacket().forEach(session::sendQueueMessage);
        session.flush();
        session.getAvatar().getFriendship().onConnect();
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
