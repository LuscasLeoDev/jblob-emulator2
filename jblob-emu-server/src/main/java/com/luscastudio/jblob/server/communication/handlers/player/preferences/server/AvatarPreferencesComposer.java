package com.luscastudio.jblob.server.communication.handlers.player.preferences.server;

import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;

/**
 * Created by Lucas on 09/12/2016.
 */

public class AvatarPreferencesComposer extends MessageComposer {
    @Override
    public String id() {
        return "SoundSettingsMessageComposer";
    }

    public AvatarPreferencesComposer(AvatarPreferences preferences){

        message.putInt(preferences.getIntOrSet(AvatarPreferences.VOLUME_SYSTEM, JBlobSettings.AP_DEFAULT_VOLUME_SYSTEM));
        message.putInt(preferences.getIntOrSet(AvatarPreferences.VOLUME_FURNI, JBlobSettings.AP_DEFAULT_VOLUME_FURNI));
        message.putInt(preferences.getIntOrSet(AvatarPreferences.VOLUME_MUSIC, JBlobSettings.AP_DEFAULT_VOLUME_MUSIC));

        message.putBool(preferences.getBoolOrSet(AvatarPreferences.OLD_CHAT_ENABLED, JBlobSettings.AP_DEFAULT_OLD_CHAT_ENABLED));
        message.putBool(preferences.getBoolOrSet(AvatarPreferences.MESSENGER_INVINTE_ENABLED, JBlobSettings.AP_DEFAULT_MESSENGER_INVINTE_ENABLED));
        message.putBool(preferences.getBoolOrSet(AvatarPreferences.DISABLE_CAMERA_FOLLOW, JBlobSettings.AP_DEFAULT_DISABLE_CAMERA_FOLLOW));

        message.putInt(preferences.getIntOrSet(AvatarPreferences.FRIENDBAR_STATUS, JBlobSettings.AP_DEFAULT_FRIENDBAR_STATUS));
        message.putInt(preferences.getIntOrSet(AvatarPreferences.LAST_CHAT_BUBBLE, JBlobSettings.AP_DEFAULT_LAST_CHAT_BUBBLE));

    }
}
