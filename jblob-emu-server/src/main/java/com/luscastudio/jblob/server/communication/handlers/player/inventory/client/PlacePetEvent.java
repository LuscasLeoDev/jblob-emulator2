package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.RemoveInventoryPetComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomUsersComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.UserUpdateComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.AvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 29/01/2017 at 19:46.
 */
public class PlacePetEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int petId = packet.getInt();

        int x = packet.getInt();
        int y = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();

        if(room == null || (!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_place_pets") && room.getProperties().getAllowPets() == 0))
            return;

        PetData petData = session.getAvatar().getInventory().removePet(petId);

        if(petData == null)
            return;

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().addPet(petData, new AvatarObjectPosition(x, y, room.getRoomMap().getSquareDynamicHeight(x, y), 4, 4));
        petData.setRoomId(room.getProperties().getId());

        //room.getRoomAvatarService().addAvatarEnteringPacketQueue(petAvatar);
        petAvatar.onEnter();
        room.sendQueueMessage(new RoomUsersComposer(petAvatar));
        room.sendQueueMessage(new UserUpdateComposer(petAvatar));
        petAvatar.updateNeeded(true);
        petAvatar.onPlace(session.getAvatar());


        //petAvatar.onEnter();

        session.sendMessage(new RemoveInventoryPetComposer(petId));

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            DBConnPrepare prepare = reactor.prepare("UPDATE avatars_pets_data SET room_id = ?, x = ?, y = ?, rotation = ? WHERE id = ?");
            prepare.setInt(1, room.getProperties().getId());
            prepare.setInt(2, petAvatar.getPosition().getX());
            prepare.setInt(3, petAvatar.getPosition().getY());
            prepare.setInt(4, petAvatar.getPosition().getRotation());
            prepare.setInt(5, petData.getId());
            prepare.run();
        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }


    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
