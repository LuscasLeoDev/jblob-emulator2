//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events;

/**
 * Created by Lucas on 18/06/2017 at 16:08.
 */
public interface EventHandler {
    EventDelegate on(String keys, IEventDelegateFuture eventDelegateFuture);

    EventArgs fireEvent(String key);

    EventArgs fireEvent(String key, EventArgs args);

    void dispose();
}
