package com.luscastudio.jblob.server.network.server;

import com.luscastudio.jblob.server.network.client.ClientConnectionHandler;
import com.luscastudio.jblob.server.protocols.ClientResponseIn;
import com.luscastudio.jblob.server.protocols.ClientResponseOut;
import com.luscastudio.jblob.server.protocols.ClientResponseXml;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.EventExecutorGroup;


/**
 * Created by Lucas on 18/09/2016.
 */
public class ClientConnectionInitializer extends ChannelInitializer<SocketChannel> {

    private final EventExecutorGroup executor;
    private final ClientConnectionHandler clientHandler;

    public ClientConnectionInitializer(EventExecutorGroup exe, ClientConnectionHandler ClientHandler) {
        executor = exe;
        this.clientHandler = ClientHandler;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.config().setTrafficClass(0x18);

        ch.pipeline().addLast("xmlDecoder", new ClientResponseXml());
        ch.pipeline().addLast("messageDecoder", new ClientResponseIn());

        ch.pipeline().addLast("stringEncoder", new StringEncoder(CharsetUtil.UTF_8));
        ch.pipeline().addLast("messageEncoder", new ClientResponseOut());


        ch.pipeline().addLast(this.executor, "clientHandler", this.clientHandler);


    }
}
