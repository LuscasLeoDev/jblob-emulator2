package com.luscastudio.jblob.server.communication.handlers.notification.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 20/01/2017 at 01:20.
 */
public class MaintenanceStatusComposer extends MessageComposer {
    @Override
    public String id() {
        return "MaintenanceStatusMessageComposer";
    }

    public MaintenanceStatusComposer(boolean somebool, int timeToShutdown, int timeToBeBack){
        message.putBool(somebool);
        message.putInt(timeToShutdown); //s
        //message.putInt(timeToBeBack);
    }
}
