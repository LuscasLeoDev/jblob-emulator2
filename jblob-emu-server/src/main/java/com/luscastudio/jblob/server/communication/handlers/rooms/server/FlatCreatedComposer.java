package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 13/02/2017 at 19:17.
 */
public class FlatCreatedComposer extends MessageComposer {
    @Override
    public String id() {
        return "FlatCreatedMessageComposer";
    }

    public FlatCreatedComposer(int roomId, String roomName){
        message.putInt(roomId);
        message.putString(roomName);
    }
}
