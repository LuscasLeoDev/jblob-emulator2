package com.luscastudio.jblob.server.game.habbohotel.navigator.search;

import com.luscastudio.jblob.server.game.habbohotel.navigator.categories.NavigatorCategory;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;

import java.util.List;

/**
 * Created by Lucas on 03/10/2016.
 */

public class NavigatorCategorySearchResult {

    private NavigatorCategory category;
    private List<RoomProperties> rooms;

    private int expandStatus;
    private boolean collapsed;
    private int showType;

    public  NavigatorCategorySearchResult(NavigatorCategory category, List<RoomProperties> rooms){
        this.category = category;
        this.rooms = rooms;
        this.expandStatus = category.getExpandStatus();
        this.collapsed = category.isCollapsed();
        this.showType = category.getShowType();
    }

    public int getExpandStatus() {
        return expandStatus;
    }

    public int getShowType() {
        return showType;
    }

    public List<RoomProperties> getRooms() {
        return rooms;
    }

    public NavigatorCategory getCategory() {
        return category;
    }

    public boolean isCollapsed() {
        return collapsed;
    }
}
