package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 16/12/2016.
 */

public class RoomOwnerControllerComposer extends MessageComposer {
    @Override
    public String id() {
        return "YouAreOwnerMessageComposer";
    }
}
