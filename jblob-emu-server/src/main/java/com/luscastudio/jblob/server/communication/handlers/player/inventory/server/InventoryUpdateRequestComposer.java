package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 08/12/2016.
 */

public class InventoryUpdateRequestComposer extends MessageComposer {

    @Override
    public String id() {
        return "FurniListUpdateMessageComposer";
    }

    public InventoryUpdateRequestComposer(){

    }
}
