package com.luscastudio.jblob.server.game.habbohotel.pet.custom;

import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomPart;

import java.util.Map;

/**
 * Created by Lucas on 08/02/2017 at 21:24.
 */
public class PetCustomDataResult {

    private Map<Integer, PetCustomPart> customData;
    private int rarityLevel;
    private String name;

    public PetCustomDataResult(String name, Map<Integer, PetCustomPart> customData, int rarityLevel) {
        this.customData = customData;
        this.rarityLevel = rarityLevel;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getRarityLevel() {
        return rarityLevel;
    }

    public Map<Integer, PetCustomPart> getCustomData() {
        return customData;
    }

    public String generateResult(){
        String r = "";
        boolean first = true;
        for (Map.Entry<Integer, PetCustomPart> entry : this.customData.entrySet()) {
            r += (!first ? ";" : "") + entry.getKey() + "," + entry.getValue().getPartId() + "," + entry.getValue().getColorId();
            first = false;
        }

        return r;
    }
}
