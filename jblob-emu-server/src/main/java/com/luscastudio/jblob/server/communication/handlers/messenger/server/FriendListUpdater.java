package com.luscastudio.jblob.server.communication.handlers.messenger.server;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.Objects;

/**
 * Created by Lucas on 09/10/2016.
 */

public class FriendListUpdater {

    public static void serializeFriend(PacketWriting message, IAvatarDataCache avatar, int relationType) {

        //region _SafeStr_3051
        message.putInt(avatar.getId()); //Id
        message.putString(avatar.getUsername()); //Username
        message.putInt(Objects.equals(avatar.getGender().toUpperCase(), "F") ? 1 : 0); //Gender
        message.putBool(avatar.isOnline()); //Online
        message.putBool(avatar.isInRoom()); //Can follow
        message.putString(avatar.getFigureString()); //Look
        message.putInt(0); //Category id TODO: add a category System
        message.putString(avatar.getMotto()); //Motto
        message.putString(""); //Facebook
        message.putString(""); //Unknown
        message.putBool(true); //Offline messaging TODO Change offline messaging
        message.putBool(false); //Unknown
        message.putBool(false); //Has Phone
        message.putShort(relationType); //Relation type
        //endregion
    }

}
