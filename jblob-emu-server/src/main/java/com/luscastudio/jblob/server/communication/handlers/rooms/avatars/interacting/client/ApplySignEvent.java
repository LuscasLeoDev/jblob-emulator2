package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 12/12/2016.
 */

public class ApplySignEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int signId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        RoomPlayerAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());
        if(avatar == null)
            return;

        avatar.getStatusses().set("sign", String.valueOf(signId), 5);
        avatar.updateNeeded(true);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
