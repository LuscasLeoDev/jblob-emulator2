package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.inventory.badge.Badge;

import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 10/11/2016.
 */

public class BadgesComposer extends MessageComposer {
    @Override
    public String id() {
        return "BadgesMessageComposer";
    }

    public BadgesComposer(Collection<Badge> badgeList) {

        List<Badge> slotedBadges = BCollect.newList();

        message.putInt(badgeList.size());

        for(Badge badge : badgeList){

            message.putInt(badge.getId());
            message.putString(badge.getCode());

            if(badge.getSlotId() > 0)
                slotedBadges.add(badge);
        }

        message.putInt(slotedBadges.size());

        for(Badge badge : slotedBadges){

            message.putInt(badge.getSlotId());
            message.putString(badge.getCode());

        }

    }
}
