package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.engine.RoomMap;

public class HeightMapComposer extends MessageComposer {
    @Override
    public String id() {
        return "HeightMapMessageComposer";
    }

    public HeightMapComposer(RoomMap map) {

        message.putInt(map.getMapWidth());// Width
        message.putInt(map.getMapWidth() * map.getMapHeight());// Total


        for (int y = 0; y < map.getMapHeight(); y++)
            for (int x = 0; x < map.getMapWidth(); x++) {


                message.putShort(map.isValidSquare(x, y) ? (short) ((Math.min(127, map.getSquareDynamicHeightToFloorMap(x, y)) * 256)) : -1);
                //message.putShort(map.isValidSquare(x, y) ? (short) ((Math.min(127, map.getSquareDynamicHeight(x, y)) * 256)) : -1);

            }

    }
}
