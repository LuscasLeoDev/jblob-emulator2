package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 04/10/2016.
 */

import com.luscastudio.jblob.server.communication.handlers.navigator.RoomSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

import java.util.Collection;

public class UserUpdateComposer extends MessageComposer {
    public UserUpdateComposer(IRoomAvatar avatar) {
        message.putInt(1);
        RoomSerializer.serializeRoomAvatarsStatus(avatar, message);
    }

    public UserUpdateComposer(Collection<IRoomAvatar> avatars) {

        message.putInt(avatars.size());

        for (IRoomAvatar avatar : avatars) {
            RoomSerializer.serializeRoomAvatarsStatus(avatar, message);
        }
    }

    @Override
    public String id() {
        return "UserUpdateMessageComposer";
    }
}
