//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.google.common.collect.ArrayListMultimap;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.catalog.gifts.handlers.IGiftHandler;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.GiftRoomItemExtradata;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.ResultSet;

/**
 * Created by Lucas on 28/06/2017 at 21:47.
 */
public class GiftRoomItem extends SimpleRoomItem {

    private GiftRoomItemExtradata extradata;
    public GiftRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.extradata = ((GiftRoomItemExtradata) properties.getExtradata());
    }

    @Override
    public GiftRoomItemExtradata getExtradata() {
        return extradata;
    }

    public void openGift(PlayerSession session) {

        if(this.getProperties().getOwnerId() != session.getAvatar().getId())
            return;

        ArrayListMultimap<String, Integer> itemsTypesMap = ArrayListMultimap.create();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM gifts_contents WHERE gift_id = ?");
            prepare.setInt(this.getProperties().getId());

            ResultSet resultSet = prepare.runQuery();
            while (resultSet.next())
                itemsTypesMap.put(resultSet.getString("type"), resultSet.getInt("value"));

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

        IGiftHandler handler = JBlob.getGame().getCatalogManager().getGiftHandler(itemsTypesMap);

        if(handler == null){
            session.sendMessage(new BubbleNotificationComposer("debug", "OOps! No handler for this gift!"));
            return;
        }

        if(handler.handle(session, itemsTypesMap, room, this.getPosition())){
            this.extradata.setState(this.extradata.getState().equals("0") ? "1" : "0");
            this.room.getRoomItemHandlerService().updateItem(this, false, true);

            this.room.getRoomMap().updateSqHeight(this.getAffectedTiles());
            this.room.getRoomItemHandlerService().removeRoomItem(this);
            this.room.sendMessage(new RemoveFloorItemComposer(this.getId(), 0, true, 5000));

            JBlob.getGame().getRoomItemFactory().hideFurniProperties(this.getId());


        }

    }
}
