package com.luscastudio.jblob.server.database;

import com.luscastudio.jblob.server.debug.BLogger;

import java.sql.Array;

/**
 * Created by Lucas on 01/10/2016.
 */
public class DBConnPrepareQueue {

    public DBConnPrepare prepare;

    public DBConnPrepareQueue(DBConnPrepare prepare, int index, int val) {
        this.prepare = prepare;
        this.setInt(index, val);
    }

    public DBConnPrepareQueue(DBConnPrepare prepare, int index, String val) {
        this.prepare = prepare;
        this.setString(index, val);
    }

    public DBConnPrepareQueue(DBConnPrepare prepare, int index, Array val) {
        this.prepare = prepare;
        this.setArray(index, val);
    }

    public DBConnPrepareQueue setInt(int index, int val) {
        try {
            this.prepare.prepare.setInt(index, val);
            return this;
        } catch (Exception e) {
            BLogger.error("Could not add a parameter to prepare statement:", e, this.getClass());
            return this;
        }
    }

    public DBConnPrepareQueue setString(int index, String val) {
        try {
            this.prepare.prepare.setString(index, val);
            return this;
        } catch (Exception e) {
            BLogger.error("Could not add a parameter to prepare statement:", e, this.getClass());
            return this;
        }
    }

    public DBConnPrepareQueue setArray(int index, Array val) {
        try {
            this.prepare.prepare.setArray(index, val);
            return this;
        } catch (Exception e) {
            BLogger.error("Could not add a parameter to prepare statement:", e, this.getClass());
            return this;
        }
    }

    public DBConnPrepare back() {
        return this.prepare;
    }


}
