//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.api.utils.engine.IPoint;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:21.
 */
public class AvatarWalkOnSquareEventArgs extends EventArgs {
    private IRoomAvatar avatar;
    private IPoint point;

    public AvatarWalkOnSquareEventArgs(IPoint point) {
        this.point = point;
    }

    public AvatarWalkOnSquareEventArgs(IRoomAvatar avatar, IPoint point) {
        this.avatar = avatar;
        this.point = point;
    }

    public IRoomAvatar getAvatar() {
        return avatar;
    }

    public IPoint getPoint() {
        return point;
    }
}
