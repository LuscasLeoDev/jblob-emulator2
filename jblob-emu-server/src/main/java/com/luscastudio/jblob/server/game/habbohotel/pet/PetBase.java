package com.luscastudio.jblob.server.game.habbohotel.pet;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 24/01/2017 at 19:38.
 */
public class PetBase {
    /* note: Pets Base
        The Max experience, Max Energy and Max Happiness changes each level the pet is
        Example:
            lvl 1 :
                Max Energy: 100
                Max Experience: 100
                Max Happiness: 100

            lvl 2:
                Max Energy: 200
                Max Experience: 200
                Max Happiness: 200

        Doubts:
            Does it have to be a PetBase for each Race?

     */

    private int id;
    private int petType;

    private int rarityLevel;

    private int maxLevels;

    //      Map<Level, Value>
    private Map<Integer, Integer> maxExperience;
    private Map<Integer, Integer> maxEnergy;
    private Map<Integer, Integer> maxHappiness;

    //      Map<Value, Level>
    private Map<Integer, Integer> commandList;

    //Helpers in case of error
    private int lastMaxExperience;
    private int lastMaxEnergy;

    private int lastMaxHappiness;
    private int raceId;

    private String petBehavior;

    public PetBase(int id, int petType, int raceId, int rarityLevel, int maxLevels, Map<Integer, Integer> maxExperience, Map<Integer, Integer> maxEnergy,Map<Integer, Integer> maxHappiness, Map<Integer, Integer> commandList, String petBehavior) {
        this.id = id;
        this.raceId = raceId;
        this.rarityLevel = rarityLevel;
        this.petType = petType;

        this.maxLevels = maxLevels;

        this.maxExperience = maxExperience;
        this.maxEnergy = maxEnergy;
        this.maxHappiness = maxHappiness;

        this.commandList = commandList;

        this.petBehavior = petBehavior;
    }

    public int getId() {
        return id;
    }

    public int getMaxLevels() {
        return maxLevels;
    }

    public int getRarityLevel() {
        return rarityLevel;
    }

    public int getPetType() {
        return petType;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getMaxHappiness(int level) {
        return this.maxHappiness.containsKey(level) ? this.maxHappiness.get(level) : lastMaxHappiness;
    }

    public int getMaxEnergy(int level) {
        return this.maxEnergy.containsKey(level) ? this.maxEnergy.get(level) : lastMaxEnergy;
    }

    public int getLastMaxExperience(int level) {
        return this.maxExperience.containsKey(level) ? this.maxExperience.get(level) : lastMaxExperience;
    }

    public List<Integer> getCommandsForLevel(int level) {
        List<Integer> commands = BCollect.newList();

        for(Map.Entry<Integer, Integer> cmd : this.commandList.entrySet()){
            if(cmd.getValue() <= (level))
                commands.add(cmd.getKey());
        }

        return commands;
    }

    public String getPetBehavior() {
        return petBehavior;
    }

    public Collection<Integer> getCommands() {
        return this.commandList.keySet();
    }
}
