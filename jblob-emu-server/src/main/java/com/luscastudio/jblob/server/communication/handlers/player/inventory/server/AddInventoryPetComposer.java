package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;

/**
 * Created by Lucas on 31/01/2017 at 14:13.
 */
public class AddInventoryPetComposer extends MessageComposer {
    @Override
    public String id() {
        return "AddInventoryPetMessageComposer";
    }

    public AddInventoryPetComposer(PetData petData) {

        ItemSerializer.serializeInventoryPet(message, petData);

        message.putBool(false);
    }
}
