package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 01/10/2016.
 */
public class HomeRoomComposer extends MessageComposer {
    @Override
    public String id() {
        return "NavigatorSettingsMessageComposer";
    }

    public HomeRoomComposer(int home1, int home2) {
        message.putInt(home1);
        message.putInt(home2);
    }
}
