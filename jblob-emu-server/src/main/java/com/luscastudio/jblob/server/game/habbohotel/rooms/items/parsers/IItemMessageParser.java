package com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers;

import com.luscastudio.jblob.server.communication.server.PacketWriting;

/**
 * Created by Lucas on 10/10/2016.
 */
public interface IItemMessageParser {

    void parse(PacketWriting message);

    int getCode();
}
