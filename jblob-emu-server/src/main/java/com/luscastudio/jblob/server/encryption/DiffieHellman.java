package com.luscastudio.jblob.server.encryption;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created by Lucas on 27/09/2016.
 */
public class DiffieHellman {

    private BigInteger generator;
    private BigInteger prime;
    private BigInteger publicKey;
    private BigInteger privateKey;

    private BigInteger sharedKey;
    private BigInteger publicClientKey;

    public DiffieHellman(String publicKey, int exp, int gen, int mod) {

    }

    public DiffieHellman() {

        Random r = new Random();
        this.publicKey = BigInteger.valueOf(0L);

        while (this.publicKey.intValue() == 0) {
            prime = BigInteger.probablePrime(30, r);
            generator = BigInteger.probablePrime(30, r);
            privateKey = BigInteger.probablePrime(30, r);

            if (this.privateKey.intValue() > 0) {
                if (prime.intValue() < generator.intValue()) {
                    BigInteger t = prime;
                    prime = generator;
                    generator = t;
                }
            }

            publicKey = generator.modPow(this.privateKey, prime);
        }

    }

    public BigInteger getGenerator() {
        return generator;
    }

    public BigInteger getPrime() {
        return prime;
    }

    public BigInteger getPublicKey() {
        return publicKey;
    }

    public BigInteger getSharedKey() {
        return sharedKey;
    }

    public void generateSharedKey(String ckey) {
        this.publicClientKey = new BigInteger(ckey);
        this.sharedKey = this.publicClientKey.modPow(this.privateKey, this.prime);
    }

}
