package com.luscastudio.jblob.server.game.habbohotel.rooms.process;

import com.luscastudio.jblob.api.utils.time.DateTimeUtils;

/**
 * Created by Lucas on 12/01/2017 at 17:38.
 */
public class RunProcess {

    private Runnable run;
    private boolean loop;

    private double delay;
    private double interval;

    private boolean done;
    private boolean cancelled;

    private double pDelayTsp;
    private double pIntervalTsp;


    public RunProcess(Runnable run, int delay, int interval){
        this.run = run;
        this.delay = (double)delay / 1000;
        this.interval = (double)interval / 1000;

        this.loop = true;
        this.done = false;

        this.pDelayTsp = DateTimeUtils.getUnixTimestamp() + this.delay;
        this.pIntervalTsp = 0;
    }

    public RunProcess(Runnable run, int delay){
        this.run = run;
        this.delay = (double)delay / 1000;

        this.loop = this.done = false;

        this.pDelayTsp = DateTimeUtils.getUnixTimestamp() + this.delay;
        this.pIntervalTsp = 0;
    }

    public Runnable getRun() {
        return run;
    }

    public int getDelay() {
        return (int)delay * 1000;
    }

    public boolean isLoop() {
        return loop;
    }

    public void doCycle() {
        /*if (this.pDelay > 0)
            pDelay--;
        else if(pInterval > 0){
            pInterval--;
        }
        else {
            this.run.run();
            if(!this.loop)
                this.done = true;

            pInterval = interval;
        }*/

        if(this.pDelayTsp > DateTimeUtils.getUnixTimestamp())
            return;
        if(this.pIntervalTsp > DateTimeUtils.getUnixTimestamp())
            return;

        this.run.run();
        if(!this.loop)
            this.done = true;
        else
            this.pIntervalTsp = DateTimeUtils.getUnixTimestamp() + this.interval;

    }

    public boolean isEnd() {
        return this.done || this.cancelled;
    }

    public boolean isCancelled(){
        return this.cancelled;
    }

    public void cancel(){
        this.cancelled = true;
    }

    public void pause(int time) {
        this.pDelayTsp = DateTimeUtils.getUnixTimestamp() + (double)time / 1000;
    }
}
