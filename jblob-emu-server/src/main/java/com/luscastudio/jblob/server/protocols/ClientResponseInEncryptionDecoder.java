package com.luscastudio.jblob.server.protocols;

import com.luscastudio.jblob.server.encryption.RC4;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Created by Lucas on 30/09/2016.
 */
public class ClientResponseInEncryptionDecoder extends ByteToMessageDecoder {
    private final RC4 rc4;

    public ClientResponseInEncryptionDecoder(byte[] key) {
        this.rc4 = new RC4(key);
    }

    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        out.add(this.rc4.decipher(in));
    }
}
