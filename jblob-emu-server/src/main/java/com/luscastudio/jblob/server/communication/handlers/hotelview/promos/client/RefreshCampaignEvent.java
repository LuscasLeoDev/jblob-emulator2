package com.luscastudio.jblob.server.communication.handlers.hotelview.promos.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.hotelview.promos.server.CampaignComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 01/10/2016.
 */
public class RefreshCampaignEvent implements IMessageEventHandler {

    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        String value = packet.getString();
        String campaingName = "";
        String[] parser = value.split(";");

        for (int i = 0; i < parser.length; i++) {
            if (parser[i].isEmpty() || parser[i].endsWith(","))
                continue;
            try {
                String[] data = parser[i].split(",");
                campaingName = data[1];
            } catch (Exception ignored){

            }
        }

        session.sendMessage(new CampaignComposer(value, campaingName));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
