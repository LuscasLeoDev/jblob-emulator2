package com.luscastudio.jblob.server.database;

import com.luscastudio.jblob.server.debug.BLogger;

/**
 * Created by Lucas on 11/11/2016.
 */

public abstract class IMultiAsyncQuery implements IQueryRunnable {

    abstract DBConnReactor getReactor();

    abstract IMultiAsyncQueryCallBack getCallback();

    @Override
    public void run() {

        try {
            getCallback().run(getReactor());
            getReactor().close();
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

    }
}
