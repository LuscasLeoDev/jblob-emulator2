package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.RoomItemTonerEnabledEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.TonerRoomItemExtradata;

/**
 * Created by Lucas on 20/02/2017 at 19:46.
 */
public class TonerRoomItem extends SimpleRoomItem {
    private TonerRoomItemExtradata extradata;
    private EventDelegate delegate;
    public TonerRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.extradata = (TonerRoomItemExtradata)properties.getExtradata();
        this.delegate = room.getEventHandler().on("room.item.toner.enabled", this::onEvent);
    }

    private void onEvent(EventDelegate eventDelegate, EventArgs args) {
        switch(args.getEventKey()) {
            case "room.item.toner.enabled":
                RoomItemTonerEnabledEventArgs eventArgs = (RoomItemTonerEnabledEventArgs) args;
                if (eventArgs.getToner() == this)
                    break;
                this.extradata.setEnabled(0);
                this.room.getRoomItemHandlerService().updateItem(this, false, true);
                this.room.getRoomItemHandlerService().saveItem(this);
                break;
        }
    }

    public void setTonerData(int hue, int saturation, int lightness) {
        this.extradata.setTonerData(hue, saturation, lightness);
    }

    @Override
    public void toggle() {
        this.extradata.setEnabled(this.extradata.getEnabled() == 1 ? 0 : 1);
        if(this.extradata.getEnabled() == 1)
            this.room.getEventHandler().fireEvent("room.item.toner.enabled", new RoomItemTonerEnabledEventArgs(this));
        this.room.getRoomItemHandlerService().updateItem(this, false, true);

        this.room.getRoomItemHandlerService().saveItem(this);
    }

    @Override
    public TonerRoomItemExtradata getExtradata() {
        return extradata;
    }

    @Override
    public void dispose() {
        this.delegate.cancel();
        super.dispose();
    }
}
