package com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.PacketWriting;

import java.util.Map;

/**
 * Created by Lucas on 25/02/2017 at 21:38.
 */
public class MapStuffDataParser implements IItemMessageParser {

    private Map<String, String> stuffData;

    public MapStuffDataParser(){
        this.stuffData = BCollect.newMap();
    }

    public void setStuffData(Map<String, String> stuffData) {
        this.stuffData = stuffData;
    }

    @Override
    public void parse(PacketWriting message) {
        message.putInt(this.stuffData.size());
        for (Map.Entry<String, String> entry : this.stuffData.entrySet()) {
            message.putString(entry.getKey());
            message.putString(entry.getValue());
        }
    }

    @Override
    public int getCode() {
        return 1;//MapStuffData
    }
}
