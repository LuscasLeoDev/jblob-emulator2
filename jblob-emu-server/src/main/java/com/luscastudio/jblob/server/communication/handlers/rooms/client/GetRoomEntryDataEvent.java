package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.CloseFlatConnectionComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 03/10/2016.
 */

public class GetRoomEntryDataEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom(false);

        if (room == null) {
            session.sendMessage(new CloseFlatConnectionComposer());
            return;
        }

        //session.sendQueueMessage(new RoomSpectatorModeComposer());

        RoomPlayerAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());

        room.getRoomAvatarService().addAvatarEnteringPacketQueue(avatar);

        ///oomManager.serializeRoomData(session, room);
        room.sendQueuedObjects(session);


        room.flush();

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
