package com.luscastudio.jblob.server.game.players.authentication;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.accounts.PlayerAccount;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import org.apache.log4j.Logger;

import java.sql.ResultSet;

/**
 * Created by Lucas on 01/10/2016.
 */
public class PlayerAuthenticator {

    public static Logger log = Logger.getLogger(PlayerAuthenticator.class);

    public static int tryAuthenticate(String ssoTicket, PlayerSession session) {

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM players_session WHERE sso_ticket = ? ");

            prepare.setString(1, ssoTicket);

            ResultSet set = prepare.runQuery();

            if (set == null) {
                log.error("Could not get rows data from query");
                return 2;
            }

            HabboAvatar avatar;
            PlayerAccount account;

            if (!set.next()) //get the first row
                return 1;

            int accountId = set.getInt("acc_id");
            int avatarId = set.getInt("target_avatar_id");

            PlayerSession activeSession = JBlob.getGame().getSessionManager().getSession(avatarId);
            if(activeSession != null){
                activeSession.sendMessage(new BubbleNotificationComposer("handshake.error.account.already.online"));
                return 6;
            }

            prepare = reactor.prepare("SELECT * FROM players_avatar WHERE id = ?");
            prepare.setInt(1, avatarId);
            set = prepare.runQuery();

            if (!set.next())
                return 4;

            avatar = JBlob.getGame().getUserCacheManager().createHabboAvatar(reactor, set, session, ssoTicket);

            if (avatar == null)
                return 5;

            session.setHabboAvatar(avatar);

            JBlob.getGame().getSessionManager().onSessionAuthenticated(session);

            return 0;

            /*prepare = reactor.prepare("SELECT * FROM players_account WHERE id = ?");
            prepare.setInt(1, accountId);
            set = prepare.runQuery();
            account = createPlayerAccount(set, session, ssoTicket);*/


            //avatar = createHabboAvatar(set, session, ssoTicket);

        } catch (Exception e) {
            BLogger.error(e, PlayerAuthenticator.class);
            return 3;
        }
    }


}

