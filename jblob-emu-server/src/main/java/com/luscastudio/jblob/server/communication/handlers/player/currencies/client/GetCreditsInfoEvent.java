package com.luscastudio.jblob.server.communication.handlers.player.currencies.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CreditBalanceComposer;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CurrencyComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 07/12/2016.
 */

public class GetCreditsInfoEvent implements IMessageEventHandler {

    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new CreditBalanceComposer(session.getAvatar().getBalance().get(1)));

        session.sendMessage(new CurrencyComposer(session.getAvatar().getBalance().getList()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
