package com.luscastudio.jblob.server.communication.handlers.handshake.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.permissions.server.UserPerksComposer;
import com.luscastudio.jblob.server.communication.handlers.player.server.PlayerSessionDataComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 01/10/2016.
 */
public class InfoRetrieveEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        session.sendQueueMessage(new PlayerSessionDataComposer(session.getAvatar()));
        session.sendQueueMessage(new UserPerksComposer()).flush();
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
