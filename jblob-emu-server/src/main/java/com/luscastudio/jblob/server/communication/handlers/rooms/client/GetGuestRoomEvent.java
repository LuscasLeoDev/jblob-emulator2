package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.GuestRoomResultComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 03/10/2016.
 */

public class GetGuestRoomEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int roomId = packet.getInt();

        boolean isLoading = packet.getInt() == 1;
        boolean checkEntry = packet.getInt() == 1;

        RoomProperties room = JBlob.getGame().getRoomManager().getRoomData(roomId);

        //todo: set static variable "room_right_mute"
        if (room == null)
            return;

        session.sendQueueMessage(new GuestRoomResultComposer(isLoading, checkEntry, room, room.avatarHasRight(session.getAvatar().getId(), "room_right_mute")));

        session.flush();
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
