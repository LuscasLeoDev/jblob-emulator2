package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomSettingsDataComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/12/2016.
 */

public class GetRoomSettingsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int roomId = packet.getInt();

        RoomProperties roomData = JBlob.getGame().getRoomManager().getRoomData(roomId);

        if(roomData == null){
            //session.sendSimpleAlert("Room Not Found!");
            return;
        }

        session.sendMessage(new RoomSettingsDataComposer(roomData, true));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
