package com.luscastudio.jblob.server.game.players;

/**
 * Created by Lucas on 01/10/2016.
 */
public class MachineId {

    private String deviceId;
    private String fingerPrint;
    private String osInfo;

    public MachineId(String device, String finger, String OS) {
        this.deviceId = device;
        this.fingerPrint = finger;
        this.osInfo = OS;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getFingerPrint() {
        return fingerPrint;
    }

    public String getOsInfo() {
        return osInfo;
    }
}
