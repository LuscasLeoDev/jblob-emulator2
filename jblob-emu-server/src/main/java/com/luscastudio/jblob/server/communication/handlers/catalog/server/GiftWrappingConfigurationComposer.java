package com.luscastudio.jblob.server.communication.handlers.catalog.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.List;

/**
 * Created by Lucas on 31/01/2017 at 18:15.
 */
public class GiftWrappingConfigurationComposer extends MessageComposer {
    @Override
    public String id() {
        return "GiftWrappingConfigurationMessageComposer";
    }

    public GiftWrappingConfigurationComposer(int price,
                                             List<Integer> giftsWrapping,
                                             List<Integer> giftsWrappingDecoration,
                                             List<Integer> oldGiftsWrapping,
                                             List<Integer> oldGiftsWrappingDecoration){

        message.putBool(false); //?
        message.putInt(price); //Gift Wraps Price

        message.putInt(giftsWrapping.size()); //New Gift Types count 1
            giftsWrapping.forEach(message::putInt);

        message.putInt(oldGiftsWrappingDecoration.size()); //Old gift types count count 2
        oldGiftsWrappingDecoration.forEach(message::putInt);

        message.putInt(giftsWrappingDecoration.size()); //New Gift Types Wrap Decoration count 3
        giftsWrappingDecoration.forEach(message::putInt);

        message.putInt(oldGiftsWrapping.size()); //Old Gift Types Wrap Decoration count 4
        oldGiftsWrapping.forEach(message::putInt);


    }
}
