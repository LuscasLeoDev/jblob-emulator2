package com.luscastudio.jblob.server.game.habbohotel.chat.commands.list;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.chat.commands.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.chat.commands.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 16/02/2017 at 21:47.
 */
public class MakeBundleCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        int bundleId = JBlob.getGame().getCatalogManager().generateCatalogDeal(room);

        session.sendMessage(new BubbleNotificationComposer("debug", "Generated.\nRoom Bundle Id: " + bundleId));

        return true;
    }
}
