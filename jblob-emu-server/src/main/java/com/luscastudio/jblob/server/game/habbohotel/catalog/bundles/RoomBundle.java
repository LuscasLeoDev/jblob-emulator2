package com.luscastudio.jblob.server.game.habbohotel.catalog.bundles;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Lucas on 16/02/2017 at 17:11.
 */
public class RoomBundle {

    private int id;
    private String name;
    private String description;

    private String modelName;

    private int lockType;
    private String password;

    private int maxUsers;

    private int tradeType;

    private String wallData;
    private String floorData;
    private String landscapeData;

    private int allowPets;
    private int allowPetsEating;
    private int hideWall;
    private int wallThickness;
    private int floorThickness;
    private int allowWalkTrough;

    private int whoCanMute;
    private int whoCanKick;
    private int whoCanBan;

    private int chatMode;
    private int chatSize;
    private int chatSpeed;
    private int chatExtraFlood;
    private int chatDistance;

    private Map<Integer, BundleFurniProperties> bundleItems;

    public RoomBundle(ResultSet set) throws SQLException {

        this.bundleItems = BCollect.newConcurrentMap();

        this.id = set.getInt("id");
        this.name = set.getString("name");
        this.description = set.getString("description");
        this.modelName = set.getString("model_name");
        this.lockType = set.getInt("lock_type");
        this.password = set.getString("password");
        this.maxUsers = set.getInt("max_users");
        this.tradeType = set.getInt("trade_type");

        this.wallData = set.getString("wallpaper_data");
        this.floorData = set.getString("floor_data");
        this.landscapeData = set.getString("landscape_data");

        this.allowPets = set.getInt("allow_pets");
        this.allowPetsEating = set.getInt("allow_pets_eating");

        this.hideWall = set.getInt("hide_wall");
        this.wallThickness = set.getInt("wall_thickness");
        this.floorThickness = set.getInt("floor_thickness");

        this.allowWalkTrough = set.getInt("allow_walk_trough");

        this.whoCanMute = set.getInt("who_can_mute");
        this.whoCanKick = set.getInt("who_can_kick");
        this.whoCanBan = set.getInt("who_can_ban");

        this.chatMode = set.getInt("chat_mode");
        this.chatSize = set.getInt("chat_size");
        this.chatSpeed = set.getInt("chat_speed");
        this.chatExtraFlood = set.getInt("chat_extra_flood");
        this.chatDistance = set.getInt("chat_distance");

    }

    public Map<Integer, BundleFurniProperties> getBundleItems() {
        return bundleItems;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getModelName() {
        return modelName;
    }

    public int getLockType() {
        return lockType;
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    public int getTradeType() {
        return tradeType;
    }

    public String getFloorData() {
        return floorData;
    }

    public String getLandscapeData() {
        return landscapeData;
    }

    public String getPassword() {
        return password;
    }

    public String getWallData() {
        return wallData;
    }

    public int getAllowPets() {
        return allowPets;
    }

    public int getAllowPetsEating() {
        return allowPetsEating;
    }

    public int getFloorThickness() {
        return floorThickness;
    }

    public int getHideWall() {
        return hideWall;
    }

    public int getWallThickness() {
        return wallThickness;
    }

    public int getAllowWalkTrough() {
        return allowWalkTrough;
    }

    public int getChatDistance() {
        return chatDistance;
    }

    public int getChatExtraFlood() {
        return chatExtraFlood;
    }

    public int getChatMode() {
        return chatMode;
    }

    public int getChatSize() {
        return chatSize;
    }

    public int getChatSpeed() {
        return chatSpeed;
    }

    public int getWhoCanBan() {
        return whoCanBan;
    }

    public int getWhoCanKick() {
        return whoCanKick;
    }

    public int getWhoCanMute() {
        return whoCanMute;
    }

}
