package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;

/**
 * Created by Lucas on 02/10/2016.
 */

public class NavigatorPreferencesComposer extends MessageComposer {
    public NavigatorPreferencesComposer(AvatarPreferences preferences) {
        this.message.putInt(preferences.getIntOrDefault("navigator.x", 68)); //X
        this.message.putInt(preferences.getIntOrDefault("navigator.y", 42)); //Y
        this.message.putInt(preferences.getIntOrDefault("navigator.width", 300)); //Width
        this.message.putInt(preferences.getIntOrDefault("navigator.width", 300)); //Height
        this.message.putBool(preferences.getBoolOrDefault("navigator.searches.collapsed", false)); //Saved Searches collapsed
        this.message.putInt(10); //Unknown
    }

    @Override
    public String id() {
        return "NavigatorPreferencesMessageComposer";
    }
}
