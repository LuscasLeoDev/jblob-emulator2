//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 11/06/2017 at 22:13.
 */
public class RoomRightsListComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomRightsListMessageComposer";
    }

    public RoomRightsListComposer(int roomId, Collection<IAvatarDataCache> users){
        message.putInt(roomId);

        message.putInt(users.size());
        for (IAvatarDataCache user : users) {
            message.putInt(user.getId());
            message.putString(user.getUsername());
        }
    }
}
