package com.luscastudio.jblob.server.communication.client.builds;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.io.BFileReader;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.encryption.RSA;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Map;

/**
 * Created by Lucas on 01/10/2016.
 */
public class ClientBuildManager implements IFlushable {
    public Map<String, ClientBuild> builds;
    Logger logger = Logger.getLogger(this.getClass().toString());

    public ClientBuildManager() {
        builds = BCollect.newMap();
    }

    private int lastUpdate = 0;

    public ClientBuild getBuild(String name) {
        if (!builds.containsKey(name))
            return null;

        return this.builds.get(name);
    }

    //region adding
    public void flush() throws Exception{

        this.lastUpdate = (int) DateTimeUtils.getTimeUnit();

        builds.clear();

        File info = new File("./packets");

        if(!info.exists())
            throw new Exception("packets folder doesn't exists!");

        String[] directories = info.list((dir, name) -> dir.isDirectory());

        for (String dir : directories) {
            ClientBuild build;
            File filename = new File("packets/" + dir);

            //I think this won't happen
            if (builds.containsKey(filename.getName()))
                continue;

            if ((build = tryCreateBuild(filename)) == null)
                continue;

            this.builds.put(build.getBuildName(), build);

        }

    }

    @Override
    public void performFlush() {

    }

    public ClientBuild tryCreateBuild(File directory) {


        String dir = directory.getAbsolutePath();
        String name = directory.getName();
        Map<Short, String> incomings = parseMap(getPairs(BFileReader.getFileContent(dir + "/incoming.packet")));
        Map<String, Short> outgoing = parseMapReverse(getPairs(BFileReader.getFileContent(dir + "/outgoing.packet")));
        Map<String, String> rsa = getPairs(BFileReader.getFileContent(dir + "/rsakey.packet"));
        Map<String, String> localization = getPairs(BFileReader.getFileContent(dir + "/localization.packet"));

        if (rsa == null)
            return null;

        if (!rsa.containsKey("N") || !rsa.containsKey("D") || !rsa.containsKey("E")) {
            return null;

        }

        if (incomings == null) {
            return null;
        }
        if (outgoing == null) {
            return null;
        }
        if (localization == null)
            return null;

        return new ClientBuild(new RSA(rsa.get("N"), rsa.get("E"), rsa.get("D")), name, incomings, outgoing, lastUpdate);

    }

    public Map<Short, String> parseMap(Map<String, String> map) {
        Map<Short, String> r = BCollect.newMap();

        for (Map.Entry<String, String> pair : map.entrySet()) {

            String key = pair.getKey();
            String valuestr = pair.getValue();

            if (!NumberHelper.isInteger(valuestr)) {
                BLogger.warn("Invalid Header '" + valuestr + "'", this.getClass());
                continue;
            }

            short value = Short.parseShort(valuestr);

            r.put(value, key);

        }

        return r;
    }

    public Map<String, Short> parseMapReverse(Map<String, String> map) {
        Map<String, Short> r = BCollect.newMap();

        for (Map.Entry<String, String> pair : map.entrySet()) {

            String key = pair.getKey();
            String valuestr = pair.getValue();

            if (!NumberHelper.isInteger(valuestr)) {
                BLogger.warn("Invalid Header '" + valuestr + "'", this.getClass());
                continue;
            }

            short value = Short.parseShort(valuestr);

            r.put(key, value);

        }

        return r;
    }

    public Map<String, String> getPairs(String content) {

        content = content.replace("\r", "");
        Map<String, String> data = BCollect.newMap();

        for (String line : content.split("\n")) {

            if (line.startsWith("#") || line.startsWith("["))
                continue;

            //line = line.replace("#", "").replace("/", "");

            if (line.indexOf('=') == -1)
                continue;

            String key = line.substring(0, line.indexOf('='));

            if (data.containsKey(key)) {
                BLogger.warn("Header '" + key+ "' already is registered", this.getClass());
                continue;
            }


            String value = line.substring(line.indexOf('=') + 1).split(",")[0].split("/")[0].trim();


            //int val = Integer.parseInt(value);

            data.put(key, value);
        }

        return data;
    }

    public int getBuildCount() {
        return builds.size();
    }

}
