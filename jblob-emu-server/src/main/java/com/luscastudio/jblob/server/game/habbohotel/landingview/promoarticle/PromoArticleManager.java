package com.luscastudio.jblob.server.game.habbohotel.landingview.promoarticle;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.hotelview.promos.server.PromoArticlesComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Lucas on 14/02/2017 at 19:41.
 */
public class PromoArticleManager implements IFlushable {

    private Map<Integer, PromoArticleItem> articleItemMap;

    public PromoArticleManager(){
        this.articleItemMap = BCollect.newConcurrentMap();
    }

    @Override
    public void flush() throws Exception {
        this.articleItemMap.clear();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("SELECT * FROM server_hotelview_promoarticles ORDER BY order_id, id LIMIT ?");
            prepare.setInt(1, JBlob.getGame().getDbConfig().getInt("landingview.promoarticle.article.count", 15));

            ResultSet set = prepare.runQuery();

            while(set.next()){
                this.articleItemMap.put(set.getInt("id"), new PromoArticleItem(set.getInt("id"), set.getString("title"), set.getString("context"), set.getString("image_url"), set.getString("button_text"), set.getString("button_link"), set.getInt("button_type")));
            }

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    public Collection<PromoArticleItem> getArticleList(){
        return this.articleItemMap.values();
    }

    @Override
    public void performFlush() {
        JBlob.getGame().getSessionManager().sendMessage(new PromoArticlesComposer(this.getArticleList()));
    }
}
