//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.catalog.gifts.handlers;

import com.google.common.collect.Multimap;
import com.luscastudio.jblob.api.utils.engine.IObjectPosition;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.database.QueryHelper;
import com.luscastudio.jblob.server.database.dao.rooms.items.RoomItemHandlerDao;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 30/06/2017 at 00:45.
 */
public class MultiFurniGiftHandler implements IGiftHandler {
    @Override
    public boolean validate(Multimap<String, Integer> itemsTypesMap) {
        return itemsTypesMap.size() > 1 && itemsTypesMap.keySet().size() == 1 && itemsTypesMap.keys().contains("furni");
    }

    @Override
    public boolean handle(PlayerSession session, Multimap<String, Integer> itemsTypesMap, Room room, IObjectPosition giftPosition) {
        List<FurniProperties> furniProperties = JBlob.getGame().getRoomItemFactory().getFurniProperties(itemsTypesMap.get("furni"));

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare(String.format("UPDATE avatars_items_data SET user_id = ? WHERE id IN %s", QueryHelper.makeArrayValues("?", furniProperties.size())));

            prepare.setInt(session.getAvatar().getId());
            for (FurniProperties property : furniProperties) {
                prepare.setInt(property.getId());
            }

            prepare.runUpdate();

            InventoryNotificationComposer notificationComposer = new InventoryNotificationComposer();
            for (FurniProperties property : furniProperties) {
                session.getAvatar().getInventory().addFurni(property);
                notificationComposer.add(InventoryNotificationComposer.FURNI, property.getId());
            }

            session.sendQueueMessage(new InventoryUpdateRequestComposer());
            session.sendQueueMessage(notificationComposer);
            session.flush();

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return true;
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
