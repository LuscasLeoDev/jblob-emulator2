package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.statusses;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;

import java.util.Map;

/**
 * Created by Lucas on 03/10/2016.
 */

public class RoomAvatarStatusses {
    private Map<String, RoomAvatarStatus> statuses;

    public RoomAvatarStatusses() {
        this.statuses = BCollect.newConcurrentMap();
    }

    public Map<String, RoomAvatarStatus> getStatuses() {
        return this.statuses;
    }

    public void set(String key, String val, int expireSec) {
        int time = expireSec;
        if(time >= 0)
            time = DateTimeUtils.getUnixTimestampInt() + expireSec;
        else
            time = expireSec;
        if (this.statuses.containsKey(key))
            this.statuses.replace(key, new RoomAvatarStatus(key, val, time));
        else
            this.statuses.put(key, new RoomAvatarStatus(key, val, time));
    }

    public void set(String key, String val){
        this.set(key, val, -1);
    }

    public void clear() {
        this.statuses.clear();
    }

    public void remove(String key) {
        if (this.statuses.containsKey(key))
            this.statuses.remove(key);
    }

    public boolean hasStatus(String key) {
        return this.statuses.containsKey(key);
    }

    public void dispose() {
        this.statuses.clear();
    }
}
