package com.luscastudio.jblob.server.process;

import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 08/12/2016.
 */

public class HabboAvatarExecutorService implements Runnable {

    private final HabboAvatar avatar;

    private ScheduledFuture scheduledFuture;

    private boolean saving;

    public HabboAvatarExecutorService(HabboAvatar avatar){
        this.avatar = avatar;
        this.saving = false;
    }

    public void start(){
        this.scheduledFuture = JBlob.getGame().getProcessManager().runLoop(this, JBlobSettings.AVATAR_EXECUTOR_CYCLE_INTERVAL, JBlobSettings.AVATAR_EXECUTOR_CYCLE_INTERVAL, TimeUnit.MILLISECONDS);
    }

    public boolean isSaving() {
        return saving;
    }

    public void stop(boolean interrupt){
        this.scheduledFuture.cancel(interrupt);
    }

    @Override
    public void run() {
        this.saving = true;
        this.save();
        this.saving = false;
    }

    public synchronized void save(){
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            this.avatar.getPreferences().save(reactor);
            this.avatar.getBalance().save(reactor);

            this.avatar.setLastOnlineTimestamp(DateTimeUtils.getUnixTimestampInt());
            DBConnPrepare prepare = reactor.prepare("UPDATE players_avatar SET" +
                    " motto = ?" +
                    ", figure_string = ?" +
                    ", gender = ?" +
                    ", home_room = ?" +
                    ", club_level = ?" +
                    ", respect_count = ?" +
                    ", respect_give_count = ?" +
                    ", respect_give_pets_count = ?" +
                    ", last_online_timestamp = ?" +
                    ", achievement_score = ?" +
                    " WHERE id = ?");

            prepare.setString(1, this.avatar.getMotto());
            prepare.setString(2, this.avatar.getFigure().toString());
            prepare.setString(3, this.avatar.getFigure().getGender());
            prepare.setInt(4, this.avatar.getHomeRoom());
            prepare.setInt(5, this.avatar.getClubLevel());
            prepare.setInt(6, this.avatar.getRespectCount());
            prepare.setInt(7, this.avatar.getRespectsToGiveCount());
            prepare.setInt(8, this.avatar.getRespectsToGiveToPetsCount());
            prepare.setInt(9, this.avatar.getLastOnlineTimestamp());
            prepare.setInt(10, this.avatar.getAchievementScore());
            prepare.setInt(11, this.avatar.getId());
            prepare.runUpdate();

        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }
}
