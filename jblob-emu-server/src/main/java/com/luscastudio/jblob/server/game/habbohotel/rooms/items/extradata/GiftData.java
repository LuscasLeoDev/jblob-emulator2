//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.google.common.collect.Multimap;
import com.google.common.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.json.JSONUtils;

/**
 * Created by Lucas on 28/06/2017 at 22:28.
 */
public class GiftData {

    private int userId;
    private String message;
    private int ribbon;
    private int wrapType;
    private boolean showSender;

    public GiftData(int userId, String message, int ribbon, int wrapType, boolean showSender) {
        this.userId = userId;
        this.message = message;
        this.ribbon = ribbon;
        this.wrapType = wrapType;
        this.showSender = showSender;
    }

    public int getUserId() {
        return userId;
    }

    public String getMessage() {
        return message;
    }

    public int getRibbon() {
        return ribbon;
    }

    public int getWrapType() {
        return wrapType;
    }

    public boolean isShowSender() {
        return showSender;
    }

    public String toJson() {
        return JSONUtils.toJson(this, new TypeToken<GiftData>(){}.getType());
    }
}
