package com.luscastudio.jblob.server.game.habbohotel.catalog;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 28/10/2016.
 */

public class CatalogNode {

    private int id;

    private String title;

    private String link;

    private int icon;

    private int parentId;

    private List<String> textSet;
    private List<String> imageSet;

    private Map<Integer, CatalogNode> children;

    private Map<Integer, CatalogDeal> deals;

    private String model;

    private boolean isBuilder;

    private String permission;

    private boolean enabled;

    private int orderNum;

    public CatalogNode(int id, String title, int icon, String link, int parentId,  List<String> textSet, List<String> imageSet, String model, boolean isBuilder, String permission, boolean enabled, int orderNum){

        this.id = id;
        this.title = title;

        this.icon =icon;
        this.link = link;

        this.parentId = parentId;

        this.textSet = textSet;
        this.imageSet = imageSet;

        this.children = BCollect.newLinkedMap();
        this.deals = BCollect.newLinkedMap();

        this.model = model;

        this.isBuilder = isBuilder;

        this.permission = permission;

        this.enabled = enabled;
        this.orderNum = orderNum;
    }

    public void setDeals(Map<Integer, CatalogDeal> deals) {
        this.deals = deals;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getIcon() {
        return icon;
    }

    public String getLink() {
        return link;
    }

    public List<String> getTextSet() {
        return textSet;
    }

    public List<String> getImageSet() {
        return imageSet;
    }

    public Map<Integer, CatalogNode> getChildren() {
        return children;
    }

    public Map<Integer, CatalogDeal> getDeals() {
        return deals;
    }

    public String getModel() {
        return model;
    }

    public int getParentId() {
        return parentId;
    }

    public boolean isBuilder() {
        return isBuilder;
    }

    public String getPermission() {
        return permission;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public CatalogDeal getDeal(int dealId) {
        if (!this.deals.containsKey(dealId))
            return null;
        return this.deals.get(dealId);
    }

    @Override
    public String toString() {
        return "#" + this.id + " " + this.title + " ##" + this.orderNum;
    }
}