//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;

/**
 * Created by Lucas on 08/03/2017 at 23:53.
 */
public class WiredSaveDataEventArgs extends EventArgs {
    private WiredSaveData saveData;
    private int itemId;

    public WiredSaveDataEventArgs(WiredSaveData saveData, int itemId) {
        this.saveData = saveData;
        this.itemId = itemId;
    }

    public WiredSaveData getSaveData() {
        return saveData;
    }

    public int getItemId() {
        return itemId;
    }
}
