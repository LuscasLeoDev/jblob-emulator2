package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 10/12/2016.
 */

public class RoomEventComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomEventMessageComposer";
    }

    public RoomEventComposer(){
        message.putInt(-1);
        message.putInt(-1);
        message.putString("");
        message.putInt(0);
        message.putInt(0);
        message.putString("");
        message.putString("");
        message.putInt(0);
        message.putInt(0);
        message.putInt(0);
    }
}

