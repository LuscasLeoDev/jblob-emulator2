//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.rooms.items.gifts.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.GiftRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 29/06/2017 at 19:25.
 */
public class OpenGiftEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int itemId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomItem roomItem = room.getRoomItemHandlerService().getRoomItemById(itemId);
        if(roomItem == null || !(roomItem instanceof GiftRoomItem))
            return;

        GiftRoomItem gift = (GiftRoomItem) roomItem;

        gift.openGift(session);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
