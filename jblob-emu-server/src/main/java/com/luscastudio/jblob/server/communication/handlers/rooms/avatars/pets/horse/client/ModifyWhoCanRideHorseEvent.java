package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.horse.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.horse.HorseRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 19/02/2017 at 23:12.
 */
public class ModifyWhoCanRideHorseEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int petId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();

        if(room == null)
            return;

        IRoomPetAvatar pet = room.getRoomAvatarService().getPet(petId);

        if(pet == null || !(pet instanceof HorseRoomPetAvatar))
            return;

        HorseRoomPetAvatar horse = (HorseRoomPetAvatar)pet;

        horse.getHorseData().setAnyoneCanRide(!horse.getHorseData().anyoneCanRide());

        horse.saveHorseData();
        room.getRoomAvatarService().saveAvatar(horse);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
