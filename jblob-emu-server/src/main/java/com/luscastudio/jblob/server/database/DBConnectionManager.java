package com.luscastudio.jblob.server.database;

import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.io.Configuration;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * Created by Lucas on 01/10/2016.
 */
public class DBConnectionManager implements IFlushable {

    private HikariDataSource sc;
    private Logger log = Logger.getLogger(this.getClass());
    private Configuration config;
    private DBConnectionAsyncQueriesHandler asyncQueriesHandler;

    public DBConnectionManager(Configuration config) {

        this.config = config;
    }

    public DBConnReactor getReactor() throws SQLException {
        return new DBConnReactor(this.sc.getConnection());
    }

    public void getAsyncResult(IMultiAsyncQueryCallBack callBack) throws SQLException {
        DBConnReactor reactor = getReactor();

        this.asyncQueriesHandler.enqueue(new IMultiAsyncQuery() {
            @Override
            public DBConnReactor getReactor() {
                return reactor;
            }

            @Override
            public IMultiAsyncQueryCallBack getCallback() {
                return callBack;
            }
        });
    }


    /*public void getAsyncResult(String query, IASyncQueryCallback callback) throws SQLException {

        DBConnPrepare prepare = new DBConnPrepare(query, this.sc.getConnection().prepareStatement(sql)

        callback.prepare(prepare);

        this.asyncQueriesHandler.enqueue(new IASyncQuery() {
            @Override
            public DBConnPrepare getPrepare() {
                return prepare;
            }

            @Override
            public IASyncQueryCallback getCallback() {
                return callback;
            }
        });
    }*/

    @Override
    public void flush() {
        try {

            //HikariConfig config = new HikariConfig();
            HikariConfig ds = new HikariConfig();
            //ds.setMaximumPoolSize(10);
            //ds.setDriverClassName("com.mysql.jdbc.Driver");
            ds.setJdbcUrl("jdbc:mysql://"
                    + config.get("database.connection.host")
                    + ":" + config.get("database.connection.port")
                    + "/" + config.get("database.connection.name")
                    + "?tcpKeepAlive="
                    + config.getBool("database.connection.keepalive")
                    + "&autoReconnect="
                    + config.getBool("database.connection.autoreconnect"));

            ds.setUsername(config.get("database.connection.username"));
            ds.setPassword(config.get("database.connection.password"));
            ds.addDataSourceProperty("cachePrepStmts", "true");
            ds.addDataSourceProperty("prepStmtCacheSize", "250");
            ds.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            ds.addDataSourceProperty("useServerPrepStmts", "true");

            this.sc = new HikariDataSource(ds);

            this.asyncQueriesHandler = new DBConnectionAsyncQueriesHandler();
            this.asyncQueriesHandler.init();

        } catch (Exception e) {
            log.error("Could not start the connection with database!", e);
            System.exit(-1);
        }
    }

    @Override
    public void performFlush() {

    }

    public void close() {
        sc.close();
    }
}
