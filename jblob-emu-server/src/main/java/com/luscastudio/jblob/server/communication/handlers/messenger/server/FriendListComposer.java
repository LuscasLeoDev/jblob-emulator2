package com.luscastudio.jblob.server.communication.handlers.messenger.server;

import com.luscastudio.jblob.api.utils.validator.ValueGetter;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.friendship.AvatarFriendshipItem;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.Collection;

/**
 * Created by Lucas on 02/10/2016.
 */
public class FriendListComposer extends MessageComposer {
    @Override
    public String id() {
        return "BuddyListMessageComposer";
    }

    public FriendListComposer(int pageCount, int pageIndex, Collection<Integer> friendIds, ValueGetter<Integer, IAvatarDataCache> cacheGetter, ValueGetter<Integer, Short> friendShipGetter) {

        message.putInt(pageCount); //Friend Page Count
        message.putInt(pageIndex); //Friend Page Index

        message.putInt(friendIds.size()); //Friend Count

        for (Integer friendId : friendIds){

            IAvatarDataCache cache = cacheGetter.getValue(friendId);

            if (cache == null)
                cache = JBlob.getGame().getUserCacheManager().getGenericError();

            FriendListUpdater.serializeFriend(message, cache, friendShipGetter.getValue(friendId));
        }
    }
}
