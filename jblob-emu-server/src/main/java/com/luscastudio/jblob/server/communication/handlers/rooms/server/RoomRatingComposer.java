package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class RoomRatingComposer extends MessageComposer {
    public RoomRatingComposer(int score, boolean canVote) {
        message.putInt(score);
        message.putBool(canVote);
    }

    @Override
    public String id() {
        return "RoomRatingMessageComposer";
    }
}
