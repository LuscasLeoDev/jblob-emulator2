package com.luscastudio.jblob.server.communication.handlers.handshake.client;

import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.*;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.preferences.server.AvatarPreferencesComposer;
import com.luscastudio.jblob.server.communication.handlers.player.server.BuildersClubMembershipComposer;
import com.luscastudio.jblob.server.communication.handlers.player.server.FavouritesComposer;
import com.luscastudio.jblob.server.communication.handlers.player.server.UserRightsComposer;
import com.luscastudio.jblob.server.game.players.authentication.PlayerAuthenticator;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 30/09/2016.
 */
public class SSOTicketEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        String sso = packet.getString();

        //region SUBMITTING
        //todo: implement this event
        session.sendQueueMessage(new AuthenticationOKComposer());
        session.sendQueueMessage(new UniqueIdComposer(session.getMachineId().getFingerPrint()));

        int errorCode;
        //todo: cancel debug test
        double startTime = DateTimeUtils.getUnixTimestamp();
        if ((errorCode = PlayerAuthenticator.tryAuthenticate(sso, session)) != 0) {

            System.out.println("Could not authenticate user sso '" + sso + "' | error #" + errorCode);

            session.disconnect();
            return;
        }

        if(session.getAvatar().getPreferences().getBoolOrDefault("debug.mode.on", false))
            session.sendMessage(new BubbleNotificationComposer("debug","Debug:\nAcc loaded in " + (DateTimeUtils.getUnixTimestamp() - startTime) + " seconds"));

        session.sendQueueMessage(new HomeRoomComposer(session.getAvatar().getHomeRoom(), session.getAvatar().getHomeRoom()));
        session.sendQueueMessage(new UserRightsComposer(session.getAvatar().getRank(), session.getAvatar().getClubLevel(), JBlob.getGame().getPermissionManager().validateRight(session.getAvatar().getRank(), "p_ambassador_min_rank")));
        session.sendQueueMessage(new BuildersClubMembershipComposer(Integer.MAX_VALUE, 10000, 0, Integer.MAX_VALUE));
        session.sendQueueMessage(new FavouritesComposer());
        session.sendQueueMessage(new DebugConsoleComposer(false));
        session.sendQueueMessage(new AvatarPreferencesComposer(session.getAvatar().getPreferences()));
        //session.sendQueueMessage(new ());
        session.sendQueueMessage(new AvailabilityStatusComposer()).flush();

        //endregion


    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
