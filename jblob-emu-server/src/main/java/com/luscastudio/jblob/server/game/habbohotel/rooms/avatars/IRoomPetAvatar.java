package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;

/**
 * Created by Lucas on 03/10/2016.
 */
public interface IRoomPetAvatar extends IRoomAvatar {

    PetData getPetData();

    void composePetAvatarInformation(PacketWriting message);

    void onPlace(HabboAvatar placerAvatar);

    void onRemove(HabboAvatar removerAvatar);

    void onRespect(HabboAvatar avatar);

    void onAvatarSay(HabboAvatar avatar, String message);

    void dispose();

}
