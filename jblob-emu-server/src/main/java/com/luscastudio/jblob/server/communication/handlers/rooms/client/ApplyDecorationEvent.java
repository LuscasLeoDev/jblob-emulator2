package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.RemoveInventoryFurniComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomPropertyComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 08/01/2017 at 20:58.
 */
public class ApplyDecorationEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();

        if(room == null)
            return;

        //if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), PermissionManagerPermissions.ROOM_APPLY_DECORATION))
          //  return;

        int itemId = packet.getInt();

        FurniProperties item = session.getAvatar().getInventory().getFurni(itemId);

        if(item == null)
            return;

        String type = "";

        session.getAvatar().getInventory().removeRoomItem(itemId);
        session.sendQueueMessage(new RemoveInventoryFurniComposer(itemId));

        switch(item.getBase().getInventoryCode()){
            default:
                session.sendMessage(new BubbleNotificationComposer("error_bubble").add("message_id", "apply_decoration"));
                return;

            case 2: // wallpaper
                room.sendMessage(new RoomPropertyComposer(type = "wallpaper", item.getExtradata().toString()));
                room.getProperties().setWallData(item.getExtradata().toString());
                break;

            case 3: //Floor
                room.sendMessage(new RoomPropertyComposer(type = "floor", item.getExtradata().toString()));
                room.getProperties().setFloorData(item.getExtradata().toString());
                break;

            case 4: //landscape
                room.sendMessage(new RoomPropertyComposer(type = "landscape", item.getExtradata().toString()));
                room.getProperties().setLandscapeData(item.getExtradata().toString());
                break;
        }

        try(DBConnReactor reactor  = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("UPDATE players_rooms SET " + type + "_data = ?  WHERE id = ?");
            prepare.setString(1, item.getExtradata().toString());
            prepare.setInt(2, room.getProperties().getId());
            prepare.run();

            prepare =  reactor.prepare("DELETE FROM avatars_items_data WHERE id = ?");
            prepare.setInt(1, item.getId());
            prepare.run();
        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }


    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
