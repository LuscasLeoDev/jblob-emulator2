package com.luscastudio.jblob.server.communication.handlers.catalog.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 31/01/2017 at 15:15.
 */
public class CheckPetNameComposer extends MessageComposer {
    @Override
    public String id() {
        return "CheckPetNameMessageComposer";
    }

    public CheckPetNameComposer(int errorCode, String errorInfo){
        message.putInt(errorCode);
        message.putString(errorInfo);
    }
}
