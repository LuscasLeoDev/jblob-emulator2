package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 04/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class UserRemoveComposer extends MessageComposer {
    public UserRemoveComposer(int virtualId) {
        message.putString(String.valueOf(virtualId));
    }

    @Override
    public String id() {
        return "UserRemoveMessageComposer";
    }
}
