package com.luscastudio.jblob.server.game.habbohotel.navigator.flatcats;

/**
 * Created by Lucas on 06/02/2017 at 10:30.
 */
public class NavigatorFlatCategory {

    private int id;
    private String name;
    private String permissionName;
    private String publicName;

    public NavigatorFlatCategory(int id, String name, String publicName, String permissionName){
        this.id = id;
        this.name = name;
        this.publicName = publicName;
        this.permissionName = permissionName;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public String getPublicName() {
        return publicName;
    }
}
