package com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.ConditionWiredBox;

/**
 * Created by Lucas on 15/02/2017 at 23:24.
 */
public class WiredConditionConfigComposer extends MessageComposer {
    @Override
    public String id() {
        return "WiredConditionConfigMessageComposer";
    }

    public WiredConditionConfigComposer(ConditionWiredBox wiredBox){
        wiredBox.parseHeader(message);

        message.putInt(wiredBox.getWiredCode());
    }
}
