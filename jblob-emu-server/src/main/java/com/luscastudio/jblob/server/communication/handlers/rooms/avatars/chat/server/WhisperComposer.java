package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server;

/**
 * Created by Lucas on 31/01/2017 at 14:28.
 */
public class WhisperComposer extends ChatComposer {

    @Override
    public String id() {
        return "WhisperMessageComposer";
    }

    public WhisperComposer(int virtualId, int colorId, int gesture, String text) {
        super(virtualId, colorId, gesture, text);
    }

    public WhisperComposer(int virtualId, int colorId, int gesture, String text, int delay) {
        super(virtualId, colorId, gesture, text, delay);
    }
}
