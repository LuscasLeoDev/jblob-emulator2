package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

/**
 * Created by Lucas on 17/10/2016.
 */

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

public class AddFloorItemComposer extends MessageComposer {
    @Override
    public String id() {
        return "ObjectAddMessageComposer";
    }

    public AddFloorItemComposer(IRoomItem item, String ownerName) {
        ItemSerializer.serializeFloorItem(message, item);
        message.putString(ownerName);
    }
}
