package com.luscastudio.jblob.server.game.permissions;

/**
 * Created by Lucas on 12/12/2016.
 */

public class PermissionData {

    private String name;
    private int minRank;
    private boolean equalsTo;

    public PermissionData(String name, int minRank, boolean equalsTo){
        this.name = name;
        this.minRank = minRank;
        this.equalsTo = equalsTo;
    }

    public String getName() {
        return name;
    }

    public int getMinRank() {
        return minRank;
    }

    public boolean isEqualsTo() {
        return equalsTo;
    }
}
