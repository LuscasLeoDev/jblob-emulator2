package com.luscastudio.jblob.server.encryption;

import java.math.BigInteger;

/**
 * Created by Lucas on 27/09/2016.
 */
public class RSA {

    public static int NUMLEN = 60;

    private BigInteger exponent;
    private BigInteger n;
    private BigInteger privateKey;

    public RSA(String N, String E, String D) {
        n = new BigInteger(N, 16);
        exponent = new BigInteger(E, 16);
        privateKey = new BigInteger(D, 16);
    }

    public BigInteger getN() {
        return n;
    }

    public String sign(String text) {
        BigInteger m = new BigInteger(this.pkcs1pad2(text.getBytes(), this.getBlockSize()));
        if (m.equals(BigInteger.ZERO)) {
            return null;
        } else {
            BigInteger c = m.modPow(privateKey, n);
            if (c.equals(BigInteger.ZERO)) {
                return null;
            } else {
                String result = c.toString(16);
                return (result.length() & 1) == 0 ? result : result;
            }
        }
    }

    public String decrypt(String strc) {
        BigInteger c = new BigInteger(strc, 16);
        BigInteger m = c.modPow(this.privateKey, this.n);
        if (m.equals(BigInteger.ZERO)) {
            return null;
        } else {
            byte[] bytes = this.pkcs1unpad2(m, this.getBlockSize());
            return bytes == null ? null : new String(bytes);
        }
    }


    private byte[] pkcs1unpad2(BigInteger src, int n) {
        byte[] bytes = src.toByteArray();

        int i;
        for (i = 0; i < bytes.length && bytes[i] == 0; ++i) {
        }

        if (bytes.length - i == n - 1 && bytes[i] <= 2) {
            ++i;

            while (bytes[i] != 0) {
                ++i;
                if (i >= bytes.length) {
                    return null;
                }
            }

            byte[] out = new byte[bytes.length - i + 1];
            int p = 0;

            while (true) {
                ++i;
                if (i >= bytes.length) {
                    return out;
                }

                out[p++] = bytes[i];
            }
        } else {
            return null;
        }
    }

    public int getBlockSize() {
        return (this.n.bitLength() + 7) / 8;
    }

    private byte[] pkcs1pad2(byte[] data, int n) {
        byte[] bytes = new byte[n];

        for (int i = data.length - 1; i >= 0 && n > 11; bytes[n] = data[i--]) {
            --n;
        }

        --n;

        for (bytes[n] = 0; n > 2; bytes[n] = -1) {
            --n;
        }

        --n;
        bytes[n] = 1;
        --n;
        bytes[n] = 0;
        return bytes;
    }


}
