package com.luscastudio.jblob.server.communication.handlers.messenger.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/10/2016.
 */
public class MessengerInitComposer extends MessageComposer {
    @Override
    public String id() {
        return "MessengerInitMessageComposer";
    }

    public MessengerInitComposer() {

        message.putInt(5000); //Friend Limit
        message.putInt(1); //Unknown
        message.putInt(70000); //Club friend Limit

        message.putInt(0);// Category Count

        //region _SafeStr_3045 #1

        message.putInt(1); //Id
        message.putString("Family"); //Id

        //endregion

        //region _SafeStr_3045 #2

        message.putInt(2); //Id
        message.putString("As puyas da vida"); //Id

        //endregion

    }
}
