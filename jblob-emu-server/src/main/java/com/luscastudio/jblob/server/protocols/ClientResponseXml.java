package com.luscastudio.jblob.server.protocols;

import com.luscastudio.jblob.server.debug.BLogger;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Created by Lucas on 18/09/2016.
 */
public class ClientResponseXml extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        try {
            in.markReaderIndex();
            if (in.readableBytes() > 0) {
                char fChar = (char) in.readByte();
                in.resetReaderIndex();
                switch (fChar) {
                    case '<':
                        ctx.channel().writeAndFlush("<?xml version=\"1.0\"?>\r\n<!DOCTYPE cross-domain-policy SYSTEM " +
                                "\"/xml/dtds/cross-domain-policy" +
                                ".dtd\">\r\n<cross-domain-policy>\r\n<allow-access-from domain=\"*\" to-ports=\"*\" />\r\n</cross-domain-policy>\u0000").addListener(ChannelFutureListener.CLOSE);
                        break;

                    default:
                        ctx.channel().pipeline().remove(this);
                        ClientResponseIn In = ctx.channel().pipeline().get(ClientResponseIn.class);
                        In.decode(ctx, in, out);
                        break;
                }
            }


        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }
}
