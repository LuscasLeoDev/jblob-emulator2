package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 05/02/2017 at 18:26.
 */
public class UserNameChangeComposer extends MessageComposer {
    @Override
    public String id() {
        return "UserNameChangeMessageComposer";
    }

    public UserNameChangeComposer(int roomId, int virtualId, String name){
        message.putInt(roomId);
        message.putInt(virtualId);
        message.putString(name);
    }
}
