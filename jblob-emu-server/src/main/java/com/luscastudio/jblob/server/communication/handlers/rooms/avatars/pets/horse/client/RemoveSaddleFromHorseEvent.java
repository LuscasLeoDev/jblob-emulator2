package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.horse.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.horse.HorseRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 19/02/2017 at 23:11.
 */
public class RemoveSaddleFromHorseEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int petId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().getPet(petId);

        if(!(petAvatar instanceof HorseRoomPetAvatar))
            return;

        HorseRoomPetAvatar horse = (HorseRoomPetAvatar)petAvatar;

        if(petAvatar.getPetData().getOwnerId() == session.getAvatar().getId())
            horse.removeSaddle();
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
