package com.luscastudio.jblob.server.communication.handlers.player.profile.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.inventory.badge.Badge;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.List;

/**
 * Created by Lucas on 11/11/2016.
 */

public class AvatarSelectedBadgeList extends MessageComposer {
    public AvatarSelectedBadgeList(IAvatarDataCache cache) {

        message.putInt(cache.getId());

        List<Badge> badges = cache.getInventory().getEquippedBadges();

        message.putInt(cache.getInventory().getEquippedBadges().size());

        for(Badge badge : badges){
            message.putInt(badge.getSlotId());
            message.putString(badge.getCode());
        }

    }

    @Override
    public String id() {
        return "HabboUserBadgesMessageComposer";
    }
}
