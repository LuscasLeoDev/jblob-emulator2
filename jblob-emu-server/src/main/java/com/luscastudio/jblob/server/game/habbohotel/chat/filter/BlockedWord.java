//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.chat.filter;

import com.luscastudio.jblob.server.game.habbohotel.chat.filter.parsers.IParser;

/**
 * Created by Lucas on 16/06/2017 at 11:05.
 */
public class BlockedWord {

    private int id;
    private String value;
    private IParser phaser;
    private String replacement;

    public BlockedWord(int id, String value, IParser phaser, String replacement) {
        this.id = id;
        this.value = value;
        this.phaser = phaser;
        this.replacement = replacement;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public IParser getPhaser() {
        return phaser;
    }

    public String getReplacement() {
        return replacement;
    }
}
