package com.luscastudio.jblob.server.debug;

import com.luscastudio.jblob.server.database.DBConnPrepare;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Lucas on 07/12/2016.
 */

public class BLogger {

    public static void info(Object object, Class<?> cls){
        Logger.getLogger(cls).info(object);
    }

    public static void warn(Object object, Class<?> cls){
        Logger.getLogger(cls).warn(object);
    }

    public static void error(Object object, Class<?> cls){
        if(object instanceof Throwable)
            ((Throwable)object).printStackTrace();
        else
            Logger.getLogger(cls).error(object);
    }

    public static void trace(Object object, Class<?> cls){
        Logger.getLogger(cls).trace(object);
    }

    public static void debug(Object object, Class<?> cls) {
        //System.out.println("[DEBUG] [" + cls.getName() + "]" + " " + object);
        System.out.println("[" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME) + "][DEBUG] " + object);
        //Logger.getLogger(cls).debug(object);
    }

    public static void error(String s, Exception e, Class<?> aClass) {
        Logger.getLogger(aClass).error(s, e);
    }
}
