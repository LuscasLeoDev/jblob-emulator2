package com.luscastudio.jblob.server.communication.handlers.player.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.server.ScrSendUserInfoComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 10/12/2016.
 */

public class ScrGetUserInfoEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new ScrSendUserInfoComposer());
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
