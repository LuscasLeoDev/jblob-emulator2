package com.luscastudio.jblob.server.database;

import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Lucas on 01/10/2016.
 */
public class DBConnReactor implements AutoCloseable {
    private DBConnPrepare prepare;
    private Connection connection;

    public DBConnReactor(Connection connection) {
        this.connection = connection;
    }

    public DBConnPrepare prepare(String sql, boolean returnKeys) throws SQLException {
        if (this.prepare != null)
            this.prepare.close();

        return this.prepare = new DBConnPrepare(sql, returnKeys ? this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS) : this.connection.prepareStatement(sql), returnKeys);
    }

    public DBConnPrepare prepare(String sql) throws SQLException {
        return this.prepare(sql, false);
    }

    @Override
    public void close() throws Exception {
        if(prepare != null)
            prepare.close();

        this.connection.close();
    }
}
