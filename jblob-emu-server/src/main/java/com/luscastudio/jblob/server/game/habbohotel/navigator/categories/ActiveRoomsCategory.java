package com.luscastudio.jblob.server.game.habbohotel.navigator.categories;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Lucas on 18/12/2016 at 13:09.
 */
public class ActiveRoomsCategory extends NavigatorCategory {

    public ActiveRoomsCategory(int id, String identifier, String name, int parentId, int orderNum, boolean collapse, int expandStatus, int showType, String extradata) {
        super(id, identifier, name, parentId, orderNum, collapse, expandStatus, showType, extradata);
    }

    @Override
    public void init(DBConnReactor reactor) throws SQLException {

    }

    @Override
    protected void onRoomUnload(RoomProperties sender) {

    }

    @Override
    public List<RoomProperties> getRoomsData(PlayerSession session, String search) {
        List<RoomProperties> list = BCollect.newList();
        if(search.isEmpty())
            return BCollect.newList(JBlob.getGame().getRoomManager().getActiveRoomList());
        return list;
    }

}
