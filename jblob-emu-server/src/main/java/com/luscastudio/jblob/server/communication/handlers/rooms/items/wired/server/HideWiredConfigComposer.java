package com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 15/02/2017 at 17:32.
 */
public class HideWiredConfigComposer extends MessageComposer {
    @Override
    public String id() {
        return "HideWiredConfigMessageComposer";
    }

    public HideWiredConfigComposer(){

    }
}
