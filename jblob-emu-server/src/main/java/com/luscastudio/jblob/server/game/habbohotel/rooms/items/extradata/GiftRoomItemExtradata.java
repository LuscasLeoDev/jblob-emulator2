//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.google.common.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.GiftItemMessageParser;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;

/**
 * Created by Lucas on 28/06/2017 at 22:03.
 */
public class GiftRoomItemExtradata implements IRoomItemExtradata {

    private GiftItemMessageParser messageParser;
    private String dbExtradata;
    private FurniProperties furni;

    private GiftData giftData;
    private String state;

    public GiftRoomItemExtradata(String extradata) {
        this.dbExtradata = extradata;
    }

    @Override
    public String getExtradata() {
        return "";
    }

    @Override
    public String getExtradataToDb() {
        return dbExtradata;
    }

    @Override
    public void setExtradata(String extradata) {

    }

    @Override
    public void init(FurniProperties properties) {
        this.furni = properties;
        this.messageParser = new GiftItemMessageParser(this);
        this.state = "0";
        this.giftData = JSONUtils.fromJson(this.dbExtradata, new TypeToken<GiftData>(){}.getType());
    }

    public void setGiftData(GiftData giftData) {
        this.giftData = giftData;
    }

    public GiftData getGiftData() {
        return giftData;
    }

    @Override
    public IItemMessageParser getMessageParser() {
        return this.messageParser;
    }

    @Override
    public int getExtraInt() {
        if(this.giftData == null)
            return 0;
        return this.giftData.getWrapType() * 1000 + this.giftData.getRibbon();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
