package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.EffectWiredBox;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 23/02/2017 at 17:31.
 */
public class EffectToggleState extends EffectWiredBox {
    private int delay;
    private RunProcess process;
    private Map<Integer, IRoomItem> selectedItems;
    public EffectToggleState(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.delay = 0;
        this.selectedItems = BCollect.newMap();
    }

    @Override
    public void saveWiredData(PlayerSession session, List<Integer> integerList, String stringData, List<Integer> selectedFurniList, int delay, int lastInt1) {

        this.delay = delay;
        this.selectedItems = this.generateSelectedItems(selectedFurniList);
        this.getExtradata().save(new WiredSaveData(null, this.selectedItems.keySet(), this.delay, ""));
        this.room.getRoomItemHandlerService().saveItem(this);
        session.sendMessage(new HideWiredConfigComposer());
    }

    @Override
    public int getDelay() {
        return this.delay;
    }

    @Override
    public void onWiredTriggered(IRoomAvatar avatarTrigger, IRoomItem triggeredItem) {
        this.run();
        this.getExtradata().toggle();
        this.room.getRoomItemHandlerService().updateItem(this, false);
    }

    private void run() {
        if(this.process != null)
            return;

        this.room.getRoomProcess().enqueue(this.process = new RunProcess(this::doEffect, this.delay * 500));
        this.process = null;
    }

    private void doEffect() {
        for (IRoomItem item : this.selectedItems.values())
            item.toggle();
    }

    @Override
    public int getWiredCode() {
        return 8;
    }

    @Override
    public boolean requireAvatar() {
        return false;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return this.selectedItems.values();
    }

    @Override
    public String getStringData() {
        return "";
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return Collections.emptyList();
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        WiredSaveData saveData = this.getExtradata().getSavedData();
        if(saveData == null)
            return;

        this.delay = saveData.getDelay();
        this.selectedItems = this.generateSelectedItems(saveData.getItemsId());
    }
}
