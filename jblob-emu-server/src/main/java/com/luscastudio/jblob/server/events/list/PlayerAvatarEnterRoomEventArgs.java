//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:05.
 */
public class PlayerAvatarEnterRoomEventArgs extends EventArgs {

    private HabboAvatar avatar;
    private RoomPlayerAvatar playerAvatar;

    public PlayerAvatarEnterRoomEventArgs(HabboAvatar avatar, RoomPlayerAvatar playerAvatar) {
        this.avatar = avatar;
        this.playerAvatar = playerAvatar;
    }

    public HabboAvatar getAvatar() {
        return avatar;
    }

    public RoomPlayerAvatar getPlayerAvatar() {
        return playerAvatar;
    }
}
