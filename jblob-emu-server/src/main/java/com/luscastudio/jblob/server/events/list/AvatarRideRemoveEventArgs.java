//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:47.
 */
public class AvatarRideRemoveEventArgs extends EventArgs {
    private IRoomAvatar avatar;

    public AvatarRideRemoveEventArgs(IRoomAvatar avatar) {
        this.avatar = avatar;
    }

    public AvatarRideRemoveEventArgs() {
    }

    public IRoomAvatar getAvatar() {
        return avatar;
    }
}
