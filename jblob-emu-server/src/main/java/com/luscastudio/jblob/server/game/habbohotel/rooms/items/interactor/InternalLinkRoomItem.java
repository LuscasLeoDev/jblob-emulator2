package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.InternalLinkRoomItemExtradata;

import java.util.Map;

/**
 * Created by Lucas on 25/02/2017 at 21:20.
 */
public class InternalLinkRoomItem extends SimpleRoomItem implements ISavableInternalLink {
    private InternalLinkRoomItemExtradata extradata;
    public InternalLinkRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.extradata = (InternalLinkRoomItemExtradata)super.extradata;
    }

    @Override
    public InternalLinkRoomItemExtradata getExtradata() {
        return extradata;
    }

    @Override
    public void parseStringData(Map<String, String> data) {
        this.extradata.parse(data);
        this.room.getRoomItemHandlerService().updateItem(this, false);
        this.room.getRoomItemHandlerService().saveItem(this);
    }

    @Override
    public void toggle() {
        this.extradata.toggleState();
        this.room.getRoomItemHandlerService().updateItem(this, false);
        this.room.getRoomItemHandlerService().saveItem(this);
    }
}
