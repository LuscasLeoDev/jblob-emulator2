package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 16/01/2017 at 05:54.
 */
public class PressureTileRoomItem extends SimpleRoomItem {

    RunProcess turnOffProcess;
    EventDelegate delegate;

    public PressureTileRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.extradata.setExtradata("0");
        this.delegate = this.room.getEventHandler().on("room.map.square.state.change", this::onEvent);
    }

    private void onEvent(EventDelegate delegate, EventArgs args) {
        if(this.room.getRoomMap().getRoomAvatars(this.getPosition()).size() == 0)
            return;

        this.turnOn();
    }

    private void turnOn(){
        this.cancelTurnOff();
        this.extradata.setExtradata("1");
        this.room.getRoomItemHandlerService().updateItem(this, false);
        this.turnOff();
    }

    @Override
    public void onMoved(Point from) {
        super.onMoved(from);

        if(this.room.getRoomMap().getRoomAvatars(this.getPosition()).size() == 0)
            return;

        this.turnOn();
    }

    @Override
    public void onUserWalkOn(IRoomAvatar avatar) {
        super.onUserWalkOn(avatar);
        this.turnOn();
    }

    @Override
    public void onUserWalkOff(IRoomAvatar avatar) {
        super.onUserWalkOff(avatar);
    }

    private void turnOff(){
        this.room.getRoomProcess().enqueue(this.turnOffProcess = new RunProcess(new Runnable() {
            @Override
            public void run() {

                if(PressureTileRoomItem.this.room.getRoomMap().getRoomAvatars(PressureTileRoomItem.this.position).size() > 0){
                    PressureTileRoomItem.this.cancelTurnOff();
                    PressureTileRoomItem.this.turnOff();
                    return;
                }

                PressureTileRoomItem.this.getExtradata().setExtradata("0");
                PressureTileRoomItem.this.room.getRoomItemHandlerService().updateItem(PressureTileRoomItem.this, false, true);
            }
        }, 1000));
    }

    @Override
    public void onPlayerTrigger(PlayerSession session, int requestId) {
        super.onPlayerTrigger(session, requestId);
    }

    @Override
    public void toggle() {
        //super.toggle();
    }

    private void cancelTurnOff(){
        if(this.turnOffProcess != null){
            this.turnOffProcess.cancel();
            this.turnOffProcess = null;
        }
    }

    @Override
    public void dispose() {
        if(turnOffProcess != null)
            this.turnOffProcess.cancel();
        this.turnOffProcess = null;
        if(delegate != null)
            this.delegate.cancel();
        this.delegate = null;
    }
}
