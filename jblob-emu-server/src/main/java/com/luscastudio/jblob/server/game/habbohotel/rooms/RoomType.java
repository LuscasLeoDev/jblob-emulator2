package com.luscastudio.jblob.server.game.habbohotel.rooms;

/**
 * Created by Lucas on 04/10/2016.
 */
public enum RoomType {

    PUBLIC,
    PRIVATE

}
