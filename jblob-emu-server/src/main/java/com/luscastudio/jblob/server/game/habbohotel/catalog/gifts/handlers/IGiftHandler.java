//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.catalog.gifts.handlers;

import com.google.common.collect.Multimap;
import com.luscastudio.jblob.api.utils.engine.IObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 29/06/2017 at 21:32.
 */
public interface IGiftHandler {

    boolean validate(Multimap<String, Integer> itemsTypesMap);

    boolean handle(PlayerSession session, Multimap<String, Integer> itemsTypesMap, Room room, IObjectPosition giftPosition);

    int getOrder();

}
