package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CatalogItemDiscountComposer;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CatalogPagesComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 28/10/2016.
 */

public class GetCatalogPagesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        //It's a custom code, i guess customization is good OUT of the mais source
        if (JBlob.getGame().getPermissionManager().validateRight(session.getAvatar().getRank(), "p_hotel_staff"))
            session.sendMessage(new CatalogPagesComposer("NORMAL", JBlob.getGame().getCatalogManager().getPagesForUser(session), catalogNode -> String.format("%s [%d]", catalogNode.getTitle(), catalogNode.getId()), false));
        else
            session.sendMessage(new CatalogPagesComposer("NORMAL", JBlob.getGame().getCatalogManager().getPagesForUser(session), false));

        session.sendMessage(JBlob.getGame().getCatalogManager().getDiscountComposer());

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
