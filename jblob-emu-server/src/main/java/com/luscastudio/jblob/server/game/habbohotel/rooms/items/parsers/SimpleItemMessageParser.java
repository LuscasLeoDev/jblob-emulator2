package com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;

/**
 * Created by Lucas on 10/10/2016.
 */

public class SimpleItemMessageParser implements IItemMessageParser {

    protected FurniProperties item;

    public SimpleItemMessageParser(FurniProperties item) {
        this.item = item;
    }

    @Override
    public int getCode() {
        return 0;
    }

    @Override
    public void parse(PacketWriting message) {
        message.putString(this.item.getExtradata().getExtradata());
        /*if(!this.item.getExtradata().toString().contains("\t")) {
            message.putInt(1);//while
            message.putString("state");
            message.putString(this.item.getExtradata().getExtradata());
        } else {
            String[] spl = this.item.getExtradata().toString().split("\t");

            message.putInt(spl.length / 2);//while

            for(int i = 0; i < spl.length / 2; i++) {
                message.putString(spl[i]);
                message.putString(spl[i + 1]);
            }
        }*/
    }
}
