package com.luscastudio.jblob.server.communication.handlers.messenger.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomForwardComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 21/01/2017 at 00:42.
 */
public class FollowFriendEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int userId = packet.getInt();

        PlayerSession friendSession = JBlob.getGame().getSessionManager().getSession(userId);
        if(friendSession == null)
            return;

        Room room = friendSession.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        session.sendMessage(new RoomForwardComposer(room.getProperties().getId()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
