package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 12/02/2017 at 20:20.
 */
public class FlatAccessDeniedComposer extends MessageComposer {
    @Override
    public String id() {
        return "FlatAccessDeniedMessageComposer";
    }

    public FlatAccessDeniedComposer(String username){
        message.putString(username);
    }

    public FlatAccessDeniedComposer(){

    }
}
