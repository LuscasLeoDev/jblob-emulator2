package com.luscastudio.jblob.server.communication.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by Lucas on 18/09/2016.
 */
public class MessageEvent {

    private ByteBuf raw;
    private short header;

    public MessageEvent(ChannelHandlerContext channel, ByteBuf byteBuf) {
        if (byteBuf.readableBytes() < 2)
            return;
        this.header = byteBuf.readShort();

        raw = byteBuf;
    }

    public boolean getBool() {
        return raw.readByte() == 1;
    }

    public String getString() {
        short len = raw.readShort();
        byte[] buff = raw.readBytes(len).array();
        return new String(buff, JBlob.getDefaultCharset());
    }

    public short getShort() {
        return raw.readShort();
    }

    public int getInt() {
        return raw.readInt();
    }

    public short getHeader() {
        return header;
    }

    @Override
    public String toString() {
        String str = this.raw.toString(JBlob.getDefaultCharset());
        StringBuilder strb = new StringBuilder();

        for(char s : str.toCharArray()){
            switch((int) s){
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                    strb.append("[").append((int) s).append("]");
                    break;

                default:
                    strb.append(s);
                    break;
            }
        }

        return strb.toString();
    }

    public int remaining() {
        return Math.max(raw.capacity() - raw.readerIndex(), 0);
    }
}
