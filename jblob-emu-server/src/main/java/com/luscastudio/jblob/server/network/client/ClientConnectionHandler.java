package com.luscastudio.jblob.server.network.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by Lucas on 18/09/2016.
 */
@ChannelHandler.Sharable
public abstract class ClientConnectionHandler extends SimpleChannelInboundHandler<MessageEvent> {

    @Override
    protected abstract void channelRead0(ChannelHandlerContext ctx, MessageEvent msg);

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        try {
            if (!JBlob.getGame().getSessionManager().tryCreateSession(ctx))
                ctx.disconnect();
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        try {
            PlayerSession session = ctx.attr(PlayerSession.CLIENT_ATTR).get();
            if (session != null) {
                session.disconnect();
            }
            ctx.attr(PlayerSession.CLIENT_ATTR).remove();
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        BLogger.error(cause, this.getClass());
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext context) {
        context.flush();
    }
}
