package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server.SleepComposer;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.list.AvatarCancelMoveEventArgs;
import com.luscastudio.jblob.server.events.list.AvatarMoveEventArgs;
import com.luscastudio.jblob.server.events.list.AvatarStartMoveEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 04/10/2016.
 */

public class MoveAvatarEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int x = packet.getInt();
        int y = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();

        if (room == null)
            return;

        RoomPlayerAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());

        if (avatar == null)
            return;

        EventArgs args = room.getEventHandler().fireEvent("room.avatar.start.move", new AvatarStartMoveEventArgs(avatar));

        if(args.isCancelled())
            return;

        avatar.setIdle(false);
        room.sendMessage(new SleepComposer(avatar.getVirtualId(), avatar.isIdle()));

        //List<Point> path = room.getRoomMap().getPath(avatar.position.getPoint(), new Point(x, y));
        Point p = new Point(x, y);
        if (p.equals(avatar.getSetPosition().getPoint()) || p.equals(avatar.getPath().getGoal()))
            return;
        avatar.getPath().clearPath();
        avatar.getPath().setGoal(new Point(x, y));
        avatar.getPath().recall(true);

        room.getEventHandler().fireEvent("room.avatar.move", new AvatarMoveEventArgs(p, avatar));
        if(avatar.getPath().isWalking()) {
            avatar.getEventHandler().fireEvent("avatar.cancel.move", new AvatarCancelMoveEventArgs(p, avatar));
        }
        avatar.getEventHandler().fireEvent("avatar.move", new AvatarMoveEventArgs(p));

        //avatar.setPosition(x, y, room.getRoomMap().getSquareHeight(x, y));
        //avatar.updateNeeded(true);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
