package com.luscastudio.jblob.server.game.habbohotel.friendship;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.FriendListComposer;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.FriendListUpdateComposer;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.FriendRequestData;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.FriendshipsRequestsComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import com.luscastudio.jblob.server.game.sessions.collections.PlayerSessionCollection;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 02/10/2016.
 */
public class AvatarFriendship {

    private Map<Integer, AvatarFriendshipItem> friendshipMap;
    private Map<Integer, Integer> friendshipRequests;
    private boolean ready;
    private HabboAvatar avatar;

    public AvatarFriendship(HabboAvatar avatar, DBConnReactor reactor) throws SQLException {
        this.avatar = avatar;
        this.friendshipMap = BCollect.newMap();
        this.friendshipRequests = BCollect.newMap();
        this.load(reactor);

    }

    public static AvatarFriendshipItem generateFriend(HabboAvatar avatar, ResultSet set) {

        try {
            int idone = set.getInt("user_one_id");
            int idtwo = set.getInt("user_two_id");

            int friendId;

            if (idone == avatar.getId())
                friendId = idtwo;
            else
                friendId = idone;

            return new AvatarFriendshipItem(friendId);

        } catch (Exception e) {
            return null;
        }
    }

    public List<AvatarFriendshipItem> getFriends() {
        return BCollect.newList(friendshipMap.values());
    }

    //Update Handler

    public AvatarFriendshipItem getFriend(int id) {
        if (this.friendshipMap.containsKey(id))
            return this.friendshipMap.get(id);

        return null;
    }

    public void removeFriend(int id) {
        this.friendshipMap.remove(id);
    }

    public AvatarFriendshipItem addFriend(int id){
        AvatarFriendshipItem friend;
        if(!this.friendshipMap.containsKey(id))
            this.friendshipMap.put(id, friend = new AvatarFriendshipItem(id));
        else
            friend = this.friendshipMap.get(id);
        return friend;
    }

    public boolean hasFriend(int id){
        return this.friendshipMap.containsKey(id);
    }

    public PlayerSessionCollection getFriendSessions() {
        List<PlayerSession> s = BCollect.newList();
        for (AvatarFriendshipItem friend : getFriends()) {
            if (friend.getSession() != null)
                s.add(friend.getSession());
        }

        return new PlayerSessionCollection(s);
    }

    //Connect Handler

    public void onUpdate() {
        for (PlayerSession friend : getFriendSessions().getCollection()) {
            friend.getAvatar().getFriendship().onUserUpdate(avatar);
        }
    }

    public void onUserUpdate(HabboAvatar avatar) {

        AvatarFriendshipItem friend = this.getFriend(avatar.getId());
        if (friend == null) {
            return;
        }

        this.avatar.getSession().sendMessage(new FriendListUpdateComposer(BCollect.newMap()).addUpdate(friend, avatar, 0));
    }

    //Disconnect Handler

    public void onConnect() {
        this.ready = true;

        for (PlayerSession friend : getFriendSessions().getCollection()) {
            friend.getAvatar().getFriendship().onUserConnect(avatar);
        }
    }

    public void onUserConnect(HabboAvatar avatar) {

        if (!this.ready)
            return;

        AvatarFriendshipItem friend = this.getFriend(avatar.getId());
        if (friend == null) {
            return;
        }

        this.avatar.getSession().sendMessage(new FriendListUpdateComposer(BCollect.newMap()).addUpdate(friend, avatar, 1));
    }

    public void onDisconnect() {

        for (PlayerSession friend : getFriendSessions().getCollection()) {
            friend.getAvatar().getFriendship().onUserDisconnect(avatar);
        }

    }

    public void onUserDisconnect(HabboAvatar avatar) {

        AvatarFriendshipItem friend = this.getFriend(avatar.getId());
        if (friend == null) {
            return;
        }

        this.avatar.getSession().sendMessage(new FriendListUpdateComposer(BCollect.newMap()).addUpdate(friend, avatar, 0));
    }

    public void load() {

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            this.load(reactor);

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }

    public void load(DBConnReactor reactor) throws SQLException {
        DBConnPrepare prepare = reactor.prepare("SELECT * FROM avatars_friendships WHERE user_one_id = ? OR" + " user_two_id = ?");

        prepare.setInt(1, this.avatar.getId());
        prepare.setInt(2, this.avatar.getId());

        ResultSet set = prepare.runQuery();

        while (set.next()) {
            AvatarFriendshipItem friend = generateFriend(this.avatar, set);
            if (friend == null)
                continue;

            this.friendshipMap.put(friend.getUserId(), friend);
        }

        prepare = reactor.prepare("SELECT from_user_id FROM avatars_friendship_request WHERE to_user_id = ?");
        prepare.setInt(1, this.avatar.getId());

        set = prepare.runQuery();
        while(set.next())
            this.friendshipRequests.put(set.getInt("from_user_id"), set.getInt("from_user_id"));
    }

    public List<FriendListComposer> friendListPacket() {
        List<FriendListComposer> messages = BCollect.newList();
        ItemSerializer.serializeItemsCollections(friendshipMap.keySet(),
                100,
                (pageCount, pageIndex, friendIds) -> messages.add(new FriendListComposer(pageCount, pageIndex, friendIds, integer -> {
                    IAvatarDataCache cache;
                    AvatarFriendshipItem avatarFriendshipItem = this.friendshipMap.get(integer);
                    if(avatarFriendshipItem == null || (cache = avatarFriendshipItem.getUserData()) == null)
                        cache = JBlob.getGame().getUserCacheManager().getGenericError();

                    return cache;
                }, integer -> {
                    AvatarFriendshipItem item;
                    if((item = this.friendshipMap.get(integer)) != null)
                        return item.getRelationType();
                    return (short)0;
                })));
        return messages;
    }

    //region Handling Friendship Requests
    public void addFriendRequest(int id) {
        this.friendshipRequests.put(id, id);
    }

    public boolean hasFriendshipRequest(int id){
        return this.friendshipRequests.containsKey(id);
    }

    public void removeFriendRequest(int id) {
        if(this.friendshipRequests.containsKey(id))
            this.friendshipRequests.remove(id);
    }

    public FriendshipsRequestsComposer getFriendRequestComposer() {
        return new FriendshipsRequestsComposer(this.friendshipRequests.keySet(), id -> {
            IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCache(id);
            if(cache == null)
                return new FriendRequestData(id, "Unknown_" + id, "");
            return new FriendRequestData(cache.getId(), cache.getUsername(), "");
        });
    }

    //endregion
}
