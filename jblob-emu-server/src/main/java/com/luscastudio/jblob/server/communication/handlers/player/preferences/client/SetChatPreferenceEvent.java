package com.luscastudio.jblob.server.communication.handlers.player.preferences.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/12/2016.
 */

public class SetChatPreferenceEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.getAvatar().getPreferences().setBool(AvatarPreferences.OLD_CHAT_ENABLED, packet.getBool());
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
