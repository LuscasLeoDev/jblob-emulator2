package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 13/02/2017 at 23:57.
 */
public class FlatAccessibleComposer extends MessageComposer {
    @Override
    public String id() {
        return "FlatAccessibleMessageComposer";
    }

    public FlatAccessibleComposer(String name) {
        message.putString(name);
    }
}
