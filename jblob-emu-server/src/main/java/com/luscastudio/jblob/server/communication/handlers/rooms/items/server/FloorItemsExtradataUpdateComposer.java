package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

/**
 * Created by Lucas on 10/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

import java.util.List;

public class FloorItemsExtradataUpdateComposer extends MessageComposer {

    public FloorItemsExtradataUpdateComposer(IRoomItem item) {
        message.putInt(1);

        message.putInt(item.getId());
        message.putInt(item.getMessageParser().getCode());
        item.getMessageParser().parse(message);
    }

    public FloorItemsExtradataUpdateComposer(List<IRoomItem> items) {

        message.putInt(items.size());

        for (IRoomItem item : items) {
            message.putInt(item.getId());

            message.putInt(item.getMessageParser().getCode());
            item.getMessageParser().parse(message);
        }
    }

    @Override
    public String id() {
        return "UpdateObjectsExtradataMessageComposer";
    }
}
