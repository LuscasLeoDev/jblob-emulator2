//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:43.
 */
public class AvatarLeaveRoomEventArgs extends EventArgs {
    private IRoomAvatar avatar;

    public AvatarLeaveRoomEventArgs(IRoomAvatar avatar) {
        this.avatar = avatar;
    }

    public AvatarLeaveRoomEventArgs() {
    }

    public IRoomAvatar getAvatar() {
        return avatar;
    }
}
