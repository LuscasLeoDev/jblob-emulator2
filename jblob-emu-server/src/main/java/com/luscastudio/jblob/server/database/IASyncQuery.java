package com.luscastudio.jblob.server.database;

import java.sql.ResultSet;

/**
 * Created by Lucas on 24/10/2016.
 */

public abstract class IASyncQuery implements IQueryRunnable {

    abstract DBConnPrepare getPrepare();

    abstract IASyncQueryCallback getCallback();

    @Override
    public void run() {

        switch (this.getCallback().getType()) {
            case UPDATE:
                try {
                    int result = this.getPrepare().runUpdate();
                    this.getCallback().success(result, null);
                } catch (Exception e) {
                    this.getCallback().error(e);
                }
                break;

            case NONE:
                try {
                    this.getPrepare().run();
                    this.getCallback().success(0, null);
                } catch (Exception e) {
                    this.getCallback().error(e);
                }
                break;

            case QUERY:
                try {
                    ResultSet result = this.getPrepare().runQuery();
                    this.getCallback().success(0, result);
                } catch (Exception e) {
                    this.getCallback().error(e);
                }
                break;
        }

        this.getCallback().end(this.getPrepare());


    }
}
