package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.handlers.navigator.RoomSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;

/**
 * Created by Lucas on 10/12/2016.
 */

public class RoomSettingsDataComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomSettingsDataMessageComposer";
    }

    public RoomSettingsDataComposer(RoomProperties room, boolean canMuteRoom) {

        message.putInt(room.getId());
        message.putString(room.getName());
        message.putString(room.getDescription());
        message.putInt(room.getLockType());
        message.putInt(room.getCategoryId());
        message.putInt(room.getMaxUsers());
        message.putInt(room.getModel().getTotalSize());

        message.putInt(room.getTags().size());
        for(String tag : room.getTags())
            message.putString(tag);

        message.putInt(room.getTradeType());
        message.putInt(room.getAllowPets());
        message.putInt(room.getAllowPetsEating());
        message.putInt(room.getAllowWalkTrough());
        message.putInt(room.getHideWall());
        message.putInt(room.getWallThickness());
        message.putInt(room.getFloorThickness());

        RoomSerializer.serializeRoomChatSettings(message, room);

        message.putBool(canMuteRoom);

        RoomSerializer.serializeRoomPermissions(message, room);

    }
}
