//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.DimmerRoomItemExtradata;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 14/05/2017 at 21:16.
 */
public class DimmerRoomItem extends SimpleRoomItem {
    private DimmerRoomItemExtradata extradata;

    public DimmerRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.extradata = ((DimmerRoomItemExtradata) properties.getExtradata());
    }

    @Override
    public DimmerRoomItemExtradata getExtradata() {
        return extradata;
    }

    @Override
    public void onPlaced() {
        super.onPlaced();
    }

    @Override
    public boolean onPlace(PlayerSession session) {
        session.sendMessage(new BubbleNotificationComposer("debug", "Deu bug!"));
        return false;
    }

    @Override
    public void onRoomLoad() {
        super.onRoomLoad();
    }

    @Override
    public void onPlayerTrigger(PlayerSession session, int requestId) {
        if(!this.room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_use_dimmer"))
            return;
    }
}
