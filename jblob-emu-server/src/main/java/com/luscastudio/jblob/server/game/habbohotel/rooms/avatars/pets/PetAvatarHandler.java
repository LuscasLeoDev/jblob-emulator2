package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.horse.HorseRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.monsterplant.MonsterPlantRoomPetAvatar;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by Lucas on 28/01/2017 at 16:04.
 */
public class PetAvatarHandler implements IFlushable {

    private Map<String, Class<? extends IRoomPetAvatar>> petInstanceMap;

    public PetAvatarHandler(){
        this.petInstanceMap = BCollect.newConcurrentMap();
    }

    public void flush(){

        //Default
        this.addPetInstance("default", SimpleRoomPetAvatar.class, false);
        this.addPetInstance("monsterplant", MonsterPlantRoomPetAvatar.class, false);
        this.addPetInstance("horse", HorseRoomPetAvatar.class, false);
    }

    @Override
    public void performFlush() {

    }

    public void addPetInstance(String behaviorName, Class<? extends IRoomPetAvatar> instance, boolean replace){
        if(this.petInstanceMap.containsKey(behaviorName) && replace){
            this.petInstanceMap.replace(behaviorName, instance);
        } else if (!this.petInstanceMap.containsKey(behaviorName)){
            this.petInstanceMap.put(behaviorName, instance);
        }
    }

    public IRoomPetAvatar generateRoomPetAvatar(String behaviorName, Room room, int virtualId, PetData petData) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<? extends IRoomPetAvatar> instance;
        if(!this.petInstanceMap.containsKey(behaviorName))
            instance = this.petInstanceMap.get("default");
        else
            instance = this.petInstanceMap.get(behaviorName);

        if(instance == null)
            return null;

        Constructor<? extends IRoomPetAvatar> constructor = instance.getConstructor(Room.class, int.class, PetData.class);
        if(constructor == null)
            return null;

        return constructor.newInstance(room, virtualId, petData);
    }

    public IRoomPetAvatar generateRoomPetAvatar(Room room, int virtualId, PetData petData) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return this.generateRoomPetAvatar(petData.getPetBase().getPetBehavior(), room, virtualId, petData);
    }

}
