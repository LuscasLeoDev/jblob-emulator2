package com.luscastudio.jblob.server.protocols;

import com.luscastudio.jblob.server.communication.server.MessageBufferComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposerBoxed;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Created by Lucas on 18/09/2016.
 */
public class ClientResponseOut extends MessageToByteEncoder<MessageComposerBoxed> {

    @Override
    protected void encode(ChannelHandlerContext ctx, MessageComposerBoxed msg, ByteBuf out) throws Exception {
        try {
            PlayerSession session = msg.getSession();
            short header = -1;

            if (session != null && session.getBuild() != null)
                header = session.getBuild().getOutgoingHeader(msg.getComposer().id());

            msg.getComposer().composePacket();

            MessageBufferComposer bufferComposer = new MessageBufferComposer(out);
            bufferComposer.putInt(-1);//length
            bufferComposer.putShort(header);
            msg.getComposer().parse(bufferComposer);
            bufferComposer.content().setInt(0, bufferComposer.content().writerIndex() - 4);
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

    }
}
