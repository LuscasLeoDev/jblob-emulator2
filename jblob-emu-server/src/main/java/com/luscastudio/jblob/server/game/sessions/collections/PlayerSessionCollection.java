package com.luscastudio.jblob.server.game.sessions.collections;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.players.IMessageComposerHandler;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 02/10/2016.
 */
public class PlayerSessionCollection implements IMessageComposerHandler {

    private List<PlayerSession> collection;

    public PlayerSessionCollection(List<PlayerSession> list) {
        this.collection = list;
    }

    public PlayerSessionCollection() {
        this.collection = BCollect.newList();
    }

    public void add(PlayerSession session) {
        this.collection.add(session);
    }

    public boolean remove(PlayerSession session) {
        return this.collection.remove(session);
    }

    public IMessageComposerHandler sendQueueMessage(MessageComposer message) {
        for (PlayerSession session : collection)
            session.sendMessage(message, false);

        return this;
    }

    public List<PlayerSession> getCollection() {
        return collection;
    }

    @Override
    public void sendMessage(MessageComposer message) {
        for (PlayerSession session : collection)
            session.sendMessage(message);
    }

    public void flush() {
        for (PlayerSession session : collection)
            session.flush();
    }

}
