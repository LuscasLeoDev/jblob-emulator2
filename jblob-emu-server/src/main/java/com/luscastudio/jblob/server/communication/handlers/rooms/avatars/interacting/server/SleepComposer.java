package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 12/12/2016.
 */

public class SleepComposer extends MessageComposer {
    @Override
    public String id() {
        return "SleepMessageComposer";
    }

    public SleepComposer(int virtualId, boolean idle) {
        message.putInt(virtualId).putBool(idle);
    }
}
