package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.ShoutComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.ISavableInternalLink;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Map;

/**
 * Created by Lucas on 31/01/2017 at 16:33.
 */
public class SaveBrandingItemEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int itemId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);

        if (item == null || !(item instanceof ISavableInternalLink))
            return;

        ISavableInternalLink savableInternalLink = (ISavableInternalLink)item;

        int itemLength = packet.getInt() / 2;
        int i = 0;
        Map<String, String> stringData = BCollect.newMap();
        while(i++ < itemLength)
            stringData.put(packet.getString(), packet.getString());

        savableInternalLink.parseStringData(stringData);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
