package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/01/2017 at 23:49.
 */
public class UpdateStickyNoteEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();
        if (room == null)
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(packet.getInt());
        if (item == null)
            return;

        String color = packet.getString();
        String text = packet.getString();

        if(text.startsWith("#") && text.length() > 6) {
            color = text.substring(1, 7);
            text = text.substring(7);
        }

        item.getExtradata().setExtradata(color + " " + text);
        room.getRoomItemHandlerService().saveItem(item);
        room.getRoomItemHandlerService().updateItem(item, true);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
