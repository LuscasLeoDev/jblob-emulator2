package com.luscastudio.jblob.server.game.permissions;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 12/12/2016.
 */

public class PermissionManager implements IFlushable {

    private Map<String, PermissionData> permissions;
    private Map<Integer, List<PermissionData>> specialPermissions;


    public PermissionManager(){
        this.permissions = BCollect.newConcurrentMap();
        this.specialPermissions = BCollect.newMap();
    }

    public boolean validateRight(int avatarRank, String permissionName){
        if(!this.permissions.containsKey(permissionName))
            return false;
        PermissionData data = this.permissions.get(permissionName);
        if(data.isEqualsTo())
            return avatarRank == data.getMinRank();
        return avatarRank >= data.getMinRank();
    }

    @Override
    public void flush() throws Exception {
        this.permissions.clear();
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("SELECT * FROM permissions_data");

            ResultSet set = prepare.runQuery();

            while (set.next())
                this.permissions.put(set.getString("permission_name"), new PermissionData(set.getString("permission_name"), set.getInt("min_rank"), set.getInt("equals_to") == 1));
        }
    }

    @Override
    public void performFlush() {

    }
}