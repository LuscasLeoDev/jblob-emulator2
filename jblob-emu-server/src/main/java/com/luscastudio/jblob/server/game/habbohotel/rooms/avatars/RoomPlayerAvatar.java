package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;

/**
 * Created by Lucas on 03/10/2016 at 21:12.
 */

public class RoomPlayerAvatar extends RoomAvatar {

    private HabboAvatar avatar;

    public RoomPlayerAvatar(HabboAvatar avatar, Room room, int virtualId) {
        super(room, virtualId);
        this.avatar = avatar;
    }

    public HabboAvatar getAvatar() {
        return avatar;
    }

    public int getScore() {
        //todo: implement achievement system
        return avatar.getAchievementScore();
    }

    @Override
    public int getId() {
        return avatar.getId();
    }

    @Override
    public String getName() {
        return avatar.getUsername();
    }

    @Override
    public String getMotto() {
        return avatar.getMotto();
    }

    @Override
    public String getFigureString() {
        return avatar.getFigure().toString();
    }

    public String getGender() {
        return avatar.getFigure().getGender();
    }

    @Override
    public void composeAvatar(PacketWriting message) {
        message.putString(this.getGender());
        message.putInt(-1); // Group id
        message.putInt(0); // Group something
        message.putString(""); // Group name

        message.putString(""); // Some Strange thing, maybe group badge

        message.putInt(this.getScore()); //Avatar Score
        message.putBool(true); //Maybe Builders ?? o.o
    }

    @Override
    public void onEnter() {

    }

    @Override
    public void onLeave() {

    }

    @Override
    public void save(DBConnReactor reactor) {
        //Nothing to do, it's not a bot or pet or anything...
    }

    @Override
    public RoomAvatarType getType() {
        return RoomAvatarType.PLAYER;
    }

    public int getAvatarType() {
        return 1;
        //Default user code
    }

    @Override
    public void dispose() {
        super.dispose();
        this.avatar = null;
    }
}
