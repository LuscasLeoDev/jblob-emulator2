package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.google.common.collect.Multimap;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.AddInventoryPetComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDealItem;
import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomData;
import com.luscastudio.jblob.server.game.habbohotel.pet.PetBase;
import com.luscastudio.jblob.server.game.habbohotel.pet.custom.PetCustomDataValues;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Lucas on 08/12/2016.
 */

public class DealItemPet extends CatalogDealItem {

    public DealItemPet(int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);
    }

    @Override
    public boolean generateValue(int ownerId, int userId, String pageExtradata, List<Object> outValues, int amount, DBConnReactor reactor, Multimap<String, Integer> generatedItems) {
        String[] data = pageExtradata.split("\n");

        if(data.length < 3){
            outValues.add(new BubbleNotificationComposer("error", "error while parsing your pet data!"));
            return false;
        }

        String name = data[0];
        String strRaceId = data[1];
        String color = data[2];

        if(!NumberHelper.isInteger(strRaceId)){
            outValues.add(new BubbleNotificationComposer("error", "error while parsing your pet race!"));
            return false;
        }

        int raceId = Integer.parseInt(strRaceId);

        Collection<PetBase> petBases = JBlob.getGame().getPetManager().getSellableBreeds(this.getExtradata());

        if(petBases == null){
            outValues.add(new BubbleNotificationComposer("error", "The pet base wasn't found for '" + this.getExtradata() + "'!"));
            return false;
        }

        PetBase base = null;

        for(PetBase petBase :petBases){
            if(petBase.getRaceId() == raceId)
                base = petBase;
        }

        if(base == null){
            outValues.add(new BubbleNotificationComposer("error", "Pet base wasn't found for race id " + raceId + "!"));
            return false;
        }

        int startLevel = 1;
        int startEnergy = 100;
        int startExperience = 0;
        int startHappiness = 100;
        String startCustomData = "";
        PetCustomDataValues dataValues = JBlob.getGame().getPetManager().getPetCustomDataValues(base.getPetType());
        if(dataValues != null){
            startCustomData = dataValues.getRandomInitialCustom();
        }

        PetData petData = JBlob.getGame().getPetManager().createPetData(AvatarInventory.ITEM_NOROOM_ID, name, base, ownerId, color, new PetCustomData(startCustomData), startLevel, 0, startEnergy, startExperience, startHappiness);

        outValues.add(petData);

        //outValues.add(new TriggerClientEventComposer("inventory/open/pets/" + petData.getId()));
        return true;
    }

    @Override
    public void handleValues(HabboAvatar avatar, List<Object> values) {

        List<Object> out = BCollect.newList();
        Iterator<Object> iterator = values.iterator();
        while (iterator.hasNext()) {

            Object next = iterator.next();
            if (next instanceof PetData) {
                PetData petData = (PetData) next;
                avatar.getInventory().addPet(petData);
                out.add(new AddInventoryPetComposer(petData));
                out.add(new InventoryNotificationComposer().add(InventoryNotificationComposer.PET, petData.getId()));

                iterator.remove();
            }
        }
        values.addAll(out);
        out.clear();
    }


    @Override
    public String getGiftTypeIdentification() {
        return null;
    }

    @Override
    public boolean isGiftable() {
        return false;
    }

    @Override
    public void parsePageComposer(PacketWriting message) {
        super.parsePageComposer(message);
    }
}
