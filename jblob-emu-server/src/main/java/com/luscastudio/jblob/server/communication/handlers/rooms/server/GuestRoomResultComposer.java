package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.handlers.navigator.RoomSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;

/**
 * Created by Lucas on 03/10/2016.
 */

public class GuestRoomResultComposer extends MessageComposer {
    public GuestRoomResultComposer(boolean isLoading, boolean checkEntry, RoomProperties roomData, boolean canMuteRoom) {
        message.putBool(isLoading);

        RoomSerializer.serializeRoomData1(message, roomData);

        message.putBool(checkEntry);

        message.putBool(roomData.isStaffPicked()); //Room Is On Staff Pick
        message.putBool(false); //?
        message.putBool(roomData.isMuted()); //Room Is Muted

        RoomSerializer.serializeRoomPermissions(message, roomData);

        message.putBool(canMuteRoom); //Show mute button

        RoomSerializer.serializeRoomChatSettings(message, roomData);
    }

    @Override
    public String id() {
        return "GetGuestRoomResultMessageComposer";
    }
}
