package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.WiredBoxRoomItemExtradata;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.SimpleRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Lucas on 14/02/2017 at 23:19.
 */
public abstract class WiredBoxRoomItem extends SimpleRoomItem {
    private WiredBoxRoomItemExtradata extradata;
    public WiredBoxRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.extradata = (WiredBoxRoomItemExtradata)properties.getExtradata();
    }

    @Override
    public WiredBoxRoomItemExtradata getExtradata() {
        return extradata;
    }

    @Override
    public int getUseButtonCode() {
        return 1;
    }

    @Override
    public void onPlayerTrigger(PlayerSession session, int requestId) {
        if(!this.room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_use_furni"))
            return;

        this.getExtradata().toggle();
        this.room.getRoomItemHandlerService().updateItem(this, false);
        session.sendMessage(this.getWiredConfigComposer());

    }

    public void triggerEffectBoxes(IRoomAvatar avatar, IRoomItem triggeredItem){
        for (IRoomItem item : this.room.getRoomMap().getRoomItemsByCoordinate(this.getPosition().getX(), this.getPosition().getY())) {
            if(!(item instanceof EffectWiredBox))
                continue;
            if(item == this)
                continue;
            EffectWiredBox effect = (EffectWiredBox)item;

            if(avatar == null && this.requireAvatar())
                continue;

            if(triggeredItem == null && this.requireTriggeredItem())
                continue;

            effect.onWiredTriggered(avatar, triggeredItem);
        }
    }

    public boolean triggerConditionBoxes(IRoomAvatar avatar, IRoomItem triggeredItem) {
        boolean r = true;
        for (IRoomItem item : this.room.getRoomMap().getRoomItemsByCoordinate(this.getPosition().getX(), this.getPosition().getY())) {
            if(!(item instanceof ConditionWiredBox))
                continue;

            ConditionWiredBox condition = (ConditionWiredBox)item;

            if(avatar == null && condition.requireAvatar())
                continue; //Cancel the effect or continue acting like this type of wired is not on the stack?

            if(triggeredItem == null && condition.requireTriggeredItem())
                continue;

            if(!condition.validateCase(avatar, triggeredItem))
                r = false;
        }

        return r;
    }

    protected abstract MessageComposer getWiredConfigComposer();

    public abstract int getWiredCode();

    public abstract boolean requireAvatar();

    public abstract boolean requireTriggeredItem();

    public abstract Collection<IRoomItem> getSelectedItems();

    public abstract String getStringData();

    public abstract Collection<Integer> getIntegersData();

    public abstract Collection<IRoomItem> getIncompatibleItems();

    protected abstract void loadWiredData();

    public int getSelectableItemsLimit(){
        return JBlob.getGame().getDbConfig().getInt("room.furni.wired.max.select", 5);
    }

    public void parseHeader(PacketWriting message){
        message.putBool(false); //Some boolean related to the comment XX about
        message.putInt(this.getSelectableItemsLimit());//Limit of Selectable items

        message.putInt(this.getSelectedItems().size());//items ids count

        for (IRoomItem item : this.getSelectedItems()) {
            message.putInt(item.getProperties().getId());
        }


        message.putInt(this.getBase().getSpriteId());
        message.putInt(this.getProperties().getId());


        message.putString(this.getStringData());

        message.putInt(this.getIntegersData().size()); //Integers Data Count
        for (Integer i : this.getIntegersData()) {
            message.putInt(i);
        }

        message.putInt(10); //Comment XX: Something on external flash texts like wiredfurni.pickfurnis. effect/trigger/conditon . x
    }

    @Override
    public boolean onRemove() {
        return super.onRemove();
    }

    @Override
    public boolean onPlace(PlayerSession session) {
        return super.onPlace(session);
    }

    @Override
    public void onRoomLoad() {
        this.loadWiredData();
    }

    protected Map<Integer, IRoomItem> generateSelectedItems(Collection<Integer> itemsId) {
        Map<Integer, IRoomItem> items = BCollect.newMap();
        for (Integer id : itemsId) {
            IRoomItem item = this.room.getRoomItemHandlerService().getRoomItemById(id);
            if(item != null)
                items.put(item.getProperties().getId(), item);
        }

        return items;
    }

    @Override
    public void onPlaced() {
        if(!this.getExtradata().getExtradataToDb().equals("")){
            this.getExtradata().clear();
            this.room.getRoomItemHandlerService().saveItem(this);
        }
    }
}
