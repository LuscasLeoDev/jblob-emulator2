package com.luscastudio.jblob.server.communication.handlers.player.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/02/2017 at 16:50.
 */
public class BuildersClubMembershipComposer extends MessageComposer {
    @Override
    public String id() {
        return "BuildersClubMembershipMessageComposer";
    }

    public BuildersClubMembershipComposer(int int1, int int2, int int3, int int4){
        message.putInt(int1);
        message.putInt(int2);
        message.putInt(int3);
        message.putInt(int4);
    }
}
