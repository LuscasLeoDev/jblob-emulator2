//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.api.utils.engine.IPoint;
import com.luscastudio.jblob.server.events.EventArgs;

/**
 * Created by Lucas on 09/03/2017 at 00:16.
 */
public class RoomMapSquareStateChangeEventArgs extends EventArgs {
    private IPoint point;

    public RoomMapSquareStateChangeEventArgs(IPoint point) {
        this.point = point;
    }

    public IPoint getPoint() {
        return point;
    }
}
