package com.luscastudio.jblob.server.communication.handlers.messenger.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/10/2016.
 */
public class MessengerMessageComposer extends MessageComposer {
    public MessengerMessageComposer(int sender, String message, int time) {
        this.message.putInt(sender);
        this.message.putString(message);
        this.message.putInt(time);
    }

    public MessengerMessageComposer(int groupId, String message, int time, int userId, String username, String figureString) {
        this.message.putInt(-groupId);
        this.message.putString(message);
        this.message.putInt(time);
        this.message.putString(username + "/" + figureString + "/" + userId);
    }

    @Override
    public String id() {
        return "NewConsoleMessageMessageComposer";
    }
}
