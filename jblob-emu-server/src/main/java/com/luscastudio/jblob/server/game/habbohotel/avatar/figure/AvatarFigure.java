package com.luscastudio.jblob.server.game.habbohotel.avatar.figure;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.server.PlayerSessionDataComposer;
import com.luscastudio.jblob.server.communication.handlers.player.server.UserChangeComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import org.apache.log4j.Logger;

/**
 * Created by Lucas on 19/09/2016.
 */
public class AvatarFigure {
    Logger log = Logger.getLogger(this.getClass());
    private String figureString;
    private String gender;

    private HabboAvatar avatar;

    public AvatarFigure(String figureString, String gender, HabboAvatar avatar) {
        this.figureString = figureString;
        this.gender = gender;
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return figureString;
    }

    public String getGender() {
        return gender.toUpperCase();
    }

    public void update(String figureString, String gender) {

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            /*DBConnPrepare prepare = reactor.prepare("UPDATE players_avatar SET figure_string = ?, gender = ? WHERE id = ?");
            prepare.setString(1, figureString);
            prepare.setString(2, gender);
            prepare.setInt(3, avatar.getId());
            prepare.run();*/

            this.figureString = figureString;
            this.gender = gender;

            this.avatar.getSession().sendMessage(new PlayerSessionDataComposer(this.avatar));
            this.avatar.getFriendship().onUpdate();

            Room room;
            if ((room = this.avatar.getCurrentRoom()) != null) {
                RoomPlayerAvatar roomAvatar = room.getRoomAvatarService().getRoomAvatarByUserId(avatar.getId());
                if (roomAvatar != null) {
                    room.sendMessage(new UserChangeComposer(roomAvatar));
                }
            }

        } catch (Exception e) {
            log.error("Error while update figure of " + avatar + ": ", e);
        }
    }

}
