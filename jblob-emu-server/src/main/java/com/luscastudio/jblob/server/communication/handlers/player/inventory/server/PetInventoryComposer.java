package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;

import java.util.Collection;

/**
 * Created by Lucas on 29/01/2017 at 19:25.
 */
public class PetInventoryComposer extends MessageComposer {
    @Override
    public String id() {
        return "PetInventoryMessageComposer";
    }

    public PetInventoryComposer(int pageCount, int pageIndex, Collection<PetData> pets){
        message.putInt(pageCount);
        message.putInt(pageIndex);

        message.putInt(pets.size());

        for(PetData petData : pets)
            ItemSerializer.serializeInventoryPet(message, petData);
    }
}
