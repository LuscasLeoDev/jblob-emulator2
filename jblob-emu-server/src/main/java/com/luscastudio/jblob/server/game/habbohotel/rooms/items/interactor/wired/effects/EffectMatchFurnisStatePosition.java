package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects;

import com.google.common.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.ObjectPosition;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.SlideObjectBundleComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.EffectWiredBox;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.utils.SavedItemState;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 16/02/2017 at 13:57.
 */
public class EffectMatchFurnisStatePosition extends EffectWiredBox {

    private int delay;
    private Map<Integer, IRoomItem> selectedFurnis;
    private Map<Integer, SavedItemState> savedStates;
    private List<Integer> integers;
    private RunProcess process;

    public EffectMatchFurnisStatePosition(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);

        this.selectedFurnis = BCollect.newMap();
        this.savedStates = BCollect.newMap();
        this.integers = BCollect.newList();

    }

    @Override
    public void saveWiredData(PlayerSession session, List<Integer> integerList, String stringData, List<Integer> selectedFurniList, int delay, int lastInt1) {
        this.integers = integerList;
        this.selectedFurnis = this.generateSelectedItems(selectedFurniList);
        this.savedStates.clear();
        this.delay = delay;

        for (IRoomItem item : this.selectedFurnis.values()) {
            this.savedStates.put(item.getProperties().getId(), new SavedItemState(new ObjectPosition(item.getPosition().getX(), item.getPosition().getY(), item.getPosition().getZ(), item.getPosition().getRotation()), item.getExtradata().getExtradata()));
        }

        String jsonSavedStates = JSONUtils.toJson(this.savedStates, new TypeToken<Map<Integer, SavedItemState>>(){}.getType());

        WiredSaveData saveData = new WiredSaveData(integerList, this.selectedFurnis.keySet(), delay, jsonSavedStates);
        this.getExtradata().save(saveData);
        this.room.getRoomItemHandlerService().saveItem(this);
        session.sendMessage(new HideWiredConfigComposer());
    }

    @Override
    public int getDelay() {
        return this.delay;
    }

    private boolean checkExtradata(){
        if(this.integers.size() == 0)
            return false;
        return this.integers.get(0) == 1;
    }

    private boolean checkRotation(){
        if(this.integers.size() <= 1)
            return false;
        return this.integers.get(1) == 1;
    }

    private boolean checkPosition(){
        if(this.integers.size() <= 2)
            return false;
        return this.integers.get(2) == 1;
    }

    @Override
    public void onWiredTriggered(IRoomAvatar avatarTrigger, IRoomItem triggeredItem) {
        this.runProcess();
    }

    private void runProcess() {
        if(this.process == null)
            this.room.getRoomProcess().enqueue(this.process = new RunProcess(this::run, this.delay));
    }

    private void run(){
        for (IRoomItem item : this.selectedFurnis.values()) {
            if(!this.savedStates.containsKey(item.getProperties().getId()))
                continue;

            SavedItemState state = this.savedStates.get(item.getProperties().getId());

            if(this.checkExtradata()){
                item.getExtradata().setExtradata(state.getExtradata());
                this.room.getRoomItemHandlerService().updateItem(item, false);
                this.room.getRoomItemHandlerService().saveItem(item);
            }

            boolean rotated = false;
            if(this.checkRotation() && item.getPosition().getRotation() != state.getPosition().getRotation()){
                Point oldp = new Point(item.getPosition().getX(), item.getPosition().getY());
                this.room.getRoomMap().removeItemFromMap(item);
                this.room.getRoomMap().updateSqHeight(item.getAffectedTiles());
                item.setAffectedTiles(this.room.getRoomMap().getAffectedTilesForItem(item, item.getPosition().getX(), item.getPosition().getY(), state.getPosition().getRotation()));
                item.getPosition().setPosition(item.getPosition().getX(), item.getPosition().getY(), item.getPosition().getZ(), state.getPosition().getRotation());
                this.room.getRoomMap().addItemToMap(item);
                this.room.getRoomMap().updateSqHeight(item.getAffectedTiles());
                this.room.getRoomItemHandlerService().updateItem(item, true);

                item.onMoved(oldp);
                rotated = true;
                this.room.getRoomItemHandlerService().saveItem(item);
            }

            if(this.checkPosition() && !state.getPosition().equals(item.getPosition())){
                Point oldp = new Point(item.getPosition().getX(), item.getPosition().getY());
                this.room.getRoomMap().updateSqHeight(item.getAffectedTiles());
                this.room.getRoomMap().removeItemFromMap(item);

                Point oldP = new Point(item.getPosition().getX(), item.getPosition().getY());
                double oldZ = item.getPosition().getZ();

                item.getPosition().setPosition(state.getPosition().getX(), state.getPosition().getY(), state.getPosition().getZ());
                item.setAffectedTiles(this.room.getRoomMap().getAffectedTilesForItem(item, item.getPosition().getX(), item.getPosition().getY(), item.getPosition().getRotation()));
                this.room.getRoomMap().updateSqHeight(item.getAffectedTiles());
                if(!rotated)
                    this.room.sendQueueMessage(new SlideObjectBundleComposer(oldP, new Point(item.getPosition().getX(), item.getPosition().getY()), 0).addMoveItem(item.getProperties().getId(), oldZ, item.getPosition().getZ()));


                this.room.getRoomMap().addItemToMap(item);
                item.onMoved(oldp);
                this.room.getRoomItemHandlerService().saveItem(item);
            }
        }

        this.getExtradata().toggle();
        this.room.getRoomItemHandlerService().updateItem(this, false);
        this.room.flush();
        this.process = null;
    }

    @Override
    public int getWiredCode() {
        return 3;
    }

    @Override
    public boolean requireAvatar() {
        return false;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return this.selectedFurnis.values();
    }

    @Override
    public String getStringData() {
        return "";
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return this.integers;
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        WiredSaveData data = this.getExtradata().getSavedData();
        if(data != null){
            this.selectedFurnis = this.generateSelectedItems(data.getItemsId());
            this.delay = data.getDelay();
            this.integers = data.getIntegerList();
            this.savedStates = JSONUtils.fromJson(data.getStringData(), new TypeToken<Map<Integer, SavedItemState>>(){}.getType());
            if(this.savedStates == null) {
                this.savedStates = BCollect.newMap();
                this.selectedFurnis = BCollect.newMap();
            }
        }
    }
}
