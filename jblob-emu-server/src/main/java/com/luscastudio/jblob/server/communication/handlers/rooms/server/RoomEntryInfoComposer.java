package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class RoomEntryInfoComposer extends MessageComposer {
    public RoomEntryInfoComposer(int roomId, boolean isOwner) {
        message.putInt(roomId);
        message.putBool(isOwner);
    }

    @Override
    public String id() {
        return "RoomEntryInfoMessageComposer";
    }
}
