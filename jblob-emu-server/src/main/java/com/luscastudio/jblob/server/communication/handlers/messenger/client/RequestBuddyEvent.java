package com.luscastudio.jblob.server.communication.handlers.messenger.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.NewBuddyRequestComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 26/02/2017 at 18:33.
 */
public class RequestBuddyEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        String name = packet.getString();

        PlayerSession targetSession = JBlob.getGame().getSessionManager().getSession(s -> s.getAvatar().getUsername().toLowerCase().equals(name.toLowerCase()));

        int avatarId;

        if (targetSession == null) {
            IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCacheByName(name);
            if (cache == null)
                return;

            if (session.getAvatar().getFriendship().hasFriendshipRequest(cache.getId()))
                return;

            avatarId = cache.getId();

        } else {
            if (session.getAvatar().getFriendship().hasFriendshipRequest(targetSession.getAvatar().getId()))
                return;
            targetSession.getAvatar().getFriendship().addFriendRequest(session.getAvatar().getId());

            avatarId = targetSession.getAvatar().getId();

            targetSession.sendMessage(new NewBuddyRequestComposer(session.getAvatar().getId(), session.getAvatar().getUsername(), ""));
        }

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reactor.prepare("REPLACE INTO avatars_friendship_request (from_user_id, to_user_id) VALUES (?, ?)");
            prepare.setInt(1, session.getAvatar().getId());
            prepare.setInt(2, avatarId);

            prepare.run();
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
