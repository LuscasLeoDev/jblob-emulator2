package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class RoomReadyComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomReadyMessageComposer";
    }

    public RoomReadyComposer(int roomId, String modelName) {
        message.putInt(roomId);
        message.putString(modelName);
    }
}
