package com.luscastudio.jblob.server.communication.handlers.rooms.pets.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/02/2017 at 19:39.
 */
public class RespectPetEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int petId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().getPet(petId);

        petAvatar.onRespect(session.getAvatar());

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
