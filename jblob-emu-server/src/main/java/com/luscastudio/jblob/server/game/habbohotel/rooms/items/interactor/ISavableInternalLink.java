package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import java.util.Map;

/**
 * Created by Lucas on 25/02/2017 at 21:22.
 */
public interface ISavableInternalLink {
    void parseStringData(Map<String, String> data);
}
