package com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.TriggerWiredBox;

/**
 * Created by Lucas on 14/02/2017 at 23:47.
 */
public class WiredTriggerConfigComposer extends MessageComposer {
    @Override
    public String id() {
        return "WiredTriggerConfigMessageComposer";
    }

    public WiredTriggerConfigComposer(TriggerWiredBox wiredItem){

        wiredItem.parseHeader(message);

        message.putInt(wiredItem.getWiredCode()); //Wired Trigger Code

        message.putInt(wiredItem.getIncompatibleItems().size()); //Incompatible Effects Wired Item Sprite Ids
        for (IRoomItem item : wiredItem.getIncompatibleItems()) {
            message.putInt(item.getProperties().getBase().getSpriteId());
        }
    }
}
