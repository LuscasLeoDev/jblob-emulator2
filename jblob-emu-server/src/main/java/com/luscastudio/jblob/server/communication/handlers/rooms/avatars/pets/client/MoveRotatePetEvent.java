package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.client;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 08/02/2017 at 12:56.
 */
public class MoveRotatePetEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int petId = packet.getInt();
        int x = packet.getInt();
        int y = packet.getInt();
        int rotation = packet.getInt();


        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_move_rotate_pet"))
            return;

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().getPet(petId);
        if(petAvatar == null)
            return;

        petAvatar.updateNeeded(true);

        if(!room.getRoomMap().isValidSquare(x, y))
            return;


        room.getRoomMap().updateAvatarCoordinate(petAvatar, petAvatar.getPosition().getPoint(), new Point(x, y));
        double z = room.getRoomMap().getSquareDynamicHeight(x, y);
        petAvatar.setPosition(x, y, z);
        petAvatar.setSetPosition(x, y, z);
        petAvatar.getPosition().setRotation(rotation);
        petAvatar.getPosition().setHeadRotation(rotation);




        room.getRoomAvatarService().saveAvatar(petAvatar);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
