//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;

/**
 * Created by Lucas on 06/03/2017 at 22:36.
 */
public class AvatarEventArgs extends EventArgs {
    private int avatarId;

    public AvatarEventArgs(int avatarId) {
        this.avatarId = avatarId;
    }
}
