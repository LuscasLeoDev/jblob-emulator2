package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server.CanCreateRoomComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 02/10/2016.
 */

public class CanCreateRoomEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new CanCreateRoomComposer(true, 100));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
