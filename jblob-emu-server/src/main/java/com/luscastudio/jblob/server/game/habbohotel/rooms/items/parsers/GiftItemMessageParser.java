//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.GiftRoomItemExtradata;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

/**
 * Created by Lucas on 28/06/2017 at 22:21.
 */
public class GiftItemMessageParser implements IItemMessageParser {

    private GiftRoomItemExtradata extradata;

    public GiftItemMessageParser(GiftRoomItemExtradata giftRoomItemExtradata) {
        this.extradata = giftRoomItemExtradata;
    }

    @Override
    public void parse(PacketWriting message) {

        message.putInt(5);

        if(this.extradata.getGiftData() != null) {
            message.putString("MESSAGE");
            message.putString(this.extradata.getGiftData().getMessage());

            //Get the user data cache always when the packet composer is called is not a good thing...
            IAvatarDataCache cache;
            if (this.extradata.getGiftData().isShowSender() && (cache = JBlob.getGame().getUserCacheManager().getUserCache(this.extradata.getGiftData().getUserId())) != null) {
                message.putString("PURCHASER_NAME");
                message.putString(cache.getUsername());

                message.putString("PURCHASER_FIGURE");
                message.putString(cache.getFigureString());
            } else {
                message.putString("PURCHASER_NAME");
                message.putString("");
                message.putString("PURCHASER_FIGURE");
                message.putString("");
            }

        } else {
            message.putString("MESSAGE");
            message.putString("Empty gift. don't open it. \n Or open... ª");

            message.putString("PURCHASER_NAME");
            message.putString("");

            message.putString("PURCHASER_FIGURE");
            message.putString("");
        }

        message.putString("PRODUCT_CODE");
        message.putString("wf_deal1");

        message.putString("state");
        message.putString(this.extradata.getState());


    }

    @Override
    public int getCode() {
        return 1;
    }
}
