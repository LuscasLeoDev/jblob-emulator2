//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;

/**
 * Created by Lucas on 09/03/2017 at 00:07.
 */
public class AvatarSaysSomethingEventArgs extends EventArgs {
    private IRoomAvatar avatar;
    private String message;

    public AvatarSaysSomethingEventArgs(IRoomAvatar avatar, String message) {
        this.avatar = avatar;
        this.message = message;
    }

    public IRoomAvatar getAvatar() {
        return avatar;
    }

    public String getMessage() {
        return message;
    }
}
