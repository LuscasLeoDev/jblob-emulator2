package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 21/01/2017 at 17:53.
 */
public class RoomSpectatorModeComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomSpectatorModeMessageComposer";
    }

    public RoomSpectatorModeComposer(){

    }
}
