package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 16/12/2016.
 */

public class RoomForwardComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomForwardMessageComposer";
    }

    public RoomForwardComposer(int id) {
        message.putInt(id);
    }
}
