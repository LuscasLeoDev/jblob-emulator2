package com.luscastudio.jblob.server.communication.handlers.rooms.items.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.FloorItemUpdateComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;


/**
 * Created by Lucas on 10/10/2016.
 */

public class MoveFloorItemEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        //todo: add permission checker to check if user can move item

        int itemId = packet.getInt();

        int x = packet.getInt();
        int y = packet.getInt();
        int rot = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if (room == null)
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);

        //todo: Set this permission name as static
        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_move_furni")) {
            session.sendMessage(new FloorItemUpdateComposer(item));
            return;
        }

        if (item == null)
            return;

        if(!item.onMove(session, item.getPosition())){
            session.sendMessage(new FloorItemUpdateComposer(item));
            return;
        }

        List<Point> oldAffectedTiles = BCollect.newList(item.getAffectedTiles());

        List<Point> newAffectedTiles = room.getRoomMap().getAffectedTilesForItem(item, x, y, rot);

        for (Point p : newAffectedTiles) {
            if (!room.getRoomMap().isValidSquare(p)) {
                room.getRoomItemHandlerService().updateItem(item, true);
                return;
            }
        }

        //Step 1 - Remove from map
        room.getRoomMap().removeItemFromMap(item);
        item.setAffectedTiles(newAffectedTiles);

        //Step 2 - set up the new position
        item.getPosition().setPosition(x, y, 0);


        //Step 3 - Add back to map
        room.getRoomMap().addItemToMap(item);

        //Step 4 - Set the new Z
        double z = room.getRoomMap().getSquareHeightForItem(item);
        item.getPosition().setPosition(x, y, z, rot);

        room.getRoomItemHandlerService().saveItem(item);
        room.getRoomItemHandlerService().updateItem(item, true);


        room.getRoomMap().updateSqHeight(newAffectedTiles);
        room.getRoomMap().updateSqHeight(oldAffectedTiles);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
