package com.luscastudio.jblob.server.game.habbohotel.inventory;

/**
 * Created by Lucas on 16/10/2016.
 */
public interface IInventoryItem {
    int getId();

    InventoryItemType getType();

}
