package com.luscastudio.jblob.server.communication.handlers.player.notifications.server;

/**
 * Created by Lucas on 17/10/2016.
 */

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.Map;

public class BubbleNotificationComposer extends MessageComposer {

    private Map<String, String> vars;
    private String codeOrImage;

    @Override
    public String id() {
        return "RoomNotificationMessageComposer";
    }

    public BubbleNotificationComposer(){
        this.codeOrImage = "";
        this.vars = BCollect.newMap();
    }
    public BubbleNotificationComposer(String codeOrImage){
        this.codeOrImage = codeOrImage;
        this.vars = BCollect.newMap();
    }

    public BubbleNotificationComposer add(String key, String value){
        if(this.vars.containsKey(key))
            this.vars.replace(key, value);
        else
            this.vars.put(key, value);

        return this;
    }

    @Override
    public void compose(){
        if(this.vars == null)
            return;

        message.putString(codeOrImage);

        message.putInt(this.vars.size() + 1);

        for(Map.Entry<String, String> pair : this.vars.entrySet()){
            message.putString(pair.getKey());
            message.putString(pair.getValue());
        }

        message.putString("display");
        message.putString("BUBBLE");
    }

    public BubbleNotificationComposer(String image, String text) {

        message.putString(image);

        message.putInt(2);

        message.putString("display");
        message.putString("BUBBLE");

        message.putString("message");
        message.putString(text);
    }

    public BubbleNotificationComposer(String image, String text, String link) {

        message.putString(image);

        message.putInt(3);

        message.putString("display");
        message.putString("BUBBLE");

        message.putString("message");
        message.putString(text);

        message.putString("linkUrl");
        message.putString(link);
    }


}
