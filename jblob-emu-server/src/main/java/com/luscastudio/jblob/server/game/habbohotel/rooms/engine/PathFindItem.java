package com.luscastudio.jblob.server.game.habbohotel.rooms.engine;

import com.luscastudio.jblob.api.utils.engine.Point;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lucas on 09/10/2016.
 */

public class PathFindItem implements Comparable<PathFindItem> {

    private Point point;
    private PathFindItem father;

    private List<Point> sides;

    private boolean isOpen;
    private boolean isClosed;

    private boolean isMain;

    private Integer f;
    private int g;
    private int h;


    private boolean closed;
    private boolean open;


    public PathFindItem(Point point) {
        this.point = point;
        this.isMain = true;

        this.f = 0;
        this.g = 0;
        this.h = 0;
        init();
    }

    public PathFindItem(Point point, PathFindItem father, Point end) {
        this.point = point;
        this.father = father;
        init();
        if (end.getX() != this.point.getX() || end.getY() != this.point.getY())
            this.g = father.g + 10;
        else
            this.g = father.g + 5;

        this.h = end.getDistance(this.point);

        this.f = this.g + this.h;

        this.isMain = false;
    }

    public List<Point> getSides() {
        return sides;
    }

    public PathFindItem getFather() {
        return father;
    }

    public Point getPoint() {
        return point;
    }

    public Integer getF() {
        return f;
    }

    public int getG() {
        return g;
    }

    public int getH() {
        return h;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public boolean isMain() {
        return isMain;
    }

    public void setF(int f) {
        this.f = f;
    }

    public void setFather(PathFindItem father) {
        this.father = father;
    }

    public void setOpen(boolean open) {
        this.isOpen = open;
    }

    public void setClosed(boolean closed) {
        this.isClosed = closed;
    }

    private void init() {
        this.isOpen = false;
        this.isClosed = false;
        this.sides = new LinkedList<Point>(Arrays.asList(
                new Point(point.getX(), point.getY() - 1), //Top
                new Point(point.getX() - 1, point.getY()), //Left
                new Point(point.getX(), point.getY() + 1), //Bottom
                new Point(point.getX() + 1, point.getY()), //Right

                new Point(point.getX() - 1, point.getY() - 1), //Top-Left

                new Point(point.getX() - 1, point.getY() + 1), //Bottom-Left

                new Point(point.getX() + 1, point.getY() + 1), //Bottom-Right

                new Point(point.getX() + 1, point.getY() - 1)  //Top-Right
        ));
    }


    public int count(){
        PathFindItem item = this;
        int i = 1;
        while((item = item.father) != null)
            i++;

        return i;
    }

    public int getDistanceSquared(Point to) {
        int x = Math.abs(this.point.getX() - to.getX());
        int y = Math.abs(this.point.getY() - to.getY());
        return (x * x) + (y * y);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PathFindItem && ((PathFindItem) obj).point.equals(this.point);
    }

    @Override
    public String toString() {
        return this.point.toString();
    }

    @Override
    public int compareTo(PathFindItem o) {
        return this.getF().compareTo(o.getF());
    }

    public void dispose() {

        this.sides.clear();

        if(this.father != null) {
            this.father.dispose();
            this.father = null;
        }
    }
}

