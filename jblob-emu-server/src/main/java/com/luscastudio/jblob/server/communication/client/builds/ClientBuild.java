package com.luscastudio.jblob.server.communication.client.builds;

import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.encryption.RSA;

import java.util.Map;

/**
 * Created by Lucas on 01/10/2016.
 */
public class ClientBuild implements Cloneable {

    private RSA rsa;
    private String buildName;

    private Map<Short, String> incomings;
    private Map<String, Short> outgoings;
    private Map<Short, String> localization;

    private int lastUpdate;

    public ClientBuild(RSA rsa, String build, Map<Short, String> incoming, Map<String, Short> outgoing, int lastUpdate) {

        this.rsa = rsa;
        this.buildName = build;
        this.incomings = incoming;
        this.outgoings = outgoing;
        this.lastUpdate = lastUpdate;
    }

    public String getIncomingHeader(short id) {
        if (this.incomings.containsKey(id))
            return this.incomings.get(id);

        return "UnknownMessageEvent";
    }

    public short getOutgoingHeader(String name) {
        if (this.outgoings.containsKey(name))
            return this.outgoings.get(name);

        return -1;
    }

    public ClientBuild clone() {
        try {

            return (ClientBuild) super.clone();

        } catch (Exception e) {

            BLogger.error(e, this.getClass());
            return null;
        }
    }

    public RSA getRsa() {
        return rsa;
    }

    public Map<Short, String> getIncomings() {
        return incomings;
    }

    public Map<Short, String> getLocalization() {
        return localization;
    }

    public Map<String, Short> getOutgoings() {
        return outgoings;
    }

    public String getBuildName() {
        return buildName;
    }

    public int getLastUpdate() {
        return lastUpdate;
    }
}
