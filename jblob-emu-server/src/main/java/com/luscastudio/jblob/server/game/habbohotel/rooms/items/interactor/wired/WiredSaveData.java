package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by Lucas on 15/02/2017 at 18:49.
 */
public class WiredSaveData {

    private List<Integer> integerList;
    private List<Integer> itemsId;

    private Integer delay;
    private String stringData;

    public WiredSaveData(Collection<Integer> integerList, Collection<Integer> itemsId, Integer delay, String stringData) {
        this.integerList = integerList == null ? null : BCollect.newList(integerList);
        this.itemsId = itemsId == null ? null : BCollect.newList(itemsId);
        this.delay = delay;
        this.stringData = stringData;
    }

    public Integer getDelay() {
        return delay;
    }

    public List<Integer> getIntegerList() {
        return integerList;
    }

    public List<Integer> getItemsId() {
        return itemsId;
    }

    public String getStringData() {
        return stringData;
    }
}
