package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

/**
 * Created by Lucas on 16/10/2016.
 */

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;

import java.util.Collection;
import java.util.List;

public class InventoryFurnisResponseComposer extends MessageComposer {

    public InventoryFurnisResponseComposer(List<FurniProperties> furnis) {
        this(1,1, furnis);
    }

    public InventoryFurnisResponseComposer(int pageCount, int pageIndex, Collection<FurniProperties> furnis) {

        message.putInt(pageCount); //Page Count
        message.putInt(pageIndex); //Page Index

        message.putInt(furnis.size());

        for (FurniProperties furni : furnis) {

            ItemSerializer.serializeInventoryFurni(message, furni);

        }
    }

    @Override
    public String id() {
        return "FurniListMessageComposer";
    }
}
