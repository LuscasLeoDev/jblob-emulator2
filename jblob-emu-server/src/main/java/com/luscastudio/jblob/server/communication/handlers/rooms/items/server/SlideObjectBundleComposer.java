package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.List;

/**
 * Created by Lucas on 16/02/2017 at 14:42.
 */
public class SlideObjectBundleComposer extends MessageComposer {
    @Override
    public String id() {
        return "SlideObjectBundleMessageComposer";
    }

    private List<MoveObjectData> moveObjectDatas;
    private MoveObjectData moveAvatar;

    private Point from;
    private Point to;

    private int triggerItemId;

    public SlideObjectBundleComposer(Point from, Point to, int triggerItemId){
        this.from = from;
        this.to = to;
        this.moveObjectDatas = BCollect.newList();
        this.moveAvatar = null;
        this.triggerItemId = triggerItemId;
    }

    public SlideObjectBundleComposer moveAvatar(int virtualId, double fromZ, double toZ, int moveType){
        this.moveAvatar = new MoveObjectData(virtualId, fromZ, toZ, moveType);
        return this;
    }

    public SlideObjectBundleComposer addMoveItem(int itemId, double fromZ, double toZ){
        this.moveObjectDatas.add(new MoveObjectData(itemId, fromZ, toZ, 0));
        return this;
    }

    @Override
    public void compose() {
        message.putInt(this.from.getX()); //from X ?
        message.putInt(this.from.getY()); //from Y ?
        message.putInt(this.to.getX()); //to X ?
        message.putInt(this.to.getY()); //to Y ?

        message.putInt(this.moveObjectDatas.size()); // ? length

        for (MoveObjectData moveObjectData : this.moveObjectDatas) {

            message.putInt(moveObjectData.objectId); //item id ?
            message.putString(NumberHelper.doubleToString(moveObjectData.fromZ)); //from Z ?
            message.putString(NumberHelper.doubleToString(moveObjectData.toZ)); //to Z ?

        }

        message.putInt(this.triggerItemId); //Trigger item Id?


        if(this.moveAvatar != null){
            message.putInt(this.moveAvatar.moveType); //? Avatar character movetype

            message.putInt(this.moveAvatar.objectId); //?
            message.putString(NumberHelper.doubleToString(this.moveAvatar.fromZ)); //from Z ?
            message.putString(NumberHelper.doubleToString(this.moveAvatar.toZ)); //to Z ?
        }




        this.moveObjectDatas.clear();
        this.moveAvatar = null;

        super.compose();
    }

    private class MoveObjectData {
        private int objectId;
        private double fromZ;
        private double toZ;
        private int moveType;

        public MoveObjectData(int objectId, double fromZ, double toZ, int moveType){
            this.objectId = objectId;
            this.fromZ = fromZ;
            this.toZ = toZ;
            this.moveType = moveType;
        }
    }
}
