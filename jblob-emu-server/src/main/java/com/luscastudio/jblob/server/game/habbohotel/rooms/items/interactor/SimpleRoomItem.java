package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.events.list.AvatarWalkOnFurniEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.RoomItemPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.WallItemCoordinate;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.IRoomItemExtradata;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.SimpleRoomItemExtradata;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 09/10/2016.
 */

public class SimpleRoomItem implements IRoomItem {
    protected RoomItemPosition position;
    protected List<Point> affectedTiles;
    protected Room room;
    //Basic Properties
    protected FurniProperties properties;
    protected IRoomItemExtradata extradata;
    protected WallItemCoordinate wallCoords;

    public SimpleRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        this.room = room;
        this.properties = properties;
        this.position = new RoomItemPosition(properties.getBase().getWidth(), properties.getBase().getLength(), x, y, z, rotation);
        this.affectedTiles = BCollect.newList();
        this.extradata = properties.getExtradata();
        this.wallCoords = new WallItemCoordinate(wallCoordinate);
    }

    @Override
    public int getId() {
        return properties.getId();
    }

    @Override
    public int getBaseId() {
        return properties.getBaseId();
    }

    @Override
    public RoomItemPosition getPosition() {
        return position;
    }

    @Override
    public List<Point> getAffectedTiles() {
        return affectedTiles;
    }

    @Override
    public void setAffectedTiles(List<Point> tiles) {
        this.affectedTiles.clear();
        this.affectedTiles.addAll(tiles);
    }

    @Override
    public IRoomItemExtradata getExtradata() {
        return extradata;
    }

    @Override
    public int getOwnerId() {
        return properties.getOwnerId();
    }

    @Override
    public IItemMessageParser getMessageParser() {
        return properties.getExtradata().getMessageParser();
    }

    @Override
    public FurniBase getBase() {
        return properties.getBase();
    }

    @Override
    public FurniProperties getPorperties() {
        return properties;
    }

    @Override
    public void onUserWalkOn(IRoomAvatar avatar) {
        this.room.getEventHandler().fireEvent("avatar.walk.on.furni", new AvatarWalkOnFurniEventArgs(this, avatar));
    }

    @Override
    public void onUserWalkStand(IRoomAvatar avatar) {

    }

    @Override
    public void onUserWalkOff(IRoomAvatar avatar) {

    }

    @Override
    public boolean canWalk(IRoomAvatar avatar) {
        return this.getBase().isCanStandOn() || this.getBase().isCanSitOn();

    }

    @Override
    public boolean canWalk(IRoomAvatar avatar, Point from) {
        return this.canWalk(avatar);
    }

    @Override
    public boolean canContinuePath(IRoomAvatar avatar, Point from, boolean finalStep) {

        if(this.getBase().isCanSitOn() && (avatar instanceof IRoomPetAvatar))
            return false;

        if (!this.getBase().isCanStandOn()) {
            return getBase().isCanSitOn() && finalStep;
        }

        return true;
    }

    @Override
    public boolean onMove(PlayerSession session, Point from) {
        return true;
    }

    @Override
    public void onMoved(Point from) {

    }

    @Override
    public boolean onPlace(PlayerSession session) {
        return true;
    }

    @Override
    public void onPlaced() {

    }

    @Override
    public boolean onRemove(PlayerSession session) {
        return true;
    }

    @Override
    public boolean onRemove() {
        return false;
    }

    @Override
    public void onRemoved() {

    }

    @Override
    public void onRoomLoad() {

    }

    @Override
    public void onPlayerTrigger(PlayerSession session, int requestId) {
        //todo: make this static
        if(!this.room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_use_furni"))
            return;

        this.toggle();
    }

    @Override
    public void toggle() {
        SimpleRoomItemExtradata extradata = (SimpleRoomItemExtradata)this.extradata;
        extradata.toggle();

        this.room.getRoomItemHandlerService().updateItem(this, this.properties.getBase().getType().equals("i"));
        this.room.getRoomItemHandlerService().saveItem(this);
        this.room.getRoomMap().updateSqHeight(this.affectedTiles);
    }

    @Override
    public double getHeight() {

        if (this.getBase().getHeights().size() == 0)
            return this.getBase().getHeight();

        SimpleRoomItemExtradata extradata = (SimpleRoomItemExtradata)this.extradata;

        int exint = extradata.getExtradataInt();
        if (exint >= this.getBase().getHeights().size())
            return this.getBase().getHeight();

        return this.getBase().getHeights().get(exint);
    }

    @Override
    public int getUseButtonCode() {
        return this.getBase().getInteractionCount() > 0 ? 1 : 0;
    }

    @Override
    public FurniProperties getProperties() {
        return properties;
    }

    @Override
    public WallItemCoordinate getWallCoordinate() {
        return this.wallCoords;
    }

    @Override
    public void dispose() {
        this.position = null;
        this.affectedTiles.clear();
        this.properties = null;
        this.room = null;
        this.extradata = null;
        this.wallCoords = null;
    }

    @Override
    public String toString() {
        return "SimpleRoomItem #" + this.getId() + " / base #" + this.getBase().getId() + "[ " + this.position.getX() + " / " + this.position.getY() + " / " + this.position.getZ() + " ]";
    }
}
