package com.luscastudio.jblob.server.communication.handlers.navigator.savedsearchs.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 23/02/2017 at 19:35.
 */
public class NavigatorSavedSearchesComposer extends MessageComposer {
    @Override
    public String id() {
        return "NavigatorSavedSearchesMessageComposer";
    }

    public NavigatorSavedSearchesComposer(){
        message.putInt(4);

        {
            message.putInt(1);
            message.putString("Eu1"); //Search Code
            message.putString("Eu2"); //Filter
            message.putString("Eu3"); //Localization
        }

        {
            message.putInt(2);
            message.putString("Quero1"); //Search Code
            message.putString("Quero2"); //Filter
            message.putString("Quero3"); //Localization
        }

        {
            message.putInt(3);
            message.putString("Quero1"); //Search Code
            message.putString("Quero2"); //Filter
            message.putString("Quero3"); //Localization
        }

        {
            message.putInt(4);
            message.putString("Quero1"); //Search Code
            message.putString("Quero2"); //Filter
            message.putString("Quero3"); //Localization
        }


    }
}
