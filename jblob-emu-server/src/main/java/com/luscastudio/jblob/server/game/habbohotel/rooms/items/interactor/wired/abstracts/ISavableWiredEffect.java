package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts;

import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 15/02/2017 at 21:26.
 */
public interface ISavableWiredEffect {
    void saveWiredData(PlayerSession session, List<Integer> integerList, String stringData, List<Integer> selectedFurniList, int delay, int lastInt1);
}
