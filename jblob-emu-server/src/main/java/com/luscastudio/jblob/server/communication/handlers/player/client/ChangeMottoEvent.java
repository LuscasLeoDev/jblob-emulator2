//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.player.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.server.UserChangeComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 03/06/2017 at 17:40.
 */
public class ChangeMottoEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        String motto = packet.getString().trim();

        session.getAvatar().setMotto(motto);

        if(session.getAvatar().getCurrentRoom() != null) {
            RoomPlayerAvatar playerAvatar = session.getAvatar().getCurrentRoom().getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());
            if(playerAvatar != null) // Anything...
                session.getAvatar().getCurrentRoom().sendMessage(new UserChangeComposer(playerAvatar));
        }
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
