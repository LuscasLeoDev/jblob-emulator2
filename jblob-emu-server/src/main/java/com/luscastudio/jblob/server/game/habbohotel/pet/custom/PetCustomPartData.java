package com.luscastudio.jblob.server.game.habbohotel.pet.custom;

/**
 * Created by Lucas on 08/02/2017 at 20:29.
 */
public class PetCustomPartData implements IPetPartData {

    private int partType;
    private int partId;
    private int rarityLevel;
    private String name;

    public PetCustomPartData(int partType, int partId, int rarityLevel, String name) {
        this.partId = partId;
        this.partType = partType;
        this.rarityLevel = rarityLevel;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getRarityLevel() {
        return rarityLevel;
    }

    public int getPartType() {
        return partType;
    }

    public int getPartId() {
        return partId;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof IPetPartData && ((IPetPartData)o).getPartType() == this.partType && ((IPetPartData)o).getPartId() == this.partId;
    }
}