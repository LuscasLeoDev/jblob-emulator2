package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 15/02/2017 at 20:20.
 */
public class AvatarEffectComposer extends MessageComposer {
    @Override
    public String id() {
        return "AvatarEffectMessageComposer";
    }

    public AvatarEffectComposer(int virtualId, int effectid, int delay){
        message.putInt(virtualId);
        message.putInt(effectid);
        message.putInt(delay);
    }
}
