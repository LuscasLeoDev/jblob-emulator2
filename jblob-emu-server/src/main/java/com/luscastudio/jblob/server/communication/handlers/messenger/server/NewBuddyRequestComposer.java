package com.luscastudio.jblob.server.communication.handlers.messenger.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 27/02/2017 at 15:42.
 */
public class NewBuddyRequestComposer extends MessageComposer {
    @Override
    public String id() {
        return "NewBuddyRequestMessageComposer";
    }

    public NewBuddyRequestComposer(int id, String username, String figure) {
        message.putInt(id);
        message.putString(username);
        message.putString(figure);
    }
}
