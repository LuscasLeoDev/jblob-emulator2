package com.luscastudio.jblob.server.communication.handlers.catalog.server;

/**
 * Created by Lucas on 28/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class CatalogUpdateComposer extends MessageComposer {
    @Override
    public String id() {
        return "CatalogUpdatedMessageComposer";
    }

    public CatalogUpdateComposer(boolean open, String newFurnitureLink) {
        message.putBool(open);
        message.putString(newFurnitureLink);
    }

    public CatalogUpdateComposer(boolean open) {
        message.putBool(open);
    }
}
