package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 01/10/2016.
 */
public class AuthenticationOKComposer extends MessageComposer {
    public AuthenticationOKComposer() {

    }

    @Override
    public String id() {
        return "AuthenticationOKMessageComposer";
    }

}
