package com.luscastudio.jblob.server.game.habbohotel.avatar.currency;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 19/09/2016.
 */
public class AvatarCurrencyBalance {

    public static int CREDITS_TYPE = 1;

    private Map<Integer, AvatarCurrency> currencies;
    private Map<Integer, AvatarCurrency> toInsert;
    private Map<Integer, AvatarCurrency> toUpdate;

    private HabboAvatar avatar;

    private boolean saving;

    public AvatarCurrencyBalance(HabboAvatar avatar){
        this.currencies = BCollect.newConcurrentMap();
        this.toUpdate = BCollect.newConcurrentMap();
        this.toInsert = BCollect.newConcurrentMap();

        this.avatar = avatar;

        this.saving = false;

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            this.load(reactor);

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }

    public AvatarCurrencyBalance(HabboAvatar avatar, DBConnReactor reactor) throws SQLException {
        this.currencies = BCollect.newConcurrentMap();
        this.toUpdate = BCollect.newConcurrentMap();
        this.toInsert = BCollect.newConcurrentMap();
        this.avatar = avatar;
        this.load(reactor);
    }

    private void parseData(ResultSet set) throws SQLException {
        int cId;

        while(set.next()){
            this.currencies.put(cId = set.getInt("type"), new AvatarCurrency(cId, set.getInt("value")));
        }
    }

    public void load(DBConnReactor reactor) throws SQLException {
        DBConnPrepare prepare = reactor.prepare("SELECT * FROM players_avatars_currencies WHERE user_id = ?");

        prepare.setInt(1, avatar.getId());

        ResultSet set = prepare.runQuery();

        this.parseData(set);
    }

    private synchronized AvatarCurrency getCurrency(int id) {
        if(!this.currencies.containsKey(id)){
            AvatarCurrency newCurrency = new AvatarCurrency(id, 0);
            this.toInsert.put(id, newCurrency);
            return newCurrency;
        }

        return this.currencies.get(id);
    }

    public synchronized int get(int id) {
        if (this.currencies.containsKey(id))
            return this.currencies.get(id).getValue();
        return 0;
    }

    public synchronized void set(int id, int value){
        AvatarCurrency currency;
        if(!this.currencies.containsKey(id)) {
            this.currencies.put(id, currency = new AvatarCurrency(id, value));
            this.toInsert.put(id, currency);
        }
        else {
            currency = this.currencies.get(id);
            currency.setValue(value);
            if(!this.toUpdate.containsKey(id))
                this.toUpdate.put(id, currency);
        }

    }

    public synchronized  List<AvatarCurrency> getList() {
        return BCollect.newList(this.currencies.values());
    }

    public synchronized  int getCreditValue() {
        return this.get(CREDITS_TYPE);
    }

    public synchronized  void decrease(int value) {
        this.set(CREDITS_TYPE, getCreditValue() - value);
    }

    public synchronized  void decrease(int id, int amount) {
        this.set(id, get(id) - amount);
    }

    public synchronized  void increase(int value) {
        this.set(CREDITS_TYPE, getCreditValue() + value);
    }

    public synchronized  void increase(int id, int amount) {
        this.set(id, get(id) + amount);
    }

    public void save(){
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            this.save(reactor);

        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    public synchronized void save(DBConnReactor reactor) throws SQLException{

        Iterator<AvatarCurrency> currencyIterator;
        AvatarCurrency currency;

        DBConnPrepare prepare;

        currencyIterator = this.toInsert.values().iterator();
        while(currencyIterator.hasNext()){
            currency = currencyIterator.next();
            prepare = reactor.prepare("INSERT INTO players_avatars_currencies (user_id, type, value) VALUES (?, ?, ?)", true);
            prepare.setInt(1, avatar.getId());
            prepare.setInt(2, currency.getCurrencyId());
            prepare.setInt(3, currency.getValue());

            prepare.runInsert();

            currencyIterator.remove();
        }

        currencyIterator = this.toUpdate.values().iterator();
        while(currencyIterator.hasNext()){
            currency = currencyIterator.next();
            prepare = reactor.prepare("UPDATE players_avatars_currencies SET value = ? WHERE type = ? AND user_id = ?");
            prepare.setInt(1, currency.getValue());
            prepare.setInt(2, currency.getCurrencyId());
            prepare.setInt(3, avatar.getId());

            prepare.runUpdate();

            currencyIterator.remove();
        }

    }


    public void dispose() {
        this.toInsert.clear();
        this.toUpdate.clear();
        this.currencies.clear();
    }
}
