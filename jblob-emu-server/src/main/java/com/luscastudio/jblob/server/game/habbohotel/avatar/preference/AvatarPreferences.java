package com.luscastudio.jblob.server.game.habbohotel.avatar.preference;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Lucas on 09/12/2016.
 */

public class AvatarPreferences {

    public static final String VOLUME_SYSTEM = "volume.system";
    public static final String VOLUME_FURNI = "volume.furni";
    public static final String VOLUME_MUSIC = "volume.music";

    public static final String OLD_CHAT_ENABLED = "old.chat.enabled";
    public static final String MESSENGER_INVINTE_ENABLED = "messenger.invinte.enabled";
    public static final String DISABLE_CAMERA_FOLLOW = "disable.camera.follow";
    public static final String FRIENDBAR_STATUS = "friendbar.status";
    public static final String LAST_CHAT_BUBBLE = "last.chat.bubble";

    private Map<String, AvatarPreference> preferences;

    private Map<String, AvatarPreference> toInsert;
    private Map<String, AvatarPreference> toUpdate;
    private boolean saving;

    private HabboAvatar avatar;

    public AvatarPreferences(HabboAvatar avatar){

        this.saving = false;

        this.avatar = avatar;

        this.preferences = BCollect.newConcurrentMap();

        this.toInsert = BCollect.newConcurrentMap();
        this.toUpdate = BCollect.newConcurrentMap();

        this.load();

    }

    public AvatarPreferences(HabboAvatar avatar, DBConnReactor reactor) throws SQLException {
        this.saving = false;

        this.avatar = avatar;

        this.preferences = BCollect.newConcurrentMap();

        this.toInsert = BCollect.newConcurrentMap();
        this.toUpdate = BCollect.newConcurrentMap();

        this.load(reactor);
    }

    private AvatarPreference add(String key, String val) {
        AvatarPreference preference = new AvatarPreference(key, val);
        this.preferences.put(key, preference);
        this.toInsert.put(key, preference);
        return preference;
    }

    public boolean hasKey(String key){
        return preferences.containsKey(key);
    }

    //region String Getter

    public String get(String key){
        AvatarPreference value = this.preferences.get(key);
        return value == null ? null : value.getValue();
    }

    public String getOrSet(String key, String value){
        String v = get(key);
        if(v == null)
            return add(key, value).getValue();
        return v;
    }

    public String getOrDefault(String key, String value){
        String r;
        return (r = get(key)) != null ? r : value;
    }

    public void  set(String key, String value){
        if(this.get(key) == null) {
            add(key, value);
            return;
        }

        AvatarPreference preference = this.preferences.get(key);
        preference.setValue(value);
        this.toUpdate.put(key, preference);
    }

    //endregion

    //region Int Getter
    public Integer getInt(String key){
        AvatarPreference value = this.preferences.get(key);
        return value == null || !NumberHelper.isInteger(value.getValue()) ? null : Integer.parseInt(value.getValue());
    }

    public int getIntOrSet(String key, int value){
        Integer v = getInt(key);
        if(v == null)
            return Integer.parseInt(add(key, String.valueOf(value)).getValue());
        return v;
    }

    public int getIntOrDefault(String key, int value){
        Integer v = getInt(key);
        if(v == null)
            return value;
        return v;
    }

    public void setInt(String key, int value) {
        set(key, String.valueOf(value));
    }

    //endregion

    //region Bool Getter
    public Boolean getBool(String key){
        return getIntOrDefault(key, 0) == 1;
    }

    public boolean getBoolOrSet(String key, boolean value){
        return getIntOrSet(key, value ? 1 : 0) == 1;
    }

    public boolean getBoolOrDefault(String key, boolean def){
        return hasKey(key) ? getInt(key) == 1 : def;
    }

    public void setBool(String key, boolean value){
        setInt(key, value ? 1 : 0);
    }

    //endregion

    public void load(){

        this.preferences.clear();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){



        }catch (Exception e){
            BLogger.error("Error when getting user preferences", this.getClass());
        }
    }

    public void load(DBConnReactor reactor) throws SQLException {
        DBConnPrepare prepare = reactor.prepare("SELECT * FROM avatars_preferences WHERE avatar_id = ?");
        prepare.setInt(1, avatar.getId());

        ResultSet set = prepare.runQuery();

        while(set.next()){
            String key;
            this.preferences.put(key = set.getString("name"), new AvatarPreference(key, set.getString("value")));
        }
    }


    public synchronized void save() {
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            this.save(reactor);
        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    public void dispose() {
        this.preferences.clear();
    }

    public void save(DBConnReactor reactor) throws SQLException{

        Iterator<AvatarPreference> preferenceIterator;
        DBConnPrepare prepare;
        AvatarPreference preference;

        preferenceIterator = toInsert.values().iterator();
        while(preferenceIterator.hasNext()){
            preference = preferenceIterator.next();
            prepare = reactor.prepare("INSERT INTO avatars_preferences (avatar_id, name, value) VALUES (?, ?, ?)", true);
            prepare.setInt(1, avatar.getId());
            prepare.setString(2, preference.getName());
            prepare.setString(3, preference.getValue());

            prepare.runInsert();

            preferenceIterator.remove();
        }

        preferenceIterator = toUpdate.values().iterator();
        while(preferenceIterator.hasNext()) {
            preference = preferenceIterator.next();
            prepare = reactor.prepare("UPDATE avatars_preferences SET value = ? WHERE name = ? AND  avatar_id = ?");
            prepare.setString(1, preference.getValue());
            prepare.setString(2, preference.getName());
            prepare.setInt(3, avatar.getId());

            prepare.run();

            preferenceIterator.remove();
        }

    }
}
