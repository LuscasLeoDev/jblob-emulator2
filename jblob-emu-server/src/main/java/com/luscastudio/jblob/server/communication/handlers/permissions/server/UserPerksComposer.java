package com.luscastudio.jblob.server.communication.handlers.permissions.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/10/2016.
 */
public class UserPerksComposer extends MessageComposer {
    public UserPerksComposer() {
        message.putInt(16); // Count
        message.putString("USE_GUIDE_TOOL");
        message.putString("");
        message.putBool(true);
        message.putString("GIVE_GUIDE_TOURS");
        message.putString("");
        message.putBool(true);
        message.putString("JUDGE_CHAT_REVIEWS");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("VOTE_IN_COMPETITIONS");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("CALL_ON_HELPERS");
        message.putString(""); // ??
        message.putBool(false);
        message.putString("CITIZEN");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("TRADE");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("HEIGHTMAP_EDITOR_BETA");
        message.putString(""); // ??
        message.putBool(false);
        message.putString("EXPERIMENTAL_CHAT_BETA");
        message.putString("");
        message.putBool(true);
        message.putString("EXPERIMENTAL_TOOLBAR");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("BUILDER_AT_WORK");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("CAMERA");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("NAVIGATOR_PHASE_ONE_2014");
        message.putString(""); // ??
        message.putBool(false);
        message.putString("NAVIGATOR_PHASE_TWO_2014");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("MOUSE_ZOOM");
        message.putString(""); // ??
        message.putBool(true);
        message.putString("NAVIGATOR_ROOM_THUMBNAIL_CAMERA");
        message.putString(""); // ??
        message.putBool(true);//
        message.putString("HABBO_CLUB_OFFER_BETA");
        message.putString(""); // ??
        message.putBool(true);//
    }

    @Override
    public String id() {
        return "UserPerksMessageComposer";
    }
}
