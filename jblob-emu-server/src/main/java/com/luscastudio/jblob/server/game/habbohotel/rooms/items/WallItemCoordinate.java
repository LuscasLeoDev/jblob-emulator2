package com.luscastudio.jblob.server.game.habbohotel.rooms.items;

/**
 * Created by Lucas on 20/12/2016 at 18:45.
 */
public class WallItemCoordinate {

    private String data;

    public WallItemCoordinate(String data){
        this.data = data;
    }

    @Override
    public String toString() {
        return data;
    }

    public void set(String data){
        this.data = data;
    }
}
