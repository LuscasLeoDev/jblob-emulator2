package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.handlers.navigator.RoomSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

import java.util.Collection;

public class RoomUsersComposer extends MessageComposer {
    @Override
    public String id() {
        return "UsersMessageComposer";
    }

    public RoomUsersComposer(Collection<IRoomAvatar> avatars) {

        message.putInt(avatars.size());

        for (IRoomAvatar avatar : avatars) {
            RoomSerializer.serializeRoomAvatar(avatar, this.message);
        }
    }

    public RoomUsersComposer(IRoomAvatar avatar) {
        message.putInt(1);
        RoomSerializer.serializeRoomAvatar(avatar, this.message);
    }
}
