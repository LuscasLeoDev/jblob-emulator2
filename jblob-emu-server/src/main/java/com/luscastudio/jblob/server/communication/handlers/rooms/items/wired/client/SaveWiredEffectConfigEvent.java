package com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.events.list.WiredSaveDataEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.ISavableWiredEffect;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 15/02/2017 at 21:24.
 */
public class SaveWiredEffectConfigEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int itemId = packet.getInt();

        int integersCount = packet.getInt();
        List<Integer> integerList = BCollect.newList();
        for (int i = 0; i < integersCount; i++)
            integerList.add(packet.getInt());

        String stringData = packet.getString();

        int selectedItemsLen = packet.getInt();
        List<Integer> selectedFurniList = BCollect.newList();
        for (int i = 0; i < selectedItemsLen; i++)
            selectedFurniList.add(packet.getInt());

        int delay = packet.getInt();

        int lasInt = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);
        if(item != null) {


            if ((item instanceof ISavableWiredEffect))
                ((ISavableWiredEffect) item).saveWiredData(session, integerList, stringData, selectedFurniList, delay, lasInt);
        }

        session.getAvatar().getEventHandler().fireEvent("save.wired.effect", new WiredSaveDataEventArgs(new WiredSaveData(integerList, selectedFurniList, delay, stringData), itemId));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
