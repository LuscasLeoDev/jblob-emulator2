package com.luscastudio.jblob.server.game.habbohotel.navigator.categories;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.navigator.flatcats.NavigatorFlatCategory;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Lucas on 18/12/2016 at 16:22.
 */
public class ByCategoryFlatCat extends NavigatorCategory {

    private int categoryId;
    private NavigatorFlatCategory flatCategory;
    public ByCategoryFlatCat(int id, String identifier, String name, int parentId, int orderNum, boolean collapse, int expandStatus, int showType, String extradata) {
        super(id, identifier, name, parentId, orderNum, collapse, expandStatus, showType, extradata);
        try{
            this.categoryId = Integer.parseInt(extradata);
        } catch (Exception e){
            this.categoryId = 1;
            BLogger.error(e, this.getClass());
        }
    }

    @Override
    public void init(DBConnReactor reactor) throws SQLException {
        this.flatCategory = JBlob.getGame().getNavigatorManager().getFlatCategory(this.categoryId);
    }

    @Override
    protected void onRoomUnload(RoomProperties sender) {

    }

    @Override
    public List<RoomProperties> getRoomsData(PlayerSession session, String search) {
        List<RoomProperties> properties = BCollect.newList();

        if(search.isEmpty()) {
            for (RoomProperties room : JBlob.getGame().getRoomManager().getActiveRoomList()) {
                if (room.getCategoryId() == this.categoryId)
                    properties.add(room);
            }
        }

        return properties;
    }
}
