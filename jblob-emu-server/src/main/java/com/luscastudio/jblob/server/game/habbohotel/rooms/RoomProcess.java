package com.luscastudio.jblob.server.game.habbohotel.rooms;

import com.google.common.collect.Queues;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 04/10/2016.
 */

public class RoomProcess implements Runnable {

    private ScheduledFuture scheduledFuture;
    private boolean started;
    private int loop;

    private Queue<RunProcess> processList;

    public RoomProcess() {
        this.loop = 1;
        this.processList = Queues.newConcurrentLinkedQueue();
    }

    public void start() {
        this.started = true;
        this.scheduledFuture = JBlob.getGame().getProcessManager().runLoop(this, 1, this.loop, TimeUnit.MILLISECONDS);
    }

    public void restart(boolean forceStop) {
        this.stop();
        this.start();
    }

    public void stop(boolean forced) {
        if (!started)
            return;

        this.scheduledFuture.cancel(forced);
    }

    public void stop() {
        this.stop(false);
    }

    public synchronized void enqueue(RunProcess runProcess) {
        this.processList.add(runProcess);
    }

    @Override
    public void run() {
        synchronized (this.processList) {
            Iterator<RunProcess> iterator = processList.iterator();

            while (iterator.hasNext()) {
                RunProcess process = iterator.next();
                if (process.isEnd())
                    iterator.remove();
                else
                    try {
                        process.doCycle();
                    } catch (Exception e) {
                        iterator.remove();
                        BLogger.error(e, this.getClass());
                    }
            }
        }
    }
}
