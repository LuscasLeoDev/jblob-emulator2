//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.catalog.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 29/06/2017 at 20:39.
 */
public class GiftWrappingErrorComposer extends MessageComposer {
    @Override
    public String id() {
        return "GiftWrappingErrorMessageComposer";
    }

    public GiftWrappingErrorComposer() {

    }
}
