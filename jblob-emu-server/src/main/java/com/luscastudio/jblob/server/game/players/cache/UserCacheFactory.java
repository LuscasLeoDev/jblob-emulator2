package com.luscastudio.jblob.server.game.players.cache;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.accounts.PlayerAccount;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.avatar.currency.AvatarCurrencyBalance;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;
import com.luscastudio.jblob.server.game.habbohotel.friendship.AvatarFriendship;
import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Lucas on 02/10/2016.
 */
public class UserCacheFactory {

    private static Logger log = Logger.getLogger(UserCacheFactory.class);

    private Map<Integer, IAvatarDataCache> cachedUsers;
    private Map<String, IAvatarDataCache> cachedUsersByName;

    //region Avatar examples cache

    private IAvatarDataCache genericError;
    private IAvatarDataCache genericFrank;

    //enregion

    public UserCacheFactory() {
        this.cachedUsers = BCollect.newMap();
        this.cachedUsersByName = BCollect.newMap();
    }

    public static PlayerAccount createPlayerAccount(ResultSet set, PlayerSession session, String ssoTicket) {
        return null;
    }

    public HabboAvatar createHabboAvatar(DBConnReactor reactor, ResultSet set, PlayerSession session, String ssoTicket) {

        try {
            HabboAvatar avatar = new HabboAvatar(session, set);
            /*avatar.setId(set.getInt("id"));
            avatar.setUsername(set.getString("username"));
            avatar.setMotto(set.getString("motto"));
            avatar.setFigure(new AvatarFigure(set.getString("figure_string"), set.getString("gender"), avatar));
            avatar.setHomeRoom(set.getInt("home_room"));*/


            avatar.setFriendship(new AvatarFriendship(avatar, reactor));
            avatar.setInventory(new AvatarInventory(avatar.getId(), reactor));
            avatar.setBalance(new AvatarCurrencyBalance(avatar, reactor));
            avatar.setPreferences(new AvatarPreferences(avatar, reactor));

            avatar.getExecutorService().start();

            avatar.getInventory().load(reactor);

            setCache(avatar);

            return avatar;
        } catch (SQLException e) {
            log.error("Could not get information from user SSO TICKET [" + ssoTicket + "]", e);
            return null;
        }
    }

    public HabboAvatar createHabboAvatar(int avatarId, PlayerSession session, String ssoTicket) {

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM players_avatar WHERE id = ?").setInt(1, avatarId).back();

            ResultSet set = prepare.runQuery();

            if (!set.next())
                return null;

            return createHabboAvatar(reactor, set, session, ssoTicket);

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
            return null;
        }

    }

    public IAvatarDataCache getUserCache(int id) {

        if (cachedUsers.containsKey(id))
            return cachedUsers.get(id);

        return generateUser(id);

    }

    public IAvatarDataCache getUserCacheByName(String name) {
        if(cachedUsersByName.containsKey(name))
            return cachedUsersByName.get(name);
        return generateUser(name);
    }

    public IAvatarDataCache generateUser(String name) {
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM players_avatar WHERE username = ?").setString(1, name).back();

            ResultSet set = prepare.runQuery();

            if (!set.next())
                return null;

            AvatarDataCache cache = generateUser(set);

            setCache(cache);

            return cache;

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
            return null;
        }

    }

    public IAvatarDataCache generateUser(int id) {

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM players_avatar WHERE id = ?").setInt(1, id).back();

            ResultSet set = prepare.runQuery();

            if (!set.next())
                return null;

            AvatarDataCache cache = generateUser(set);

            setCache(cache);

            return cache;

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
            return null;
        }

    }

    public AvatarDataCache generateUser(ResultSet set) throws SQLException {
        return new AvatarDataCache(set.getInt("id"), set.getString("username"), set.getString("figure_string"), set.getString("motto"), set.getInt("last_online_timestamp"), set.getString("gender"));
    }

    public void setCache(IAvatarDataCache cache) {

        if (cachedUsers.containsKey(cache.getId()))
            cachedUsers.replace(cache.getId(), cache);
        else
            cachedUsers.put(cache.getId(), cache);

        if(cachedUsersByName.containsKey(cache.getUsername()))
            cachedUsersByName.replace(cache.getUsername(), cache);
        else
            cachedUsersByName.put(cache.getUsername(), cache);

    }

    public void onSessionDisconnect(PlayerSession session) {

        // If not authenticated and even this way disconnected
        if (session.getAvatar() == null) {
            return;
        }

        if (!cachedUsers.containsKey(session.getAvatar().getId()))
            return;

        HabboAvatar avatar = session.getAvatar();

        AvatarDataCache cache = new AvatarDataCache(avatar.getId(), avatar.getUsername(), avatar.getFigureString(), avatar.getMotto(), avatar.getLastOnlineTimestamp(), avatar.getGender(), avatar.getRank());

        setCache(cache);
    }

    public IAvatarDataCache getGenericError() {
        if (genericError == null)
            genericError = new AvatarDataCache(0, "Unknown", "", "Oppss!", 0, "M");
        return genericError;
    }

    public IAvatarDataCache getGenericFrank() {
        if (genericFrank == null)
            genericFrank = new AvatarDataCache(0, "Frank", "", "jBlob Reception", 0, "M");
        return genericFrank;
    }
}
