package com.luscastudio.jblob.server.protocols;

import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.debug.BLogger;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Created by Lucas on 18/09/2016.
 */
public class ClientResponseIn extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        try {
            if (in.readableBytes() > 0 && in.isReadable()) {

                if (in.readableBytes() < 6) {
                    return;
                }

                in.markReaderIndex();
                int sla = in.readInt();
                if (in.readableBytes() < sla) {
                    in.resetReaderIndex();
                    return;
                }

                if (sla < 0) {
                    return;
                }

                out.add(new MessageEvent(ctx, in.readBytes(sla)));

                in.readableBytes();

            }
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

    }
}
