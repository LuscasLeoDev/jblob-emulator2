package com.luscastudio.jblob.server.communication.handlers.rooms.groups.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 10/12/2016.
 */

public class HabboGroupBadgesComposer extends MessageComposer {
    @Override
    public String id() {
        return "HabboGroupBadgesMessageComposer";
    }

    public HabboGroupBadgesComposer(int groupId, String badge){
        message.putInt(1);
        message.putInt(groupId);
        message.putString(badge);
    }

    //public HabboGroupBadgesComposer(List<>)
}
