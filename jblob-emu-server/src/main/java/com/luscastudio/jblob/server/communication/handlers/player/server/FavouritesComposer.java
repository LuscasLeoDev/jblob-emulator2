package com.luscastudio.jblob.server.communication.handlers.player.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.List;

/**
 * Created by Lucas on 10/12/2016.
 */

public class FavouritesComposer extends MessageComposer {
    @Override
    public String id() {
        return "FavouritesMessageComposer";
    }

    public FavouritesComposer(){
        message.putInt(50);
        message.putInt(0);
    }

    public FavouritesComposer(int limit, List<Integer> favorites){
        message.putInt(limit);

        message.putInt(favorites.size());
        for(int i : favorites)
            message.putInt(i);
    }
}
