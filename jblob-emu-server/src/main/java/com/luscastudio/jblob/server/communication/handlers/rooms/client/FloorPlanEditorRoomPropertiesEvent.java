package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.FloorPlanFloorMapComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.FloorPlanSendDoorComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomVisualizationSettingsComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 02/02/2017 at 18:04.
 */
public class FloorPlanEditorRoomPropertiesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();

        if(room == null)
            return;

        session.sendQueueMessage(new FloorPlanFloorMapComposer(room.getRoomItemHandlerService().getFloorItems().values()));
        session.sendQueueMessage(new FloorPlanSendDoorComposer(room.getProperties().getModel().getDoorPosition()));
        session.sendQueueMessage(new FloorHeightMapComposer(room.getProperties().getModel()));
        session.sendQueueMessage(new RoomVisualizationSettingsComposer(room.getProperties().getHideWall() == 1, room.getProperties().getWallThickness(), room.getProperties().getFloorThickness()));

        session.flush();
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
