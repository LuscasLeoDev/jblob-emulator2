package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.horse;

/**
 * Created by Lucas on 18/02/2017 at 16:11.
 */
public class HorseData {
    private int saddleId;
    private int saddlePartId;
    private boolean anyoneCanRide;
    private int saddleEffectId;

    public HorseData(){
        this.anyoneCanRide = false;
        this.saddleId = -1;
        this.saddleEffectId = 0;
    }

    public boolean anyoneCanRide() {
        return anyoneCanRide;
    }

    public void setAnyoneCanRide(boolean anyoneCanRide) {
        this.anyoneCanRide = anyoneCanRide;
    }

    public int getSaddleId() {
        return saddleId;
    }

    public void setSaddleId(int saddleId) {
        this.saddleId = saddleId;
    }

    public int getSaddleEffectId() {
        return saddleEffectId;
    }

    public void setSaddleEffectId(int saddleEffectId) {
        this.saddleEffectId = saddleEffectId;
    }

    public int getSaddlePartId() {
        return saddlePartId;
    }

    public void setSaddlePartId(int saddlePartId) {
        this.saddlePartId = saddlePartId;
    }
}
