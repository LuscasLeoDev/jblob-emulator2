package com.luscastudio.jblob.server.communication.handlers.messenger.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.FriendListUpdateComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 14/02/2017 at 14:49.
 */
public class RemoveBuddyEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int friendLen = packet.getInt();
        int i = 0;

        FriendListUpdateComposer composer = new FriendListUpdateComposer(BCollect.newMap());
        while(i++ < friendLen){
            int friendId = packet.getInt();
            if(i++ > JBlob.getGame().getDbConfig().getInt("messenger.delete.friend.limit"))
                break;

            PlayerSession friendSession = JBlob.getGame().getSessionManager().getSession(friendId);

            if(friendSession != null){
                friendSession.getAvatar().getFriendship().removeFriend(session.getAvatar().getId());
                friendSession.sendMessage(new FriendListUpdateComposer(BCollect.newMap()).addUpdate(session.getAvatar().getId()));
            }

            composer.addUpdate(friendId);

            try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

                DBConnPrepare prepare = reactor.prepare("DELETE FROM avatars_friendships WHERE (user_one_id = ? AND user_two_id = ?) OR (user_one_id = ? AND user_two_id = ?)");

                prepare.setInt(1, session.getAvatar().getId());
                prepare.setInt(2, friendId);
                prepare.setInt(3, friendId);
                prepare.setInt(4, session.getAvatar().getId());

                prepare.run();

            }catch(Exception e){
                BLogger.error(e, this.getClass());
            }
        }

        session.sendMessage(composer);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
