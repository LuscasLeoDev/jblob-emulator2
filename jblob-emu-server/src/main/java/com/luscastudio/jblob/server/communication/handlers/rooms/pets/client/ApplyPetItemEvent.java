package com.luscastudio.jblob.server.communication.handlers.rooms.pets.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.IPetAppliableItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.PetItemRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 10/02/2017 at 02:42.
 */
public class ApplyPetItemEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int itemId = packet.getInt();
        int petId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);

        if(item == null)
            return;

        IRoomPetAvatar pet = room.getRoomAvatarService().getPet(petId);

        if(pet == null)
            return;

        if(!(pet instanceof IPetAppliableItem))
            return;

        if(!(item instanceof PetItemRoomItem))
            return;

        ((IPetAppliableItem)pet).applyItem((PetItemRoomItem)item);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
