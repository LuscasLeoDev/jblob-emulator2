package com.luscastudio.jblob.server.communication.handlers.catalog.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.List;

/**
 * Created by Lucas on 29/01/2017 at 15:54.
 */
public class SellablePetBreedsComposer extends MessageComposer {
    @Override
    public String id() {
        return "SellablePetBreedsMessageComposer";
    }

    private String petTypeCode;
    private List<SellablePetBreed> breedList;

    public SellablePetBreedsComposer(String petTypeCode){
        this.petTypeCode = petTypeCode;
        this.breedList = BCollect.newList();
    }

    @Override
    public void compose() {
        super.compose();

        message.putString(petTypeCode);

        message.putInt(breedList.size());

        for(SellablePetBreed breed : breedList){
            message.putInt(breed.getType());
            message.putInt(breed.getRaceId());
            message.putInt(breed.getPaletteId());

            message.putBool(breed.isSellable());
            message.putBool(breed.isRare());
        }
    }

    public SellablePetBreedsComposer addBreed(int type, int raceId, int paletteId, boolean sellable, boolean rare) {
        this.breedList.add(new SellablePetBreed(type, raceId, paletteId, sellable, rare));
        return this;
    }

    class SellablePetBreed {
        private int type;
        private int raceId;
        private int paletteId;

        private boolean sellable;
        private boolean rare;

        public SellablePetBreed(int type, int raceId, int paletteId, boolean sellable, boolean rare){
            this.type = type;
            this.raceId = raceId;
            this.paletteId = paletteId;
            this.sellable = sellable;
            this.rare = rare;
        }

        public int getRaceId() {
            return raceId;
        }

        public int getPaletteId() {
            return paletteId;
        }

        public int getType() {
            return type;
        }

        public boolean isRare() {
            return rare;
        }

        public boolean isSellable() {
            return sellable;
        }
    }
}
