package com.luscastudio.jblob.server.communication.handlers.player.profile.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.profile.server.AvatarSelectedBadgeList;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 11/11/2016.
 */

public class GetSelectedBadgesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        session.sendMessage(new AvatarSelectedBadgeList(JBlob.getGame().getUserCacheManager().getUserCache(packet.getInt())));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
