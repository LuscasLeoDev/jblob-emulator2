//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.ChatComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server.ActionComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server.RespectNotificationComposer;
import com.luscastudio.jblob.server.events.list.AvatarMoveEventArgs;
import com.luscastudio.jblob.server.events.list.AvatarStandOnSquareEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 08/06/2017 at 22:29.
 */
public class RespectUserEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

            //Etapa 1: Pega as variaveis que vêm no packet
            int userId = packet.getInt();

            //Etapa 2: Pega as variaves com os dados do usuário e checa pra ver se não tem nenhuma invalidez
            HabboAvatar avatar = session.getAvatar();

            //Se o usuario nao tem mais respeito pra dar, volta
            if (avatar.getRespectsToGiveCount() <= 0)
                return;

            //Se esse usuario não estivar em um quarto (geralmente isso é packet injection)
            if (avatar.getCurrentRoom() == null)
                return;

            //Declara o quarto pra usar depois
            Room currentRoom = avatar.getCurrentRoom();

            //Pega o usuário a ser respeitado pelo gerenciador de avatares do quarto
            RoomPlayerAvatar respectedAvatar = currentRoom.getRoomAvatarService().getRoomAvatarByUserId(userId);

            //Se esse usuário não existir no quarto, volta
            if (respectedAvatar == null)
                return;

            //Pega o avatar do proprio usuário quem vai respeitar
            RoomPlayerAvatar roomAvatar = currentRoom.getRoomAvatarService().getRoomAvatarByUserId(avatar.getId());

            //Se ele, por algum motivo, não existir no quarto, volta
            if (roomAvatar == null)
                return;

        //Etapa 3: Mudança dentro do sistema
            //Muda os dados dos usuarios, tanto respeitos de um como respeitos pra dar do outro.
        respectedAvatar.getAvatar().setRespectCount(respectedAvatar.getAvatar().getRespectCount() + 1);
        roomAvatar.getAvatar().setRespectsToGiveCount(roomAvatar.getAvatar().getRespectsToGiveCount() - 1);


        //Etapa 4: mudança na interface
            //Manda os packets que fazer aparecer as notificações, levanta o dedo do avatar e muda um deles de posição

        Runnable doIt = () -> {
            roomAvatar.getPosition().setRotation(roomAvatar.getPosition().getPoint().getRotation(respectedAvatar.getPosition().getPoint()));

            roomAvatar.getPosition().setHeadRotation(roomAvatar.getPosition().getPoint().getRotation(respectedAvatar.getPosition().getPoint()));
            roomAvatar.updateNeeded(true);

            currentRoom.sendQueueMessage(new ActionComposer(roomAvatar.getVirtualId(), 7));
            currentRoom.sendQueueMessage(new RespectNotificationComposer(respectedAvatar.getId(), 1));
            currentRoom.flush();
        };

        Point squareInFront = respectedAvatar.getPosition().getSquareInFront();

        if(squareInFront.equals(roomAvatar.getPosition().getPoint())){
            doIt.run();
        }else {
            roomAvatar.moveTo(respectedAvatar.getPosition().getSquareInFront());
            roomAvatar.getEventHandler().on("avatar.move path.create.fail avatar.stand.on.square", (delegate, eventArgs) -> {
                switch (eventArgs.getEventKey()) {
                    case "avatar.move":
                    case "path.create.fail":
                        delegate.cancel();
                        doIt.run();
                        break;

                    case "avatar.stand.on.square":
                        AvatarStandOnSquareEventArgs args = (AvatarStandOnSquareEventArgs) eventArgs;
                        if (args.getPoint().equals(squareInFront)) {
                            doIt.run();
                            delegate.cancel();
                        }
                        break;
                }
            });
        }

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
