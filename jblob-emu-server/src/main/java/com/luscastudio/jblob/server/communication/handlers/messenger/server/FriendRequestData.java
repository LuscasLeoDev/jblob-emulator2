package com.luscastudio.jblob.server.communication.handlers.messenger.server;

/**
 * Created by Lucas on 27/02/2017 at 15:48.
 */
public class FriendRequestData {
    private int id;
    private String name;
    private String figure;

    public FriendRequestData(int id, String name, String figure){
        this.id = id;
        this.name = name;
        this.figure = figure;
    }

    public int getId() {
        return id;
    }

    public String getFigure() {
        return figure;
    }

    public String getName() {
        return name;
    }
}
