package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 20/12/2016 at 20:07.
 */
public class RemoveWallItemComposer extends MessageComposer{

    @Override
    public String id() {
        return "ItemRemoveMessageComposer";
    }

    public RemoveWallItemComposer(int itemId, int userId){
        message.putString(String.valueOf(itemId));
        //obmessage.putBool(expired);
        message.putInt(userId);
    }
}
