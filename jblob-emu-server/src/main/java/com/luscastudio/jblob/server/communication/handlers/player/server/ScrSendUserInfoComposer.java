package com.luscastudio.jblob.server.communication.handlers.player.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 10/12/2016.
 */

public class ScrSendUserInfoComposer extends MessageComposer {
    @Override
    public String id() {
        return "ScrSendUserInfoMessageComposer";
    }

    public ScrSendUserInfoComposer(){
        message.putString("habbo_club");
        message.putInt(10);
        message.putInt(2);
        message.putInt(10);
        message.putInt(0);
        message.putBool(true);
        message.putBool(true);
        message.putInt(0);
        message.putInt(0);
        message.putInt(1000);
    }
}
