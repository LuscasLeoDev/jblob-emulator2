package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 10/12/2016.
 */

public class RoomVisualizationSettingsComposer extends MessageComposer {
    @Override
    public String id() {
        return "RoomVisualizationSettingsMessageComposer";
    }

    public RoomVisualizationSettingsComposer(boolean hideWalls, int wallThickness, int floorThickness){
        message.putBool(hideWalls);
        message.putInt(wallThickness);
        message.putInt(floorThickness);
    }
}
