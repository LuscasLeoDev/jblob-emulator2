package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomUsersComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.UserUpdateComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomData;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomPart;
import com.luscastudio.jblob.server.game.habbohotel.pet.PetBase;
import com.luscastudio.jblob.server.game.habbohotel.pet.custom.PetCustomColorData;
import com.luscastudio.jblob.server.game.habbohotel.pet.custom.PetCustomDataValues;
import com.luscastudio.jblob.server.game.habbohotel.pet.custom.PetCustomPartData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.AvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.MonsterPlantSeedRoomItemExtradata;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Lucas on 06/02/2017 at 19:02.
 */
public class MonsterPlantSeedRoomItem extends SimpleRoomItem {
    public MonsterPlantSeedRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
    }

    @Override
    public MonsterPlantSeedRoomItemExtradata getExtradata() {
        return (MonsterPlantSeedRoomItemExtradata)super.getExtradata();
    }

    @Override
    public void onPlayerTrigger(PlayerSession session, int requestId) {
        if(!this.room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_plant_monsterplant_seed"))
            return;

        this.generateMonsterPlant(session);
    }

    private void generateMonsterPlant(PlayerSession session) {

        int monsterPlantPetType = JBlob.getGame().getDbConfig().getInt("pet.monsterplant.type", 16);

        Map<Integer, PetBase> basesByType = JBlob.getGame().getPetManager().getBasesByType(monsterPlantPetType);

        if(basesByType.size() == 0) {

            return;
        }



        PetCustomDataValues values = JBlob.getGame().getPetManager().getPetCustomDataValues(monsterPlantPetType);

        if(values == null) {

            return;
        }

        Random random = new Random(this.hashCode());
        PetBase base;

        {
            List<PetBase> petBases = BCollect.newList();
            petBases.addAll(basesByType.values());
            base = BCollect.getRandomThing(petBases, random);
        }


        int rarityLevel = this.getExtradata().getRarityLevel();

        String name = "";

        Map<Integer, PetCustomPart> customPartMap = BCollect.newMap();

        for(int i = 1; i < 6; i++)
        {
            if(i == 1) {
                int rarity = rarityLevel > 0 ? rarityLevel - random.nextInt(rarityLevel) : 0;

                boolean reverse = random.nextBoolean();

                int partRarity = reverse ? rarity : rarityLevel - rarity;
                int colorRarity = reverse ? rarityLevel - rarity : rarity;

                random = new Random(random.hashCode());
                PetCustomColorData colorData = values.getRandomColor(colorRarity, random);
                random = new Random(random.hashCode());
                PetCustomPartData partData = values.getRandomCustomPartData(1, partRarity, random);

                while(partData == null && partRarity > 0){
                    partRarity -= 1;
                    colorRarity += 1;

                    partData = values.getRandomCustomPartData(1, partRarity, random);
                }

                while(colorData == null && colorRarity > 0){
                    partRarity += 1;
                    colorRarity -= 1;

                    colorData = values.getRandomColor(colorRarity, random);
                }

                if(colorData == null || partData == null){
                    session.sendMessage(new BubbleNotificationComposer("error", "Não foi possivel achar EventArgs combinação para EventArgs planta, tente novamente.."));
                    return;
                }

                customPartMap.put(i, new PetCustomPart(i, partData.getPartId(), colorData.getColorId()));

                name =  partData.getName()+ " " + colorData.getName();;
            }
            else {
                random = new Random(random.hashCode());
                PetCustomColorData colorData = values.getRandomColor(0, random);
                random = new Random(random.hashCode());
                PetCustomPart partData = values.getRandomCustomPart(i, random);
                if(partData == null || colorData == null)
                    continue;
                customPartMap.put(i, new PetCustomPart(i, partData.getPartId(), colorData.getColorId()));
            }
        }


        PetCustomData customData = new PetCustomData(customPartMap);

        int startLevel = 1;

        PetData petData = JBlob.getGame().getPetManager().createPetData(room.getProperties().getId(), name, base, this.properties.getOwnerId(), "FFFFFF", customData, startLevel, rarityLevel, base.getMaxEnergy(startLevel), 0, base.getMaxHappiness(startLevel));
        room.getRoomItemHandlerService().removeRoomItem(this);

        IRoomPetAvatar petAvatar = this.room.getRoomAvatarService().addPet(petData, new AvatarObjectPosition(this.position.getX(), this.position.getY(), this.position.getZ(), 0, 0));

        room.getRoomAvatarService().saveAvatar(petAvatar);
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("DELETE FROM avatars_items_data WHERE id = ?");
            prepare.setInt(1, this.properties.getId());
            prepare.run();
        } catch (Exception e){

        }

        petAvatar.onEnter();
        room.sendQueueMessage(new RoomUsersComposer(petAvatar));
        room.sendQueueMessage(new UserUpdateComposer(petAvatar));
        room.sendQueueMessage(new RemoveFloorItemComposer(this.properties.getId(), -1, false, 0));
        room.flush();

        petAvatar.onPlace(session.getAvatar());


        this.dispose();

    }
}
