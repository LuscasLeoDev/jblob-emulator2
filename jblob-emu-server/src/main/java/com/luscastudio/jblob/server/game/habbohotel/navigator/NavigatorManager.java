package com.luscastudio.jblob.server.game.habbohotel.navigator;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.navigator.categories.ActiveRoomsCategory;
import com.luscastudio.jblob.server.game.habbohotel.navigator.categories.ByCategoryFlatCat;
import com.luscastudio.jblob.server.game.habbohotel.navigator.categories.MyRoomsCategory;
import com.luscastudio.jblob.server.game.habbohotel.navigator.categories.NavigatorCategory;
import com.luscastudio.jblob.server.game.habbohotel.navigator.flatcats.NavigatorFlatCategory;
import com.luscastudio.jblob.server.game.habbohotel.navigator.tabs.NavigatorTopLevel;

import java.lang.reflect.Constructor;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 02/10/2016.
 */

public class NavigatorManager implements IFlushable {

    private Map<Integer, NavigatorTopLevel> topLevelMap;
    private Map<String, NavigatorTopLevel> topLevelMapByName;
    private Map<Integer, NavigatorFlatCategory> flatCategoryMap;
    private Map<Integer, NavigatorCategory> categoryMap;

    private Map<String, Class<? extends NavigatorCategory>> categoryInstances;

    public NavigatorManager(){
        this.topLevelMap = BCollect.newLinkedMap();
        this.topLevelMapByName = BCollect.newConcurrentMap();
        this.flatCategoryMap = BCollect.newLinkedMap();
        this.categoryInstances = BCollect.newMap();
        this.categoryMap = BCollect.newConcurrentMap();
    }

    private void addInstances(){
        this.categoryInstances.put("by_flatcat", ByCategoryFlatCat.class);
        this.categoryInstances.put("active_rooms", ActiveRoomsCategory.class);
        this.categoryInstances.put("my", MyRoomsCategory.class);
    }

    @Override
    public void flush() {
        this.topLevelMap.clear();
        this.topLevelMapByName.clear();
        this.flatCategoryMap.clear();
        this.categoryInstances.clear();
        this.addInstances();


        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("SELECT * FROM navigator_top_levels ORDER BY order_id ASC");

            ResultSet set = prepare.runQuery();

            while(set.next()){
                NavigatorTopLevel topLevel = new NavigatorTopLevel(set.getInt("id"), set.getString("name"), set.getString("permission_name"));
                this.topLevelMapByName.put(topLevel.getName(), topLevel);
                this.topLevelMap.put(topLevel.getId(), topLevel);
            }

            prepare = reactor.prepare("SELECT * FROM navigator_flat_categories");

            set = prepare.runQuery();

            while(set.next()){
                this.flatCategoryMap.put(set.getInt("id"), new NavigatorFlatCategory(set.getInt("id"), set.getString("name"), set.getString("public_name"), set.getString("permission_name")));
            }

            prepare = reactor.prepare("SELECT * FROM navigator_categories ORDER BY order_id ASC");

            set = prepare.runQuery();

            while(set.next()){
                int parentId = set.getInt("parent_id");
                if(!this.topLevelMap.containsKey(parentId))
                    continue;

                NavigatorCategory navigatorCategory = this.generateCategory(set.getInt("id"), set.getString("name"), set.getString("identifier"), set.getInt("order_id"), parentId, set.getInt("collapsed") == 1, set.getInt("expand_status"), set.getInt("show_type"), set.getString("behaviour"), set.getString("extradata"));
                if(navigatorCategory != null) {
                    this.topLevelMap.get(parentId).addCategory(navigatorCategory);
                    this.categoryMap.put(navigatorCategory.getId(), navigatorCategory);
                }
            }

            for (NavigatorCategory navigatorCategory : this.categoryMap.values()) {
                navigatorCategory.init(reactor);
            }


        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        JBlob.getGame().getEventHandler().fireEvent("service.navigatormanager.flush");
    }

    @Override
    public void performFlush() {

    }

    //region TopLevel Managing

    public Collection<NavigatorTopLevel> getTopLevels(){
        return this.topLevelMap.values();
    }

    public Collection<NavigatorTopLevel> getTopLevels(int avatarRank){
        List<NavigatorTopLevel> topLevelList = BCollect.newList();
        this.topLevelMap.values().forEach(navigatorTopLevel -> {
            if(JBlob.getGame().getPermissionManager().validateRight(avatarRank, navigatorTopLevel.getPermissionName()))
                topLevelList.add(navigatorTopLevel);
        });

        return topLevelList;
    }



    //endregion

    public NavigatorTopLevel getTopLevel(String name){
        return this.topLevelMapByName.get(name);
    }

    //region Flat Categories Managing

    public Collection<NavigatorFlatCategory> getFlatCategoryMap() {
        return flatCategoryMap.values();
    }

    public NavigatorFlatCategory getFlatCategory(int categoryId) {
        return this.flatCategoryMap.get(categoryId);
    }

    //endregion

    //region NavigatorCategory Managing

    private NavigatorCategory generateCategory(int id, String name, String identifier, int orderNum, int topLevelId, boolean collapsed, int expandStatus, int showType, String behaviour, String extradata){
        Class<? extends NavigatorCategory> categoryInstance = this.categoryInstances.get(behaviour);
        if(categoryInstance == null)
            return null;

        try {
            Constructor<? extends NavigatorCategory> constructor = categoryInstance.getConstructor(int.class, String.class, String.class, int.class, int.class, boolean.class, int.class, int.class, String.class);
            if(constructor == null)
                return null;

            NavigatorCategory navigatorCategory = constructor.newInstance(id, identifier, name, topLevelId, orderNum, collapsed, expandStatus, showType, extradata);

            return navigatorCategory;

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return null;
    }

    //endregion

}
