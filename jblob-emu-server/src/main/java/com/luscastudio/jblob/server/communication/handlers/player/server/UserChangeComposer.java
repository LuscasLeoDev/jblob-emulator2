package com.luscastudio.jblob.server.communication.handlers.player.server;

/**
 * Created by Lucas on 09/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;

public class UserChangeComposer extends MessageComposer {
    public UserChangeComposer(RoomPlayerAvatar avatar) {
        message.putInt(avatar.getVirtualId());
        message.putString(avatar.getFigureString());
        message.putString(avatar.getGender());
        message.putString(avatar.getMotto());
        message.putInt(avatar.getScore());
    }

    @Override
    public String id() {
        return "UserChangeMessageComposer";
    }
}
