//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.network.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by Lucas on 19/03/2017 at 13:53.
 */
public class SyncClientConnectionHandler extends ClientConnectionHandler {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageEvent msg) {
        try {
            JBlob.getGame().getMessageEventManager().handleMessage(ctx.attr(PlayerSession.CLIENT_ATTR).get(), msg);
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }
}
