package com.luscastudio.jblob.server.game.habbohotel.pet.custom;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomPart;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Lucas on 08/02/2017 at 20:24.
 */
public class PetCustomDataValues {

    private ListMultimap<Integer, PetCustomPartData> partTypes;
    private Map<IPetPartData, String> partNames;
    private Map<Integer, PetCustomColorData> colorTypes;
    private ListMultimap<Integer, PetCustomPart> randomParts;
    private List<String> initialCustoms;

    private Random random;

    public PetCustomDataValues() {
        this.partTypes = ArrayListMultimap.create();
        this.colorTypes = BCollect.newMap();
        this.randomParts = ArrayListMultimap.create();
        this.partNames = BCollect.newMap();
        this.initialCustoms = BCollect.newList();

        this.random = new Random(this.hashCode());
    }

    public void parseSet(ResultSet set) throws SQLException {

        switch (set.getString("type")) {
            case "color":
                if (!this.colorTypes.containsKey(set.getInt("value")))
                    this.colorTypes.put(set.getInt("value"), new PetCustomColorData(set.getInt("value"), set.getInt("rarity_level"), set.getString("name")));
                break;

            case "part":
                String[] partData = set.getString("value").split(",");
                if (partData.length < 2)
                    break;

                try {
                    int partType = Integer.parseInt(partData[0]);
                    int partId = Integer.parseInt(partData[1]);
                    PetCustomPartData data;
                    this.partTypes.put(partType, data = new PetCustomPartData(partType, partId, set.getInt("rarity_level"), set.getString("name")));
                    this.partNames.put(data, data.getName());

                } catch (Exception ignored) {
                }

                break;

            case "random_part":

                String value = set.getString("value");

                int index = value.indexOf(":");

                try {
                    int partType = Integer.parseInt(value.substring(0, index));

                    for (String s : value.substring(index + 1).split(",")) {
                        this.randomParts.put(partType, new PetCustomPart(partType, Integer.parseInt(s), 0));
                    }

                } catch (Exception ignored) {

                }

                break;

            case "initial_custom":

                this.initialCustoms.add(set.getString("value"));

                break;
        }

    }

    public List<String> getInitialCustoms() {
        return initialCustoms;
    }

    public String getRandomInitialCustom(){
        if(this.initialCustoms.size() == 0)
            return "";

        return BCollect.getRandomThing(this.initialCustoms, new Random(this.hashCode()));
    }

    public PetCustomPartData getRandomCustomPartData(int type, int rarityLevel, Random random){
        if(!this.partTypes.containsKey(type))
            return null;

        List<PetCustomPartData> l = BCollect.newList();
        for (PetCustomPartData customPartData : this.partTypes.get(type)) {
            if(customPartData.getRarityLevel() == rarityLevel)
                l.add(customPartData);
        }

        return BCollect.getRandomThing(l, random);
    }

    public PetCustomPart getRandomCustomPart(int type, Random random){
        if(!this.randomParts.containsKey(type))
            return null;

        return BCollect.getRandomThing(this.randomParts.get(type), random);
    }

    public PetCustomColorData getRandomColor(int rarityLevel, Random random){

        List<PetCustomColorData> l = BCollect.newList();

        for (PetCustomColorData colorData : this.colorTypes.values()) {
            if(colorData.getRarityLevel() == rarityLevel)
                l.add(colorData);
        }

        return BCollect.getRandomThing(l, random);
    }


    public PetCustomDataResult generateResult(List<RequiredPetPart> requiredTypes, Random random){

        String name = "";
        int rarityLevel = 0;

        Map<Integer, PetCustomPart> customPartDataMap = BCollect.newMap();

        for (RequiredPetPart requiredType : requiredTypes) {
            if(this.partTypes.containsKey(requiredType.getPartType())){
                Collections.shuffle(this.partTypes.get(requiredType.getPartType()), random);
                int level = requiredType.getMaxRarityLevel();
                if(requiredType.getMaxRarityLevel() != requiredType.getMinRarityLevel())
                    Math.abs(random.nextInt(requiredType.getMaxRarityLevel()) - random.nextInt(requiredType.getMinRarityLevel()));
                else
                    level = requiredType.getMaxRarityLevel();
                Collections.shuffle(this.partTypes.get(requiredType.getPartType()), random);
                for (PetCustomPartData petCustomPartData : this.partTypes.get(requiredType.getPartType())) {
                    if(petCustomPartData.getRarityLevel() >= level / 2){

                        rarityLevel+=petCustomPartData.getRarityLevel();
                        PetCustomPart partData;

                        PetCustomColorData color = null;
                        List<PetCustomColorData> colorDatas = BCollect.newList();
                        colorDatas.addAll(this.colorTypes.values());
                        Collections.shuffle(colorDatas, random);
                        for (PetCustomColorData colorData : colorDatas){
                            if(colorData.getRarityLevel() + (level / 2) >= level){
                                color = colorData;
                                rarityLevel += colorData.getRarityLevel();
                                break;
                            }
                        }

                        colorDatas.clear();

                        if(color == null){
                            color = this.colorTypes.get(0);
                        }

                        if(this.partNames.containsKey(petCustomPartData)){
                            name = color.getName() + " " + petCustomPartData.getName();
                        }

                        partData = new PetCustomPart(petCustomPartData.getPartType(), petCustomPartData.getPartId(), color.getColorId());

                        customPartDataMap.put(petCustomPartData.getPartType(), partData);

                        break;
                    }
                }
            } else if(this.randomParts.containsKey(requiredType.getPartType())){
                PetCustomColorData color = BCollect.getRandomThing(this.colorTypes, this.random);
                if(color == null)
                    color = this.colorTypes.get(0);

                PetCustomPart partData = BCollect.getRandomThing(this.randomParts.get(requiredType.getPartType()), this.random);
                if (partData != null)
                    customPartDataMap.put(requiredType.getPartType(), new PetCustomPart(requiredType.getPartType(), partData.getPartId(), color.getColorId()));
            }
        }

        return new PetCustomDataResult(name, customPartDataMap, rarityLevel);

    }

    public void dispose() {
        this.colorTypes.clear();
        this.randomParts.clear();
        this.random = null;
        this.partTypes.clear();
        this.partNames.clear();
    }
}
