package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.PetInventoryComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;

/**
 * Created by Lucas on 29/01/2017 at 19:17.
 */
public class GetPetInventoryEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int itemByTypeLimit = JBlob.getGame().getDbConfig().getInt("avatar.inventory.pets.show.maxpertype", JBlobSettings.AVATAR_INVENTORY_FURNIS_SHOW_MAX_PER_TYPE);

        Collection<PetData> userPets = session.getAvatar().getInventory().getPetList();

        ItemSerializer.serializeItemsCollections(userPets, itemByTypeLimit, (pageCount, pageIndex, objects) -> session.sendMessage(new PetInventoryComposer(pageCount, pageIndex, objects)));

        //session.sendMessage(new PetInventoryComposer(1, 1, BCollect.newList()));

        session.flush();
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
