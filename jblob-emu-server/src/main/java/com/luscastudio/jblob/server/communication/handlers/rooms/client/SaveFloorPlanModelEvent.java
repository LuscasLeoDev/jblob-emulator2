package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomForwardComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.models.RoomModel;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Lucas on 02/02/2017 at 18:27.
 */
public class SaveFloorPlanModelEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();

        if (room == null)
            return;

        if (!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_save_floor_plan"))
            return;

        String model = packet.getString().trim();

        int doorX = Math.abs(packet.getInt());
        int doorY = Math.abs(packet.getInt());
        int doorRotation = Math.min(Math.max(packet.getInt(), 0), 7);

        int wallThick = Math.min(Math.max(packet.getInt(), -2), 1);
        int floorThick = Math.min(Math.max(packet.getInt(), -2), 1);
        int wallHeight = packet.remaining() == 0 ? -1 : Math.min(Math.max(packet.getInt(), -1), 16);

        if (!isValidFloorMap(model, doorX, doorY, session)) {
            return;
        }

        String customModelNameFormat = JBlob.getGame().getDbConfig().get("room.save.floor.plan.model.name");

        RoomModel modelData = new RoomModel(customModelNameFormat.replace("%id%", String.valueOf(room.getProperties().getId())), model, wallHeight, doorX, doorY, doorRotation, true);

        JBlob.getGame().getModelManager().updateModel(modelData.getName(), modelData);

        {

            try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
                DBConnPrepare prepare = reactor.prepare("UPDATE players_rooms SET model_name = ?, wall_thickness = ?, floor_thickness = ? WHERE id = ?");

                prepare.setString(1, modelData.getName());
                prepare.setInt(2, wallThick);
                prepare.setInt(3, floorThick);
                prepare.setInt(4, room.getProperties().getId());

                prepare.run();

            }catch (Exception e){
                BLogger.error(e, this.getClass());
            }
        }

        JBlob.getGame().getRoomManager().unloadRoom(room.getProperties().getId());

        session.sendMessage(new RoomForwardComposer(room.getProperties().getId()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }

    private static boolean isValidFloorMap(String model, int doorX, int doorY, PlayerSession session) {

        int squaresPerFirstSide = 0;

        int maxX = JBlob.getGame().getDbConfig().getInt("room.config.max.floor.map.x", 64);
        int maxY = JBlob.getGame().getDbConfig().getInt("room.config.max.floor.map.y", 64);

        BubbleNotificationComposer errorComposer = new BubbleNotificationComposer("floorplan_editor.error");

        if (model.isEmpty()) {
            errorComposer.add("errors", "invalid_map");
            session.sendMessage(errorComposer);
            return false;
        }

        for (char c : model.toCharArray()) {
            if (!validCharacters.contains(c)) {

                errorComposer.add("errors", "invalid_map");
                session.sendMessage(errorComposer);
                return false;
            }
        }

        String[] map = model.split("\r");

        int mapHeight = map.length;
        int mapWidth = map[0].length();

        for (int y = 0; y < mapHeight; y++) {

            if(map[y].length() != mapWidth){
                errorComposer.add("errors", "%%%invalid_map%%%");
                session.sendMessage(errorComposer);
                return false;
            }

            for (int x = 0; x < mapWidth; x++) {
                if ((x == 0 || y == 0) && (map[y].charAt(x) != 'x')) {
                    squaresPerFirstSide++;
                }
            }
        }


        if (squaresPerFirstSide > 1 && false) {
            errorComposer.add("errors", "%%%invalid_door_setup%%%");
            session.sendMessage(errorComposer);
            return false;
        }

        if (mapHeight > maxY && mapWidth > maxX) {
            errorComposer.add("errors", "(%%%general%%%): %%%too_large_area%%% (%%%max%%% " + (maxX * maxY) + " %%%tiles%%%)");
            session.sendMessage(errorComposer);
            return false;
        } else if (mapHeight > maxY) {
            errorComposer.add("errors", "(%%%general%%%): %%%too_large_height%%% (%%%max%%% " + (maxY) + " %%%tiles%%%)");
            session.sendMessage(errorComposer);
            return false;
        } else if (mapWidth > maxX) {
            errorComposer.add("errors", "(%%%general%%%): %%%too_large_width%%% (%%%max%%% " + (maxX) + " %%%tiles%%%)");
            session.sendMessage(errorComposer);
            return false;
        }

        if (doorX >= mapWidth || doorY >= mapHeight) {
            errorComposer.add("errors", "%%%entry_tile_outside_map%%%");
            session.sendMessage(errorComposer);
            return false;
        }


        return true;
    }

    private static List<Character> validCharacters = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\r');
}
