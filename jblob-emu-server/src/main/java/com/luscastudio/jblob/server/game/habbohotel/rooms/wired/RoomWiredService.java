package com.luscastudio.jblob.server.game.habbohotel.rooms.wired;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.WiredBoxRoomItem;

import java.util.Map;

/**
 * Created by Lucas on 14/02/2017 at 23:25.
 */
public class RoomWiredService {

    private Map<Integer, WiredBoxRoomItem> wiredBoxMap;

    public RoomWiredService(){
        this.wiredBoxMap = BCollect.newConcurrentMap();
    }

}
