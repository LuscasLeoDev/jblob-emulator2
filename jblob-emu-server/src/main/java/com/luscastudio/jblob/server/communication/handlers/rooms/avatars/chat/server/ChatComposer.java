package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server;

/**
 * Created by Lucas on 04/10/2016.
 */

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.Map;

public class ChatComposer extends MessageComposer {
    @Override
    public String id() {
        return "ChatMessageComposer";
    }

    private int virtualId;
    private int colorId;
    private int gesture;
    private String text;
    private Map<String, String> links;
    private int delay;

    private boolean compose;
    public ChatComposer(int virtualId, int colorId, int gesture, String text) {

        message.putInt(virtualId);
        message.putString(text);
        message.putInt(gesture);
        message.putInt(colorId);

        message.putInt(0);//while

        message.putInt(0);

        this.compose = false;
    }

    public ChatComposer(int virtualId, int colorId, int gesture, String text, int delay) {
        this.links = BCollect.newMap();
        this.virtualId = virtualId;
        this.colorId = colorId;
        this.gesture = gesture;
        this.text = text;
        this.delay = delay;
        this.compose = true;
    }

    public ChatComposer addLink(String key, String value ){
        this.links.put(key, value);
        return this;
    }

    @Override
    public void compose() {

        super.compose();

        if(!this.compose)
            return;

        message.putInt(this.virtualId);
        message.putString(this.text);
        message.putInt(this.gesture);
        message.putInt(this.colorId);

        message.putInt(this.links.size());//while

        for(Map.Entry<String, String> k : this.links.entrySet()) {
            message.putString(k.getKey());
            message.putString(k.getValue());
            message.putBool(true);
        }

        message.putInt(this.delay);
    }
}
