//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.chat.filter;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.chat.filter.parsers.IParser;
import com.luscastudio.jblob.server.game.habbohotel.chat.filter.parsers.RegexChatFilterParser;
import com.luscastudio.jblob.server.game.habbohotel.chat.filter.parsers.SimpleChatFilterParser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Lucas on 16/06/2017 at 11:04.
 */
public class ChatFilterService implements IFlushable {

    private Map<String, IParser> parsers;
    private Map<Integer, BlockedWord> blockedWords;

    public ChatFilterService() {
        this.parsers = BCollect.newConcurrentMap();
        this.blockedWords = BCollect.newConcurrentMap();
    }

    public void addPhase(String name, IParser phaser){

        if(this.parsers.containsKey(name))
            return;

        this.parsers.put(name, phaser);
    }

    public String parseWord(String input){
        String r = input;

        for (BlockedWord blockedWord : this.blockedWords.values()) {
            r = blockedWord.getPhaser().phase(input, blockedWord.getValue(), blockedWord.getReplacement());
        }

        return r;
    }

    public void putPhase(String name, IParser phaser) {
        this.parsers.put(name, phaser);
    }

    public void load(){
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM word_filter");

            ResultSet set = prepare.runQuery();

            while(set.next())
                this.setWord(set);

        } catch(Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    private void setWord(ResultSet set) throws SQLException {
        int id = set.getInt("id");
        String parser;
        IParser phaser = this.parsers.get(parser = set.getString("parser"));
        if(phaser == null) {
            BLogger.warn(String.format("Word Filter Parser '%s' not found! Check the line #%d", parser, id), this.getClass());
            return;
        }
        this.blockedWords.put(id, new BlockedWord(id, set.getString("value"), phaser, set.getString("replacement")));

    }

    private void addDefaults(){
        this.addPhase("contains", new SimpleChatFilterParser());
        this.addPhase("regex", new RegexChatFilterParser());


    }


    @Override
    public void flush() throws Exception {
        this.blockedWords.clear();
        this.parsers.clear();
        addDefaults();
        load();
    }

    @Override
    public void performFlush() {

    }
}