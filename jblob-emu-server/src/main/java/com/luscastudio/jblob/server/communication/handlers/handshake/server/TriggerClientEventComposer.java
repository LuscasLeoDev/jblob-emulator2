package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 02/02/2017 at 14:22.
 */
public class TriggerClientEventComposer extends MessageComposer {
    @Override
    public String id() {
        return "TriggerClientEventMessageComposer";
    }

    public TriggerClientEventComposer(String link){
        message.putString(link);
    }
}
