package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 10/12/2016.
 */

public class DebugConsoleComposer extends MessageComposer {
    @Override
    public String id() {
        return "DebugConsoleMessageComposer";
    }

    public DebugConsoleComposer(boolean b){
        message.putBool(b);
    }
}
