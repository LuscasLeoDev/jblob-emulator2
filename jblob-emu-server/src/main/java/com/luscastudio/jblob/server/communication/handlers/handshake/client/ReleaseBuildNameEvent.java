package com.luscastudio.jblob.server.communication.handlers.handshake.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.client.builds.ClientBuild;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.GenericErrorComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 19/09/2016.
 */
public class ReleaseBuildNameEvent implements IMessageEventHandler {


    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        String buildstr = packet.getString();

        ClientBuild build = JBlob.getGame().getBuildManager().getBuild(buildstr);

        if (build == null) {
            session.sendMessage(new GenericErrorComposer(-3));
            session.disconnect();

            return;
        }

        session.setBuild(build);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
