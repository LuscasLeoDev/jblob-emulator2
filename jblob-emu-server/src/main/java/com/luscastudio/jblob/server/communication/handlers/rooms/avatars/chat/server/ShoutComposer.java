package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server;

/**
 * Created by Lucas on 31/01/2017 at 14:27.
 */
public class ShoutComposer extends ChatComposer {
    @Override
    public String id() {
        return "ShoutMessageComposer";
    }

    public ShoutComposer(int virtualId, int colorId, int gesture, String text) {
        super(virtualId, colorId, gesture, text);
    }

    public ShoutComposer(int virtualId, int colorId, int gesture, String text, int delay) {
        super(virtualId, colorId, gesture, text, delay);
    }
}
