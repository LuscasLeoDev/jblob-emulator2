package com.luscastudio.jblob.api.utils.console;


import java.util.function.Consumer;

/**
 * Created by Lucas on 03/10/2016.
 */

public class InitializeBar {

    private Consumer<String> callback;

    public InitializeBar(Consumer<String> callback) {
        this.callback = callback;
    }

    public void init(String message, IFlushable flushable, FinalMessageAsync finalMessage) throws Exception {
        System.out.print("\n" + message);
        flushable.flush();
        System.out.print("\t" + finalMessage.getFinalMessage() + "\n");
    }

    public void init(String message, IFlushable flushable) throws Exception {
        init(message, flushable, "Done!");
    }

    public void init(String message, IFlushable flushable, String finalMessage) throws Exception {
        System.out.print("\n" + message);
        flushable.flush();
        System.out.print("\t" + finalMessage + "\n");
    }

    public void write(String message){
        System.out.print("\n" + message);
    }

}
