package com.luscastudio.jblob.api.utils.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 28/01/2017 at 13:20.
 */
public class JSONUtils {

    private static Gson gson;
    private static GsonBuilder gsonBuilder;

    public static void init(){
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        gsonBuilder.disableHtmlEscaping();
    }



    public static <K,V> Map<K,V> toMap(String jsonData, Type type) {
        return gson.fromJson(jsonData, type);
    }

    public static <T> List<T> toList(String jsonData, Type type){
        return gson.fromJson(jsonData, type);
    }

    public static String toJson(Object obj, Type type){
        return gson.toJson(obj, type);
    }

    public static <T> T fromJson(String data, Type type){
        return gson.fromJson(data, type);
    }


}
