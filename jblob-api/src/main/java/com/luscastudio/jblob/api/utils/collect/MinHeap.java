package com.luscastudio.jblob.api.utils.collect; /**
 * CSE 373, Winter 2011, Jessica Miller
 * The BinaryHeap is an -generic- implementation of the PriorityQueue interface.  
 * This is a binary min-heap implementation of the priority queue ADT.
 */

import java.util.Arrays;

public class MinHeap<T extends Comparable<T>> {

    private T[] array;
    private int capacity;
    private int count;

    @SuppressWarnings("unchecked")
    public MinHeap(int size){
        this.capacity = size;
        this.array = (T[]) new Comparable[size];
    }

    public void add(T t){
        if(this.count >= this.capacity){
            this.array = Arrays.copyOf(this.array, this.capacity * 2);
            this.capacity *= 2;
        }

        this.array[count] = t;
        this.count++;
    }

    public T remove(){
        if(this.count == 0)
            throw new IllegalStateException();
        T val = this.array[0];
        this.count--;
        if(this.capacity > 1)
            this.capacity--;
        this.resize();
        return val;
    }

    public int size(){
        return this.count;
    }


    @SuppressWarnings("unchecked")
    private void resize(){

        T[] tempArray = (T[]) new Comparable[this.capacity];
        for(int i = 0; i < tempArray.length && i < array.length && i + 1 < array.length; i++){
            tempArray[i] = array[i + 1];
        }

        this.array = tempArray;

    }

    public void dispose() {
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}