package com.luscastudio.jblob.api.utils.collect;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by Lucas on 24/10/2016.
 */

public class BCollect {

    //Defaults

    public static <T> List<T> newList() {
        return Lists.newLinkedList();
    }

    public static <T> List<T> newList(Collection<T> values) {
        return Lists.newLinkedList(values);
    }

    public static <K, V> Map<K, V> newMap() {
        return Maps.newHashMap();
    }

    //Concurrents

    public static <T> Queue<T> newConcurrentQueue() {
        return Queues.newConcurrentLinkedQueue();
    }

    public static <T> BlockingQueue<T> newBlockingQueue(){
        return Queues.newLinkedBlockingDeque();
    }

    public static <K, V> ConcurrentMap<K, V> newConcurrentMap() {
        return Maps.newConcurrentMap();
    }

    public static <T> List<T> newList(Iterable<? extends T> values) {
        return Lists.newLinkedList(values);
    }

    public static <K, V> Map<K, V> newLinkedMap() {
        return Maps.newLinkedHashMap();
    }

    public static Class<?> getMapClass() {
        return HashMap.class;
    }

    public static <T> T getRandomThing(List<T> things){
        return getRandomThing(things, new Random((int)(Math.random() * 100000)));
    }

    public static <T> T getRandomThing(List<T> things, Random random){
        if(things.size() == 0)
            return null;

        int multiply = random.nextInt(1000);

        if(things.size() == 1)
            return things.get(0);
        try {
            return things.get((int) ((double) (random.nextInt((things.size() * multiply) - (multiply)) / multiply)));
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static <K, V> V getRandomThing(Map<K, V> things, Random random){
        if(things.size() == 0)
            return null;

        int multiply = 1;


        int index;
        if(things.size() == 1)
            index = 0;
        else
            index = (int)((double)random.nextInt((things.size() * multiply) - multiply) / multiply);

        int i = 0;
        for (V v : things.values()) {
            if(i == index)
                return v;
            i++;
        }

        return null;

    }
}
