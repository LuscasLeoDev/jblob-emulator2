package com.luscastudio.jblob.api.utils.io;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by Lucas on 19/01/2017 at 22:45.
 */
public class AsyncBufferReader {

    private Thread thread;
    private InputStream stream;
    private ReadingCallBack callBack;

    private Scanner scanner;

    private short runTimes;

    public AsyncBufferReader(ReadingCallBack callBack, InputStream inputStream){
        this.callBack = callBack;
        this.stream = inputStream;

        this.runTimes = 0;

        this.scanner = new Scanner(this.stream);
        this.thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!AsyncBufferReader.this.thread.isInterrupted()) {
                    if(AsyncBufferReader.this.runTimes-- > 0)
                        AsyncBufferReader.this.callBack.onRead(AsyncBufferReader.this.scanner.nextLine());
                }
            }
        });

        this.thread.start();
    }

    public void stop(){
        this.thread.interrupt();
    }

    public void beginRead(){
        this.runTimes++;
    }


}
