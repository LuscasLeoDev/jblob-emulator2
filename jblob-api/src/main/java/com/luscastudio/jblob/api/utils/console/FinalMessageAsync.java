package com.luscastudio.jblob.api.utils.console;

/**
 * Created by Lucas on 04/10/2016.
 */

public interface FinalMessageAsync {
    String getFinalMessage();
}
