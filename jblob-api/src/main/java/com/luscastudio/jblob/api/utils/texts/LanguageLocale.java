package com.luscastudio.jblob.api.utils.texts;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.util.Map;

/**
 * Created by Lucas on 28/10/2016.
 */

public class LanguageLocale {

    private String language;
    private Map<String, String> keys;

    public LanguageLocale(String lang) {
        this.language = lang;

        this.keys = BCollect.newMap();
    }
}
