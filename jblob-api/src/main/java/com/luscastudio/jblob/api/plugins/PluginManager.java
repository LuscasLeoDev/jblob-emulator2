package com.luscastudio.jblob.api.plugins;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lucas on 05/12/2016.
 */

public class PluginManager {

    public static String PLUGINS_PATH = "./plugins";

    private SecurityManager securityManager;
    private ClassLoader classLoader;

    private Map<String, IPlugin> plugins;


    public PluginManager (){

        this.classLoader = new PluginClassLoader(PLUGINS_PATH);
        this.securityManager = new PluginSecurityManager();

        this.plugins = BCollect.newMap();

        System.setSecurityManager(this.securityManager);
    }

    public void loadPlugins(){

        File[] pluginFiles = new File(PLUGINS_PATH).listFiles();

        for(File pfile : pluginFiles){

            try {
                URLClassLoader loader = URLClassLoader.newInstance(
                        new URL[]{new URL("jar:file:modules/" + pfile.getName() + "!/")},
                        getClass().getClassLoader());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

        }

    }

}
