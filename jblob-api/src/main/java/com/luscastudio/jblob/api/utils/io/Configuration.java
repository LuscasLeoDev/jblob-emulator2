package com.luscastudio.jblob.api.utils.io;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;

import java.util.Map;
import java.util.Objects;

/**
 * Created by Lucas on 01/10/2016.
 */
public class Configuration {

    public Map<String, String> data;

    public Configuration(String filename) throws Exception {

        this.data = BCollect.newMap();
        String content = BFileReader.getFileContent(filename);
        if(content == null)
            throw new Exception("Configuration file not found!");

        this.parseConfig(content);

    }

    public void parseConfig(String content) {

        content = content.replace("\r", "");

        for (String line : content.split("\n")) {

            if (line.startsWith("#"))
                continue;

            if (line.startsWith("["))
                continue;

            if (line.startsWith("/"))
                continue;

            int index = line.indexOf('=');
            if (index == -1)
                continue;

            String key = line.substring(0, index);
            String value = line.substring(index);
            if (value.length() <= 1)
                value = "";
            else
                value = value.substring(1);

            if (!data.containsKey(key))
                data.put(key, value);
            else
                data.replace(key, value);
        }
    }

    public String get(String key, String def) {
        return data.containsKey(key) ? data.get(key) : def;
    }

    public String get(String key) {
        return get(key, "");
    }

    public String getBool(String key) {
        String val = get(key, "false");
        return Objects.equals(val, "1") || Objects.equals(val, "true") ? "true" : "false";
    }

    public int getInt(String key) {
        String val = get(key);
        if (NumberHelper.isInteger(val))
            return Integer.parseInt(val);
        return 0;
    }

    public int getInt(String key, int defaultVal) {
        String val = get(key);
        if (!NumberHelper.isInteger(val))
            return defaultVal;

        return Integer.parseInt(val);
    }
}



