package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 02/02/2017 at 20:36.
 */
public interface IObject2DPosition extends IPoint {

    int getX();

    int getY();

    int getRotation();

}
