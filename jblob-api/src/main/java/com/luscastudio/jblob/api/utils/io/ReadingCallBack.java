package com.luscastudio.jblob.api.utils.io;

/**
 * Created by Lucas on 19/01/2017 at 23:20.
 */
public abstract class ReadingCallBack {

    public abstract void onRead(String message);

}
