package com.luscastudio.jblob.api.plugins;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by Lucas on 05/12/2016.
 */

public class PluginClassLoader extends ClassLoader {

    private String pluginsPath;

    public static String PLUGIN_FILE_EXTENSION = ".jar";

    public PluginClassLoader(String pluginsPath){
        super();

        this.pluginsPath = pluginsPath;
    }

    public Class loadClass(String name) throws ClassNotFoundException {

        Class cl = findLoadedClass(name);

        if(cl == null) {
            try {
                cl = findSystemClass(name);
            } catch (Exception e) {
            }
        }


        if(cl == null) {
            String filename = name.replace('.', File.separatorChar);

            File pluginFile = new File(pluginsPath, name);

            try {
                URLClassLoader loader = URLClassLoader.newInstance(
                        new URL[]{new URL("jar:file:"+PluginManager.PLUGINS_PATH+"/" + name + "!/")},
                        getClass().getClassLoader()
                );


            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return cl;
    }
}
