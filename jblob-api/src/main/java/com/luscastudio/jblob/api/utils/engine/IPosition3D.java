package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 03/10/2016.
 */
public interface IPosition3D {

    int getX();

    int getY();

    double getZ();

    void setPosition(int x, int y, double z);
}
