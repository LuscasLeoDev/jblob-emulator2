package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 23/12/2016 at 13:06.
 */
public class ObjectPosition extends Position3D implements IObjectPosition {

    protected int rotation;

    public ObjectPosition(int x, int y, double z, int rotation) {
        super(x, y, z);
        this.rotation = rotation;
    }

    public void setPosition(int x, int y, double z, int rotation) {
        super.setPosition(x, y, z);
        this.rotation = rotation;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public double getZ() {
        return this.z;
    }

    @Override
    public int getRotation() {
        return this.rotation;
    }

}
