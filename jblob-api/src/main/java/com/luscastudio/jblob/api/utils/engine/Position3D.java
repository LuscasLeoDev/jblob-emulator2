package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 03/10/2016.
 */

public class Position3D extends Point implements IPosition3D {

    protected double z;

    public Position3D(int x, int y, double z) {
        super(x, y);
        this.z = z;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public void setPosition(int x, int y, double z) {
        super.setPosition(x, y);
        this.z = z;
    }
}
