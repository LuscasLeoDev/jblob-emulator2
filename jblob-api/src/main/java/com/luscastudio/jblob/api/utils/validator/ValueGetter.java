package com.luscastudio.jblob.api.utils.validator;


/**
 * Created by Lucas on 26/02/2017 at 19:12.
 */
public interface ValueGetter<K, V> {
    V getValue(K k);
}
