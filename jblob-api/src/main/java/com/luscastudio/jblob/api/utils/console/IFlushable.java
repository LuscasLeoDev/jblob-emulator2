package com.luscastudio.jblob.api.utils.console;

/**
 * Created by Lucas on 03/10/2016.
 */
public interface IFlushable {
    void flush() throws Exception;

    void performFlush();
}
