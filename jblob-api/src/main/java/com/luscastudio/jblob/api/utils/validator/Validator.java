package com.luscastudio.jblob.api.utils.validator;

/**
 * Created by Lucas on 12/02/2017 at 15:07.
 */
public interface Validator<T> {
    boolean validate(T t);
}
