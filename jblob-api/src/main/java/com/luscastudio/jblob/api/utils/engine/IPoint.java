package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 02/02/2017 at 20:37.
 */
public interface IPoint {

    int getX();

    int getY();

}
