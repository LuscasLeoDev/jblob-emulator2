package com.luscastudio.jblob.api.utils.time;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 10/11/2016.
 */

public class DateTimeUtils {

    public static double getTimeUnit(){
        return System.currentTimeMillis();
    }

    public static double getUnixTimestamp(){
        return getTimeUnit() / 1000;
    }

    public static int getUnixTimestampInt(){
        return (int)getUnixTimestamp();
    }

    public static Date getNow(){
        return new Date();
    }

    public static String dateToString(Date date, String s) {
        //todo: use a api to handle datetime
        return "Not Implemented!";
    }
}
