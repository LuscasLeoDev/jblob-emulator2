package com.luscastudio.jblob.api.plugins;

/**
 * Created by Lucas on 05/12/2016.
 */
public interface IPlugin {
    void  load();
    void unload();
}
