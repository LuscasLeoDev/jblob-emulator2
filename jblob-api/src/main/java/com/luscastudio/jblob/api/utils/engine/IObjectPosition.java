package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 23/12/2016 at 13:05.
 */
public interface IObjectPosition extends IObject2DPosition, IPoint{

    int getX();

    int getY();

    double getZ();

    int getRotation();

}
