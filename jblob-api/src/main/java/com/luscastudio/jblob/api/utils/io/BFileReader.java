package com.luscastudio.jblob.api.utils.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Lucas on 01/10/2016.
 */
public class BFileReader {

    public static String getFileContent(String filename) {
        try {
            File file = new File(filename);
            if (!file.exists())
                return "";

            return new String(Files.readAllBytes(Paths.get(filename)));

        } catch (Exception e) {
            return null;
        }

    }

    public static ByteBuffer getFileBytes(String filename){
        try {
            File file = new File(filename);
            if (!file.exists())
                return null;

            return ByteBuffer.wrap(Files.readAllBytes(Paths.get(filename)));

        } catch (Exception e) {
            return null;
        }
    }


    public static InputStream getFileStream(String s) {
        try {
            return new FileInputStream(s);
        }catch (Exception e){
            return null;
        }
    }
}
