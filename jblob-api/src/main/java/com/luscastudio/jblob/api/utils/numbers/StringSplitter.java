package com.luscastudio.jblob.api.utils.numbers;

/**
 * Created by Lucas on 17/10/2016.
 */

public class StringSplitter {

    private String[] splitted;
    private int pointer;

    public StringSplitter(String str, String exp) {
        this.splitted = str.split(exp);
        this.pointer = 0;
    }

    public int getPointer() {
        return pointer;
    }

    public int getCount() {
        return splitted.length;
    }

    public boolean hasNext() {
        return this.splitted.length > this.pointer;
    }

    public int getInt() {
        if (!hasNext())
            return 0;

        String str = splitted[pointer++];
        if (!NumberHelper.isInteger(str))
            return 0;

        return Integer.parseInt(str);
    }

    public double getDouble() {
        if (!hasNext())
            return 0;

        String str = splitted[pointer++];
        if (!NumberHelper.isDouble(str))
            return 0;

        return Double.parseDouble(str);
    }

    public String getString() {
        if (!hasNext())
            return "";

        return splitted[pointer++];
    }

}
